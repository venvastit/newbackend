<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/12/16
 * Time: 7:31 AM.
 */

namespace App\Domains\Event\Services;

use App\Core\Entities\BaseEntity;
use App\Core\Entities\Contact;
use App\Core\Entities\Error;
use App\Core\Entities\Money\Currency;
use App\Core\Entities\Money\Money;
use App\Core\Services\LocatableService;
use App\Core\Services\SaveImageService;
use App\Domains\Category\Entities\Category;
use App\Domains\Category\Repositories\CategoryRepository;
use App\Domains\Event\Entities\EntryOption;
use App\Domains\Event\Entities\Event;
use App\Domains\Event\Repositories\EventRepository;
use App\Domains\Event\Validation\Rules\EventRules;
use App\Domains\User\Entities\User;
use App\Domains\Venue\Entities\Venue;
use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Support\Collection;
use DateTime;
use Validator;

class EventService extends LocatableService
{
    /**
     * @var EventRepository
     */
    public $repository;

    public function __construct(EventRepository $repository, CategoryRepository $categoryRepo, SaveImageService $imageService)
    {
        parent::__construct($repository, $categoryRepo, $imageService);
    }

    protected function getValidator(BaseEntity $entity)
    {
        return Validator::make($entity->toArray(), EventRules::entity());
    }

    /**
     * @param array $config
     * @param bool $paginate
     * @return Collection
     */
    public function getList(array $config, bool $paginate)
    {
        $this->addLocationQuery($config);
        $this->addCategoryQuery($config);
        if (!array_key_exists('date_from', $config) && !array_key_exists('date_to', $config)) {
            $this->addUpcomingQuery();
        } else {
            $this->addDateRangeQuery($config);
        }
        $this->addTextQuery($config);
        if (env('APP_DEBUG') !== true) {
            $this->repository->addPublishedQuery();
            $this->repository->addListedQuery();
        }
        $events = new Collection($this->repository->execute());
        $events = $events->sortBy(function (Event $event) {
            return $event->getStartsAt()->getTimestamp();
        });
        if ($paginate) {
            $events = $this->paginate($events, $config);
        }

        return $events;
    }

    /**
     * @param array $config
     * @param User $user
     * @return Collection
     */
    public function organizerList(array $config, User $user)
    {
        $this->addCategoryQuery($config);
        $this->addTextQuery($config);
        $this->repository->addPublishedQuery(true);
        $this->repository->addOrganizerQuery($user);

        $events = new Collection($this->repository->execute());
        $events = $events->sortBy(function (Event $event) {
            return $event->getStartsAt()->getTimestamp();
        });
        $events = $this->paginate(new Collection($events), $config);

        return $events;
    }

    /**
     * @param array $config
     * @param User $user
     * @return Collection
     */
    public function draftsList(array $config, User $user)
    {
        $this->addCategoryQuery($config);
        $this->addTextQuery($config);
        $this->repository->addPublishedQuery(false);
        $this->repository->addOrganizerQuery($user);
        $events = new Collection($this->repository->execute());
        $events = $events->sortBy(function (Event $event) {
            return $event->getStartsAt()->getTimestamp();
        });
        $events = $this->paginate(new Collection($events), $config);

        return $events;
    }

    public function create(
        string $name, string $description, array $categories,
        array $images, array $entryOptions, DateTime $start, DateTime $end, Contact $contact, Venue $venue, User $org
        ) {
        $event = new Event();
        $event->setName($name);
        $event->setDescription($description);
        $categoryInstances = new ArrayCollection();
        foreach ($categories as $categoryId) {
            $categoryInstances->add($this->categoryRepo->getById($categoryId));
        }
        $event->setCategories($categoryInstances);
        $event->setImages($this->makeImages($images, 'events'));
        $event->setEntryOptions($this->makeEntryOptions($entryOptions));
        $event->setStartsAt($start);
        $event->setEndsAt($end);
        $event->addOrganizer($org);
        $event->setContact($contact);
        $event->setVenue($venue);

        return $event;
    }

    public function store(BaseEntity $entity, bool $draft = false)
    {
        $validator = $this->getValidator($entity);
        $isValid = !$validator->fails();
        $entity->setPublished(false);
        if (!$isValid) {
            foreach ($validator->getMessageBag()->toArray() as $field => $message) {
                $entity->addError(new Error($field, $message));
            }
        }
        if ($draft || $isValid) {
            if (!$draft) {
                $entity->setPublished(true);
            }
            $entity->setErrors(new ArrayCollection());
            $this->repository->save($entity);

            return $entity;
        } else {
            return $validator->getMessageBag()->toArray();
        }
    }

    public function makeEntryOptions(array $options)
    {
        $entryOptions = new ArrayCollection();
        foreach ($options as $option) {
            $title = $option['title'];
            $description = '';
            if (array_key_exists('description', $option)) {
                $description = (string) $option['description'];
            }
            $price = new Money($option['fee'], new Currency(Currency::THB));
            $entryOptions->add(new EntryOption($title, $description, $price));
        }

        return $entryOptions;
    }

    public function makeContact(string $email, string $phone, string $website, Venue $venue)
    {
        $contact = clone($venue->getContact());
        $contact->setEmail($email);
        $contact->setPhone($phone);
        $contact->setWebsite($website);

        return $contact;
    }

    public function addUpcomingQuery()
    {
        $this->repository->addUpcomingQuery();
    }

    public function addDateRangeQuery(array $config)
    {
        if (array_key_exists('date_from', $config)) {
            if (array_key_exists('timezone', $config)) {
                $from = \DateTime::createFromFormat('d.m.Y', $config['date_from'], new \DateTimeZone($config['timezone']));
            } else {
                $from = \DateTime::createFromFormat('d.m.Y', $config['date_from']);
            }
            $this->repository->addDateFromQuery($from);
        }
        if (array_key_exists('date_to', $config)) {
            if (array_key_exists('timezone', $config)) {
                $to = \DateTime::createFromFormat('d.m.Y', $config['date_to'], new \DateTimeZone($config['timezone']));
            } else {
                $to = \DateTime::createFromFormat('d.m.Y', $config['date_to']);
            }
            $this->repository->addDateToQuery($to);
        }
    }

    protected function getCategoryType()
    {
        return Category::TYPE_EVENTS;
    }
}
