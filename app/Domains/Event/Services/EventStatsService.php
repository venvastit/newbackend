<?php
/**
 * Copyright (c) $today.year.  Venvast Limited - All rights reserved
 *
 * Created by PhpStorm.
 * User: hlogeon <email: hlogeon1@gmail.com>
 * Date: 9/21/16
 * Time: 1:47 PM
 */

namespace App\Domains\Event\Services;


use App\Domains\Event\Repositories\EventRepository;

class EventStatsService
{
    
    protected $eventRepo;
    
    public function __construct(EventRepository $eventRepo)
    {
        $this->eventRepo = $eventRepo;
    }


    public function getPublishedEventsPieChartData()
    {
        $published = $this->eventRepo->getPublishedCount();
        $total = $this->eventRepo->totalCount();
        $percentage = floor(($published / $total) * 100);
        return [
            'color' => 'rgba(25,140,25,0.6)',
            'description' => 'Published Events',
            'stats' => $published,
            'percent' => $percentage,
        ];
    }
    
    
    public function getClaimedEventsPieChartData()
    {
        $claimed = $this->eventRepo->getClaimedCount();
        $total = $this->eventRepo->totalCount();
        $percentage = floor(($claimed / $total) * 100);
        return [
            'color' => 'rgba(76,76,255,0.6)',
            'description' => 'Claimed Events',
            'stats' => $claimed,
            'percent' => $percentage,
        ];
    }

    public function getWeekUpdatesGroupedByDay()
    {
        return $this->eventRepo->getUpdatesGroupedByDay(7);
    }

    public function getWeekCreatedGroupedByDay()
    {
        return $this->eventRepo->getCreatesGroupedByDay(7);
    }

}