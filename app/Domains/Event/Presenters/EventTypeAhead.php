<?php
/**
 * Copyright (c) 2016.  Venvast Limited - All rights reserved.
 *
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/13/16
 * Time: 6:10 AM
 */

namespace App\Domains\Event\Presenters;

use App\Core\Presenters\TypeAheadPresenter;

class EventTypeAhead extends TypeAheadPresenter
{
}
