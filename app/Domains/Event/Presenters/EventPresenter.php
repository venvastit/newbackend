<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/13/16
 * Time: 2:18 AM.
 */

namespace App\Domains\Event\Presenters;

use App\Core\Presenters\BasePresenter;
use App\Domains\Category\Entities\Category;
use App\Domains\Event\Entities\Event;
use Carbon\Carbon;

class EventPresenter extends BasePresenter
{
    protected $publicFields = [
        'name', 'fullAddress', 'description',
        'images', 'phone', 'email',
        'category', 'icon', 'distance', 'location',
        'id', 'slug', 'starts_at', 'ends_at', 'starts_in',
        'start_date', 'end_date', 'start_day', 'start_month',
        'venue',
    ];

    /**
     * @var Event
     */
    protected $entity;

    protected $tz;

    public function __construct($entity, $timezone = null)
    {
        parent::__construct($entity);
        if ($timezone !== null) {
            $this->tz = new \DateTimeZone($timezone);
        }
    }

    public function starts_at()
    {
        if ($this->tz) {
            return Carbon::instance($this->entity->getStartsAt()->setTimezone($this->tz))->format('H:i');
        }

        return Carbon::instance($this->entity->getStartsAt())->format('H:i');
    }

    public function ends_at()
    {
        if ($this->tz) {
            return Carbon::instance($this->entity->getEndsAt()->setTimezone($this->tz))->format('H:i');
        }

        return Carbon::instance($this->entity->getEndsAt())->format('H:i');
    }

    public function starts_in()
    {
        if (env('APP_DEBUG') && !$this->entity->getStartsAt()) {
            return 'in 100 years';
        }

        $start = Carbon::instance($this->entity->getStartsAt());
        $now = Carbon::now();
        $diff = Carbon::instance($this->entity->getStartsAt())->diffForHumans($now, true);
        if ($start->gt($now)) {
            $diff = 'in '.$diff;
        } else {
            $diff .= ' ago';
        }

        return $diff;
    }

    public function start_date()
    {
        if (env('APP_DEBUG') && !$this->entity->getStartsAt()) {
            return '1993.01.01';
        }

        return $this->entity->getStartsAt()->format('Y.m.d');
    }

    public function end_date()
    {
        if (env('APP_DEBUG') && !$this->entity->getEndsAt()) {
            return '1993.01.02';
        }

        return $this->entity->getEndsAt()->format('Y.m.d');
    }

    public function start_month()
    {
        if (env('APP_DEBUG') && !$this->entity->getStartsAt()) {
            return 'Nov';
        }

        return Carbon::instance($this->entity->getStartsAt())->format('M');
    }

    public function start_day()
    {
        if (env('APP_DEBUG') && !$this->entity->getStartsAt()) {
            return '12th';
        }

        return Carbon::instance($this->entity->getStartsAt())->format('jS');
    }

    /**
     * @return string
     */
    public function name()
    {
        return $this->entity->getName();
    }

    /**
     * @return float|null
     */
    public function distance()
    {
        return $this->entity->getContact()->getAddress()->getDistance();
    }

    /**
     * @return string
     */
    public function fullAddress()
    {
        $house = trim($this->entity->getContact()->getAddress()->getHouse());
        $street = trim($this->entity->getContact()->getAddress()->getStreet());
        $district = trim($this->entity->getContact()->getAddress()->getDistrict());

        return trim(implode(', ', compact('house', 'street', 'district')));
    }

    /**
     * @return string|null
     */
    public function description()
    {
        return $this->entity->getDescription();
    }

    /**
     * @return mixed
     */
    public function images()
    {
        $images = $this->entity->getImages();
        $output = [];

        foreach ($images as $image) {
            if (starts_with('http', $image)) {
                $output[] = $image;
            } else {
                $output[] = \URL::asset($image);
            }
        }

        if (count($output) === 0) {
            return;
        }

        return $output[0];
    }

    /**
     * @return string
     */
    public function phone()
    {
        return $this->entity->getContact()->getPhone();
    }

    /**
     * @return string
     */
    public function email()
    {
        return $this->entity->getContact()->getEmail();
    }

    /**
     * @return array
     */
    public function category()
    {
        if (env('APP_DEBUG') === true) { //TODO: remove this shit
            /** @var Category $category */
            $category = $this->entity->getCategories()->first();
            if ($category) {
                return $category->present()->toArray();
            }

            return [
                'name' => $this->entity->getCategories()->first() ? $this->entity->getCategories()->first()->getName() : 'undefined',
                'icon' => 'TODO', //TODO: Full icon url
            ];
        }

        return $this->entity->getCategories()->first()->present()->toArray();
    }

    public function icon()
    {
        if (env('APP_DEBUG') === true) { //TODO: remove this shit
            /** @var Category $category */
            $category = $this->entity->getCategories()->first();
            if ($category) {
                return $category->present()->icon();
            } else {
                return 'https://api.venvast.com/images/categories/8NzFa7cguM.png';
            }
        }

        return $this->entity->getCategories()->first()->present()->icon();
    }

    public function location()
    {
        return $this->entity->getContact()->getAddress()->getLatLng();
    }

    public function id()
    {
        return $this->entity->getId();
    }

    public function slug()
    {
        return $this->entity->getSlug();
    }

    public function venue()
    {
        return $this->entity->getVenue()->present()->toJson();
    }
}
