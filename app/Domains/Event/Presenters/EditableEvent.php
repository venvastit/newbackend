<?php
/**
 * Copyright (c) $today.year.  Venvast Limited - All rights reserved.
 *
 * Created by PhpStorm.
 * User: hlogeon <email: hlogeon1@gmail.com>
 * Date: 9/15/16
 * Time: 3:40 AM
 */

namespace App\Domains\Event\Presenters;

use App\Core\Presenters\BasePresenter;
use App\Domains\Event\Entities\EntryOption;
use App\Domains\Event\Entities\Event;

class EditableEvent extends BasePresenter
{
    protected $timezone;

    protected $publicFields = [
        'id',
        'name', 'categories', 'starts_at',
        'ends_at', 'description', 'venue',
        'phone', 'email', 'website', 'free_entry',
        'entry_options', 'locale', 'images',
    ];

    /**
     * @var Event
     */
    protected $entity;

    public function __construct($entity, string $tz)
    {
        parent::__construct($entity);
        $this->timezone = new \DateTimeZone($tz);
    }

    public function id()
    {
        return $this->entity->getId();
    }

    public function name()
    {
        return $this->entity->getName();
    }

    public function categories()
    {
        $categories = $this->entity->getCategories();
        if (count($categories) > 0) {
            return [$categories[0]->id];
        }

        return [];
    }

    public function starts_at()
    {
        if ($this->entity->getStartsAt()) {
            $start = $this->entity->getStartsAt();
            $start->setTimezone($this->timezone);

            return $start->format('Y.d.m H:i');
        }

        return;
    }

    public function ends_at()
    {
        if ($this->entity->getEndsAt()) {
            $end = $this->entity->getEndsAt();
            $end->setTimezone($this->timezone);

            return $end->format('Y.d.m H:i');
        }

        return;
    }

    public function description()
    {
        return $this->entity->getDescription();
    }

    public function venue()
    {
        if ($this->entity->getVenue()) {
            return $this->entity->getVenue()->present()->toArray();
        }

        return;
    }

    public function phone()
    {
        return $this->entity->getContact()->getPhone();
    }

    public function email()
    {
        return $this->entity->getContact()->getEmail();
    }

    public function website()
    {
        return $this->entity->getContact()->getWebsite();
    }

    public function free_entry()
    {
        return true;
    }

    public function entry_options()
    {
        $options = $this->entity->getEntryOptions();
        $presentOptions = [];
        /** @var EntryOption $option */
        foreach ($options as $option) {
            $presentableOption = [
                'title' => $option->getTitle(),
                'fee' => $option->getPrice()->getAmount(),
                'description' => false,
            ];
            $presentOptions[] = $presentableOption;
        }

        return $presentOptions;
    }

    public function locale()
    {
        return 'en';
    }

    public function images()
    {
        $images = $this->entity->getImages();
        $output = [];

        foreach ($images as $image) {
            if (starts_with('http', $image)) {
                $output[] = $image;
            } else {
                $output[] = \URL::asset($image);
            }
        }

        return $output;
    }
}
