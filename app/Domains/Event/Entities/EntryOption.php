<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/12/16
 * Time: 6:07 AM.
 */

namespace App\Domains\Event\Entities;

use App\Core\Entities\BaseEntity;
use App\Core\Entities\Money\Money;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class EntryOption.
 *
 * @ODM\EmbeddedDocument
 */
class EntryOption extends BaseEntity
{
    /**
     * Title of the entry fee option.
     *
     * @ODM\Field(type="string")
     * @Gedmo\Translatable
     * @var string
     */
    private $title;

    /**
     * Description of the entry fee option.
     *
     * @ODM\Field(type="string")
     * @Gedmo\Translatable
     * @var string
     */
    private $description;

    /**
     * @ODM\EmbedOne(targetDocument="App\Core\Entities\Money\Money")
     *
     * @var Money
     */
    private $price;

    public $locale = 'en';

    /**
     * EntryOption constructor.
     *
     * @param string $title
     * @param string $description
     * @param Money $price
     */
    public function __construct(string $title, string $description, Money $price)
    {
        $this->title = $title;
        $this->description = $description;
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return Money
     */
    public function getPrice()
    {
        return $this->price;
    }
}
