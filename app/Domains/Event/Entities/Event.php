<?php

/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/12/16
 * Time: 5:59 AM.
 *
 *  Copyright (c) 2016.  Venvast Limited - All rights reserved
 */

namespace App\Domains\Event\Entities;

use App\Core\Entities\BaseEntity;
use App\Core\Entities\Contact;
use App\Core\Entities\Error;
use App\Domains\Category\Entities\Category;
use App\Domains\User\Entities\User;
use App\Domains\Venue\Entities\Venue;
use Caffeinated\Presenter\Traits\PresentableTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document(collection="events")
 * @ODM\Index(keys={"contact.address.coordinates"="2dsphere"})
 */
class Event extends BaseEntity
{
    use PresentableTrait;

    /**
     * @ODM\Field(type="string", nullable=false)
     * @Gedmo\Translatable
     *
     * @var string
     */
    protected $name;

    /**
     * @ODM\Field(type="string", nullable=false)
     * @Gedmo\Translatable
     *
     * @var string
     */
    protected $description;

    /**
     * @ODM\ReferenceMany(targetDocument="App\Domains\Category\Entities\Category", inversedBy="events", cascade={"persist"})
     *
     * @var ArrayCollection
     */
    protected $categories;

    /**
     * @ODM\Field(type="date", name="starts_at", nullable=false)
     *
     * @var DateTime
     */
    protected $startsAt;

    /**
     * @ODM\Field(type="date", name="ends_at", nullable=false)
     *
     * @var DateTime
     */
    protected $endsAt;

    /**
     * @ODM\Field(type="date", name="created_at")
     * @Gedmo\Timestampable(on="create")
     *
     * @var DateTime
     */
    protected $createdAt;

    /**
     * @ODM\Field(type="date", name="updated_at")
     * @Gedmo\Timestampable(on="update")
     *
     * @var DateTime
     */
    protected $updatedAt;

    /**
     * @ODM\EmbedMany(targetDocument="App\Domains\Event\Entities\EntryOption", name="entry_options")
     *
     * @var ArrayCollection
     */
    protected $entryOptions;

    /**
     * @ODM\ReferenceOne(targetDocument="App\Domains\Venue\Entities\Venue", inversedBy="events", cascade={"persist"})
     *
     * @var Venue
     */
    protected $venue;

    /**
     * @ODM\Field(type="collection")
     *
     * @var ArrayCollection
     */
    protected $images;

    /**
     * @ODM\Field(type="string", nullable=true)
     *
     * @var string
     */
    protected $icon;

    /**
     * @ODM\ReferenceMany(targetDocument="App\Domains\User\Entities\User", inversedBy="organizeEvents", cascade={"persist"})
     *
     * @var ArrayCollection
     */
    protected $organizers;

    /**
     * @ODM\ReferenceMany(targetDocument="App\Domains\User\Entities\User", inversedBy="goingEvents", cascade={"persist"})
     *
     * @var ArrayCollection
     */
    protected $attendees;

    /**
     * Errors of this entity (used to notify what organizer should change)
     * in this instance to get it approved.
     *
     * @ODM\EmbedMany(targetDocument="App\Core\Entities\Error", nullable=true)
     *
     * @var ArrayCollection
     */
    protected $errors;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ODM\Field(type="string")
     *
     * @var string
     */
    protected $slug;

    /**
     * @ODM\Field(type="boolean", nullable=false)
     *
     * @var bool
     */
    protected $published = false;

    /**
     * @ODM\Field(type="boolean", nullable=false)
     *
     * @var bool
     */
    protected $listed = true;

    /**
     * @ODM\Field(type="int")
     *
     * @var int
     */
    protected $source;

    /**
     * @ODM\Field(type="string", name="id_on_source")
     *
     * @var string
     */
    protected $idOnSource;

    /**
     * @ODM\EmbedOne(targetDocument="App\Core\Entities\Contact", nullable=false)
     *
     * @var Contact
     */
    public $contact;

    public $locale = 'en';

    protected $presenter = 'App\Domains\Event\Presenters\EventPresenter';

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->errors = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->organizers = new ArrayCollection();
        $this->entryOptions = new ArrayCollection();
        $this->attendees = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return DateTime
     */
    public function getStartsAt()
    {
        return $this->startsAt;
    }

    /**
     * @param mixed $startsAt
     */
    public function setStartsAt($startsAt)
    {
        $this->startsAt = $startsAt;
    }

    /**
     * @return DateTime
     */
    public function getEndsAt()
    {
        return $this->endsAt;
    }

    /**
     * @param mixed $endsAt
     */
    public function setEndsAt($endsAt)
    {
        $this->endsAt = $endsAt;
    }

    /**
     * @return mixed
     */
    public function getEntryOptions()
    {
        return $this->entryOptions;
    }

    /**
     * @param mixed $entryOptions
     */
    public function setEntryOptions($entryOptions)
    {
        $this->entryOptions = $entryOptions;
    }

    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param mixed $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param mixed $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * @return mixed
     */
    public function getVenue()
    {
        return $this->venue;
    }

    /**
     * @param mixed $venue
     */
    public function setVenue($venue)
    {
        $this->venue = $venue;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param mixed $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return ArrayCollection
     */
    public function getOrganizers()
    {
        return $this->organizers;
    }

    /**
     * @param ArrayCollection $organizers
     */
    public function setOrganizers($organizers)
    {
        $this->organizers = $organizers;
    }

    /**
     * @return bool
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * @param bool $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * @return bool
     */
    public function isListed()
    {
        return $this->listed;
    }

    /**
     * @param bool $listed
     */
    public function setListed($listed)
    {
        $this->listed = $listed;
    }

    /**
     * Add image to images collection.
     * @param string $path
     */
    public function addImage(string $path)
    {
        foreach ($this->images as $image) {
            if ($image === $path) {
                return;
            }
        }
        $this->images->add($path);
    }

    /**
     * Add category to category collection
     * Ensure that category is unique.
     *
     * @param Category $category
     */
    public function addCategory(Category $category)
    {
        /** @var Category $existingCategory */
        foreach ($this->categories as $existingCategory) {
            if ($existingCategory->getId() === $category->getId()) {
                return;
            }
        }

        $this->categories->add($category);
    }

    /**
     * Add an error to the entity.
     * @param Error $error
     */
    public function addError(Error $error)
    {
        $this->errors->add($error);
    }

    /**
     * @return ArrayCollection
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param ArrayCollection $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return int
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param int $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getIdOnSource()
    {
        return $this->idOnSource;
    }

    /**
     * @param string $idOnSource
     */
    public function setIdOnSource($idOnSource)
    {
        $this->idOnSource = $idOnSource;
    }

    /**
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     */
    public function setContact(Contact $contact)
    {
        $this->contact = $contact;
    }

    public function addOrganizer(User $user)
    {
        /** @var User $organizer */
        foreach ($this->organizers as $organizer) {
            if ($organizer->getId() === $user->getId()) {
                return;
            }
        }
        $this->organizers->add($user);
    }

    /**
     * @return ArrayCollection
     */
    public function getAttendees()
    {
        return $this->attendees;
    }

    /**
     * @param ArrayCollection $attendees
     */
    public function setAttendees($attendees)
    {
        $this->attendees = $attendees;
    }

    /**
     * @param User $user
     */
    public function addAttendee(User $user)
    {
        /** @var User $attendee */
        foreach ($this->attendees as $attendee) {
            if ($attendee->getId() === $user->getId()) {
                return;
            }
        }
        $this->attendees->add($user);
    }
}
