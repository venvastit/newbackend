<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/12/16
 * Time: 7:30 AM.
 */

namespace App\Domains\Event\Repositories;

use App\Core\Repositories\LocatableRepository;
use App\Domains\Event\Entities\Event;
use DateTime;

class EventRepository extends LocatableRepository
{
    public static function getEntityClass()
    {
        return Event::class;
    }

    /**
     * Query only upcoming events.
     * @return $this
     */
    public function addUpcomingQuery()
    {
        $now = new DateTime();

        return $this->addDateFromQuery($now);
    }

    public function addDateFromQuery(DateTime $from)
    {
        $this->queryBuilder->field('starts_at')->gte($from);

        return $this;
    }

    public function addDateToQuery(DateTime $to)
    {
        $this->queryBuilder->field('starts_at')->lte($to);

        return $this;
    }
    
}
