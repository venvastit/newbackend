<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/12/16
 * Time: 7:35 AM.
 */

namespace App\Domains\Event\Jobs\Migrate;

use App\Core\Dictionary\DataSources;
use App\Core\Entities\Error;
use App\Core\Jobs\Job;
use App\Domains\Category\Entities\Category;
use App\Domains\Category\Repositories\CategoryRepository;
use App\Domains\Event\Entities\Event;
use App\Domains\Event\Repositories\EventRepository;
use App\Domains\Event\Validation\Rules\EventRules;
use App\Domains\Venue\Repositories\VenueRepository;
use DB;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * This job is only used to migrate events from an old database
 * Why not just use Laravel migrations? Pretty simple - we use mongoDB :D.
 *
 * Class MigrateEvents
 */
class MigrateEvents extends Job
{
    /**
     * @var EventRepository
     */
    private $repository;

    /**
     * @var VenueRepository
     */
    private $venueRepo;

    /**
     * @var CategoryRepository
     */
    private $categoryRepo;

    /**
     * MigrateEvents constructor.
     *
     * @param EventRepository $repository
     * @param VenueRepository $venueRepository
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(EventRepository $repository, VenueRepository $venueRepository, CategoryRepository $categoryRepository)
    {
        $this->repository = $repository;
        $this->venueRepo = $venueRepository;
        $this->categoryRepo = $categoryRepository;
    }

    /**
     * Handles migrate job.
     */
    public function handle()
    {
        $events = DB::connection('oldmongo')->collection('events')->get();
        $unresolved = 0;
        foreach ($events as $event) {
            set_time_limit(60);
            if (!array_key_exists('location', $event)) {
                $unresolved++;
                continue;
            }
            if (!array_key_exists('coordinates', $event['location'])) {
                $unresolved++;
                continue;
            }
            $coordinates = $event['location']['coordinates'];
            $venue = $this->getVenueByCoordinates($coordinates);
            if (!$venue) {
                $unresolved++;
                continue;
            }

            $eventEntity = $this->bindData($event);
            $eventEntity->setVenue($venue);
            $eventEntity->setContact($venue->getContact());
            $validator = $this->getValidator($eventEntity);
            if ($validator->fails()) {
                foreach ($validator->getMessageBag()->toArray() as $field => $message) {
                    $eventEntity->addError(new Error($field, $message));
                    $eventEntity->setPublished(false);
                }
            } else {
                $eventEntity->setPublished(true);
            }
            $eventEntity->setListed(true);
            $this->repository->persist($eventEntity);
            $this->repository->flush();
        }
    }

    /**
     * Bind data from array to Event entity.
     *
     * @param array $data
     * @return Event
     */
    private function bindData(array $data)
    {
        $event = new Event();
        if (array_key_exists('title', $data) && !empty($data['title'])) {
            $event->setName($data['title']);
        }
        if (array_key_exists('details', $data) && !empty($data['details'])) {
            $event->setDescription($data['details']);
        }

        $imagesCollection = new ArrayCollection();
        if (array_key_exists('picture', $data) && !empty($data['picture'])) {
            if (substr($data['picture'], 0, 4) === 'http') {
                $imagesCollection->add($data['picture']);
            }
        }
        $event->setImages($imagesCollection);

        if (array_key_exists('start_at', $data) && !empty($data['start_at'])) {
            $event->setStartsAt($data['start_at']->toDateTime());
        }
        if (array_key_exists('end_at', $data) && !empty($data['end_at'])) {
            $event->setEndsAt($data['end_at']->toDateTime());
        }
        if (array_key_exists('categories', $data) && !empty($data['categories'])) {
            $category = $this->buildCategories($data['categories']);
            if ($category) {
                $event->addCategory($category);
            }
        }
        if (array_key_exists('id_on_source', $data) && !empty($data['id_on_source'])) {
            $event->setSource(DataSources::FACEBOOK);
            $event->setIdOnSource($data['id_on_source']);
        }

        return $event;
    }

    /**
     * Find venue by exact coordinates and return it.
     * If no venues were found or found more then 1 venue for the same
     * coordinates returns NULL.
     *
     * @param array $coordinates
     * @return null
     */
    private function getVenueByCoordinates(array $coordinates)
    {
        $venue = $this->venueRepo->findBy([
            'contact.address.coordinates.coordinates' => $coordinates,
        ]);
        if (count($venue) > 1 || empty($venue)) {
            return;
        }

        return $venue[0];
    }

    private function getValidator(Event $event)
    {
        /** @var \Illuminate\Contracts\Validation\Validator $validator */
        $validator = \Validator::make($event->toArray(), EventRules::entity());

        return $validator;
    }

    /**
     * Find matching category for this type(event)
     * by it's name.
     *
     * @param $categories
     * @return Category|null
     */
    private function buildCategories($categories)
    {
        if (!is_array($categories) || !array_key_exists(0, $categories)) {
            return;
        }
        $categoryId = $categories[0];
        $oldCategory = DB::connection('oldmongo')
            ->collection('categories')->where('_id', $categoryId)->first();
        if ($oldCategory && array_key_exists('name', $oldCategory)) {
            return $this->categoryRepo->findOneBy([
                'name' => $oldCategory['name'],
                'type' => Category::TYPE_EVENTS,
            ]);
        }

        return;
    }
}
