<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/12/16
 * Time: 9:06 AM.
 */

namespace App\Domains\Event\Validation\Rules;

use App\Core\Validation\EntityValidationRulesContract;

class EventRules implements EntityValidationRulesContract
{
    public static function entity() : array
    {
        return [
            'name' => 'required|string|min:3|max:65',
            'description' => 'required|min:140|max:1200',
            'categories' => 'required|categories',
            'startsAt' => 'required',
            'endsAt' => 'required',
            'venue' => 'has_venue',
            'contact' => 'required|contact',
        ];
    }

    public static function request() : array
    {
        return [
            'name' => 'required|string|min:3|max:65',
            'description' => 'required|min:140|max:1200',
            'locale' => 'required',
            'categories' => 'required|array|min:1',
            'images' => 'required|array|min:1|max:3',
            'starts_at' => 'required|date',
            'ends_at' => 'required|date|after:start_date',
            'phone' => 'required',
            'email' => 'required|email',
            'venue' => 'required',
            'timezone' => 'required',
        ];
    }
}
