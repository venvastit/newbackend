<?php
/**
 * Copyright (c) $today.year.  Venvast Limited - All rights reserved
 *
 * Created by PhpStorm.
 * User: hlogeon <email: hlogeon1@gmail.com>
 * Date: 9/21/16
 * Time: 2:27 PM
 */

namespace App\Domains\Venue\Services;


use App\Domains\Venue\Repositories\VenueRepository;

class VenueStatsService
{

    /**
     * @var VenueRepository
     */
    protected $venueRepo;

    /**
     * VenueStatsService constructor.
     * @param VenueRepository $venueRepo
     */
    public function __construct(VenueRepository $venueRepo)
    {
        $this->venueRepo = $venueRepo;
    }

    /**
     * @return array
     */
    public function getPublishedVenuesPieChartData()
    {
        $published = $this->venueRepo->getPublishedCount();
        $total = $this->venueRepo->totalCount();
        $percentage = floor(($published / $total) * 100);
        return [
            'color' => 'rgba(25,140,25,0.6)',
            'description' => 'Published Venues',
            'stats' => $published,
            'percent' => $percentage,
        ];
    }


    /**
     * @return array
     */
    public function getClaimedVenuesPieChartData()
    {
        $claimed = $this->venueRepo->getClaimedCount();
        $total = $this->venueRepo->totalCount();
        $percentage = floor(($claimed / $total) * 100);
        return [
            'color' => 'rgba(76,76,255,0.6)',
            'description' => 'Claimed Venues',
            'stats' => $claimed,
            'percent' => $percentage,
        ];
    }
    
    public function getWeekUpdatesGroupedByDay()
    {
        return $this->venueRepo->getUpdatesGroupedByDay(7);
    }

    public function getWeekCreatedGroupedByDay()
    {
        return $this->venueRepo->getCreatesGroupedByDay(7);
    }

}