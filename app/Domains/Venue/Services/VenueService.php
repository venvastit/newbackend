<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/5/16
 * Time: 6:52 AM.
 */

namespace App\Domains\Venue\Services;

use App\Core\Entities\BaseEntity;
use App\Core\Entities\Contact;
use App\Core\Entities\Error;
use App\Core\Entities\Schedule\Schedule;
use App\Core\Services\LocatableService;
use App\Core\Services\SaveImageService;
use App\Domains\Category\Entities\Category;
use App\Domains\Category\Repositories\CategoryRepository;
use App\Domains\User\Entities\User;
use App\Domains\Venue\Entities\Venue;
use App\Domains\Venue\Repositories\VenueRepository;
use App\Domains\Venue\Validation\Rules\VenueRules;
use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Support\Collection;
use Validator;
use DateTime;

/**
 * Venue service provides functionality of venue. Do not use anything else to interact with
 * venue domain except this service. By using this service you make unicorns more happy as they
 * don't see all the horrible shit behind the scene.
 *
 * Class VenueService
 */
class VenueService extends LocatableService
{
    /**
     * @var VenueRepository
     */
    public $repository;

    /**
     * VenueService constructor.
     *
     * @param VenueRepository $repository
     * @param CategoryRepository $categoryRepo
     * @param SaveImageService $imageService
     */
    public function __construct(VenueRepository $repository, CategoryRepository $categoryRepo, SaveImageService $imageService)
    {
        parent::__construct($repository, $categoryRepo, $imageService);
    }

    /**
     * Create venue with.
     *
     * @param string $name
     * @param string $desc
     * @param array $categories
     * @param array $images
     * @param Contact $cont
     * @param Schedule $openHours
     * @param User $org
     * @return Venue
     */
    public function create(string $name, string $desc, array $categories, array $images, Contact $cont, Schedule $openHours, User $org)
    {
        $venue = new Venue();
        $categoryInstances = new ArrayCollection();
        foreach ($categories as $categoryId) {
            $categoryInstances->add($this->categoryRepo->getById($categoryId));
        }
        $venue->setImages($this->makeImages($images, 'venues'));
        $venue->setCategories($categoryInstances);
        $venue->addOrganizer($org);
        $venue->setContact($cont);
        $venue->setBusinessHours($openHours);
        $venue->setName($name);
        $venue->setDescription($desc);

        return $venue;
    }

    /**
     * Store venue in database, perform validation,
     * raise corresponding events etc.
     *
     * @param BaseEntity $entity
     * @param bool $draft
     *
     * @return Venue|array
     */
    public function store(BaseEntity $entity, bool $draft = false)
    {
        $validator = $this->getValidator($entity);
        $isValid = !$validator->fails();
        $entity->setPublished(false);
        if (!$isValid) {
            foreach ($validator->getMessageBag()->toArray() as $field => $message) {
                $entity->addError(new Error($field, $message));
                \Log::alert($message);
            }
        }
        if ($draft || $isValid) {
            if (!$draft) {
                $entity->setPublished(true);
            }

            $entity->setErrors(new ArrayCollection());

            $this->repository->save($entity);

            return $entity;
        } else {
            return $validator->getMessageBag()->toArray();
        }
    }

    /**
     * Returns venue validator.
     *
     * @param BaseEntity $venue
     * @return \Illuminate\Validation\Validator
     */
    protected function getValidator(BaseEntity $venue)
    {
        $validator = Validator::make($venue->toArray(), VenueRules::entity());

        return $validator;
    }

    /**
     * Lists venues by criteria from configuration array.
     *
     * @param array $config
     * @param bool $paginate
     *
     * @return Collection
     */
    public function getList(array $config, bool $paginate)
    {
        $this->addLocationQuery($config);
        $this->addCategoryQuery($config);
        $this->addOpenQuery($config);
        $this->addTextQuery($config);
        if (env('APP_DEBUG') !== true) {
            $this->repository->addPublishedQuery();
            $this->repository->addListedQuery();
        }
        $venues = new Collection($this->repository->execute());
        $venues->sort(function (Venue $venue) {
            return $venue->getContact()->getAddress()->getDistance();
        });
        if ($paginate) {
            $venues = $this->paginate($venues, $config);
        }

        return $venues;
    }


    /**
     * List of venues for admin panel. Initialy used to display Grid
     * @param array $config
     * @return Collection
     */
    public function getAdminList(array $config)
    {
        if (!array_key_exists('page', $config)) {
            $config['page'] = 0;
        }
        $this->addCityQuery($config);
        $this->addEmailQuery($config);
        $this->addPhoneQuery($config);
        $this->repository->addLimitOffsetQuery(self::PER_PAGE, self::PER_PAGE * (int) $config['page']);
        $collection = new Collection($this->repository->execute());
        return $collection;
    }



    /**
     * Organizer list.
     *
     * @param array $config
     * @param User $user
     * @return Collection
     */
    public function organizerList(array $config, User $user)
    {
        $this->addCategoryQuery($config);
        $this->addTextQuery($config);
        $this->repository->addPublishedQuery(true);
        $this->repository->addOrganizerQuery($user);

        $venues = $this->paginate(new Collection($this->repository->execute()), $config);

        return $venues;
    }

    /**
     * List drafts of venues.
     *
     * @param array $config
     * @param User $user
     * @return Collection
     */
    public function draftsList(array $config, User $user)
    {
        $this->addCategoryQuery($config);
        $this->addTextQuery($config);
        $this->repository->addPublishedQuery(false);
        $this->repository->addOrganizerQuery($user);

        $venues = $this->paginate(new Collection($this->repository->execute()), $config);

        return $venues;
    }

    protected function getCategoryType()
    {
        return Category::TYPE_VENUES;
    }

    /**
     * Add open query.
     *
     * @param array $config
     */
    public function addOpenQuery(array $config)
    {
        if (array_key_exists('open', $config) && !empty($config['open'])) {
            $open = $config['open'] === 'true';
            $time = new DateTime();

            if (array_key_exists('timezone', $config) && !empty($config['timezone'])) {
                $tz = new \DateTimeZone($config['timezone']);
                $time->setTimezone($tz);
            }
            $this->repository->addIsOpenQuery($time, $open);
        }
    }
}
