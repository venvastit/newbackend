<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/4/16
 * Time: 8:47 PM.
 */

namespace App\Domains\Venue\Repositories;

use App\Core\Entities\Schedule\Schedule;
use App\Core\Repositories\BaseRepository;
use App\Core\Repositories\LocatableRepository;
use App\Domains\Category\Entities\Category;
use App\Domains\User\Entities\User;
use App\Domains\Venue\Entities\Venue;
use Doctrine\ODM\MongoDB\DocumentManager;
use GeoJson\Geometry\Point;
use DateTime;
use Cache;

class VenueRepository extends LocatableRepository
{
    public static function getEntityClass()
    {
        return Venue::class;
    }

    /**
     * Search by host property.
     *
     * @param bool $host
     * @return $this
     */
    public function addIsHostQuery($host = false)
    {
        $this->queryBuilder->field('host')->equals($host);

        return $this;
    }

    /**
     * Is this venue open on time specified.
     *
     * @param bool $isOpen
     * @param DateTime $time
     * @return $this
     */
    public function addIsOpenQuery(DateTime $time, $isOpen = true)
    {
        $ret = [true => 'true', false => 'false'];
        $this->queryBuilder->where('function () {
            var days = this.business_hours.days;
            for(var i = 0; i < days.length; i++) {
                if(days[i].day == '.$time->format('w').') {
                    if( (days[i].start <= ' .$time->format('Gi').') && (days[i].end >= '.$time->format('Gi').')) {
                        return '.$ret[$isOpen].';
                    }
                }
            }
            return ' .$ret[!$isOpen].';
        }');

        return $this;
    }
}
