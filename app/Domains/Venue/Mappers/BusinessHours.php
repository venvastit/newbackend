<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/8/16
 * Time: 1:41 PM.
 */

namespace App\Domains\Venue\Mappers;

use App\Core\Entities\Schedule\Schedule;
use App\Core\Entities\Schedule\ScheduleDay;
use App\Core\Mappers\AbstractRequestMapper;
use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Http\Request;

class BusinessHours extends AbstractRequestMapper
{
    public function to(Request $request)
    {
        $data = $request->get('business_hours');
        $businessHours = new Schedule();
        foreach ($data['days'] as $index => $value) {
            $this->normalizeValue($value);
            $day = new ScheduleDay($value['day'], $value['start'], $value['end']);
            $businessHours->addDay($day);
        }
//        $businessHours->setDays($days);
        return $businessHours;
    }

    /**
     * @param array $request
     * @return Schedule
     */
    public function fromArray(array $request)
    {
        $data = $request['business_hours'];
        $businessHours = new Schedule();
        $days = new ArrayCollection();
        foreach ($data['days'] as $index => $value) {
            $value = $this->normalizeValue($value);
            $day = new ScheduleDay($value['day'], $value['start'], $value['end']);
            $days->add($day);
        }
        $businessHours->setDays($days);

        return $businessHours;
    }

    /**
     * In case if we got some wrong values lets
     * assume wrong day is just a day off.
     *
     * @param $value
     */
    private function normalizeValue(&$value)
    {
        if (!array_key_exists('start', $value) || $value['start'] === null) {
            $value['start'] = false;
        }
        if (intval($value['start']) < 0 || intval($value['start']) > 2400) {
            $value['start'] = false;
        }
        if (!array_key_exists('end', $value) || $value['end'] === null) {
            $value['end'] = false;
        }
        if (intval($value['end']) < 0 || intval($value['end']) > 2400) {
            $value['end'] = false;
        }

        return $value;
    }
}
