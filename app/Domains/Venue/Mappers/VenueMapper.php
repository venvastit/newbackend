<?php
/**
 * Copyright (c) $today.year.  Venvast Limited - All rights reserved.
 *
 * Created by PhpStorm.
 * User: hlogeon <email: hlogeon1@gmail.com>
 * Date: 9/14/16
 * Time: 10:37 PM
 */

namespace App\Domains\Venue\Mappers;

use App\Core\Mappers\AbstractRequestMapper;
use App\Core\Mappers\ContactMapper;
use App\Domains\User\Services\UserService;
use App\Domains\Venue\Services\VenueService;
use Doctrine\ODM\MongoDB\DocumentNotFoundException;
use Illuminate\Http\Request;

class VenueMapper extends AbstractRequestMapper
{
    protected $service;

    protected $contactMapper;

    protected $businessHoursMapper;

    protected $userService;

    public function __construct(
        VenueService $service, ContactMapper $contactMapper,
        BusinessHours $businessHoursMapper, UserService $userService
    ) {
        $this->service = $service;
        $this->contactMapper = $contactMapper;
        $this->businessHoursMapper = $businessHoursMapper;
        $this->userService = $userService;
    }

    public function to(Request $request)
    {
        if (is_string($request->get('venue'))) {
            return $this->fromString($request->get('venue'));
        } elseif (is_array($request->get('venue'))) {
            return $this->fromArray($request->get('venue'));
        }
    }

    /**
     * If venue been passed as a string it means that we have to fnd exising one
     * and throw NotFoundException if we can't find it.
     *
     * @param string $venue
     * @return \App\Domains\Venue\Entities\Venue
     * @throws DocumentNotFoundException
     */
    private function fromString(string $venue)
    {
        $entity = $this->service->repository->getById($venue);
        if (!$entity) {
            throw new DocumentNotFoundException;
        }

        return $entity;
    }

    /**
     * If we've got an array from request it means that we have to create new Venue.
     *
     * @param array $venue
     * @return \App\Domains\Venue\Entities\Venue|array
     */
    private function fromArray(array $venue)
    {
        $contact = $this->contactMapper->fromArray($venue);
        $user = $this->userService->getUserRepository()->current();
        $businessHours = $this->businessHoursMapper->fromArray($venue);
        $venue = $this->service->create(
            $venue['name'], (string) $venue['description'], $venue['categories'],
            $venue['images'], $contact, $businessHours, $user
        );

        return $this->service->store($venue);
    }
}
