<?php

namespace App\Domains\Venue\Entities;

use App\Core\Entities\BaseEntity;
use App\Core\Entities\Error;
use App\Domains\Category\Entities\Category;
use App\Domains\User\Entities\User;
use Caffeinated\Presenter\Traits\PresentableTrait;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Core\Entities\Contact;
use App\Core\Entities\Schedule\Schedule;
use DateTime;

/**
 * @ODM\Document(collection="venues")
 *
 * @ODM\HasLifecycleCallbacks
 * @ODM\Index(keys={"contact.address.coordinates"="2dsphere"})
 */
class Venue extends BaseEntity
{
    use PresentableTrait;

    /**
     * @ODM\Field(type="string", nullable=false)
     *
     * @Gedmo\Translatable
     */
    protected $name;

    /**
     * @ODM\Field(type="date", name="created_at")
     * @Gedmo\Timestampable(on="create")
     *
     * @var DateTime
     */
    protected $createdAt;

    /**
     * Date user were updated at last time.
     *
     * @ODM\Field(type="date", name="updated_at")
     * @Gedmo\Timestampable(on="update")
     *
     * @var DateTime
     */
    protected $updatedAt;

    /**
     * @ODM\Field(type="string", nullable=false)
     *
     * @Gedmo\Translatable
     *
     * @var string
     */
    protected $description;

    /**
     * @ODM\Field(type="collection")
     *
     * @var ArrayCollection
     */
    protected $images;

    /**
     * @ODM\EmbedOne(targetDocument="App\Core\Entities\Contact", nullable=false)
     *
     * @var Contact
     */
    public $contact;

    /**
     * @ODM\Field(type="boolean", nullable=false)
     * @var bool
     */
    protected $published = false;

    /**
     * @ODM\Field(type="boolean", nullable=false)
     *
     * @var bool
     */
    protected $listed = true;

    /**
     * @ODM\ReferenceMany(targetDocument="App\Domains\Category\Entities\Category", inversedBy="venues", cascade={"persist"})
     * @var ArrayCollection
     */
    protected $categories;

    /**
     * @ODM\ReferenceMany(targetDocument="App\Domains\User\Entities\User", inversedBy="organizeVenues", cascade={"persist"})
     *
     * @var ArrayCollection
     */
    protected $organizers;

    /**
     * @ODM\ReferenceMany(targetDocument="App\Domains\Event\Entities\Event", mappedBy="venue")
     *
     * @var ArrayCollection
     */
    protected $events;

    /**
     * @ODM\EmbedOne(targetDocument="App\Core\Entities\Schedule\Schedule", name="business_hours")
     *
     * @var Schedule
     */
    protected $businessHours;

    /**
     * @ODM\Field(type="boolean")
     * @var bool
     */
    protected $host = false;

    /**
     * @ODM\ReferenceMany(targetDocument="App\Domains\Category\Entities\Category", inversedBy="hosts", name="host_categories")
     *
     * @var ArrayCollection
     */
    protected $hostCategories;

    /**
     * @ODM\Field(type="string", name="host_message", nullable=true)
     *
     * @Gedmo\Translatable
     *
     * @var string
     */
    protected $hostMessage;

    /**
     * Errors of this entity (used to notify what organizer should change)
     * in this instance to get it approved.
     *
     * @ODM\EmbedMany(targetDocument="App\Core\Entities\Error", nullable=true)
     *
     * @var ArrayCollection
     */
    protected $errors;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ODM\Field(type="string")
     *
     * @var string
     */
    protected $slug;

    /**
     * @ODM\Field(type="int")
     *
     * @var int
     */
    protected $source;

    /**
     * @ODM\Field(type="string", name="id_on_source")
     *
     * @var string
     */
    protected $idOnSource;

    /**
     * @Gedmo\Language
     */
    public $locale;

    protected $presenter = 'App\Domains\Venue\Presenters\VenuePresenter';

    public function __construct()
    {
        $this->organizers = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->hostCategories = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->errors = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     */
    public function setContact(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return bool
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * @param bool $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * @return ArrayCollection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param mixed $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    /**
     * Add category to category collection
     * Ensure that category is unique.
     *
     * @param Category $category
     */
    public function addCategory(Category $category)
    {
        /** @var Category $existingCategory */
        foreach ($this->categories as $existingCategory) {
            if ($existingCategory->getId() === $category->getId()) {
                return;
            }
        }

        $this->categories->add($category);
    }

    /**
     * @return ArrayCollection
     */
    public function getOrganizers()
    {
        return $this->organizers;
    }

    /**
     * @param ArrayCollection $organizers
     */
    public function setOrganizers($organizers)
    {
        $this->organizers = $organizers;
    }

    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param mixed $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * Add image to images collection.
     * @param string $path
     */
    public function addImage(string $path)
    {
        foreach ($this->images as $image) {
            if ($image === $path) {
                return;
            }
        }
        $this->images->add($path);
    }

    /**
     * @return ArrayCollection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @param ArrayCollection $events
     */
    public function setEvents($events)
    {
        $this->events = $events;
    }

    /**
     * @return Schedule
     */
    public function getBusinessHours()
    {
        return $this->businessHours;
    }

    /**
     * @param Schedule $businessHours
     */
    public function setBusinessHours($businessHours)
    {
        $this->businessHours = $businessHours;
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return mixed
     */
    public function getHostCategories()
    {
        return $this->hostCategories;
    }

    /**
     * Add host category.
     * @param Category $category
     */
    public function addHostCategory(Category $category)
    {
        /** @var Category $existingCategory */
        foreach ($this->hostCategories as $existingCategory) {
            if ($existingCategory->getId() === $category->getId()) {
                return;
            }
        }
        $this->hostCategories->add($category);
    }

    /**
     * @param mixed $hostCategories
     */
    public function setHostCategories($hostCategories)
    {
        $this->hostCategories = $hostCategories;
    }

    /**
     * @return string
     */
    public function getHostMessage()
    {
        return $this->hostMessage;
    }

    /**
     * @param string $hostMessage
     */
    public function setHostMessage(string $hostMessage)
    {
        $this->hostMessage = $hostMessage;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    public function addOrganizer(User $user)
    {
        /** @var User $organizer */
        foreach ($this->organizers as $organizer) {
            if ($organizer->getId() === $user->getId()) {
                return;
            }
        }
        $this->organizers->add($user);
    }

    /**
     * @return bool
     */
    public function isListed()
    {
        return $this->listed;
    }

    /**
     * @param bool $listed
     */
    public function setListed($listed)
    {
        $this->listed = $listed;
    }

    /**
     * @return ArrayCollection
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param ArrayCollection $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    public function addError(Error $error)
    {
        $this->errors->add($error);
    }

    /**
     * @return int
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param int $source
     */
    public function setSource(int $source)
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getIdOnSource()
    {
        return $this->idOnSource;
    }

    /**
     * @param string $idOnSource
     */
    public function setIdOnSource(string $idOnSource)
    {
        $this->idOnSource = $idOnSource;
    }
}
