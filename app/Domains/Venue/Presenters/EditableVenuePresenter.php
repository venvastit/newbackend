<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/10/16
 * Time: 8:54 AM.
 */

namespace App\Domains\Venue\Presenters;

use App\Core\Presenters\BasePresenter;
use App\Domains\Category\Entities\Category;
use App\Domains\Venue\Builders\BusinessHoursBuilder;
use App\Domains\Venue\Entities\Venue;

class EditableVenuePresenter extends BasePresenter
{
    /**
     * @var Venue
     */
    protected $entity;

    protected $publicFields = [
        'name', 'categories', 'description',
        'contact', 'business_hours', 'listed',
        'images', 'id', 'errors', 'full_address',
        'locale',
    ];

    public function id()
    {
        return $this->entity->getId();
    }

    public function name()
    {
        return $this->entity->getName();
    }

    public function description()
    {
        return $this->entity->getDescription();
    }

    public function listed()
    {
        return $this->entity->isListed();
    }

    public function categories()
    {
        $categories = [];
        /** @var Category $category */
        foreach ($this->entity->getCategories() as $category) {
            $categories[] = $category->getId();
        }

        return $categories;
    }

    public function contact()
    {
        $contact = $this->entity->getContact();
        $address = $contact->getAddress();
        $coordinates = $address->getCoordinates();

        return [
            'phone' => $contact->getPhone(),
            'email' => $contact->getEmail(),
            'website' => $contact->getWebsite(),
            'additional_info' => $contact->getAdditionalInfo(),
            'address' => [
                'house' => $address->getHouse(),
                'street' => $address->getStreet(),
                'district' => $address->getDistrict(),
                'sub_district' => $address->getSubDistrict(),
                'city' => $address->getCity(),
                'country' => $address->getCountry(),
                'coordinates' => [
                    'lat' => $coordinates['coordinates'][1],
                    'lng' => $coordinates['coordinates'][0],
                ],
            ],
        ];
    }

    public function business_hours()
    {
        if ($this->entity->getBusinessHours() === null || count($this->entity->getBusinessHours()->getDays()) !== 7) {
            return [
                'days' => BusinessHoursBuilder::buildDefault()->toArray(),
            ];
        }

        return [
        'days' => $this->entity->getBusinessHours()->toArray(),
        ];
    }

    public function images()
    {
        $images = $this->entity->getImages();
        $output = [];

        foreach ($images as $image) {
            if (starts_with('http', $image)) {
                $output[] = $image;
            } else {
                $output[] = \URL::asset($image);
            }
        }

        return $output;
    }

    public function errors()
    {
        return $this->entity->getErrors()->toArray();
    }

    public function full_address()
    {
        $house = trim($this->entity->getContact()->getAddress()->getHouse());
        $street = trim($this->entity->getContact()->getAddress()->getStreet());
        $district = trim($this->entity->getContact()->getAddress()->getDistrict());
        $city = $this->entity->getContact()->getAddress()->getCity();
        $country = $this->entity->getContact()->getAddress()->getCountry();

        return trim(implode(', ', compact('house', 'street', 'district', 'city')));
    }

    public function locale()
    {
        return 'en';
    }
}
