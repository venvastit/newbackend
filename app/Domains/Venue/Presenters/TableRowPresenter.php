<?php
/**
 * Copyright (c) $today.year.  Venvast Limited - All rights reserved
 *
 * Created by PhpStorm.
 * User: hlogeon <email: hlogeon1@gmail.com>
 * Date: 9/21/16
 * Time: 11:04 PM
 */

namespace App\Domains\Venue\Presenters;


use App\Core\Entities\Contact;
use App\Core\Presenters\BasePresenter;
use App\Domains\Category\Entities\Category;
use App\Domains\Venue\Entities\Venue;

class TableRowPresenter extends BasePresenter
{

    const UNDEFINED_STR = 'Undefined';

    /** @var  Venue */
    protected $entity;

    protected $publicFields = [
        'id', 'title', 'category',
        'city', 'published', 'phone', 'email',
        'errors', 'organizers', 'upcoming_events',
    ];

    /**
     * Id of venue
     *
     * @return string
     */
    public function id()
    {
        return $this->entity->getId();
    }

    /**
     * Venue title
     *
     * @return string
     */
    public function title()
    {
        return $this->entity->getName();
    }

    /**
     * Name of category if set
     *
     * @return string
     */
    public function category()
    {
        if ($this->entity->getCategories()->count() === 0) {
            return self::UNDEFINED_STR;
        }
        /** @var Category $category */
        $category = $this->entity->getCategories()->first();
        return $category->getName();
    }

    /**
     * Name of the city if set
     *
     * @return string
     */
    public function city()
    {
        /** @var Contact|null $contact */
        if ($contact = $this->entity->getContact()) {
            if ($address = $contact->getAddress()) {
                return $address->getCity() ?: self::UNDEFINED_STR;
            }
        }

        return self::UNDEFINED_STR;
    }

    /**
     * Is published?
     *
     * @return bool
     */
    public function published()
    {
        return $this->entity->isPublished();
    }

    /**
     * Phone if set
     *
     * @return string
     */
    public function phone()
    {
        if ($contact = $this->entity->getContact()) {
            return $contact->getPhone() ?: self::UNDEFINED_STR;
        }
        return self::UNDEFINED_STR;
    }

    /**
     * Email if set
     *
     * @return string
     */
    public function email()
    {
        if ($contact = $this->entity->getContact()) {
            return $contact->getEmail() ?: self::UNDEFINED_STR;
        }
        return self::UNDEFINED_STR;
    }

    /**
     * Count of errors in this model
     *
     * @return int
     */
    public function errors()
    {
        if ($errors = $this->entity->getErrors()) {
            return $errors->count();
        }
        return 0;
    }

    /**
     * Count of organizers of venue
     *
     * @return int
     */
    public function organizers()
    {
        if ($organizers = $this->entity->getOrganizers()) {
            return $organizers->count();
        }
        return 0;
    }

    /**
     * Count of upcoming events
     *
     * @return int
     */
    public function upcoming_events()
    {
        return 0; //TODO
    }




}