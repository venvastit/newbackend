<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/5/16
 * Time: 12:03 PM.
 */

namespace App\Domains\Venue\Presenters;

use App\Core\Entities\Schedule\ScheduleDay;
use App\Core\Presenters\BasePresenter;
use App\Domains\Venue\Builders\BusinessHoursBuilder;
use App\Domains\Venue\Entities\Venue;
use DateTimeZone;
use DateTime;

/**
 * Class VenuePresenter.
 */
class VenuePresenter extends BasePresenter
{
    protected $publicFields = [
        'name', 'fullAddress', 'description',
        'images', 'phone', 'email',
        'category', 'icon', 'businessHours',
        'distance', 'location', 'id', 'slug',
    ];

    /**
     * @var Venue
     */
    protected $entity;

    /**
     * @return string
     */
    public function name()
    {
        return $this->entity->getName();
    }

    /**
     * @return float|null
     */
    public function distance()
    {
        return $this->entity->getContact()->getAddress()->getDistance();
    }

    /**
     * @return string
     */
    public function fullAddress()
    {
        $house = trim($this->entity->getContact()->getAddress()->getHouse());
        $street = trim($this->entity->getContact()->getAddress()->getStreet());
        $district = trim($this->entity->getContact()->getAddress()->getDistrict());

        return trim(implode(', ', compact('house', 'street', 'district')));
    }

    /**
     * @return string|null
     */
    public function description()
    {
        return $this->entity->getDescription();
    }

    /**
     * @return mixed
     */
    public function images()
    {
        $images = $this->entity->getImages();
        $output = [];

        foreach ($images as $image) {
            if (starts_with('http', $image)) {
                $output[] = $image;
            } else {
                $output[] = \URL::asset($image);
            }
        }

        if (count($output) === 0) {
            return;
        }

        return $output[0];
    }

    /**
     * @return string
     */
    public function phone()
    {
        return $this->entity->getContact()->getPhone();
    }

    /**
     * @return string
     */
    public function email()
    {
        return $this->entity->getContact()->getEmail();
    }

    /**
     * @return array
     */
    public function category()
    {
        if (env('APP_DEBUG') === true) { //TODO: remove this shit
            $category = $this->entity->getCategories()->first();
            if ($category) {
                return $category->present()->toArray();
            }

            return [
                'name' => $this->entity->getCategories()->first() ? $this->entity->getCategories()->first()->getName() : 'undefined',
                'icon' => 'TODO', //TODO: Full icon url
            ];
        }

        return $this->entity->getCategories()->first()->present()->toArray();
    }

    public function icon()
    {
        if (env('APP_DEBUG') === true) { //TODO: remove this shit
            $category = $this->entity->getCategories()->first();
            if ($category) {
                return $category->present()->icon();
            } else {
                return 'https://api.venvast.com/images/categories/8NzFa7cguM.png';
            }
        }

        return $this->entity->getCategories()->first()->present()->icon();
    }

    public function location()
    {
        return $this->entity->getContact()->getAddress()->getLatLng();
    }

    /**
     * @return \App\Core\Entities\Schedule\Schedule
     */
    public function businessHours()
    {
        $bHours = $this->entity->getBusinessHours();
        if (empty($bHours) /*&& env('APP_DEBUG') === true*/) {
            $bHours = BusinessHoursBuilder::buildDefault();
        }
        $bHours = $bHours->toArray();
        foreach ($bHours as $key => $day) {
            $day['alias'] = $this->getDayAlias($day['day']);
            $bHours[$key] = $day;
        }

        return $bHours;
    }

    public function id()
    {
        return $this->entity->getId();
    }

    public function slug()
    {
        return $this->entity->getSlug();
    }

    public function open(string $tz)
    {
        $tz = new DateTimeZone($tz);
        $time = new DateTime('now', $tz);
//        $time->setTimezone($tz);
        $day = (int) $time->format('w');
        $intTime = (int) $time->format('Gi');
        $businessHours = $this->entity->getBusinessHours();
        if (/*env('APP_DEBUG') === true &&*/ (empty($businessHours) || empty($businessHours->getDays()))) {
            $businessHours = BusinessHoursBuilder::buildDefault();
        }
        /** @var ScheduleDay $businessDay */
        foreach ($businessHours->getDays() as $businessDay) {
            $start = $businessDay->start;
            $end = $businessDay->end;
            if ($start > 1000) {
                $starts = $start !== false && $start !== null && $start <= $intTime;
            } else {
                $starts = $start !== false && $start !== null && $start >= $intTime;
            }
            if ($end > 1000) {
                $ends = $end >= $intTime || $end === null || $end === false;
            } else {
                $ends = $end <= $intTime || $end === null || $end === false;
            }
            if ((int) $businessDay->day === $day) {
                return $starts && $ends;
            }
        }

        return false;
    }

    /**
     * Returns alias for day.
     * @param int $day
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    private function getDayAlias(int $day)
    {
        switch ($day) {
            case 0:
                return trans('daytime.sun');
            case 1:
                return trans('daytime.mon');
            case 2:
                return trans('daytime.tue');
            case 3:
                return trans('daytime.wed');
            case 4:
                return trans('daytime.thu');
            case 5:
                return trans('daytime.fri');
            case 6:
                return trans('daytime.sat');
        }
    }
}
