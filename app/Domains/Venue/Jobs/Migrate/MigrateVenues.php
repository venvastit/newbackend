<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/6/16
 * Time: 4:49 AM.
 */

namespace App\Domains\Venue\Jobs\Migrate;

use App\Core\Dictionary\DataSources;
use App\Core\Entities\Address;
use App\Core\Entities\Contact;
use App\Core\Entities\Error;
use App\Core\Jobs\Job;
use App\Core\Validation\Rules\AddressRules;
use App\Core\Validation\Rules\ContactRules;
use App\Domains\Category\Entities\Category;
use App\Domains\Category\Repositories\CategoryRepository;
use App\Domains\Venue\Builders\BusinessHoursBuilder;
use App\Domains\Venue\Entities\Venue;
use App\Domains\Venue\Repositories\VenueRepository;
use App\Domains\Venue\Validation\Rules\VenueRules;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use MongoDB\BSON\ObjectID;
use Validator;
use DB;

/**
 * Class MigrateVenues.
 *
 * Migrate venues from old Eloquent models
 * to Doctrine entities.
 *
 * 1. Perform validation
 * 2. Apply correct attributes
 * 3. Publish correct venues
 * 4. Store in database for Doctrine
 */
class MigrateVenues extends Job // implements ShouldQueue
{
    //use InteractsWithQueue;

    /**
     * @var VenueRepository
     */
    public $venueRepo;

    public function __construct()
    {
        $this->venueRepo = $this->makeRepo();
    }

    public function handle()
    {
        $venues = DB::connection('oldmongo')->collection('venues')->get();
        /*
         * Venue is an array representation of an old venue
         * Object.
         *
         * @var array
         */
        foreach ($venues as $venue) {
            set_time_limit(60);
            $entity = $this->bindDataToVenue($venue);
            $validator = $this->validate($entity);
            if ($validator->fails()) {
                foreach ($validator->getMessageBag()->toArray() as $field => $message) {
                    $entity->addError(new Error($field, $message));
                }
                $entity->setPublished(false);
            } else {
                $entity->setPublished(true);
            }
            $this->venueRepo->getDocumentManager()->persist($entity);
            $this->venueRepo->getDocumentManager()->flush();
        }
    }

    protected function validate(Venue $venue)
    {
        /** @var \Illuminate\Contracts\Validation\Validator $validator */
        $validator = Validator::make($venue->toArray(), VenueRules::entity());

        return $validator;
    }

    protected function validateContact(Contact $contact)
    {
        /** @var \Illuminate\Contracts\Validation\Validator $validator */
        $validator = Validator::make($contact->toArray(), ContactRules::entity());

        return $validator;
    }

    protected function validateAddress(Address $address)
    {
        $validator = Validator::make($address->toArray(), AddressRules::entity());

        return $validator;
    }

    /**
     * @param array $data
     * @return Venue
     */
    public function bindDataToVenue(array $data)
    {
        $venue = new Venue();
        if (array_key_exists('name', $data)) {
            $venue->setName($data['name']);
        }
        if (array_key_exists('description', $data)) {
            $venue->setDescription($data['description']);
        }
        if (array_key_exists('cover_image', $data)) {
            $venue->addImage($data['cover_image']);
        }
        if (array_key_exists('id_on_source', $data) && !empty($data['id_on_source'])) {
            $venue->setSource(DataSources::FACEBOOK);
            $venue->setIdOnSource($data['id_on_source']);
        }
        if (array_key_exists('google_place_id', $data) && !empty($data['google_place_id'])) {
            $venue->setSource(DataSources::GOOGLE);
            $venue->setIdOnSource($data['google_place_id']);
        }
        if (array_key_exists('businessHours', $data) && !empty($data['businessHours'])) {
            $venue->setBusinessHours(BusinessHoursBuilder::build($data['businessHours']));
        }
        if (array_key_exists('categories', $data) && !empty($data['categories'])) {
            $category = $this->buildCategories($data['categories']);
            if ($category instanceof Category) {
                $venue->addCategory($category);
            }
        }

        $address = $this->buildAddress($data);
        $contact = $this->buildContact($data, $address);
        $venue->setContact($contact);

        return $venue;
    }

    public function buildCategories($categories)
    {
        if (!is_array($categories) || !array_key_exists(0, $categories)) {
            return;
        }
        $categoryId = $categories[0];
        $oldCategory = DB::connection('oldmongo')
            ->collection('categories')->where('_id', $categoryId)->first();
        if ($oldCategory && array_key_exists('name', $oldCategory)) {
            return $this->categoryRepository()->findOneBy([
                'name' => $oldCategory['name'],
                'type' => Category::TYPE_VENUES,
            ]);
        }

        return;
    }

    /**
     * @param array $data
     * @param Address $address
     * @return Contact
     */
    public function buildContact(array $data, Address $address)
    {
        $contact = new Contact();
        $contact->setAddress($address);
        if (array_key_exists('email', $data) && !empty($data['email'])) {
            $contact->setEmail($data['email']);
        }

        if (array_key_exists('phone', $data) && !empty($data['phone'])) {
            $contact->setPhone($data['phone']);
        }

        if (array_key_exists('website', $data) && !empty($data['website'])) {
            $contact->setWebsite($data['website']);
        }

        if (array_key_exists('public_transit', $data) && !empty($data['public_transit'])) {
            $contact->setAdditionalInfo($data['public_transit']);
        }

        return $contact;
    }

    public function buildAddress(array $data)
    {
        $address = new Address();
        if (array_key_exists('country', $data) && !empty($data['country'])) {
            $address->setCountry($data['country']);
        }
        if (array_key_exists('location', $data) && !empty($data['location'])) {
            $location = $data['location'];
            if (array_key_exists('coordinates', $location) && count($location['coordinates']) === 2) {
                $coordinates = $location['coordinates'];
                $address->makeCoordinates($coordinates[0], $coordinates[1]);
            }
        }
        if (array_key_exists('city', $data) && !empty($data['city'])) {
            $address->setCity($data['city']);
        }
        if (array_key_exists('street', $data) && !empty($data['street'])) {
            $street = $this->parseStreet($data['street']);
            $house = $this->parseHouse($data['street']);
            if ($street !== null) {
                $address->setStreet($street);
            }
            if ($house !== null) {
                $address->setHouse($house);
            }
        }

        return $address;
    }

    /**
     * Try to parse street from the data we have.
     *
     * @param string $street
     * @return null
     */
    public function parseStreet(string $street)
    {
        $parts = explode(',', $street);
        if (count($parts) > 0 && strlen($parts[0]) > 3 && !is_numeric($parts[0])) {
            return is_numeric(substr($parts[0], 0, 1)) ? null : $parts[0];
        }

        return;
    }

    public function parseHouse(string $street)
    {
        $parts = explode(',', $street);
        if (count($parts) > 0) {
            $parts = explode(' ', $parts[0]);
            if (count($parts) > 0) {
                $potential = $parts[0];
                if (is_numeric($potential)) {
                    return $potential;
                }
            }
        }

        return;
    }

    public function makeRepo()
    {
        return \App::make('VenueRepository');
    }

    /**
     * @return CategoryRepository
     */
    public function categoryRepository()
    {
        return \App::make('CategoryRepository');
    }
}
