<?php

namespace App\Domains\Venue\Builders;

use App\Core\Entities\Schedule\Schedule;
use App\Core\Entities\Schedule\ScheduleDay;

/**
 * Just helps to build Schedule instances correctly to represent
 * Business hours data.
 */
class BusinessHoursBuilder
{
    /**
     * Build schedule by the days configuration as an array
     * days format ['day' => integer, 'start' => integer, 'end' => integer]
     * Note that all days which are not corresponding this format will be ignored.
     *
     * @param  array  $days [description]
     * @return Schedule
     */
    public static function build(array $days)
    {
        $schedule = new Schedule();
        foreach ($days as $key => $day) {
            if (static::validateDayData($day)) {
                $day = static::makeDayFromArray($day);
                $schedule->days->set($day->day, $day);
            }
        }

        return $schedule;
    }

    /**
     * Build default schedule
     * From 09:00 to 1800 Monday-Friday.
     *
     * Closed on weekend
     *
     * @return Schedule
     */
    public static function buildDefault()
    {
        $schedule = new Schedule();
        for ($i = 0; $i < 7; $i++) {
            if ($i === 0 || $i === 6) {
                $day = static::makeDayOff($i);
            } else {
                $day = static::makeDefualtDay($i);
            }
            $schedule->days->set($day->day, $day);
        }

        return $schedule;
    }

    /**
     * Make sure that data provided is valid for creation ScheduleDay.
     *
     * @param  array $day
     * @return bool      true if valid
     */
    public static function validateDayData($day)
    {
        $hasDay = array_key_exists('day', $day) && is_numeric($day['day']);
        if ($hasDay && (self::valid24h($day) || self::validClose($day) || self::validStartEnd($day))) {
            return true;
        }

        return false;
    }

    public static function validStartEnd(array $day)
    {
        $hasStart = 0;
        $hasEnd = 0;
        if (array_key_exists('start', $day)) {
            $hasStart = 1;
        }
        if (array_key_exists('open', $day)) {
            $hasStart = 2;
        }
        if (array_key_exists('end', $day)) {
            $hasEnd = 1;
        }
        if (array_key_exists('close', $day)) {
            $hasEnd = 2;
        }
        if ($hasStart > 0 && $hasEnd > 0) {
            if ($hasStart === 1) {
                return is_numeric($day['start']) && $day['start'] != $day['end'];
            } else {
                return is_numeric($day['open']) && $day['open'] != $day['close'];
            }
        }

        return false;
    }

    public static function validClose(array $day)
    {
        if (array_key_exists('start', $day)) {
            return $day['start'] === false;
        }
        if (array_key_exists('open', $day)) {
            return $day['open'] === false;
        }

        return false;
    }

    public static function valid24h(array $day)
    {
        $hasStart = 0;
        $hasEnd = 0;
        if (array_key_exists('start', $day)) {
            $hasStart = 1;
        }
        if (array_key_exists('open', $day)) {
            $hasStart = 2;
        }
        if (array_key_exists('end', $day)) {
            $hasEnd = 1;
        }
        if (array_key_exists('close', $day)) {
            $hasEnd = 2;
        }
        if ($hasStart > 0 && $hasEnd > 0) {
            if ($hasStart === 1) {
                return is_numeric($day['start']) && $day['start'] == $day['end'];
            } else {
                return is_numeric($day['open']) && $day['open'] == $day['close'];
            }
        }

        return false;
    }

    /**
     * Create ScheduleDay instance from array provided.
     *
     * @param  array  $day [description]
     * @return ScheduleDay
     */
    public static function makeDayFromArray(array $day)
    {
        $start = false;
        $end = false;
        if (array_key_exists('start', $day)) {
            $start = $day['start'];
        }
        if (array_key_exists('end', $day)) {
            $end = $day['end'];
        }

        return new ScheduleDay($day['day'], $start, $end);
    }

    /**
     * Create ScheduleDay instance with default
     * open hours (09:00 - 18:00).
     * @param  int    $day
     * @return ScheduleDay
     */
    public static function makeDefualtDay(int $day)
    {
        return new ScheduleDay($day, 900, 1800);
    }

    /**
     * Create ScheduleDay instance representing
     * open 24 hours.
     * @param  int    $day
     * @return ScheduleDay
     */
    public static function make24HDay(int $day)
    {
        return new ScheduleDay($day, 900, 900);
    }

    /**
     * Create ScheduleDay representing day off.
     *
     * @param  int    $day
     * @return ScheduleDay
     */
    public static function makeDayOff(int $day)
    {
        return new ScheduleDay($day);
    }
}
