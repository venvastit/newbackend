<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/6/16
 * Time: 5:45 AM.
 */

namespace App\Domains\Venue\Validation\Rules;

use App\Core\Validation\EntityValidationRulesContract;

class VenueRules implements EntityValidationRulesContract
{
    public static function entity() : array
    {
        return [
            'name' => 'required|min:3|max:65',
            'description' => 'required|min:140|max:1200',
            'businessHours' => 'required|schedule_week',
            'contact' => 'required|contact',
            'categories' => 'required|categories',
        ];
    }

    public static function request() : array
    {
        return [
            'name' => 'required|min:3|max:65',
            'description' => 'required|min:140|max:1800',
            'business_hours.days' => 'required|array|size:7',
            'locale' => 'required',
            'categories' => 'required|array|min:1',
            'images' => 'required|array|min:1|max:3',
            'contact.email' => 'required|email',
            'contact.phone' => 'required',
            'contact.address.street' => 'required|min:3|max:400',
            'contact.address.district' => 'min:3|max:400',
            'contact.address.sub_district' => 'min:3|max:400',
            'contact.address.coordinates.lat' => 'required|numeric',
            'contact.address.coordinates.lng' => 'required|numeric',
        ];
    }
}
