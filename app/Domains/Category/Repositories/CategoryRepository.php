<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/5/16
 * Time: 4:32 AM.
 */

namespace App\Domains\Category\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Domains\Category\Entities\Category;

class CategoryRepository extends BaseRepository
{
    public static function getEntityClass()
    {
        return Category::class;
    }
}
