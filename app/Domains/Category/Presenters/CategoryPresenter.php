<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/6/16
 * Time: 3:29 PM.
 */

namespace App\Domains\Category\Presenters;

use App\Core\Presenters\BasePresenter;
use App\Domains\Category\Entities\Category;

class CategoryPresenter extends BasePresenter
{
    /** @var  Category */
    protected $entity;

    protected $publicFields = [
        'name', 'priority', 'type',
        'icon', 'slug', 'locale',
        'id', 'objectsCount',
    ];

    /**
     * @return string
     */
    public function name()
    {
        return $this->entity->getName();
    }

    /**
     * @return int
     */
    public function priority() : int
    {
        return (int) $this->entity->getPriority();
    }

    /**
     * @return string
     */
    public function id()
    {
        return $this->entity->getId();
    }

    /**
     * @return string
     */
    public function type()
    {
        switch ($this->entity->getType()) {
            case Category::TYPE_VENUES:
                return 'venues';
            case Category::TYPE_EVENTS:
                return 'events';
        }
    }

    /**
     * TODO.
     * @return string
     */
    public function icon()
    {
        return 'https://api.venvast.com/images/categories/8NzFa7cguM.png';
    }

    /**
     * @return string
     */
    public function slug()
    {
        return $this->entity->getSlug();
    }

    /**
     * @return string
     */
    public function locale()
    {
        if (!$this->entity->getLocale()) {
            return 'en';
        }

        return $this->entity->getLocale();
    }

    /**
     * TODO Category::TYPE_EVENTS.
     *
     * @return int
     */
    public function objectsCount()
    {
        switch ($this->entity->getType()) {
            case Category::TYPE_VENUES:
                return $this->entity->getVenues()->count();
            case Category::TYPE_EVENTS:
                return $this->entity->getEvents()->count();
        }
    }
}
