<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/6/16
 * Time: 2:47 PM.
 */

namespace App\Domains\Category\Services;

use App\Core\Presenters\BasePresenter;
use App\Domains\Category\Entities\Category;
use App\Domains\Category\Repositories\CategoryRepository;
use Illuminate\Support\Collection;

class CategoryService
{
    public $categoryRepo;

    public function __construct(CategoryRepository $categoryRepo)
    {
        $this->categoryRepo = $categoryRepo;
    }

    public function events()
    {
        return new Collection($this->categoryRepo->findBy(['type' => Category::TYPE_EVENTS]));
    }

    /**
     * @return Collection
     */
    public function venues()
    {
        return new Collection($this->categoryRepo->findBy(['type' => Category::TYPE_VENUES]));
    }

    /**
     * @param Collection $categories
     * @param string $presenterClass
     * @param array $config
     *
     * @return Collection
     */
    public static function presentCollection(Collection $categories, string $presenterClass, array $config = [])
    {
        $categories = $categories->map(function ($category, $key) use ($presenterClass, $config) {
            /** @var BasePresenter $presenter */
            $presenter = new $presenterClass($category);
            $category = $presenter->toArray();

            return $category;
        });

        return $categories;
    }
}
