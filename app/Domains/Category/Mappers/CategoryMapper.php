<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/8/16
 * Time: 1:32 PM.
 */

namespace App\Domains\Category\Mappers;

use App\Core\Mappers\AbstractRequestMapper;
use App\Domains\Category\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryMapper extends AbstractRequestMapper
{
    /**
     * @var CategoryService
     */
    protected $categoryService;

    /**
     * CategoryMapper constructor.
     * @param CategoryService $categoryService
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * Returns an array of categories present in this request.
     *
     * @param Request $request
     * @return array
     */
    public function to(Request $request)
    {
        if ($request->has('categories')) {
            $categories = $request->get('categories');
            $result = [];
            foreach ($categories as $key => $category) {
                $result[] = $this->categoryService->categoryRepo->getById($category);
            }

            return $result;
        }

        if ($request->has('category')) {
            return [
                $this->categoryService->categoryRepo->getById($request->get('category')),
            ];
        }
    }
}
