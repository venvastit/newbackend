<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/5/16
 * Time: 4:42 AM.
 */

namespace App\Domains\Category\Entities;

use App\Core\Entities\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class SourceCategory.
 *
 * @ODM\Document(collection="source_categories")
 */
class SourceCategory extends BaseEntity
{
    /**
     * @ODM\Field(type="string", nullable=false)
     */
    public $name;

    /**
     * @ODM\Field(type="int", nullable=false)
     *
     * @var int
     */
    public $type;

    /**
     * @ODM\ReferenceOne(targetDocument="App\Domains\Category\Entities\Category", inversedBy="sourceCategories")
     *
     * @var Category
     */
    protected $targetCategory;

    public function __construct(string $name, int $type)
    {
        $this->name = $name;
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return Category
     */
    public function getTargetCategory()
    {
        return $this->targetCategory;
    }

    /**
     * @param Category $targetCategory
     */
    public function setTargetCategory($targetCategory)
    {
        $this->targetCategory = $targetCategory;
    }
}
