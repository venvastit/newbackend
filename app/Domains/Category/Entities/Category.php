<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/5/16
 * Time: 1:44 AM.
 */

namespace App\Domains\Category\Entities;

use App\Core\Entities\BaseEntity;
use App\Domains\Venue\Entities\Venue;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Caffeinated\Presenter\Traits\PresentableTrait;

/**
 * Class Category.
 *
 * @ODM\Document(collection="categories")
 */
class Category extends BaseEntity
{
    use PresentableTrait;

    const TYPE_EVENTS = 1;
    const TYPE_VENUES = 2;

    /**
     * @ODM\Field(type="string", nullable=false)
     * @Gedmo\Translatable
     *
     * @var string
     */
    public $name;

    /**
     * @ODM\Field(type="int", nullable=false)
     *
     * @var int
     */
    public $type;

    public $icon;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ODM\Field(type="string")
     *
     * @var string
     */
    public $slug;

    /**
     * @Gedmo\Locale
     *
     * @var string
     */
    public $locale;

    /**
     * @ODM\Field(type="int", nullable=true)
     *
     * @var int
     */
    public $priority;

    /**
     * @ODM\ReferenceMany(targetDocument="App\Domains\Category\Entities\SourceCategory", mappedBy="targetCategory")
     *
     * @var ArrayCollection
     */
    protected $sourceCategories;

    /**
     * @ODM\ReferenceMany(targetDocument="App\Domains\Venue\Entities\Venue", mappedBy="categories", cascade={"all"})
     *
     * @var ArrayCollection
     */
    protected $venues;

    /**
     * @ODM\ReferenceMany(targetDocument="App\Domains\Event\Entities\Event", mappedBy="categories", cascade={"all"})
     *
     * @var ArrayCollection
     */
    protected $events;

    /**
     * @ODM\ReferenceMany(targetDocument="App\Domains\Venue\Entities\Venue", mappedBy="hostCategories")
     *
     * @var ArrayCollection
     */
    protected $hosts;

    public $presenter = 'App\Domains\Category\Presenters\CategoryPresenter';

    /**
     * Category constructor.
     *
     * @param string $name
     * @param int $type
     * @param int $priority
     * @param string $locale
     */
    public function __construct(string $name, int $type, $priority = 0, $locale = 'en')
    {
        $this->name = $name;
        $this->type = $type;
        $this->priority = $priority;
        $this->sourceCategories = new ArrayCollection([]);
        $this->venues = new ArrayCollection();
        $this->events = new ArrayCollection();
    }

    /**
     * Add reference to source category.
     *
     * @param SourceCategory $sourceCategory
     */
    public function addSourceCategory(SourceCategory $sourceCategory)
    {
        $this->sourceCategories->add($sourceCategory);
    }

    public function setSourceCategories($sourceCategories)
    {
        $this->sourceCategories = $sourceCategories;
    }

    public function getSourceCategories()
    {
        return $this->sourceCategories;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param mixed $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return ArrayCollection
     */
    public function getVenues()
    {
        return $this->venues;
    }

    /**
     * @param ArrayCollection $venues
     */
    public function setVenues($venues)
    {
        $this->venues = $venues;
    }

    public function addVenue(Venue $venue)
    {
        $this->venues->add($venue);
    }

    /**
     * @return ArrayCollection
     */
    public function getHosts()
    {
        return $this->hosts;
    }

    /**
     * @param ArrayCollection $hosts
     */
    public function setHosts($hosts)
    {
        $this->hosts = $hosts;
    }

    public function addHost(Venue $venue)
    {
        $this->hosts->add($venue);
    }

    /**
     * @return ArrayCollection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @param ArrayCollection $events
     */
    public function setEvents($events)
    {
        $this->events = $events;
    }
}
