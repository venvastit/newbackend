<?php

namespace App\Domains\User\Services\Authentication\Strategies;

use App\Domains\User\Contracts\AuthContract;
use App\Domains\User\Services\AuthenticationService;

abstract class AbstractAuthStrategy implements AuthContract
{
    /**
     * AuthenticationService instance to interact with.
     *
     * @var AuthenticationService
     */
    protected $authService;

    /**
     * @param AuthenticationService $service [description]
     */
    public function __construct(AuthenticationService $service)
    {
        $this->setAuthService($service);
    }

    /**
     * Set AuthenticationService instance.
     *
     * @param AuthenticationService $authService
     */
    protected function setAuthService(AuthenticationService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * Get AuthenticationServuce.
     *
     * @return AuthenticationService
     */
    protected function getAuthService() : AuthenticationService
    {
        return $this->authService;
    }
}
