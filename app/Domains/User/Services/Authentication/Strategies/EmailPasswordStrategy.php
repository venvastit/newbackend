<?php

namespace App\Domains\User\Services\Authentication\Strategies;

use App\Domains\User\Contracts\AuthContract;
use App\Domains\User\Services\Authentication\Strategies\AbstractAuthStrategy;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Hash;

/**
 * Authenticate user with email and password.
 *
 * @class EmailPasswordStrategy
 */
class EmailPasswordStrategy extends AbstractAuthStrategy
{
    /**
     * Authenticate user by email & password.
     *
     * @param  array  $credentials array with email and password
     * @return string|null JWT if success null othervice
     */
    public function authenticate(array $credentials)
    {
        $repository = $this->getAuthService()->getUserRepository();
        $credentials = array_only($credentials, ['email', 'password']);
        $user = $repository->getByEmail($credentials['email']);
        if ($user === null) {
            $this->getAuthService()->addError('email', trans('auth.failed'));

            return;
        }
        if ($user->getActivated() === false) {
            $this->getAuthService()->addError('email', trans('auth.inactive'));

            return;
        }
        if (!Hash::check($credentials['password'], $user->getPassword())) {
            $this->getAuthService()->addError('password', trans('auth.password'));

            return;
        }

        return JWTAuth::fromUser($user);
    }
}
