<?php

namespace App\Domains\User\Services\Authentication\Strategies;

use App\Domains\User\Services\Authentication\Strategies\AbstractAuthStrategy;

/**
 * Authenticate user by facebook token provided.
 *
 * @class FacebookStrategy
 *
 * TODO: implementation
 */
class FacebookStrategy extends AbstractAuthStrategy
{
    public function authenticate(array $credentials)
    {
    }
}
