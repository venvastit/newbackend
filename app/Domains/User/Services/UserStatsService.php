<?php
/**
 * Copyright (c) $today.year.  Venvast Limited - All rights reserved
 *
 * Created by PhpStorm.
 * User: hlogeon <email: hlogeon1@gmail.com>
 * Date: 9/21/16
 * Time: 1:12 PM
 */

namespace App\Domains\User\Services;


use App\Domains\User\Repositories\UserRepository;
use App\Domains\User\Traits\HasUserRepository;

class UserStatsService
{
    use HasUserRepository;


    public function __construct(UserRepository $repository)
    {
        $this->userRepo = $repository;
    }
    

    public function getActiveInactiveChartPieChartData()
    {
        $totalCount = $this->userRepo->getTotalCount();
        $active = $this->userRepo->getActivatedCount();
        $percentage = floor(($active / $totalCount) * 100);
        return [
            'color' => 'rgba(109,64,222,0.6)',
            'description' => 'Active Users',
            'stats' => $active,
            'percent' => $percentage,
        ];
    }
    
}