<?php

namespace App\Domains\User\Services;

use App\Domains\User\Contracts\AuthContract;
use App\Domains\User\Events\Login;
use App\Domains\User\Exceptions\AuthenticateException;
use App\Domains\User\Repositories\UserRepository;
use App\Domains\User\Services\Authentication\Strategies\AbstractAuthStrategy;
use App\Domains\User\Services\Authentication\Strategies\EmailPasswordStrategy;
use App\Domains\User\Services\Authentication\Strategies\FacebookStrategy;
use App\Domains\User\Traits\HasUserRepository;
use Event;
use JWTAuth;
use Illuminate\Support\Collection;

class AuthenticationService implements AuthContract
{
    use HasUserRepository;

    /**
     * Authentication strategy instance.
     *
     * @var AbstractStrategy
     */
    protected $strategy;

    /**
     * Collection of auth errors.
     * @var Collection
     */
    protected $errors;

    /**
     * Generated token.
     *
     * @var string|null
     */
    protected $token;

    public function __construct(UserRepository $userRepo)
    {
        $this->setUserRepository($userRepo);
        $this->errors = new Collection([]);
    }

    public function authenticate(array $credentials)
    {
        $token = $this->getStrategyByCredentials($credentials)->authenticate($credentials);
        if ($this->errors->count() > 0) {
            return false;
        }
        Event::fire(new Login(JWTAuth::toUser($token)));
        $this->token = $token;

        return true;
    }

    /**
     * Get strategy by credentials array. Choose correct strategy by array keys
     * provided in credentials array.
     *
     * @param  array  $credentials credentials for authentication
     *
     * @return AbstractAuthStrategy Auth strategy to authenticate with
     * credentials provided in request
     */
    private function getStrategyByCredentials(array $credentials)
    {
        if (array_key_exists('email', $credentials)) {
            return new EmailPasswordStrategy($this);
        }
        if (array_key_exists('facebook_access_token', $credentials)) {
            return new FacebookStrategy($this);
        }
        throw new AuthenticateException("Can't define authentication strategy");
    }

    /**
     * Add an error to service so it can be displayed later.
     *
     * @param string $field   [description]
     * @param string $message [description]
     */
    public function addError(string $field, string $message)
    {
        $this->errors->put($field, $message);
    }

    /**
     * Get collection of errors.
     *
     * @return Collection|array Collection of all errors
     * caused during authentication
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Generated JWT.
     * @return string|null [description]
     */
    public function getToken()
    {
        return $this->token;
    }
}
