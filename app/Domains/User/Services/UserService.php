<?php

namespace App\Domains\User\Services;

use App\Domains\User\Entities\User;
use App\Domains\Venue\Entities\Venue;
use App\Domains\Event\Entities\Event as EventEntity;
use App\Domains\User\Events\Registration\AccountActivated;
use App\Domains\User\Events\Registration\CredentialsRegistered;
use App\Domains\User\Events\Registration\OnlyEmailRegistered;
use App\Domains\User\Events\Registration\PasswordCreated;
use App\Domains\User\Repositories\UserRepository;
use App\Domains\User\Traits\HasUserRepository;
use Auth;
use Event;
use Hash;
use Session;

/**
 * @class UserService
 *
 * Class UserService providing an abstraction over the
 * User domain.
 */
class UserService
{
    use HasUserRepository;

    /**
     * Fields that can be grabbed from the user input(means form).
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'facebookId',
        'facebookPicture', 'facebookAccessToken',
        'firstName', 'lastName',
        ];

    public function __construct(UserRepository $repository)
    {
        $this->userRepo = $repository;
    }

    /**
     * Register new user, trigger corresponding event,
     * and return an instance of newly created user.
     *
     * @param  array  $userData attributes of user
     * to be created
     *
     * @return [type]           [description]
     */
    public function register(array $userData)
    {
        $emailProvided = array_key_exists('email', $userData);
        $passwordProvided = array_key_exists('password', $userData);

        if ($emailProvided && $passwordProvided) {
            return $this->registerWithCredentials($userData);
        } elseif ($emailProvided && !$passwordProvided) {
            return $this->registerWithOnlyEmail($userData);
        }
    }

    public function favoriteVenue(Venue $venue, User $user)
    {
        $user->addFavoriteVenue($venue);
        $this->getUserRepository()->persist($user);
        $this->getUserRepository()->flush();
    }

    public function favoriteEvent(EventEntity $event, User $user)
    {
        $user->addFavoriteEvent($event);
        $this->getUserRepository()->persist($user);
        $this->getUserRepository()->flush();
    }

    public function goingEvent(EventEntity $event, User $user)
    {
        $user->addGoingEvent($event);
        $this->getUserRepository()->persist($user);
        $this->getUserRepository()->flush();
    }

    public function cancelGoingEvent(EventEntity $event, User $user)
    {
        $user->removeGoingEvent($event);
        $this->getUserRepository()->persist($user);
        $this->getUserRepository()->flush();
    }

    /**
     * Activate user, trigger corresponding event and
     * return User instance.
     *
     *	It will trigger corresponding event only if user were not registered
     *	before triggering this event
     *
     * @param  User   $user user to be activated
     * @return User
     */
    public function activate(User $user)
    {
        if ($user->getActivated() !== true) {
            $user->setActivated(true);
            $this->getUserRepository()->persist($user);
            $this->getUserRepository()->flush();
            Event::fire(new AccountActivated($user));
        }

        return $user;
    }

    /**
     * Create password, trigger corresponding event
     * and return User instance.
     *
     * @param  User   $user user which creates password
     * @param  string $password
     * @param  string $code
     *
     * @return User
     */
    public function createPassword(User $user, string $password, string $code)
    {
        if ($user->getActivationCode() !== $code) {
            return false;
        }
        $this->makePassword($user, $password);
        if ($user->getActivated() === false) {
            $this->activate($user);
        }

        return $user;
    }

    /**
     * Just hash the password provided and save it to database
     * No events would be triggered.
     * @param  User   $user     [description]
     * @param  string $password [description]
     * @return User
     */
    public function makePassword(User $user, string $password)
    {
        $user->makePassword($password);
        $this->getUserRepository()->persist($user);
        $this->getUserRepository()->flush();
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
    }

    /**
     * Bind fillable fields to User entity.
     *
     * @param  array  $userData array with data to be bind
     * @param  User   $user
     *
     * @return User
     */
    private function bindFromInput(array $userData, User $user)
    {
        foreach ($userData as $key => $value) {
            if (in_array($key, $this->fillable) && $key !== 'password') {
                call_user_func_array([$user, 'set'.ucfirst($key)], [$value]);
            }
            if ($key === 'password') {
                $this->makePassword($user, $value);
            }
        }

        return $user;
    }

    /**
     * Register user with only email provided, trigger corresponding event
     * and return instance if newly created user.
     *
     * This feature is used to fulfill business requirements:
     *
     * + Add user to database even if user didn't register
     * + Send email asking to create password
     * + Avoid sending activation link twice
     *
     * @param  array  $userData [description]
     * @return [type]           [description]
     */
    private function registerWithOnlyEmail(array $userData)
    {
        $user = new User();
        $this->bindFromInput($userData, $user);
        $user->setPasswordCreated(false);
        $this->getUserRepository()->create($user);
        Event::fire(new OnlyEmailRegistered($user));

        return $user;
    }

    /**
     * Register user with provided credentials.
     *
     * @param  array  $userData credentials(email/password)
     * @return User
     */
    private function registerWithCredentials(array $userData)
    {
        $user = new User();
        $this->bindFromInput($userData, $user);
        $user->setPasswordCreated(true);
        $this->getUserRepository()->create($user);
        Event::fire(new CredentialsRegistered($user));

        return $user;
    }

    /**
     * TODO: implement.
     * @param  array  $userData [description]
     * @return [type]           [description]
     */
    private function registerWithFacebook(array $userData)
    {
    }
}
