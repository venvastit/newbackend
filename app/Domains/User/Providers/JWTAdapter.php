<?php

namespace App\Domains\User\Providers;

use App\Domains\User\Entities\User;
use Tymon\JWTAuth\Providers\User\UserInterface;
use Doctrine\ODM\MongoDB\DocumentManager;
use App\Domains\User\Repositories\UserRepository;

/**
 * Doctrine user adapter allows to use
 * JWTAuth with doctrine entity.
 */
class JWTAdapter implements UserInterface
{
    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $user;

    protected $repository;

    /**
     * Create a new User instance.
     *
     * @param  App\Domain\User\Entities\User  $user
     */
    public function __construct(User $user, UserRepository $repository)
    {
        $this->user = $user;
        $this->repository = $repository;
    }

    /**
     * Get the user by the given key, value.
     *
     * @param  mixed  $key
     * @param  mixed  $value
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getBy($key, $value)
    {
        return $this->repository->findOneBy([$key => $value]);
    }

    public function getById(string $id)
    {
        return $this->repository->getById($id);
    }
}
