<?php

namespace App\Domains\User\Exceptions;

use Exception;

class AuthenticateException extends Exception
{
}
