<?php

namespace App\Domains\User\Contracts;

interface AuthContract
{
    /**
     * Authenticate by credentials.
     *
     * @param  array  $credentials Passed credentials
     * @return
     */
    public function authenticate(array $credentials);
}
