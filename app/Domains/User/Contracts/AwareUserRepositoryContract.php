<?php

namespace App\Domains\User\Contracts;

use App\Domains\User\Repositories\UserRepository;

interface AwareUserRepositoryContarct
{
    /**
     * Get user repository instance.
     *
     * @return UserRepository UserRepository instance
     */
    public function getUserRepository() : UserRepository;

    /**
     * Set user repository as a member of the class.
     *
     * @param UserRepository $userRepo
     */
    public function setUserRepository(UserRepository $userRepo);
}
