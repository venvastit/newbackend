<?php

namespace App\Domains\User\Listeners\Email;

use App\Domains\User\Events\BaseUserEvent;
use App\Domains\User\Listeners\Email\AbstractEmail;

/**
 * @class CompleteRegistrationEmail
 *
 * Send email saying registration complete
 */
class CompleteRegistrationEmail extends AbstractEmail
{
    public function handle(BaseUserEvent $event)
    {
        $user = $event->getUser();
        if (!$user) {
            return;
        }
        $this->mailer->send('mails.registration_finished', ['user' => $user], function ($message) use ($user) {
            $message->from('info@venvast.com', 'VenVast team');
            $message->sender('info@venvast.com', 'VenVast team');
            $message->to($user->getEmail());
            $message->subject('Successful registration');
        });
    }
}
