<?php

namespace App\Domains\User\Listeners\Email;

use App\Domains\User\Events\BaseUserEvent;
use App\Domains\User\Listeners\Email\AbstractEmail;

/**
 * @class RestorePassword
 *
 * This email contains a link to password set up
 * with activation code as one of request params
 */
class RestorePassword extends AbstractEmail
{
    public function handle(BaseUserEvent $event)
    {
        $user = $event->getUser();
        $this->mailer->send('mails.restore_password', ['user' => $user], function ($message) use ($user) {
            $message->from('info@venvast.com', 'VenVast team');
            $message->sender('info@venvast.com', 'VenVast team');
            $message->to($user->getEmail());
            $message->subject('Create your password');
        });
    }
}
