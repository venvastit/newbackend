<?php

namespace App\Domains\User\Listeners\Email;

use App\Domains\User\Events\BaseUserEvent;
use App\Domains\User\Listeners\Email\AbstractEmail;

/**
 * @class ActivationLinkEmail
 *
 * Send email which asks user to activate account.
 * This email contains a link with activation code
 */
class ActivationLinkEmail extends AbstractEmail
{
    public function handle(BaseUserEvent $event)
    {
        $user = $event->getUser();
        $this->mailer->send('mails.activate', ['user' => $user], function ($message) use ($user) {
            $message->from('info@venvast.com', 'VenVast team');
            $message->sender('info@venvast.com', 'VenVast team');
            $message->to($user->getEmail());
            $message->subject('Activate your VenVast account!');
        });
    }
}
