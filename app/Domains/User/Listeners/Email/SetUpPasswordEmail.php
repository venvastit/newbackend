<?php

namespace App\Domains\User\Listeners\Email;

use App\Domains\User\Events\BaseUserEvent;
use App\Domains\User\Listeners\Email\AbstractEmail;

/**
 * @class SetUpPasswordEmail
 *
 * Send email which asks user to set password.
 * This email contains a link to password set up
 * with activation code as one of request params
 */
class SetUpPasswordEmail extends AbstractEmail
{
    public function handle(BaseUserEvent $event)
    {
        $user = $event->getUser();
        $this->mailer->send('mails.create_password', ['user' => $user], function ($message) use ($user) {
            $message->from('info@venvast.com', 'VenVast team');
            $message->sender('info@venvast.com', 'VenVast team');
            $message->to($user->getEmail());
            $message->subject('Create your password');
        });
    }
}
