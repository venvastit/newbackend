<?php

namespace App\Domains\User\Listeners\Stats;

use App\Domains\User\Events\BaseUserEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

/**
 * @class MobileLoginStats
 *
 * trigger event on metrics(Google, Facebook, etc)
 */
class MobileLoginStats implements ShouldQueue
{
    use InteractsWithQueue;

    protected $event;

    public function handle(BaseUserEvent $event)
    {
        $this->event = $event;
    }
}
