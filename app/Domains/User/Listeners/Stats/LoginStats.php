<?php

namespace App\Domains\User\Listeners\Stats;

use App\Domains\User\Events\BaseUserEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

/**
 * @class LoginStats
 *
 * trigger event on metrics(Google, Facebook, etc)
 */
class LoginStats implements ShouldQueue
{
    use InteractsWithQueue;

    protected $event;

    public function handle(BaseUserEvent $event)
    {
        $this->event = $event;
    }
}
