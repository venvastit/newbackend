<?php

namespace App\Domains\User\Validation\Rules;

use App\Core\Validation\EntityValidationRulesContract;

/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/9/16
 * Time: 3:32 AM.
 */
class UserRules implements EntityValidationRulesContract
{
    public static function entity() : array
    {
        return [
//            'email' => 'email|unique_email',
//            'password' => ''
        ];
    }

    public static function request() : array
    {
        return [
            'email' => 'email|unique_email',
            'password' => 'string|min:4|max:20',
        ];
    }
}
