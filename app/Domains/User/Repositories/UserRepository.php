<?php

namespace App\Domains\User\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Domains\User\Entities\User;
use App;

/**
 * @class UserRepository
 *
 * User repository contains methods
 * which helps to interact with database
 */
class UserRepository extends BaseRepository
{
    /**
     * Return FQCN of entity.
     *
     * @return string FQCN of entity
     */
    public static function getEntityClass()
    {
        return User::class;
    }

    /**
     * Wrapper over flush and persist.
     *
     * @param  User   $user User entity to be created
     *
     * @return User  entity which were created
     */
    public function create(User $user)
    {
        $user->setActivated(false);
        $user->makeActivationCode();
        $this->persist($user);
        $this->flush();

        return $user;
    }

    /**
     * Find user by email.
     *
     * @param  string $email
     * @return User|null
     */
    public function getByEmail(string $email)
    {
        return $this->findOneBy(['email' => $email]);
    }

    /**
     * Return current user if authenticated.
     *
     * @return User|null
     */
    public static function current()
    {
        $user = App::make('CurrentUser');
        if ($user instanceof User) {
            return $user;
        }

        return;
    }

    /**
     * Get number of active users
     *
     * @return int
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function getActivatedCount()
    {
        return (int) $this->getDocumentManager()
            ->createQueryBuilder(UserRepository::getEntityClass())
            ->field('activated')->equals(true)->getQuery()
            ->execute()->count();
    }


    /**
     * Get total number of users
     *
     * @return int
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function getTotalCount()
    {
        return (int) $this->getDocumentManager()
            ->createQueryBuilder(UserRepository::getEntityClass())
            ->getQuery()->execute()->count();
    }


    /**
     * Get number of unactivated users
     *
     * @return int
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function getUnactivatedCount()
    {
        return (int) $this->getDocumentManager()
            ->createQueryBuilder(UserRepository::getEntityClass())
            ->field('activated')->equals(false)->getQuery()
            ->execute()->count();

    }



}
