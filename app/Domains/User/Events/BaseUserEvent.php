<?php

namespace App\Domains\User\Events;

use App\Core\Events\Event;
use App\Domains\User\Entities\User;
use Illuminate\Queue\SerializesModels;

/**
 * @class BaseUserEvent
 *
 * Base user event is abstraction for all the
 * events in the user domain. Just sets user
 * instance to the event property and defines
 * getUser() public method on Event
 */
abstract class BaseUserEvent extends Event
{
    use SerializesModels;

    /**
     * User this event were triggered for/by.
     *
     * @var User
     */
    protected $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->setUser($user);
    }

    /**
     * Return User instance.
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user.
     *
     * @param User $user
     */
    protected function setUser(User $user)
    {
        $this->user = $user;
    }
}
