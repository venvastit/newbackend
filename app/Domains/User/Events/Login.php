<?php

namespace App\Domains\User\Events;

use App\Domains\User\Events\BaseUserEvent;

/**
 * @class Login
 *
 * Trigger when user logs in or authenticates successfuly
 */
class Login extends BaseUserEvent
{
}
