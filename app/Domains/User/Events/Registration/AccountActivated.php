<?php

namespace App\Domains\User\Events\Registration;

use App\Domains\User\Events\BaseUserEvent;

class AccountActivated extends BaseUserEvent
{
}
