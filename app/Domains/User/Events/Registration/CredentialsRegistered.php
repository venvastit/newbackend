<?php

namespace App\Domains\User\Events\Registration;

use App\Domains\User\Events\BaseUserEvent;

/**
 * @class CredentialsRegistered
 *
 * Triggers when user registers with email/password
 */
class CredentialsRegistered extends BaseUserEvent
{
}
