<?php

namespace App\Domains\User\Events\Registration;

use App\Domains\User\Events\BaseUserEvent;

/**
 * @class FacebookConnected
 *
 * Triggers when user connects facebook account
 */
class FacebookConnected extends BaseUserEvent
{
}
