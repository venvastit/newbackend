<?php

namespace App\Domains\User\Events\Registration;

use App\Domains\User\Events\BaseUserEvent;

/**
 * @class FacebookRegistered
 *
 * Triggers when user registered using facebook
 */
class FacebookRegistered extends BaseUserEvent
{
}
