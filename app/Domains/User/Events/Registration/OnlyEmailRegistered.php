<?php

namespace App\Domains\User\Events\Registration;

use App\Domains\User\Entities\User;
use App\Domains\User\Events\BaseUserEvent;

/**
 * @class OnlyEmailRegistered
 *
 * Triggers when user were added to the system(using email) but there is no password were set up
 *
 * Used almost to migrate users we got from promotions(from real-world papers)
 */
class OnlyEmailRegistered extends BaseUserEvent
{
}
