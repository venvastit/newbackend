<?php

namespace App\Domains\User\Presenters;

use App\Core\Presenters\BasePresenter;

class UserPresenter extends BasePresenter
{
    protected $publicFields = [
        'id', 'email', 'name',
        'admin', 'activated', 'facebookId',
        'facebookPicture',
    ];

    public function id()
    {
        return $this->entity->getId();
    }

    public function email()
    {
        return $this->entity->getEmail();
    }

    public function name()
    {
        $firstName = $this->entity->getFirstName();
        $lastName = $this->entity->getLastName();

        return trim(implode(' ', compact('firstName', 'lastName')));
    }

    public function admin()
    {
        return $this->entity->getAdmin();
    }

    public function activated()
    {
        return $this->entity->getActivated();
    }

    public function facebookId()
    {
        return $this->entity->getFacebookId();
    }

    public function facebookPicture()
    {
        return $this->entity->getFacebookPicture();
    }
}
