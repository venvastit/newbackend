<?php

namespace App\Domains\User\Entities;

use App\Domains\Event\Entities\Event;
use App\Domains\Venue\Entities\Venue;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\ODM\MongoDB\PersistentCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Caffeinated\Presenter\Traits\PresentableTrait;
use App\Core\Entities\BaseEntity;
use DateTime;
use Hash;

/**
 * @ODM\Document(collection="users")
 *
 * @ODM\HasLifecycleCallbacks
 */
class User extends BaseEntity
{
    use PresentableTrait;

    /**
     * @ODM\Id
     */
    public $id;

    /**
     * Is this user has admin permissions?
     *
     * @ODM\Field(type="boolean", name="admin")
     *
     * @var bool
     */
    private $admin;

    /**
     * User email.
     *
     * @ODM\Field(type="string")
     * @ODM\Index(unique=true)
     *
     * @var string
     */
    private $email;

    /**
     * User password.
     *
     * @ODM\Field(type="string", name="password", nullable=true)
     *
     * @var string|null
     */
    private $password;

    /**
     * Is this user already create his password?
     *
     * @ODM\Field(type="boolean", name="password_created")
     *
     * @var bool
     */
    private $passwordCreated;

    /**
     * Is this user activated account.
     *
     * @ODM\Field(type="boolean")
     *
     * @var bool
     */
    private $activated;

    /**
     * Activation code of user.
     *
     * @ODM\Field(type="string", name="activation_code")
     *
     * @var string
     */
    private $activationCode;

    /**
     * Date user activated account.
     *
     * @ODM\Field(type="date", name="activated_at")
     * Gedmo\Timestampable(on="change", field={"activated"})
     *
     * @var DateTime
     */
    private $activatedAt;

    /**
     * Date user were created at.
     *
     * @ODM\Field(type="date", name="created_at")
     * @Gedmo\Timestampable(on="create")
     *
     * @var DateTime
     */
    private $createdAt;

    /**
     * Date user were updated at last time.
     *
     * @ODM\Field(type="date", name="updated_at")
     * @Gedmo\Timestampable(on="update")
     *
     * @var DateTime
     */
    private $updatedAt;

    /**
     * @ODM\ReferenceMany(targetDocument="App\Domains\Event\Entities\Event", cascade={"all"})
     *
     * @var ArrayCollection
     */
    private $favoriteEvents;

    /**
     * TODO: relates to many subscriptions(many-many).
     *
     * @var [type]
     */
    private $subsctiptions;

    /**
     * @ODM\ReferenceMany(targetDocument="App\Domains\Event\Entities\Event", mappedBy="organizers", cascade={"all"})
     *
     * @var ArrayCollection
     */
    private $organizeEvents;

    /**
     * @ODM\ReferenceMany(targetDocument="App\Domains\Event\Entities\Event", cascade={"all"})
     *
     * @var ArrayCollection
     */
    private $goingEvents;

    /**
     * @ODM\ReferenceMany(targetDocument="App\Domains\Venue\Entities\Venue", mappedBy="organizers", cascade={"all"})
     *
     * @var ArrayCollection
     */
    private $organizeVenues;

    /**
     * @ODM\ReferenceMany(targetDocument="App\Domains\Venue\Entities\Venue", cascade={"persist"})
     *
     * @var ArrayCollection
     */
    private $favoriteVenues;

    /**
     * TODO: embeds notifications.
     *
     * @var [type]
     */
    private $notifications;

    /**
     * User id on facebook.
     *
     * @ODM\Field(type="string", name="fb_id", nullable=true)
     *
     * @var string|null
     */
    private $facebookId;

    /**
     * User picture on facebook.
     *
     * @ODM\Field(type="string", name="fb_picture", nullable=true)
     *
     * @var string|null
     */
    private $facebookPicture;

    /**
     * User facebook access token to allow performing actions on facebook
     * from user account.
     *
     * @ODM\Field(type="string", name="fb_access_token", nullable=true)
     *
     * @var string|null
     */
    private $facebookAccessToken;

    /**
     * User first name.
     *
     * @ODM\Field(type="string", name="first_name", nullable=true)
     *
     * @var string|null
     */
    private $firstName;

    /**
     * User last name.
     *
     * @ODM\Field(type="string", name="last_name", nullable=true)
     *
     * @var string|null
     */
    private $lastName;

    /**
     *	Information about user devices.
     *
     * @ODM\Field(type="collection", name="devices", nullable=true)
     *
     * @var array|null
     */
    private $devices;

    /**
     * Define Entity presenter.
     *
     * @var string
     */
    protected $presenter = 'App\Domains\User\Presenters\UserPresenter';

    public function __construct()
    {
        $this->setAdmin(false);
        $this->setActivated(false);
        $this->makeActivationCode();
        $this->organizeVenues = new ArrayCollection();
        $this->favoriteVenues = new ArrayCollection();
        $this->favoriteEvents = new ArrayCollection();
        $this->organizeEvents = new ArrayCollection();
        $this->goingEvents = new ArrayCollection();
    }

    /**
     * Is this user has admin permissions?.
     *
     * @return bool
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * Sets the Is this user has admin permissions?.
     *
     * @param bool $admin the admin
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }

    /**
     * Gets the User email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the User email.
     *
     * @param string $email the email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Gets the User password.
     *
     * @return string|null
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Sets the User password.
     *
     * @param string|null $password the password
     */
    private function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Make password hash and store it in
     * the password field.
     *
     * @param  string $password unhashed password
     */
    public function makePassword(string $password)
    {
        $this->password = Hash::make($password);
    }

    /**
     * Gets the Is this user already create his password?.
     *
     * @return bool
     */
    public function getPasswordCreated()
    {
        if (!empty($this->password)) {
            return true;
        }

        return $this->passwordCreated;
    }

    /**
     * Sets the Is this user already create his password?.
     *
     * @param bool $passwordCreated the password created
     */
    public function setPasswordCreated($passwordCreated)
    {
        $this->passwordCreated = $passwordCreated;
    }

    /**
     * Gets the Is this user activated account.
     *
     * @return bool
     */
    public function getActivated()
    {
        return $this->activated;
    }

    /**
     * Sets the Is this user activated account.
     *
     * @param bool $activated the activated
     */
    public function setActivated($activated)
    {
        $this->activated = $activated;
    }

    /**
     * Gets the Activation code of user.
     *
     * @return string
     */
    public function getActivationCode()
    {
        return $this->activationCode;
    }

    /**
     * Creates the Activation code for user.
     */
    public function makeActivationCode()
    {
        $this->activationCode = substr(Hash::make(microtime()), 6, 12);
    }

    /**
     * Gets the Date user were created at.
     *
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Sets the Date user were created at.
     *
     * @param DateTime $createdAt the created at
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Gets the Date user activated account.
     *
     * @return DateTime
     */
    public function getActivatedAt()
    {
        return $this->activatedAt;
    }

    /**
     * Sets the Date user activated account.
     *
     * @param DateTime $activatedAt the activated at
     */
    public function setActivatedAt(DateTime $activatedAt)
    {
        $this->activatedAt = $activatedAt;
    }

    /**
     * Gets the Date user were updated at last time.
     *
     * @return DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Sets the Date user were updated at last time.
     *
     * @param DateTime $updatedAt the updated at
     */
    public function setUpdatedAt(DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Gets the User id on facebook.
     *
     * @return string|null
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * Sets the User id on facebook.
     *
     * @param string|null $facebookId the facebook id
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;
    }

    /**
     * Gets the User picture on facebook.
     *
     * @return string|null
     */
    public function getFacebookPicture()
    {
        return $this->facebookPicture;
    }

    /**
     * Sets the User picture on facebook.
     *
     * @param string|null $facebookPicture the facebook picture
     */
    public function setFacebookPicture($facebookPicture)
    {
        $this->facebookPicture = $facebookPicture;
    }

    /**
     * Gets the User facebook access token to allow performing
     * actions on facebook from user account.
     *
     * @return string|null
     */
    public function getFacebookAccessToken()
    {
        return $this->facebookAccessToken;
    }

    /**
     * Sets the User facebook access token to allow performing actions on facebook
     * from user account.
     *
     * @param string|null $facebookAccessToken the facebook access token
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebookAccessToken = $facebookAccessToken;
    }

    /**
     * Gets the User first name.
     *
     * @return string|null
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Sets the User first name.
     *
     * @param string|null $firstName the first name
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * Gets the User last name.
     *
     * @return string|null
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Sets the User last name.
     *
     * @param string|null $lastName the last name
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * Gets the Information about user devices.
     *
     * @return array|null
     */
    public function getDevices()
    {
        return $this->devices;
    }

    /**
     * Sets the Information about user devices.
     *
     * @param array|null $devices the devices
     */
    public function setDevices($devices)
    {
        $this->devices = $devices;
    }

    public function addOrganizeVenue(Venue $venue)
    {
        $this->organizeVenues->add($venue);
        $venue->addOrganizer($this);
    }

    public function addOrganizeEvent(Event $event)
    {
        $this->organizeEvents->add($event);
    }

    /**
     * @return PersistentCollection
     */
    public function getOrganizeVenues()
    {
        return $this->organizeVenues;
    }

    /**
     * @return ArrayCollection
     */
    public function getFavoriteVenues()
    {
        return $this->favoriteVenues;
    }

    /**
     * @param ArrayCollection $favoriteVenues
     */
    public function setFavoriteVenues($favoriteVenues)
    {
        $this->favoriteVenues = $favoriteVenues;
    }

    public function addFavoriteVenue(Venue $venue)
    {
        /** @var Venue $favoriteVenue */
        foreach ($this->favoriteVenues as $favoriteVenue) {
            if ($favoriteVenue->getId() === $venue->getId()) {
                return;
            }
        }
        $this->favoriteVenues->add($venue);
    }

    public function addFavoriteEvent(Event $event)
    {
        /** @var Venue $favoriteEvent */
        foreach ($this->favoriteEvents as $favoriteEvent) {
            if ($favoriteEvent->getId() === $event->getId()) {
                return;
            }
        }
        $this->favoriteEvents->add($event);
    }

    /**
     * @return ArrayCollection
     */
    public function getFavoriteEvents()
    {
        return $this->favoriteEvents;
    }

    public function addGoingEvent(Event $event)
    {
        /** @var Venue $goingEvent */
        foreach ($this->goingEvents as $goingEvent) {
            if ($goingEvent->getId() === $event->getId()) {
                return;
            }
        }
        $this->goingEvents->add($event);
    }

    public function removeGoingEvent(Event $event)
    {
        $this->goingEvents->removeElement($event);
    }

    /**
     * @return ArrayCollection
     */
    public function getGoingEvents()
    {
        return $this->goingEvents;
    }
}
