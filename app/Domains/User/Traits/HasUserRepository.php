<?php

namespace App\Domains\User\Traits;

use App\Domains\User\Contracts\AwareUserRepositoryContarct;
use App\Domains\User\Repositories\UserRepository;

/**
 * @author hlogoen
 * @trait HasUserRepository
 *
 * Trait adds basic methods and protected member
 * for interacting with UserRepository
 */
trait HasUserRepository
{
    /**
     * Returns UserRepository instance.
     *
     * @var UserRepository
     * @return UserRepository
     */
    protected $userRepo;

    public function getUserRepository() : UserRepository
    {
        return $this->userRepo;
    }

    /**
     * Set user repository as protected member of class.
     *
     * @param UserRepository $userRepo UserRepository instance
     */
    public function setUserRepository(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }
}
