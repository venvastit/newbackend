<?php

namespace DoctrineHydrators;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;
use Doctrine\ODM\MongoDB\Hydrator\HydratorInterface;
use Doctrine\ODM\MongoDB\Query\Query;
use Doctrine\ODM\MongoDB\UnitOfWork;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadataInfo;

/**
 * THIS CLASS WAS GENERATED BY THE DOCTRINE ODM. DO NOT EDIT THIS FILE.
 */
class AppCoreEntitiesMoneyMoneyHydrator implements HydratorInterface
{
    private $dm;
    private $unitOfWork;
    private $class;

    public function __construct(DocumentManager $dm, UnitOfWork $uow, ClassMetadata $class)
    {
        $this->dm = $dm;
        $this->unitOfWork = $uow;
        $this->class = $class;
    }

    public function hydrate($document, $data, array $hints = array())
    {
        $hydratedData = array();

        /** @Field(type="float") */
        if (isset($data['amount']) || (! empty($this->class->fieldMappings['amount']['nullable']) && array_key_exists('amount', $data))) {
            $value = $data['amount'];
            if ($value !== null) {
                $return = (float) $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['amount']->setValue($document, $return);
            $hydratedData['amount'] = $return;
        }

        /** @ReferenceOne */
        if (isset($data['currency'])) {
            $reference = $data['currency'];
            if (isset($this->class->fieldMappings['currency']['storeAs']) && $this->class->fieldMappings['currency']['storeAs'] === ClassMetadataInfo::REFERENCE_STORE_AS_ID) {
                $className = $this->class->fieldMappings['currency']['targetDocument'];
                $mongoId = $reference;
            } else {
                $className = $this->unitOfWork->getClassNameForAssociation($this->class->fieldMappings['currency'], $reference);
                $mongoId = $reference['$id'];
            }
            $targetMetadata = $this->dm->getClassMetadata($className);
            $id = $targetMetadata->getPHPIdentifierValue($mongoId);
            $return = $this->dm->getReference($className, $id);
            $this->class->reflFields['currency']->setValue($document, $return);
            $hydratedData['currency'] = $return;
        }
        return $hydratedData;
    }
}