<?php

namespace DoctrineHydrators;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;
use Doctrine\ODM\MongoDB\Hydrator\HydratorInterface;
use Doctrine\ODM\MongoDB\Query\Query;
use Doctrine\ODM\MongoDB\UnitOfWork;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadataInfo;

/**
 * THIS CLASS WAS GENERATED BY THE DOCTRINE ODM. DO NOT EDIT THIS FILE.
 */
class AppDomainsUserEntitiesUserHydrator implements HydratorInterface
{
    private $dm;
    private $unitOfWork;
    private $class;

    public function __construct(DocumentManager $dm, UnitOfWork $uow, ClassMetadata $class)
    {
        $this->dm = $dm;
        $this->unitOfWork = $uow;
        $this->class = $class;
    }

    public function hydrate($document, $data, array $hints = array())
    {
        $hydratedData = array();

        /** @Field(type="id") */
        if (isset($data['_id']) || (! empty($this->class->fieldMappings['id']['nullable']) && array_key_exists('_id', $data))) {
            $value = $data['_id'];
            if ($value !== null) {
                $return = $value instanceof \MongoId ? (string) $value : $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['id']->setValue($document, $return);
            $hydratedData['id'] = $return;
        }

        /** @Field(type="boolean") */
        if (isset($data['admin']) || (! empty($this->class->fieldMappings['admin']['nullable']) && array_key_exists('admin', $data))) {
            $value = $data['admin'];
            if ($value !== null) {
                $return = (bool) $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['admin']->setValue($document, $return);
            $hydratedData['admin'] = $return;
        }

        /** @Field(type="string") */
        if (isset($data['email']) || (! empty($this->class->fieldMappings['email']['nullable']) && array_key_exists('email', $data))) {
            $value = $data['email'];
            if ($value !== null) {
                $return = (string) $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['email']->setValue($document, $return);
            $hydratedData['email'] = $return;
        }

        /** @Field(type="string") */
        if (isset($data['password']) || (! empty($this->class->fieldMappings['password']['nullable']) && array_key_exists('password', $data))) {
            $value = $data['password'];
            if ($value !== null) {
                $return = (string) $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['password']->setValue($document, $return);
            $hydratedData['password'] = $return;
        }

        /** @Field(type="boolean") */
        if (isset($data['password_created']) || (! empty($this->class->fieldMappings['passwordCreated']['nullable']) && array_key_exists('password_created', $data))) {
            $value = $data['password_created'];
            if ($value !== null) {
                $return = (bool) $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['passwordCreated']->setValue($document, $return);
            $hydratedData['passwordCreated'] = $return;
        }

        /** @Field(type="boolean") */
        if (isset($data['activated']) || (! empty($this->class->fieldMappings['activated']['nullable']) && array_key_exists('activated', $data))) {
            $value = $data['activated'];
            if ($value !== null) {
                $return = (bool) $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['activated']->setValue($document, $return);
            $hydratedData['activated'] = $return;
        }

        /** @Field(type="string") */
        if (isset($data['activation_code']) || (! empty($this->class->fieldMappings['activationCode']['nullable']) && array_key_exists('activation_code', $data))) {
            $value = $data['activation_code'];
            if ($value !== null) {
                $return = (string) $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['activationCode']->setValue($document, $return);
            $hydratedData['activationCode'] = $return;
        }

        /** @Field(type="date") */
        if (isset($data['activated_at'])) {
            $value = $data['activated_at'];
            if ($value === null) { $return = null; } else { $return = \Doctrine\ODM\MongoDB\Types\DateType::getDateTime($value); }
            $this->class->reflFields['activatedAt']->setValue($document, clone $return);
            $hydratedData['activatedAt'] = $return;
        }

        /** @Field(type="date") */
        if (isset($data['created_at'])) {
            $value = $data['created_at'];
            if ($value === null) { $return = null; } else { $return = \Doctrine\ODM\MongoDB\Types\DateType::getDateTime($value); }
            $this->class->reflFields['createdAt']->setValue($document, clone $return);
            $hydratedData['createdAt'] = $return;
        }

        /** @Field(type="date") */
        if (isset($data['updated_at'])) {
            $value = $data['updated_at'];
            if ($value === null) { $return = null; } else { $return = \Doctrine\ODM\MongoDB\Types\DateType::getDateTime($value); }
            $this->class->reflFields['updatedAt']->setValue($document, clone $return);
            $hydratedData['updatedAt'] = $return;
        }

        /** @Many */
        $mongoData = isset($data['favoriteEvents']) ? $data['favoriteEvents'] : null;
        $return = $this->dm->getConfiguration()->getPersistentCollectionFactory()->create($this->dm, $this->class->fieldMappings['favoriteEvents']);
        $return->setHints($hints);
        $return->setOwner($document, $this->class->fieldMappings['favoriteEvents']);
        $return->setInitialized(false);
        if ($mongoData) {
            $return->setMongoData($mongoData);
        }
        $this->class->reflFields['favoriteEvents']->setValue($document, $return);
        $hydratedData['favoriteEvents'] = $return;

        /** @Many */
        $mongoData = isset($data['organizeEvents']) ? $data['organizeEvents'] : null;
        $return = $this->dm->getConfiguration()->getPersistentCollectionFactory()->create($this->dm, $this->class->fieldMappings['organizeEvents']);
        $return->setHints($hints);
        $return->setOwner($document, $this->class->fieldMappings['organizeEvents']);
        $return->setInitialized(false);
        if ($mongoData) {
            $return->setMongoData($mongoData);
        }
        $this->class->reflFields['organizeEvents']->setValue($document, $return);
        $hydratedData['organizeEvents'] = $return;

        /** @Many */
        $mongoData = isset($data['goingEvents']) ? $data['goingEvents'] : null;
        $return = $this->dm->getConfiguration()->getPersistentCollectionFactory()->create($this->dm, $this->class->fieldMappings['goingEvents']);
        $return->setHints($hints);
        $return->setOwner($document, $this->class->fieldMappings['goingEvents']);
        $return->setInitialized(false);
        if ($mongoData) {
            $return->setMongoData($mongoData);
        }
        $this->class->reflFields['goingEvents']->setValue($document, $return);
        $hydratedData['goingEvents'] = $return;

        /** @Many */
        $mongoData = isset($data['organizeVenues']) ? $data['organizeVenues'] : null;
        $return = $this->dm->getConfiguration()->getPersistentCollectionFactory()->create($this->dm, $this->class->fieldMappings['organizeVenues']);
        $return->setHints($hints);
        $return->setOwner($document, $this->class->fieldMappings['organizeVenues']);
        $return->setInitialized(false);
        if ($mongoData) {
            $return->setMongoData($mongoData);
        }
        $this->class->reflFields['organizeVenues']->setValue($document, $return);
        $hydratedData['organizeVenues'] = $return;

        /** @Many */
        $mongoData = isset($data['favoriteVenues']) ? $data['favoriteVenues'] : null;
        $return = $this->dm->getConfiguration()->getPersistentCollectionFactory()->create($this->dm, $this->class->fieldMappings['favoriteVenues']);
        $return->setHints($hints);
        $return->setOwner($document, $this->class->fieldMappings['favoriteVenues']);
        $return->setInitialized(false);
        if ($mongoData) {
            $return->setMongoData($mongoData);
        }
        $this->class->reflFields['favoriteVenues']->setValue($document, $return);
        $hydratedData['favoriteVenues'] = $return;

        /** @Field(type="string") */
        if (isset($data['fb_id']) || (! empty($this->class->fieldMappings['facebookId']['nullable']) && array_key_exists('fb_id', $data))) {
            $value = $data['fb_id'];
            if ($value !== null) {
                $return = (string) $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['facebookId']->setValue($document, $return);
            $hydratedData['facebookId'] = $return;
        }

        /** @Field(type="string") */
        if (isset($data['fb_picture']) || (! empty($this->class->fieldMappings['facebookPicture']['nullable']) && array_key_exists('fb_picture', $data))) {
            $value = $data['fb_picture'];
            if ($value !== null) {
                $return = (string) $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['facebookPicture']->setValue($document, $return);
            $hydratedData['facebookPicture'] = $return;
        }

        /** @Field(type="string") */
        if (isset($data['fb_access_token']) || (! empty($this->class->fieldMappings['facebookAccessToken']['nullable']) && array_key_exists('fb_access_token', $data))) {
            $value = $data['fb_access_token'];
            if ($value !== null) {
                $return = (string) $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['facebookAccessToken']->setValue($document, $return);
            $hydratedData['facebookAccessToken'] = $return;
        }

        /** @Field(type="string") */
        if (isset($data['first_name']) || (! empty($this->class->fieldMappings['firstName']['nullable']) && array_key_exists('first_name', $data))) {
            $value = $data['first_name'];
            if ($value !== null) {
                $return = (string) $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['firstName']->setValue($document, $return);
            $hydratedData['firstName'] = $return;
        }

        /** @Field(type="string") */
        if (isset($data['last_name']) || (! empty($this->class->fieldMappings['lastName']['nullable']) && array_key_exists('last_name', $data))) {
            $value = $data['last_name'];
            if ($value !== null) {
                $return = (string) $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['lastName']->setValue($document, $return);
            $hydratedData['lastName'] = $return;
        }

        /** @Field(type="collection") */
        if (isset($data['devices']) || (! empty($this->class->fieldMappings['devices']['nullable']) && array_key_exists('devices', $data))) {
            $value = $data['devices'];
            if ($value !== null) {
                $return = $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['devices']->setValue($document, $return);
            $hydratedData['devices'] = $return;
        }
        return $hydratedData;
    }
}