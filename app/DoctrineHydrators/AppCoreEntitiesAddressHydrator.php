<?php

namespace DoctrineHydrators;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;
use Doctrine\ODM\MongoDB\Hydrator\HydratorInterface;
use Doctrine\ODM\MongoDB\Query\Query;
use Doctrine\ODM\MongoDB\UnitOfWork;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadataInfo;

/**
 * THIS CLASS WAS GENERATED BY THE DOCTRINE ODM. DO NOT EDIT THIS FILE.
 */
class AppCoreEntitiesAddressHydrator implements HydratorInterface
{
    private $dm;
    private $unitOfWork;
    private $class;

    public function __construct(DocumentManager $dm, UnitOfWork $uow, ClassMetadata $class)
    {
        $this->dm = $dm;
        $this->unitOfWork = $uow;
        $this->class = $class;
    }

    public function hydrate($document, $data, array $hints = array())
    {
        $hydratedData = array();

        /** @Field(type="string") */
        if (isset($data['house']) || (! empty($this->class->fieldMappings['house']['nullable']) && array_key_exists('house', $data))) {
            $value = $data['house'];
            if ($value !== null) {
                $return = (string) $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['house']->setValue($document, $return);
            $hydratedData['house'] = $return;
        }

        /** @Field(type="string") */
        if (isset($data['street']) || (! empty($this->class->fieldMappings['street']['nullable']) && array_key_exists('street', $data))) {
            $value = $data['street'];
            if ($value !== null) {
                $return = (string) $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['street']->setValue($document, $return);
            $hydratedData['street'] = $return;
        }

        /** @Field(type="string") */
        if (isset($data['district']) || (! empty($this->class->fieldMappings['district']['nullable']) && array_key_exists('district', $data))) {
            $value = $data['district'];
            if ($value !== null) {
                $return = (string) $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['district']->setValue($document, $return);
            $hydratedData['district'] = $return;
        }

        /** @Field(type="string") */
        if (isset($data['sub_district']) || (! empty($this->class->fieldMappings['subDistrict']['nullable']) && array_key_exists('sub_district', $data))) {
            $value = $data['sub_district'];
            if ($value !== null) {
                $return = (string) $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['subDistrict']->setValue($document, $return);
            $hydratedData['subDistrict'] = $return;
        }

        /** @Field(type="string") */
        if (isset($data['city']) || (! empty($this->class->fieldMappings['city']['nullable']) && array_key_exists('city', $data))) {
            $value = $data['city'];
            if ($value !== null) {
                $return = (string) $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['city']->setValue($document, $return);
            $hydratedData['city'] = $return;
        }

        /** @Field(type="string") */
        if (isset($data['country']) || (! empty($this->class->fieldMappings['country']['nullable']) && array_key_exists('country', $data))) {
            $value = $data['country'];
            if ($value !== null) {
                $return = (string) $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['country']->setValue($document, $return);
            $hydratedData['country'] = $return;
        }

        /** @Field(type="hash") */
        if (isset($data['coordinates']) || (! empty($this->class->fieldMappings['coordinates']['nullable']) && array_key_exists('coordinates', $data))) {
            $value = $data['coordinates'];
            if ($value !== null) {
                $return = $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['coordinates']->setValue($document, $return);
            $hydratedData['coordinates'] = $return;
        }

        /** @Field(type="string") */
        if (isset($data['distance']) || (! empty($this->class->fieldMappings['distance']['nullable']) && array_key_exists('distance', $data))) {
            $value = $data['distance'];
            if ($value !== null) {
                $return = (string) $value;
            } else {
                $return = null;
            }
            $this->class->reflFields['distance']->setValue($document, $return);
            $hydratedData['distance'] = $return;
        }
        return $hydratedData;
    }
}