<?php

namespace App\Applications\Venue\Providers;

use App\Domains\Venue\Mappers\BusinessHours;
use App\Domains\Venue\Mappers\VenueMapper;
use App\Domains\Venue\Repositories\VenueRepository;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class VenueServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Applications\Venue\Http\Controllers';

    public function boot(Router $router)
    {
        $this->registerRoutes($this->app['router']);
    }

    public function register()
    {
        $this->app->singleton('VenueRepository', function ($app) {
            return new VenueRepository($app['Doctrine\ODM\MongoDB\DocumentManager']);
        });
        $this->app->singleton('BusinessHoursRequestMapper', function ($app) {
            return new BusinessHours();
        });
    }

    protected function registerRoutes(Router $router)
    {
        $router->group(['namespace' => $this->namespace, 'prefix' => 'venue', 'middleware' => ['public_api']], function ($router) {
            require app_path('Applications/Venue/Http/routes.php');
        });
    }
}
