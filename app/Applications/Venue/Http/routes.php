<?php

/**
 * List venues.
 */
Route::get('/', [
    'uses' => 'VenueController@index',
    'as' => 'venue.list',
]);

Route::get('/favorites', [
    'uses' => 'VenueController@favorites',
    'as' => 'venue.favorites',
    'middleware' => ['protected_api'], //Only logged in users are allowed
]);

/*
 * List venues user allowed to manage
 */
Route::get('/organize', [
    'uses' => 'VenueController@organize',
    'as' => 'venue.organize',
    'middleware' => ['protected_api'], //Only logged in users are allowed
]);

/*
 * List users drafts of venues
 */
Route::get('/drafts', [
    'uses' => 'VenueController@drafts',
    'as' => 'venue.drafts',
    'middleware' => ['protected_api'], //Only logged in users are allowed
]);

/*
 * Get markers
 */
Route::get('markers', [
    'uses' => 'VenueController@markers',
    'as' => 'venue.markers',
]);

Route::get('markers/favorites', [
    'uses' => 'VenueController@favoriteMarkers',
    'as' => 'venue.markers.favorite',
    'middleware' => ['protected_api'], //Only logged in users are allowed
]);

/*
 * Type ahead search
 */
Route::get('type_ahead', [
    'uses' => 'VenueController@typeAhead',
    'as' => 'venue.type_ahead',
]);

/*
 * Get single venue by id
 */
Route::get('view/{slug}', [
    'uses' => 'VenueController@one',
    'as' => 'venue.one',
]);

/*
 * Get single venue by id serialized for usage in edit forms
 */
Route::get('editable/{slug}', [
    'uses' => 'VenueController@editable',
    'as' => 'venue.editable',
    'middleware' => ['protected_api'], //Only logged in users allowed to edit venues
]);

/*
 * Create venue
 */
Route::post('create', [
    'uses' => 'VenueController@create',
    'as' => 'venue.create',
    'middleware' => ['protected_api'], //Only logged in users allowed to create venues
]);

/*
 * Create draft
 */
Route::post('draft', [
    'uses' => 'VenueController@draft',
    'as' => 'venue.create.draft',
    'middleware' => ['protected_api'], //Only logged in users allowed to create drafts
]);

/*
 * Delete venue/draft
 */
Route::delete('delete/{id}', [
    'uses' => 'VenueController@delete',
    'as' => 'venue.create.delete',
    'middleware' => ['protected_api'], //Only logged in users allowed to delete venues/drafts
]);

Route::get('favor/{slug}', [
    'uses' => 'VenueController@favor',
    'as' => 'venue.favor',
    'middleware' => ['protected_api'], //Only logged in users allowed to add to favorites
]);

Route::get('test', [
    'uses' => 'VenueController@test',
]);
