<?php

namespace App\Applications\Venue\Http\Controllers;

use App\Domains\Venue\Entities\Venue;
use App\Applications\Venue\Http\Requests\CreateVenueRequest;
use App\Applications\Venue\Http\Requests\DraftRequest;
use App\Applications\Venue\Http\Requests\LocatableListVenuesRequest;
use App\Core\Services\SaveImageService;
use App\Domains\User\Services\UserService;
use App\Domains\Venue\Presenters\VenueMarker;
use App\Domains\Venue\Presenters\VenuePresenter;
use App\Domains\Venue\Presenters\VenueTypeAhead;
use App\Domains\Venue\Presenters\EditableVenuePresenter;
use App\Core\Http\Controllers\Controller;
use App\Domains\Venue\Services\VenueService;
use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Http\JsonResponse;
use App\Applications\Venue\Http\Requests\AbstractSaveRequest;
use App\Applications\Venue\Http\Requests\ListDraftsRequest;
use App\Applications\Venue\Http\Requests\OrganizerListRequest;
use App;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class VenueController extends Controller
{
    /**
     * @var VenueService
     */
    protected $service;

    /**
     * @var SaveImageService
     */
    protected $imageService;

    /**
     * @var UserService
     */
    protected $userService;

    public function __construct(VenueService $service, SaveImageService $imageService, UserService $userService)
    {
        $this->service = $service;
        $this->imageService = $imageService;
        $this->userService = $userService;
    }

    /**
     * Returns list of venues based on user query.
     *
     * @param LocatableListVenuesRequest $request
     * @return JsonResponse
     */
    public function index(LocatableListVenuesRequest $request)
    {
        $venues = $this->service->getList($request->all(), true);
        $response = $this->service->presentCollection($venues, VenuePresenter::class, $request->all())->toArray();

        return new JsonResponse(array_values($response));
    }

    /**
     * Get list of drafts of current user.
     *
     * @param ListDraftsRequest $request
     * @return JsonResponse
     */
    public function drafts(ListDraftsRequest $request)
    {
        $user = $this->userService->getUserRepository()->current();
        $venues = $this->service->draftsList($request->all(), $user);
        $response = $this->service->presentCollection($venues, VenuePresenter::class, $request->all())->toArray();

        return new JsonResponse(array_values($response));
    }

    /**
     * Get list of venues which user is allowed to manage.
     *
     * @param OrganizerListRequest $request
     * @return JsonResponse
     */
    public function organize(OrganizerListRequest $request)
    {
        $user = $this->userService->getUserRepository()->current();
        $venues = $this->service->organizerList($request->all(), $user);
        $response = $this->service->presentCollection($venues, VenuePresenter::class, $request->all())->toArray();

        return new JsonResponse(array_values($response));
    }

    /**
     * Returns venue markers based on query.
     *
     * @param LocatableListVenuesRequest $request
     * @return JsonResponse
     */
    public function markers(LocatableListVenuesRequest $request)
    {
        $venues = $this->service->getList($request->all(), false);
        $response = $this->service->presentCollection($venues, VenueMarker::class, $request->all())->toArray();

        return new JsonResponse(array_values($response));
    }

    /**
     * Returns objects for typeahed search.
     *
     * @return JsonResponse
     */
    public function typeAhead(Request $request)
    {
        $venues = $this->service->getList($request->all(), false);
        $response = $this->service->presentCollection($venues, VenueTypeAhead::class, $request->all())->toArray();

        return new JsonResponse(array_values($response));
    }

    /**
     * Get one venue.
     * @param string $slug
     * @return JsonResponse
     */
    public function one(string $slug)
    {
        /** @var Venue $venue */
        $venue = $this->service->repository->getBySlug($slug);
        if (!$venue) {
            return new JsonResponse([
                'errors' => [
                    'id' => 'Record not found',
                ],
                'success' => false,
            ], 404);
        }

        return new JsonResponse([
            'errors' => false,
            'success' => true,
            'venue' => $venue->present()->toJson(),
        ]);
    }

    /**
     * Delete venue by id.
     *
     * @param string $id
     * @return JsonResponse
     */
    public function delete(string $id)
    {
        $user = $this->userService->getUserRepository()->current();
        /** @var Venue $venue */
        $venue = $this->service->repository->getById($id);

        if ($this->service->isOrganizer($venue, $user)) {
            $this->service->repository->remove($venue);

            return new JsonResponse([
                'success' => true,
                'errors' => false,
            ]);
        }

        return $this->unauthorized();
    }

    /**
     * Save venue in database
     * Perform validation, trigger events, etc.
     *
     * @param CreateVenueRequest $request
     * @return JsonResponse
     */
    public function create(CreateVenueRequest $request)
    {
        return $this->save($request);
    }

    /**
     * Save draft in database.
     * @param DraftRequest $request
     * @return JsonResponse
     */
    public function draft(DraftRequest $request)
    {
        return $this->save($request);
    }

    /**
     * @param string $slug
     * @param Request $request
     * @return JsonResponse
     */
    public function favor(string $slug, Request $request)
    {
        $venue = $this->service->repository->getBySlug($slug);
        if (!$venue) {
            return new JsonResponse([
                'errors' => ['Entity not found'],
                'success' => false,
            ], 404);
        }
        $user = $this->userService->getUserRepository()->current();
        $this->userService->favoriteVenue($venue, $user);

        return new JsonResponse([
            'success' => true,
            'errors' => false,
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function favoriteMarkers(Request $request)
    {
        $user = $this->userService->getUserRepository()->current();
        $venues = new Collection($user->getFavoriteVenues()->toArray());
        $response = $this->service->presentCollection($venues, VenueMarker::class, $request->all())->toArray();

        return new JsonResponse(array_values($response));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function favorites(Request $request)
    {
        $user = $this->userService->getUserRepository()->current();
        $venues = new Collection($user->getFavoriteVenues()->toArray());
        $response = $this->service->presentCollection($venues, VenuePresenter::class, $request->all())->toArray();

        return new JsonResponse(array_values($response));
    }

    /**
     * Get model serialized for usage in frontend.
     *
     * @param string $slug
     * @return JsonResponse
     */
    public function editable(string $slug)
    {
        $user = $this->userService->getUserRepository()->current();
        /** @var Venue $venue */
        $venue = $this->service->repository->getBySlug($slug);
        if ($this->service->isOrganizer($venue, $user)) {
            $presenter = new EditableVenuePresenter($venue);

            return new JsonResponse($presenter->toArray());
        }

        return $this->unauthorized();
    }

    private function getContactMapper()
    {
        return App::make('ContactRequestMapper');
    }

    private function getDraftContactMapper()
    {
        return App::make('DraftContactRequestMapper');
    }

    private function getBusinessHoursMapper()
    {
        return App::make('BusinessHoursRequestMapper');
    }

    private function save(AbstractSaveRequest $request)
    {
        $user = $this->userService->getUserRepository()->current();
        $isDraft = $request->getDraft();
        $contact = $isDraft ? $this->getDraftContactMapper()->to($request) : $this->getContactMapper()->to($request);
        $businessHours = $this->getBusinessHoursMapper()->to($request);
        if ($request->get('id')) {
            return $this->update($request->get('id'), $request);
        }
        $venue = $this->service->create(
            $request->getName(), (string) $request->getDescription(), $request->getCategories(),
            $request->getImages(), $contact, $businessHours, $user
        );
        $response = $this->service->store($venue, $isDraft);

        return $this->saveResponse($response);
    }

    /**
     * @param $id
     * @param AbstractSaveRequest $request
     * @return JsonResponse
     */
    private function update($id, AbstractSaveRequest $request)
    {
        $isDraft = $request->getDraft();
        $categories = $request->getCategories();
        /** @var Venue $venue */
        $venue = $this->service->repository->getById($id);
        if (!$venue) {
            return new JsonResponse([
                'success' => false,
                'errors' => ['Entity not found'],
            ], 404);
        }
        $contact = $isDraft ? $this->getDraftContactMapper()->to($request) : $this->getContactMapper()->to($request);
        /** @var App\Core\Entities\Schedule\Schedule $businessHours */
        $businessHours = $this->getBusinessHoursMapper()->to($request);
        $venue->setName($request->getName());
        $venue->setDescription($request->getDescription());
        $categoryInstances = new ArrayCollection();
        foreach ($categories as $categoryId) {
            $categoryInstances->add($this->service->getCategoryRepo()->getById($categoryId));
        }
        $venue->setCategories($categoryInstances);
        $venue->setContact($contact);
        $venue->setBusinessHours($businessHours);
        $venue->setImages($this->service->makeImages($request->getImages(), 'venues'));
        $response = $this->service->store($venue, $isDraft);

        return $this->saveResponse($response);
    }

    private function unauthorized()
    {
        return new JsonResponse([
            'errors' => [
                'access' => 'You are not allowed to perform this action',
            ],
            'success' => false,
        ], 401);
    }

    /**
     * @param $response
     * @return JsonResponse
     * @throws \Caffeinated\Presenter\Exceptions\PresenterException
     */
    private function saveResponse($response)
    {
        if ($response instanceof Venue) {
            return new JsonResponse([
                'success' => true,
                'errors' => false,
                'venue' => $response->present()->toJson(),
            ]);
        } else {
            return new JsonResponse([
                'success' => false,
                'errors' => $response,
            ]);
        }
    }

    public function test()
    {
        $daniel = $this->userService->getUserRepository()->findOneBy([
            'email' => 'daniel.enqvist@me.com',
        ]);
        $matt = $this->userService->getUserRepository()->findOneBy([
            'email' => 'mjc@venvast.com'
        ]);
        $touy = $this->userService->getUserRepository()->findOneBy([
            'email' => 'touy@venvast.com'
        ]);
        $me = $this->userService->getUserRepository()->findOneBy([
            'email' => 'hlogeon1@gmail.com'
        ]);
        $igor = $this->userService->getUserRepository()->findOneBy([
            'email' => 'igorlito99@gmail.com',
        ]);
        $jimmy = $this->userService->getUserRepository()->findOneBy([
            'email' => 'jimmy@Venvast.com',
        ]);
        $anastasia = $this->userService->getUserRepository()->findOneBy([
            'email' => 'nastya.savina.nn@gmail.com',
        ]);
        $steve = $this->userService->getUserRepository()->findOneBy([
            'email' => 'steve@venvast.com',
        ]);
        $barsPubs = new ArrayCollection([$daniel, $matt, $me, $igor, $jimmy, $anastasia, $steve]);
        $repo = $this->service->addCategoryQuery(['category' => 'bars-pubs']);
        $venues = $repo->execute();
        /** @var Venue $venue */
        foreach ($venues as $venue) {
            set_time_limit(60);
            $venue->setOrganizers($barsPubs);
            $this->service->repository->save($venue);
        }
        $this->service->repository->resetQb();
        $repo = $this->service->addCategoryQuery(['category' => 'restaurants']);
        $restaurants = new ArrayCollection([$touy]);
        $venues = $repo->execute();
        foreach ($venues as $venue) {
            set_time_limit(60);
            $venue->setOrganizers($restaurants);
            $this->service->repository->save($venue);

        }
        return new JsonResponse([
            'venues' => count($venues),
        ]);
    }
}
