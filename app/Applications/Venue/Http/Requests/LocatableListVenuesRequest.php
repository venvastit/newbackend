<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/5/16
 * Time: 7:56 AM.
 */

namespace App\Applications\Venue\Http\Requests;

use App\Core\Http\Requests\BaseLocatableListRequest;

class LocatableListVenuesRequest extends BaseLocatableListRequest
{
    /**
     * @return bool
     */
    public function getIsOpen()
    {
        return $this->get('open') === 'true';
    }

    /**
     * @return bool
     */
    public function getIsClose()
    {
        return $this->get('open') === 'false';
    }
}
