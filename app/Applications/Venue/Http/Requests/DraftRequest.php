<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/5/16
 * Time: 7:42 AM.
 */

namespace App\Applications\Venue\Http\Requests;

use App\Core\Http\Requests\Request;
use App\Domains\Venue\Validation\Rules\VenueRules;

class DraftRequest extends AbstractSaveRequest
{
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:65',
        ];
    }
}
