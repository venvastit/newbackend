<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/9/16
 * Time: 7:56 AM.
 */

namespace App\Applications\Venue\Http\Requests;

use App\Core\Http\Requests\Request;

class AbstractSaveRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function getName()
    {
        return $this->get('name');
    }

    public function getDescription()
    {
        return $this->get('description');
    }

    /**
     * @return array|null
     */
    public function getAddressData()
    {
        $contact = $this->getContactData();
        if (array_key_exists('address', $contact)) {
            return $contact['address'];
        }

        return;
    }

    /**
     * @return array|null
     */
    public function getContactData()
    {
        return $this->get('contact');
    }

    /**
     * @return string|null
     */
    public function getHouse()
    {
        $address = $this->getAddressData();
        if (array_key_exists('house', $address)) {
            return $address['house'];
        }

        return;
    }

    /**
     * @return string|null
     */
    public function getStreet()
    {
        $address = $this->getAddressData();
        if (array_key_exists('street', $address)) {
            return $address['street'];
        }

        return;
    }

    /**
     * @return string|null
     */
    public function getCity()
    {
        $address = $this->getAddressData();
        if (array_key_exists('city', $address)) {
            return $address['city'];
        }

        return;
    }

    /**
     * @return string|null
     */
    public function getCountry()
    {
        $address = $this->getAddressData();
        if (array_key_exists('country', $address)) {
            return $address['country'];
        }

        return;
    }

    /**
     * @return array|null
     */
    public function getCoordinates()
    {
        $address = $this->getAddressData();
        if (array_key_exists('coordinates', $address)) {
            $coordinates = $address['coordinates'];
            if (array_key_exists('lat', $coordinates) && $coordinates['lat'] !== null) {
                return [$address['coordinates']['lat'], $address['coordinates']['lng']];
            }

            return [];
        }

        return;
    }

    /**
     * @return string|null
     */
    public function getDistrict()
    {
        $address = $this->getAddressData();
        if (array_key_exists('district', $address)) {
            return $address['district'];
        }

        return;
    }

    /**
     * @return string|null
     */
    public function getPhone()
    {
        $contact = $this->getContactData();
        if (array_key_exists('phone', $contact)) {
            return $contact['phone'];
        }

        return;
    }

    /**
     * @return string|null
     */
    public function getEmail()
    {
        $contact = $this->getContactData();
        if (array_key_exists('email', $contact)) {
            return $contact['email'];
        }

        return;
    }

    /**
     * @return string|null
     */
    public function getWebsite()
    {
        $contact = $this->getContactData();
        if (array_key_exists('website', $contact)) {
            return $contact['website'];
        }

        return;
    }

    /**
     * @return string|null
     */
    public function getAdditionalContactInfo()
    {
        $contact = $this->getContactData();
        if (array_key_exists('additional_info', $contact)) {
            return $contact['additional_info'];
        }

        return;
    }

    /**
     * @return array|null
     */
    public function getCategories()
    {
        return $this->get('categories');
    }

    /**
     * @return string|null
     */
    public function getCategory()
    {
        $categories = $this->getCategories();
        if (!empty($categories) && array_key_exists(0, $categories)) {
            return $categories[0];
        }

        return;
    }

    /**
     * @return array|null
     */
    public function getImages()
    {
        return $this->get('images');
    }

    /**
     * @return array|null
     */
    public function getBusinessHours()
    {
        return $this->get('business_hours');
    }

    /**
     * @return bool
     */
    public function getDraft()
    {
        return $this->get('draft') ?: false;
    }

    /**
     * @return bool
     */
    public function getListed()
    {
        return $this->get('listed') ?: true;
    }

    /**
     * @return string|null
     */
    public function getUserLocale()
    {
        return $this->get('locale');
    }
}
