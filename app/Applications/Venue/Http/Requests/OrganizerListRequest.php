<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/10/16
 * Time: 5:52 AM.
 */

namespace App\Applications\Venue\Http\Requests;

use App\Core\Http\Requests\Request;

class OrganizerListRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }
}
