<?php
/**
 * Copyright (c) $today.year.  Venvast Limited - All rights reserved
 *
 * Created by PhpStorm.
 * User: hlogeon <email: hlogeon1@gmail.com>
 * Date: 9/21/16
 * Time: 12:46 PM
 */

namespace App\Applications\Admin\Providers;

use App\Applications\Admin\Services\UserService;
use App\Domains\Category\Repositories\CategoryRepository;
use App\Domains\Event\Repositories\EventRepository;
use App\Domains\User\Repositories\UserRepository;
use App\Domains\Venue\Repositories\VenueRepository;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;


class AdminServiceProvider extends ServiceProvider
{

    protected $namespace = 'App\Applications\Admin\Http\Controllers';

    public function boot(Router $router)
    {
        $this->registerRoutes($this->app['router']);
    }

    public function register()
    {
    }

    protected function registerRoutes(Router $router)
    {
        $router->group(['namespace' => $this->namespace, 'prefix' => 'admin', 'middleware' => ['public_api']], function ($router) {
            require app_path('Applications/Admin/Http/routes.php');
        });
    }

}