<?php

Route::get('dashboard/pie-charts', [
    'as' => 'admin.dashboard.charts.pie',
    'uses' => 'DashboardController@pie',
    'middleware' => [], //DON'T forget to protect route
]);

Route::get('dashboard/line-charts', [
    'as' => 'admin.dashboard.charts.line',
    'uses' => 'DashboardController@line',
    'middleware' => [], //DON'T forget to protect route
]);


Route::get('venues/', [
    'as' => 'admin.venues.all',
    'uses' => 'VenuesController@listAll',
    'middleware' => [], //DON'T forget to protect route
]);