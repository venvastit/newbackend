<?php
/**
 * Copyright (c) $today.year.  Venvast Limited - All rights reserved
 *
 * Created by PhpStorm.
 * User: hlogeon <email: hlogeon1@gmail.com>
 * Date: 9/22/16
 * Time: 12:06 AM
 */

namespace App\Applications\Admin\Http\Requests\Venue;


use App\Applications\Admin\Http\Requests\BaseAdminRequest;

class VenuesListRequest extends BaseAdminRequest
{

}