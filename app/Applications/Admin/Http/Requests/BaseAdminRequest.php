<?php
/**
 * Copyright (c) $today.year.  Venvast Limited - All rights reserved
 *
 * Created by PhpStorm.
 * User: hlogeon <email: hlogeon1@gmail.com>
 * Date: 9/21/16
 * Time: 12:49 PM
 */

namespace App\Applications\Admin\Http\Requests;


use App\Core\Http\Requests\Request;

class BaseAdminRequest extends Request
{
    
    public function authorize()
    {
        return true;
    }
    
    
    public function rules()
    {
        return [];
    }
    

}