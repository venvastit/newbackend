<?php
/**
 * Copyright (c) $today.year.  Venvast Limited - All rights reserved
 *
 * Created by PhpStorm.
 * User: hlogeon <email: hlogeon1@gmail.com>
 * Date: 9/21/16
 * Time: 12:25 PM
 */

namespace App\Applications\Admin\Http\Controllers;


use App\Domains\Event\Services\EventStatsService;
use App\Domains\User\Services\UserStatsService;
use App\Domains\Venue\Services\VenueStatsService;
use Zend\Diactoros\Response\JsonResponse;

class DashboardController extends AdminController
{

    /**
     * @var UserStatsService
     */
    protected $userService;

    /**
     * @var EventStatsService
     */
    protected $eventService;

    /**
     * @var VenueStatsService
     */
    protected $venueService;

    /**
     * DashboardController constructor.
     * @param UserStatsService $userService
     * @param EventStatsService $eventService
     * @param VenueStatsService $venueService
     */
    public function __construct(UserStatsService $userService, EventStatsService $eventService, VenueStatsService $venueService)
    {
        $this->userService = $userService;
        $this->eventService = $eventService;
        $this->venueService = $venueService;
    }

    public function pie()
    {
        $charts = [
            $this->userService->getActiveInactiveChartPieChartData(),
            $this->eventService->getPublishedEventsPieChartData(),
            $this->eventService->getClaimedEventsPieChartData(),
            $this->venueService->getPublishedVenuesPieChartData(),
            $this->venueService->getClaimedVenuesPieChartData(),
        ];
       return new JsonResponse($charts);
    }

    public function line()
    {
        $charts = [
            'venue-updates' => $this->venueService->getWeekUpdatesGroupedByDay(),
            'venue-creates' => $this->venueService->getWeekCreatedGroupedByDay(),
            'event-updates' => $this->eventService->getWeekUpdatesGroupedByDay(),
            'event-creates' => $this->eventService->getWeekCreatedGroupedByDay(),
        ];
        return new JsonResponse($charts);
    }
    
    
}