<?php
/**
 * Copyright (c) $today.year.  Venvast Limited - All rights reserved
 *
 * Created by PhpStorm.
 * User: hlogeon <email: hlogeon1@gmail.com>
 * Date: 9/22/16
 * Time: 12:05 AM
 */

namespace App\Applications\Admin\Http\Controllers;


use App\Applications\Admin\Http\Requests\Venue\VenuesListRequest;
use App\Domains\Venue\Presenters\TableRowPresenter;
use App\Domains\Venue\Services\VenueService;
use Zend\Diactoros\Response\JsonResponse;

class VenuesController extends AdminController
{
    /**
     * @var VenueService
     */
    protected $venueService;
    
    public function __construct(VenueService $venueService)
    {
        $this->venueService = $venueService;
    }
    
    public function listAll(VenuesListRequest $request)
    {
        $collection = $this->venueService->getAdminList($request->all());
        $response = $this->venueService->presentCollection($collection, TableRowPresenter::class)->toArray();
        return new JsonResponse(array_values($response));
    }

}