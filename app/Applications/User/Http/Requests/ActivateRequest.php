<?php

namespace App\Applications\User\Http\Requests;

use App\Core\Http\Requests\Request;
use App\Domains\User\Repositories\UserRepository;

/**
 * Apply password.
 */
class ActivateRequest extends Request
{
    protected $userRepo;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     * If there is no user with email specified
     * return false.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = $this->getUser();
        if (env('APP_DEBUG') === true) {
            return true;
        }

        return $user && $user->getActivationCode() === $this->get('code');
    }

    /**
     * Validate incoming request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'code' => 'required',
        ];
    }

    /**
     * Get user by email.
     *
     * @return
     */
    public function getUser()
    {
        return $this->userRepo->getByEmail($this->get('email'));
    }
}
