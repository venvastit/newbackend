<?php

namespace App\Applications\User\Http\Requests;

use App\Core\Http\Requests\Request;
use App;

class HomeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }
}
