<?php

namespace App\Applications\User\Http\Requests;

use App\Core\Http\Requests\Request;
use App\Domains\User\Validation\Rules\UserRules;

class RegisterRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return UserRules::request();
    }
}
