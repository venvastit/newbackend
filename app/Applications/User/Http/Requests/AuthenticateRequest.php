<?php

namespace App\Applications\User\Http\Requests;

use App\Core\Http\Requests\Request;

class AuthenticateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'email|required_without:facebook_access_token',
            'password' => 'required_with:email|string|min:4|max:20',
            'facebook_access_token' => 'required_without:email,password',
        ];
    }
}
