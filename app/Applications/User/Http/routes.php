<?php

/**
 * Send user infi ti logged in user.
 */
Route::get('home', [
    'uses' => 'UserController@home',
    'as' => 'user.home',
    'middleware' => ['protected_api'],
]);

Route::post('authenticate', [
    'uses' => 'UserController@authenticate',
    'as' => 'user.authenticate',
    'middleware' => ['public_api'],
]);

Route::post('register', [
    'uses' => 'UserController@register',
    'as' => 'user.register',
    'middleware' => ['public_api'],
]);

Route::get('activate', [
    'uses' => 'UserController@activate',
    'as' => 'user.activate',
    'middleware' => ['public_api'],
]);

Route::get('create_password', [
    'uses' => 'UserController@createPassword',
    'as' => 'user.create_password.show',
    'middleware' => ['web'],
]);

Route::post('create_password', [
    'uses' => 'UserController@applyPassword',
    'as' => 'user.create_password.do',
    'middleware' => ['public_api'],
]);

Route::get('logout', [
    'uses' => 'UserController@logout',
    'as' => 'user.logout',
    'middleware' => ['public_api'],
]);
