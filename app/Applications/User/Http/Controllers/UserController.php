<?php

namespace App\Applications\User\Http\Controllers;

use App\Applications\User\Http\Requests\ActivateRequest;
use App\Applications\User\Http\Requests\ApplyPasswordRequest;
use App\Applications\User\Http\Requests\AuthenticateRequest;
use App\Applications\User\Http\Requests\CreatePasswordRequest;
use App\Applications\User\Http\Requests\HomeRequest;
use App\Applications\User\Http\Requests\RegisterRequest;
use App\Core\Http\Controllers\ModelController;
use App\Domains\User\Entities\User;
use App\Domains\User\Repositories\UserRepository;
use App\Domains\User\Services\AuthenticationService;
use App\Domains\User\Services\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use View;

class UserController extends ModelController
{
    /**
     * AuthenticationService.
     *
     * @var AuthenticationService
     */
    protected $authService;

    /**
     * UserService.
     * @var UserService
     */
    protected $userService;

    public function __construct(UserRepository $userRepo, AuthenticationService $authService, UserService $userService)
    {
        parent::__construct($userRepo);
        $this->authService = $authService;
        $this->userService = $userService;
    }

    /**
     * Logout user and flush session.
     *
     * @return JsonResponse
     */
    public function logout()
    {
        $this->userService->logout();

        return new JsonResponse([
                'success' => true,
                'errors' => false,
            ]);
    }

    /**
     * Home is called to get current user and send it details
     * to the client.
     *
     * @param  HomeRequest $request [description]
     * @return [type]               [description]
     */
    public function home(HomeRequest $request)
    {
        $user = $this->getRepository()->current();

        return new JsonResponse([
            'success' => true,
            'errors' => false,
            'user' => $user->present()->toJson(),
        ]);
    }

    /**
     * Register new user.
     *
     * @param  Request $request [description]
     * @return JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        $user = $this->userService->register($request->all());
        if ($user instanceof User) {
            return new JsonResponse([
                'success' => true,
                'errors' => false,
                'user' => $user->present()->toJson(),
            ]);
        } else {
            return $this->errorsResponse(new Collection(['server' => trans('auth.server')]));
        }
    }

    /**
     * Authenticate user and return JWT.
     *
     * @param AuthenticateRequest $request
     * @return [type] [description]
     */
    public function authenticate(AuthenticateRequest $request)
    {
        $auth = $this->authService->authenticate($request->all());
        if ($auth === true) {
            return new JsonResponse([
                'success' => true,
                'errors' => false,
                'token' => $this->authService->getToken(),
            ]);
        }

        return $this->errorsResponse($this->authService->getErrors());
    }

    /**
     * We should not validate user and the code passed to request here
     * Request validation is deligated to Request class itself.
     *
     * @param  CreatePasswordRequest $request
     * @return
     */
    public function createPassword(CreatePasswordRequest $request)
    {
        $user = $request->getUser();

        return View::make('user.create_password', [
            'user' => $user,
        ]);
    }

    /**
     * Activate existing user by activation code.
     * @param  ActivateRequest $request [description]
     * @return [type]                   [description]
     */
    public function activate(ActivateRequest $request)
    {
        $this->userService->activate($request->getUser());

        return View::make('user.success_activation');
    }

    /**
     * @param  ApplyPasswordRequest $request [description]
     * @return [type]                        [description]
     */
    public function applyPassword(ApplyPasswordRequest $request)
    {
        $password = $request->get('password');
        $code = $request->get('code');
        if (!$this->userService->createPassword($request->getUser(), $password, $code)) {
            return abort(500);
        }

        return View::make('user.success_create_password');
    }
}
