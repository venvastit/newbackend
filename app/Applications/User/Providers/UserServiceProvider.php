<?php

namespace App\Applications\User\Providers;

use App\Domains\User\Repositories\UserRepository;
use App\Domains\User\Services\AuthenticationService;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Validator;

class UserServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Applications\User\Http\Controllers';

    public function boot(Router $router, UserRepository $userRepository)
    {
        $this->registerRoutes($this->app['router']);
        Validator::extend('unique_email', function ($attribute, $value, $parameters, $validator) use ($userRepository) {
            return !$userRepository->getByEmail($value);
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Domains\User\Services\AuthenticationServiceAuthenticationService', function ($app) {
            new AuthenticationService($app['App\Domains\User\Repositories\UserRepository']);
        });
    }

    protected function registerRoutes(Router $router)
    {
        $router->group(['namespace' => $this->namespace, 'prefix' => 'user'], function ($router) {
            require app_path('Applications/User/Http/routes.php');
        });
    }
}
