<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/12/16
 * Time: 7:40 AM.
 */

namespace App\Applications\Event\Http\Controllers;

use App\Applications\Event\Http\Requests\AbstractSaveRequest;
use App\Applications\Event\Http\Requests\CreateDraftRequest;
use App\Applications\Event\Http\Requests\CreateEventRequest;
use App\Applications\Event\Http\Requests\ListDraftsRequest;
use App\Applications\Event\Http\Requests\LocatableListEventRequest;
use App\Applications\Event\Http\Requests\OrganizerListRequest;
use App\Domains\Event\Entities\Event;
use App\Core\Http\Controllers\Controller;
use App\Core\Services\SaveImageService;
use App\Domains\Event\Presenters\EditableEvent;
use App\Domains\Event\Presenters\EventMarker;
use App\Domains\Event\Presenters\EventPresenter;
use App\Domains\Event\Presenters\EventTypeAhead;
use App\Domains\Event\Services\EventService;
use App\Domains\User\Services\UserService;
use App\Domains\Venue\Entities\Venue;
use App\Domains\Venue\Mappers\VenueMapper;
use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Zend\Diactoros\Response\JsonResponse;
use DateTime;

class EventController extends Controller
{
    /**
     * @var EventService
     */
    private $service;

    /**
     * @var SaveImageService
     */
    private $imageService;

    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var VenueMapper
     */
    private $venueMapper;

    /**
     * EventController constructor.
     *
     * @param EventService $service
     * @param SaveImageService $imageService
     * @param UserService $userService
     * @param VenueMapper $venueMapper
     */
    public function __construct(EventService $service, SaveImageService $imageService, UserService $userService, VenueMapper $venueMapper)
    {
        $this->service = $service;
        $this->imageService = $imageService;
        $this->userService = $userService;
        $this->venueMapper = $venueMapper;
    }

    public function index(LocatableListEventRequest $request)
    {
        $events = $this->service->getList($request->all(), true);
        $response = $this->service->presentCollection($events, EventPresenter::class, $request->all())->toArray();

        return new JsonResponse(array_values($response));
    }

    public function drafts(ListDraftsRequest $request)
    {
        $user = $this->userService->getUserRepository()->current();
        $events = $this->service->draftsList($request->all(), $user);
        $response = $this->service->presentCollection($events, EventPresenter::class, $request->all())->toArray();

        return new JsonResponse(array_values($response));
    }

    public function organize(OrganizerListRequest $request)
    {
        $user = $this->userService->getUserRepository()->current();
        $events = $this->service->organizerList($request->all(), $user);
        $response = $this->service->presentCollection($events, EventPresenter::class, $request->all())->toArray();

        return new JsonResponse(array_values($response));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function favorites(Request $request)
    {
        $user = $this->userService->getUserRepository()->current();
        $events = new Collection($user->getFavoriteEvents()->toArray());
        $response = $this->service->presentCollection($events, EventPresenter::class, $request->all())->toArray();

        return new JsonResponse(array_values($response));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function listGoing(Request $request)
    {
        $user = $this->userService->getUserRepository()->current();
        $events = new Collection($user->getGoingEvents()->toArray());
        $response = $this->service->presentCollection($events, EventPresenter::class, $request->all())->toArray();

        return new JsonResponse(array_values($response));
    }

    public function markers(LocatableListEventRequest $request)
    {
        $events = $this->service->getList($request->all(), false);
        $response = $this->service->presentCollection($events, EventMarker::class, $request->all())->toArray();

        return new JsonResponse(array_values($response));
    }

    public function favoriteMarkers(LocatableListEventRequest $request)
    {
        $user = $this->userService->getUserRepository()->current();
        $events = new Collection($user->getFavoriteEvents()->toArray());
        $response = $this->service->presentCollection($events, EventMarker::class, $request->all())->toArray();

        return new JsonResponse(array_values($response));
    }

    public function goingMarkers(LocatableListEventRequest $request)
    {
        $user = $this->userService->getUserRepository()->current();
        $events = new Collection($user->getGoingEvents()->toArray());
        $response = $this->service->presentCollection($events, EventMarker::class, $request->all())->toArray();

        return new JsonResponse(array_values($response));
    }

    public function typeAhead(LocatableListEventRequest $request)
    {
        $events = $this->service->getList($request->all(), false);
        $response = $this->service->presentCollection($events, EventTypeAhead::class, $request->all())->toArray();

        return new JsonResponse(array_values($response));
    }

    public function one(string $slug, Request $request)
    {
        /** @var Event $event */
        $event = $this->service->repository->getBySlug($slug);
        if (!$event) {
            return new JsonResponse([
                'errors' => [
                    'id' => 'Record not found',
                ],
                'success' => false,
            ], 404);
        }
        $presenter = new EventPresenter($event, $request->get('timezone'));

        return new JsonResponse([
            'errors' => false,
            'success' => true,
            'event' => $presenter->toJson(),
        ]);
    }

    public function delete(string $slug, Request $request)
    {
        $user = $this->userService->getUserRepository()->current();
        if(!$user) {
            return $this->unauthorized();
        }
        $event = $this->service->repository->getBySlug($slug);
        if ($this->service->isOrganizer($event, $user)) {
            $this->service->repository->remove($event);

            return new JsonResponse([
                'success' => true,
                'errors' => false,
            ]);
        }
    }

    /**
     * @param CreateEventRequest $request
     * @return JsonResponse
     */
    public function create(CreateEventRequest $request)
    {
        return $this->save($request);
    }

    /**
     * @param CreateDraftRequest $request
     * @return JsonResponse
     */
    public function draft(CreateDraftRequest $request)
    {
        return $this->save($request);
    }

    /**
     * @param AbstractSaveRequest $request
     * @return JsonResponse
     */
    public function save(AbstractSaveRequest $request)
    {
        $user = $this->userService->getUserRepository()->current();
        $isDraft = $request->isDraft();
        $venue = $this->venueMapper->to($request);
        if (!$venue instanceof Venue) {
            return new JsonResponse([
                'success' => false,
                'errors' => $venue,
            ], 422);
        }
        if ($request->get('id')) {
            return $this->update($request->get('id'), $request);
        }
        $contact = $this->service->makeContact($request->getEmail(), $request->getPhone(), $request->getWebsite(), $venue);
        $event = $this->service->create(
            $request->getName(), $request->getDescription(), $request->getCategories(),
            $request->getImages(), $request->getEntryOptions(), $request->getStartsAt(),
            $request->getEndsAt(), $contact, $venue, $user
        );
        foreach ($venue->getOrganizers() as $organizer) {
            $event->addOrganizer($organizer);
        }
        $response = $this->service->store($event, (bool) $isDraft);

        return $this->saveResponse($response);
    }

    public function update($id, AbstractSaveRequest $request)
    {
        $isDraft = $request->isDraft();
        $categories = $request->getCategories();
        /** @var Event $event */
        $event = $this->service->repository->getById($id);
        if (!$event) {
            return new JsonResponse([
                'success' => false,
                'errors' => ['Entity not found'],
            ], 404);
        }

        $venue = $this->venueMapper->to($request);
        if(!$venue instanceof Venue) {
            return new JsonResponse([
                'success' => false,
                'errors' => [
                    'venue' => 'Venue Entity not found'
                ],
            ], 404);
        }
        $contact = clone($venue->getContact());
        $contact->setEmail($request->getEmail());
        $contact->setPhone($request->getPhone());
        $contact->setWebsite($request->getWebsite());
        $event->setName($request->getName());
        $event->setDescription($request->getDescription());
        $categoryInstances = new ArrayCollection();
        foreach ($categories as $categoryId) {
            $categoryInstances->add($this->service->getCategoryRepo()->getById($categoryId));
        }
        $event->setCategories($categoryInstances);
        $event->setContact($contact);
        $event->setVenue($venue);
        $event->setImages($this->service->makeImages($request->getImages(), 'venues'));
        $response = $this->service->store($event, $isDraft);
        return $this->saveResponse($response);
    }

    public function favor(string $slug, Request $request)
    {
        $event = $this->service->repository->getBySlug($slug);
        if (!$event) {
            return new JsonResponse([
                'errors' => ['Entity not found'],
                'success' => false,
            ], 404);
        }
        $user = $this->userService->getUserRepository()->current();
        if ($user) {
            $this->userService->favoriteEvent($event, $user);

            return new JsonResponse([
                'success' => true,
                'errors' => false,
            ]);
        }

        return $this->unauthorized();
    }

    public function going(string $slug, Request $request)
    {
        $event = $this->service->repository->getBySlug($slug);
        if (!$event) {
            return new JsonResponse([
                'errors' => ['Entity not found'],
                'success' => false,
            ], 404);
        }
        $user = $this->userService->getUserRepository()->current();
        if ($user) {
            $this->userService->goingEvent($event, $user);

            return new JsonResponse([
                'success' => true,
                'errors' => false,
            ]);
        }

        return $this->unauthorized();
    }

    public function cancelGoing(string $slug, Request $request)
    {
        $event = $this->service->repository->getBySlug($slug);
        if (!$event) {
            return new JsonResponse([
                'errors' => ['Entity not found'],
                'success' => false,
            ], 404);
        }
        $user = $this->userService->getUserRepository()->current();
        if ($user) {
            $this->userService->cancelGoingEvent($event, $user);

            return new JsonResponse([
                'success' => true,
                'errors' => false,
            ]);
        }

        return $this->unauthorized();
    }

    public function editable(string $slug, Request $request)
    {
        $user = $this->userService->getUserRepository()->current();
        $event = $this->service->repository->getBySlug($slug);
        if (!$this->service->isOrganizer($event, $user)) {
            return $this->unauthorized();
        }
        $presenter = new EditableEvent($event, $request->get('timezone'));

        return new JsonResponse([
            'success' => true,
            'errors' => false,
            'event' => $presenter->toArray(),
        ]);
    }

    /**
     * @param $response
     * @return JsonResponse
     * @throws \Caffeinated\Presenter\Exceptions\PresenterException
     */
    private function saveResponse($response)
    {
        if ($response instanceof Event) {
            return new JsonResponse([
                'success' => true,
                'errors' => false,
                'event' => $response->present()->toJson(),
            ]);
        } else {
            return new JsonResponse([
                'success' => false,
                'errors' => $response,
            ]);
        }
    }

    private function unauthorized()
    {
        return new JsonResponse([
            'errors' => [
                'access' => 'You are not allowed to perform this action',
            ],
            'success' => false,
        ], 401);
    }


    private function getContactMapper()
    {
        return \App::make('ContactRequestMapper');
    }

    private function getDraftContactMapper()
    {
        return \App::make('DraftContactRequestMapper');
    }
}
