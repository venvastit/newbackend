<?php

Route::get('/', [
    'uses' => 'EventController@index',
    'as' => 'event.list',
]);

Route::get('/favorites', [
    'uses' => 'EventController@favorites',
    'as' => 'event.favorites',
    'middleware' => ['protected_api'], //Only logged in users are allowed
]);

Route::get('/list_going', [
    'uses' => 'EventController@listGoing',
    'as' => 'event.going.list',
    'middleware' => ['protected_api'], //Only logged in users are allowed
]);

/*
 * List venues user allowed to manage
 */
Route::get('/organize', [
    'uses' => 'EventController@organize',
    'as' => 'event.organize',
    'middleware' => ['protected_api'], //Only logged in users are allowed
]);

/*
 * List users drafts of venues
 */
Route::get('/drafts', [
    'uses' => 'EventController@drafts',
    'as' => 'event.drafts',
    'middleware' => ['protected_api'], //Only logged in users are allowed
]);

/*
 * Get markers
 */
Route::get('markers', [
    'uses' => 'EventController@markers',
    'as' => 'event.markers',
]);

Route::get('markers/favorites', [
    'uses' => 'EventController@favoriteMarkers',
    'as' => 'event.markers.favorite',
    'middleware' => ['protected_api'], //Only logged in users are allowed
]);

Route::get('markers/going', [
    'uses' => 'EventController@goingMarkers',
    'as' => 'event.markers.going',
    'middleware' => ['protected_api'], //Only logged in users are allowed
]);

/*
 * Type ahead search
 */
Route::get('type_ahead', [
    'uses' => 'EventController@typeAhead',
    'as' => 'event.type_ahead',
]);

/*
 * Get single venue by id
 */
Route::get('view/{slug}', [
    'uses' => 'EventController@one',
    'as' => 'event.one',
]);

/*
 * Get single venue by id serialized for usage in edit forms
 */
Route::get('editable/{slug}', [
    'uses' => 'EventController@editable',
    'as' => 'event.editable',
    'middleware' => ['protected_api'], //Only logged in users allowed to edit venues
]);

/*
 * Create venue
 */
Route::post('create', [
    'uses' => 'EventController@create',
    'as' => 'event.create',
    'middleware' => ['protected_api'], //Only logged in users allowed to create venues
]);

/*
 * Create draft
 */
Route::post('draft', [
    'uses' => 'EventController@draft',
    'as' => 'event.create.draft',
    'middleware' => ['protected_api'], //Only logged in users allowed to create drafts
]);

/*
 * Delete venue/draft
 */
Route::delete('delete/{slug}', [
    'uses' => 'EventController@delete',
    'as' => 'event.delete',
    'middleware' => ['protected_api'], //Only logged in users allowed to delete venues/drafts
]);

/*
 * Going to event
 */
Route::get('going/{slug}', [
    'uses' => 'EventController@going',
    'as' => 'event.going',
    'middleware' => ['protected_api'], //Only logged in users allowed to delete venues/drafts
]);

Route::get('favor/{slug}', [
    'uses' => 'EventController@favor',
    'as' => 'event.favor',
    'middleware' => ['protected_api'], //Only logged in users allowed to add to favorites
]);

Route::any('test', [
    'uses' => 'EventController@test',
]);
