<?php
/**
 * Copyright (c) $today.year.  Venvast Limited - All rights reserved.
 *
 * Created by PhpStorm.
 * User: hlogeon <email: hlogeon1@gmail.com>
 * Date: 9/14/16
 * Time: 8:47 AM
 */

namespace App\Applications\Event\Http\Requests;

use App\Domains\Event\Validation\Rules\EventRules;

class CreateEventRequest extends AbstractSaveRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return EventRules::request();
    }
}
