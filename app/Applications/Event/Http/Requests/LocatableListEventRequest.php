<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/13/16
 * Time: 1:10 AM.
 */

namespace App\Applications\Event\Http\Requests;

use App\Core\Http\Requests\BaseLocatableListRequest;

class LocatableListEventRequest extends BaseLocatableListRequest
{
}
