<?php
/**
 * Copyright (c) $today.year.  Venvast Limited - All rights reserved.
 *
 * Created by PhpStorm.
 * User: hlogeon <email: hlogeon1@gmail.com>
 * Date: 9/14/16
 * Time: 10:17 PM
 */

namespace App\Applications\Event\Http\Requests;

use App\Core\Http\Requests\Request;

abstract class AbstractSaveRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function isDraft()
    {
        return (bool) $this->get('draft');
    }

    public function getPhone()
    {
        return (string) $this->get('phone');
    }

    public function getCategories()
    {
        return (array) $this->get('categories');
    }

    public function getEmail()
    {
        return (string) $this->get('email');
    }

    public function getWebsite()
    {
        return (string) $this->get('website');
    }

    public function getName()
    {
        return (string) $this->get('name');
    }

    public function getDescription()
    {
        return (string) $this->get('description');
    }

    public function getImages()
    {
        return (array) $this->get('images');
    }

    /**
     * @return \DateTime|mixed
     */
    public function getStartsAt()
    {
        $start = $this->get('starts_at');
        if ($start) {
            $tz = null;
            if ($this->get('timezone')) {
                $tz = new \DateTimeZone($this->get('timezone'));
            }
            $time = \DateTime::createFromFormat('d.m.Y H:i', $start, $tz);

            return $time;
        }

        return $this->get('starts_at');
    }

    /**
     * @return \DateTime|mixed
     */
    public function getEndsAt()
    {
        $end = $this->get('ends_at');
        if ($end) {
            $tz = null;
            if ($this->get('timezone')) {
                $tz = new \DateTimeZone($this->get('timezone'));
            }
            $time = \DateTime::createFromFormat('d.m.Y H:i', $end, $tz);

            return $time;
        }

        return $this->get('ends_at');
    }

    public function getEntryOptions()
    {
        return $this->get('entry_options');
    }

    public function getFreeEntry()
    {
        return (bool) $this->get('free_entry');
    }

    public function getUserLocale()
    {
        return (string) $this->get('locale');
    }
}
