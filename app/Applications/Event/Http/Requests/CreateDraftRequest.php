<?php
/**
 * Copyright (c) $today.year.  Venvast Limited - All rights reserved.
 *
 * Created by PhpStorm.
 * User: hlogeon <email: hlogeon1@gmail.com>
 * Date: 9/15/16
 * Time: 2:25 AM
 */

namespace App\Applications\Event\Http\Requests;

class CreateDraftRequest extends AbstractSaveRequest
{
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:65',
        ];
    }
}
