<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/13/16
 * Time: 5:12 AM.
 */

namespace App\Applications\Event\Http\Requests;

use App\Core\Http\Requests\Request;

/**
 * Class ListDraftsRequest.
 */
class ListDraftsRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }
}
