<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/12/16
 * Time: 7:37 AM.
 */

namespace App\Applications\Event\Providers;

use App\Domains\Event\Repositories\EventRepository;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Applications\Event\Http\Controllers';

    public function boot(Router $router)
    {
        $this->registerRoutes($this->app['router']);
    }

    public function register()
    {
        $this->app->singleton('EventRepository', function ($app) {
            return new EventRepository($app['Doctrine\ODM\MongoDB\DocumentManager']);
        });
    }

    protected function registerRoutes(Router $router)
    {
        $router->group(['namespace' => $this->namespace, 'prefix' => 'event', 'middleware' => ['public_api']], function ($router) {
            require app_path('Applications/Event/Http/routes.php');
        });
    }
}
