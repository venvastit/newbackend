<?php
/**
 * Copyright (c) 2016  Universal Business Network - All rights reserved
 *
 * Created by hlogeon <email: hlogeon1@gmail.com>
 * Date: 10/24/16
 * Time: 4:24 PM
 */

namespace App\Applications\Landing\Mobile\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class MobileLandingProvider extends ServiceProvider
{

    protected $namespace = 'App\Applications\Landing\Mobile\Http\Controllers';

    public function boot(Router $router)
    {
        $this->registerRoutes($this->app['router']);
    }

    public function register()
    {
        // TODO: Implement register() method.
    }

    protected function registerRoutes(Router $router)
    {
        $router->group(['namespace' => $this->namespace, 'domain' => 'mobile.' . config('app.base_url'), 'middleware' => ['public_api']], function ($router) {
            require app_path('Applications/Landing/Mobile/Http/routes.php');
        });
    }

}