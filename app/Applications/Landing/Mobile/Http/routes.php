<?php
/**
 * Copyright (c) 2016  Universal Business Network - All rights reserved
 *
 * Created by hlogeon <email: hlogeon1@gmail.com>
 * Date: 10/24/16
 * Time: 4:22 PM
 */

Route::get('/', function () {
    return view('landing.mobile.index')->with(['week' => 41, 'year' => 3202, 'venues' => 400]);
});