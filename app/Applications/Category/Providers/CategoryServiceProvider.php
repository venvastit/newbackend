<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/6/16
 * Time: 2:40 PM.
 */

namespace App\Applications\Category\Providers;

use App\Domains\Category\Entities\Category;
use App\Domains\Category\Repositories\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Validator;

class CategoryServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Applications\Category\Http\Controllers';

    public function boot(Router $router)
    {
        $this->registerRoutes($this->app['router']);

        Validator::extend('categories', function ($attribute, $value, $parameters, $validator) {
            return $this->validateCategoriesCollection($value);
        });
        Validator::extend('category', function ($attribute, $value, $parameters, $validator) {
            if (!$value instanceof Category) {
                return false;
            }

            return $this->validateCategory($value);
        });
    }

    public function register()
    {
        $this->app->singleton('CategoryRepository', function ($app) {
            return new CategoryRepository($app['Doctrine\ODM\MongoDB\DocumentManager']);
        });
    }

    protected function registerRoutes(Router $router)
    {
        $router->group(['namespace' => $this->namespace, 'prefix' => 'category', 'middleware' => ['public_api']], function ($router) {
            require app_path('Applications/Category/Http/routes.php');
        });
    }

    /**
     * Categories list should not be empty and
     * category should relate to saved category.
     *
     * @param ArrayCollection $categories
     * @return bool
     */
    private function validateCategoriesCollection($categories)
    {
        if (count($categories) > 0) {
            /** @var Category $category */
            $category = $categories->first();
            if ($category instanceof Category) {
                return $category->getId() !== null;
            }

            return false;
        }

        return false;
    }

    /**
     * TODO: category instance validation.
     *
     * @param Category $category
     * @return bool
     */
    private function validateCategory(Category $category)
    {
        return true;
    }
}
