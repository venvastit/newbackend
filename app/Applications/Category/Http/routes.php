<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/6/16
 * Time: 2:37 PM.
 */
Route::get('venues', [
    'as' => 'category.venues',
    'uses' => 'CategoryController@venues',
]);

Route::get('events', [
    'as' => 'category.events',
    'uses' => 'CategoryController@events',
]);
