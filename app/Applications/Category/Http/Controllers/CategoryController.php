<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/6/16
 * Time: 2:44 PM.
 */

namespace App\Applications\Category\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Domains\Category\Presenters\CategoryPresenter;
use App\Domains\Category\Services\CategoryService;
use Illuminate\Http\JsonResponse;

class CategoryController extends Controller
{
    public $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * List Categories with type.
     *
     * @return JsonResponse
     */
    public function venues() : JsonResponse
    {
        $categories = $this->categoryService->presentCollection($this->categoryService->venues(), CategoryPresenter::class);
        if (count($categories) === 0) {
            return new JsonResponse([
                'success' => false,
                'errors' => [
                    'server' => 'Something went wrong! Try again later',
                ],
            ], 500);
        }

        return new JsonResponse([
            'success' => true,
            'errors' => false,
            'categories' => $categories->toArray(),
        ]);
    }

    /**
     * List Categories with type.
     *
     * @return JsonResponse
     */
    public function events() : JsonResponse
    {
        $categories = $this->categoryService->presentCollection($this->categoryService->events(), CategoryPresenter::class);
        if (count($categories) === 0) {
            return new JsonResponse([
                'success' => false,
                'errors' => [
                    'server' => 'Something went wrong! Try again later',
                ],
            ], 500);
        }

        return new JsonResponse([
            'success' => true,
            'errors' => false,
            'categories' => $categories->toArray(),
        ]);
    }
}
