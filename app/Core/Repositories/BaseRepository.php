<?php

namespace App\Core\Repositories;

use App\Core\Entities\BaseEntity;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\DocumentRepository;

/**
 * Base repository class.
 */
abstract class BaseRepository
{
    /**
     * Doctrine 2 DocumentManager singleton instance.
     * @var DocumentManager
     */
    protected $dm;

    /**
     * @param DocumentManager $dm [description]
     */
    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    /**
     * Returns Doctrine 2 DocumentManager singleton.
     * @return DocumentManager
     */
    public function getDocumentManager()
    {
        return $this->dm;
    }

    /**
     * Wrapper over DocumentManager::persist().
     *
     * @param  BaseEntity $entity [description]
     * @void
     */
    public function persist($entity)
    {
        $this->getDocumentManager()->persist($entity);
    }

    /**
     * Wrapper over DocumentManager::flush().
     *
     * @void
     */
    public function flush()
    {
        $this->getDocumentManager()->flush();
    }

    /**
     * Find entity by it's ID.
     * @param  string $id unique identifier
     *
     * @return App\Core\Entities\BaseEntity
     */
    public function getById($id)
    {
        return $this->getDocumentManager()->getRepository(static::getEntityClass())->find($id);
    }

    /**
     * Find entities by fileds criteria.
     *
     * @param  array  $fields array config of fields => values
     * @return array
     */
    public function findBy(array $fields)
    {
        return $this->getDocumentManager()->getRepository(static::getEntityClass())->findBy($fields);
    }

    /**
     * Find one entity(first mathing) by fields criteria.
     *
     * @param  array  $fields array config of fields => values
     * @return App\Core\Entities\BaseEntity
     */
    public function findOneBy(array $fields)
    {
        return $this->getDocumentManager()->getRepository(static::getEntityClass())->findOneBy($fields);
    }

    public function findOne()
    {
        return $this->getDocumentManager()->getRepository(static::getEntityClass())->findOneBy([]);
    }

    public function updateIndex()
    {
        return $this->getDocumentManager()->getSchemaManager()->updateIndexes();
    }

    /**
     * Return main repository entity class name.
     *
     * @return string Entity class name including namespace
     */
    abstract public static function getEntityClass();
}
