<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/12/16
 * Time: 6:20 AM.
 */

namespace App\Core\Repositories;

use App\Core\Entities\Money\Currency;

class CurrencyRepository extends BaseRepository
{
    public static function getEntityClass()
    {
        return Currency::class;
    }
}
