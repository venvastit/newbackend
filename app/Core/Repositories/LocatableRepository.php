<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/12/16
 * Time: 2:57 PM.
 */

namespace App\Core\Repositories;

use App\Core\Entities\BaseEntity;
use App\Domains\Category\Entities\Category;
use App\Domains\User\Entities\User;
use Carbon\Carbon;
use Doctrine\ODM\MongoDB\DocumentManager;
use GeoJson\Geometry\Point;

abstract class LocatableRepository extends BaseRepository
{
    protected $queryBuilder;

    protected $requiredDistance = false;

    protected $limitIsSet = false;

    /**
     * @param DocumentManager $dm [description]
     */
    public function __construct(DocumentManager $dm)
    {
        parent::__construct($dm);
        $this->queryBuilder = $this->getDocumentManager()->createQueryBuilder(static::getEntityClass());
    }

    public function getBySlug(string $slug)
    {
        return $this->findOneBy([
            'slug' => $slug,
        ]);
    }

    public function save(BaseEntity $entity)
    {
        $this->persist($entity);
        $this->flush();
    }

    /**
     * Resets Query builder.
     */
    public function resetQb()
    {
        $this->queryBuilder = $this->getDocumentManager()->createQueryBuilder(static::getEntityClass());
    }

    /**
     * Execute query from QueryBuilder instance.
     *
     * @return mixed
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function execute()
    {
        if ($this->limitIsSet === false) {
            $this->queryBuilder->limit(config('database.default_page_limit'));
        }
        $executedQ = $this->queryBuilder->getQuery()->execute();
        $result = $this->extractDistance($executedQ);
        $this->resetQb();
        $this->limitIsSet = false;

        return $result;
    }

    /**
     * Search by coordinates and distance.
     *
     * @param float $lat
     * @param float $lng
     * @param float $distance in KM
     * @return $this
     */
    public function addNearQuery(float $lat, float $lng, float $distance = 25.0)
    {
        $point = new Point([$lng, $lat]);
        $this->queryBuilder->geoNear($point)->spherical(true)->maxDistance($distance * 1000);
        $this->requiredDistance = true;

        return $this;
    }

    /**
     * Search by name LIKE.
     *
     * @param string $name
     * @return $this
     */
    public function addNameQuery(string $name)
    {
        $this->queryBuilder->field('name')->equals(new \MongoRegex('/.*'.$name.'.*/i'));

        return $this;
    }

    /**
     * @param string $field
     * @param string $value
     * @return $this
     */
    public function addTextFieldQuery(string $field, string $value)
    {
        $this->queryBuilder->field($field)->equals(new \MongoRegex('/.*'.$value.'.*/i'));
        return $this;
    }

    /**
     * Search by published property.
     *
     * @param bool $published
     * @return $this
     */
    public function addPublishedQuery($published = true)
    {
        $this->queryBuilder->field('published')->equals($published);

        return $this;
    }

    /**
     * Search by published property.
     *
     * @param bool $listed
     * @return $this
     */
    public function addListedQuery($listed = true)
    {
        $this->queryBuilder->field('listed')->equals($listed);

        return $this;
    }

    public function remove(BaseEntity $entity)
    {
        $this->getDocumentManager()->remove($entity);
        $this->getDocumentManager()->flush();
    }

    /**
     * Search all the this user can manage.
     *
     * @param User $user
     * @return $this
     */
    public function addOrganizerQuery(User $user)
    {
        $this->getDocumentManager()->persist($user);
        $this->queryBuilder->field('organizers')->includesReferenceTo($user);

        return $this;
    }

    public function addCategoryQuery(Category $category)
    {
        $this->getDocumentManager()->persist($category);
        $this->queryBuilder->field('categories')->includesReferenceTo($category);

        return $this;
    }

    /**
     * Limit and offset.
     *
     * @param int $limit
     * @param int $offset
     * @return $this
     */
    public function addLimitOffsetQuery(int $limit, int $offset)
    {
        $this->limitIsSet = true;
        $this->queryBuilder
            ->limit($limit)
            ->skip($offset);

        return $this;
    }

    /**
     * Find by slug.
     *
     * @param string $slug
     * @return $this
     */
    public function addSlugQuery(string $slug)
    {
        $this->queryBuilder->field('slug')->equals($slug);

        return $this;
    }

    /**
     * Maps distances to an objects.
     *
     * @param $result
     * @return mixed
     */
    private function extractDistance($result)
    {
        $elements = $result->toArray();
        if ($this->requiredDistance === true) {
            $commandResult = $result->getCommandResult();
            foreach ($commandResult['results'] as $key => $value) {
                $elements[$key]->getContact()->getAddress()->setDistance($value['dis'] / 1000);
            }
        }
        $this->requiredDistance = false;

        return $elements;
    }

    /**
     * Returns an array of translations.
     *
     * @param BaseEntity $entity
     * @return mixed
     */
    public function getTranslations(BaseEntity $entity)
    {
        $trans = $this->getDocumentManager()->getRepository('\Gedmo\\Translatable\\Document\\Translation');

        return $trans->findTranslations($entity);
    }

    /**
     * Returns venue with specified translation.
     *
     * @param BaseEntity $entity
     * @param string $code
     * @return BaseEntity
     */
    public function mapTranslation(BaseEntity $entity, $code = 'en')
    {
        $translations = $this->getTranslations($entity);
        if (!array_key_exists($code, $translations)) {
            $code = 'en';
        }
        if (!array_key_exists($code, $translations)) {
            return $entity;
        }
        foreach ($translations[$code] as $field => $value) {
            if (method_exists($entity, 'set'.ucfirst($field))) {
                call_user_func_array([$entity, 'set'.ucfirst($field)], [$value]);
            }
        }
        $entity->locale = $code;

        return $entity;
    }

    public function totalCount()
    {
        return (int) $this->getDocumentManager()
            ->createQueryBuilder(static::getEntityClass())
            ->getQuery()->execute()->count();
    }

    public function getPublishedCount()
    {
        return (int) $this->getDocumentManager()
            ->createQueryBuilder(static::getEntityClass())
            ->field('published')->equals(true)
            ->getQuery()->execute()->count();
    }


    public function getDraftsCount()
    {
        return (int) $this->getDocumentManager()
            ->createQueryBuilder(static::getEntityClass())
            ->field('published')->equals(false)
            ->getQuery()->execute()->count();
    }


    public function getClaimedCount()
    {
        return (int) $this->getDocumentManager()
            ->createQueryBuilder(static::getEntityClass())
            ->where("function() { return typeof this.organizers !== 'undefined' && this.organizers.length > 0; }")
            ->getQuery()->execute()->count();
    }


    public function getUnclaimedCount()
    {
        return (int) $this->getDocumentManager()
            ->createQueryBuilder(static::getEntityClass())
            ->where("function() { return typeof this.organizers === 'undefined' ||this.organizers.length === 0; }")
            ->getQuery()->execute()->count();
    }

    public function getUpdatesGroupedByDay($numDays = 7)
    {
        return $this->getWeekFieldGroupedByDay('updated_at', $numDays);
    }

    public function getCreatesGroupedByDay($numDays = 7)
    {
        return $this->getWeekFieldGroupedByDay('created_at', $numDays);
    }

    private function getWeekFieldGroupedByDay($field, $numDays)
    {
        $todayStart = (new \DateTime())->setTime(0, 0);
        $todayEnd = (new \DateTime())->setTime(23, 59);
        $results = [];
        for ($i = 0; $i < $numDays; $i++) {
            if($i !== 0) {
                $todayStart = $todayStart->sub(new \DateInterval('P1D'));
                $todayEnd = $todayEnd->sub(new \DateInterval('P1D'));
            }
            $count = $this->getDocumentManager()->createQueryBuilder(static::getEntityClass())
                ->field($field)->gte($todayStart)->lte($todayEnd)
                ->getQuery()->count();
            $results[] = [
                'date' => $todayStart->format('Y-m-d'),
                'value' => $count
            ];
        }
        return $results;
    }


}
