<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/6/16
 * Time: 5:24 AM.
 */

namespace App\Core\Validation;

/**
 * Interface EntityValidationRulesContract.
 */
interface EntityValidationRulesContract
{
    /**
     * Describe rules for validating entity itself.
     *
     * @return array
     */
    public static function entity() : array;

    /**
     * Describe rules to validate request.
     *
     * @return array
     */
    public static function request() : array;
}
