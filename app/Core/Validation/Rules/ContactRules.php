<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/6/16
 * Time: 5:50 AM.
 */

namespace App\Core\Validation\Rules;

use App\Core\Validation\EntityValidationRulesContract;

class ContactRules implements EntityValidationRulesContract
{
    public static function entity() : array
    {
        return [
            'email' => 'required|email',
            'phone' => 'required|string|min:5|max:20',
            'address' => 'required|address',
            'additionalInfo' => 'string|min:20|max:400',
            'website' => 'url',
        ];
    }

    public static function request() : array
    {
        // TODO: Implement request() method.
    }
}
