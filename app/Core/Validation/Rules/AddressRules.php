<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/6/16
 * Time: 5:52 AM.
 */

namespace App\Core\Validation\Rules;

use App\Core\Validation\EntityValidationRulesContract;

class AddressRules implements EntityValidationRulesContract
{
    public static function entity() : array
    {
        return [
            'street' => 'required|min:3|max:400',
            'district' => 'min:3|max:400',
            'sub_district' => 'min:3|max:400',
            'coordinates' => 'required|coordinates',
        ];
    }

    public static function request() : array
    {
        // TODO: Implement request() method.
    }
}
