<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/13/16
 * Time: 1:11 AM.
 */

namespace App\Core\Http\Requests;

class BaseLocatableListRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
        ];
    }

    /**
     * @return float
     */
    public function getLat()
    {
        return (float) number_format($this->get('latitude'), 4, '.', '');
    }

    /**
     * @return float
     */
    public function getLng()
    {
        return (float) number_format($this->get('longitude'), 4, '.', '');
    }

    /**
     * @return float
     */
    public function getRadius()
    {
        return (float) $this->get('radius', 25);
    }

    /**
     * @return string|null
     */
    public function getCategory()
    {
        return $this->get('category');
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return ((int) $this->get('page')) + 1;
    }

    /**
     * @return bool
     */
    public function isWebCall()
    {
        return $this->get('web') === 'true';
    }

    /**
     * @return bool
     */
    public function isMobileCall()
    {
        return $this->get('mobile') === 'true';
    }

    /**
     * @return string|null
     */
    public function getText()
    {
        return $this->get('text');
    }

    /**
     * @return string|null
     */
    public function getOrder()
    {
        return $this->get('order');
    }
}
