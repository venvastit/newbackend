<?php

namespace App\Core\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Core\Repositories\BaseRepository;

class ModelController extends Controller
{
    protected $repository;

    public function __construct(BaseRepository $repository)
    {
        $this->setRepository($repository);
    }

    public function setRepository(BaseRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getRepository()
    {
        return $this->repository;
    }
}
