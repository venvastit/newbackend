<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/6/16
 * Time: 5:10 AM.
 */

namespace App\Core\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Support\Collection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class Error.
 *
 * @ODM\EmbeddedDocument
 */
class Error
{
    /**
     * Field name(of doctrine entity).
     *
     * @ODM\Field(type="string")
     *
     * @var string
     */
    public $field;

    /**
     * Error messages collection.
     *
     * @ODM\Field(type="collection")
     *
     * @var ArrayCollection
     */
    public $messages;

    /**
     * Error constructor.
     * @param string $field
     * @param Collection|array $messages
     */
    public function __construct(string $field, $messages)
    {
        $this->field = $field;
        if ($messages instanceof Collection) {
            $this->messages = new ArrayCollection($messages->toArray());
        } else {
            $this->messages = new ArrayCollection($messages);
        }
    }
}
