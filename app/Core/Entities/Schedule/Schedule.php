<?php

namespace App\Core\Entities\Schedule;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ODM\EmbeddedDocument
 */
class Schedule
{
    /**
     * @ODM\EmbedMany(targetDocument="App\Core\Entities\Schedule\ScheduleDay")
     *
     * @var [type]
     */
    public $days;

    public function __construct()
    {
        $this->days = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * @param ArrayCollection|array $days
     */
    public function setDays($days)
    {
        $this->days = $days;
    }

    public function addDay($day)
    {
        $this->days->add($day);
    }

    public function toArray()
    {
        $result = [];
        /** @var ScheduleDay $day */
        foreach ($this->getDays() as $day) {
            $result[$day->day] = $day->toArray();
        }

        return $result;
    }
}
