<?php

namespace App\Core\Entities\Schedule;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument
 */
class ScheduleDay
{
    /**
     * @ODM\Field(type="integer", nullable=false)
     * @var int
     */
    public $day;

    /**
     * @ODM\Field(type="integer", nullable=true)
     * @var int
     */
    public $start;

    /**
     * @ODM\Field(type="integer", nullable=true)
     * @var int
     */
    public $end;

    public function __construct(int $day, $start = false, $end = false)
    {
        $this->day = $day;
        if ($start !== false) {
            $this->start = $start;
        }
        if ($end !== false) {
            $this->end = $end;
        }
    }

    public function toArray()
    {
        return [
            'day' => $this->day,
            'start' => $this->start,
            'end' => $this->end,
        ];
    }
}
