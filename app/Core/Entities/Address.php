<?php

namespace App\Core\Entities;

use App\Core\Doctrine\AsArray;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use App\Core\Entities\Coordinates;
use GeoJson\Geometry\Point;

/**
 * @ODM\EmbeddedDocument
 *
 * @ODM\Index(keys={"coordinates"="2d"})
 */
class Address
{
    use AsArray;

    /**
     * House number(required). Max length is supposed to be 10 chars.
     *
     * @ODM\Field(type="string", nullable=false)
     *
     * @var string
     */
    public $house;

    /**
     * Street address field(required).
     *
     * @ODM\Field(type="string", nullable=false)
     *
     * @var string
     */
    public $street;

    /**
     * District field(optional).
     *
     * @ODM\Field(type="string", nullable=true)
     *
     * @var string
     */
    public $district;

    /**
     * Sub district field(optional).
     *
     * @ODM\Field(type="string", nullable=true, name="sub_district")
     *
     * @var string
     */
    public $subDistrict;

    /**
     * Just for easier location based searching in admin panel.
     *
     * @ODM\Field(type="string", nullable=false)
     */
    public $city;

    /**
     * Just for easier location based searching in admin panel.
     *
     * @ODM\Field(type="string", nullable=false)
     */
    public $country;

    /**
     * ODM\EmbedOne(targetDocument="App\Core\Entities\Coordinates", nullable=false).
     *
     * @ODM\Field(type="hash", nullable=false)
     * @var Point|null
     */
    public $coordinates;

    /**
     * @ODM\Distance
     *
     * @var float
     */
    public $distance;

    /**
     * Get house number.
     *
     * @return string
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * Set house number.
     *
     * @param string $house
     */
    public function setHouse(string $house)
    {
        $this->house = $house;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @param string $district
     */
    public function setDistrict(string $district)
    {
        $this->district = $district;
    }

    /**
     * @return string
     */
    public function getSubDistrict()
    {
        return $this->subDistrict;
    }

    /**
     * @param string $subDistrict
     */
    public function setSubDistrict(string $subDistrict)
    {
        $this->subDistrict = $subDistrict;
    }

    /**
     * @return float|null distance based on query
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * @param float $distance [description]
     */
    public function setDistance(float $distance)
    {
        $this->distance = $distance;
    }

    /**
     * Return country.
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set country.
     *
     * @param string $country [description]
     */
    public function setCountry(string $country)
    {
        $this->country = ucfirst(strtolower($country));
    }

    /**
     * Get city.
     *
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city [description]
     */
    public function setCity(string $city)
    {
        $this->city = ucfirst(strtolower($city));
    }

    /**
     * @return Point
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    public function getLatLng()
    {
        return [
            'lat' => $this->coordinates['coordinates'][1],
            'lng' => $this->coordinates['coordinates'][0],
        ];
    }

    /**
     * @param Point $coordinates
     */
    public function setCoordinates($coordinates)
    {
        $this->coordinates = $coordinates;
    }

    /**
     * @param float $lat
     * @param float $lng
     */
    public function makeCoordinates(float $lat, float $lng)
    {
        $point = new Point([$lat, $lng]);
        $this->coordinates = $point->jsonSerialize();
    }
}
