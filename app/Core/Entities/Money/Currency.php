<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/12/16
 * Time: 6:13 AM.
 */

namespace App\Core\Entities\Money;

use App\Core\Entities\BaseEntity;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document(collection="currencies")
 */
class Currency extends BaseEntity
{
    const THB = 'THB';
    const USD = 'USD';

    /**
     * Code of the currency.
     *
     * @ODM\Field(type="string", nullable=false)
     *
     * @var string
     */
    private $name;

    /**
     * Currency constructor.
     *
     * Create currency
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        if (!in_array($name, self::getAll())) {
            throw new \InvalidArgumentException(
                'Currency must be one of '.implode(', ', self::getAll()).
                $name.'given'
            );
        }
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public static function getAll()
    {
        return [
            self::USD,
            self::THB,
        ];
    }
}
