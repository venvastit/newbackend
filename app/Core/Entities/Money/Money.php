<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/12/16
 * Time: 6:12 AM.
 */

namespace App\Core\Entities\Money;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument
 */
class Money
{
    /**
     * Amount.
     *
     * @ODM\Field(type="float", nullable=false)
     *
     * @var float
     */
    private $amount;

    /**
     * Currency.
     *
     * @ODM\ReferenceOne(targetDocument="App\Core\Entities\Money\Currency", cascade="all")
     *
     * @var Currency
     */
    private $currency;

    /**
     * Money constructor.
     *
     * @param float $amount
     * @param Currency $currency
     */
    public function __construct(float $amount = 0, Currency $currency)
    {
        $this->amount = $amount;
        $this->currency = $currency;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param Currency $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }
}
