<?php

namespace App\Core\Entities;

use App\Core\Exceptions\EntityValidationException;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use ReflectionClass;
use ReflectionProperty;

/**
 * Class BaseEntity.
 *
 * @ODM\MappedSuperclass
 * @ODM\HasLifecycleCallbacks
 */
abstract class BaseEntity
{
    /**
     * @ODM\Id
     */
    public $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    public function toArray()
    {
        $reflection = new ReflectionClass($this);
        $fields = $reflection->getProperties();
        $result = [];
        foreach ($fields as $field) {
            if ($field->isPrivate() || $field->isProtected()) {
                $getter = 'get'.ucfirst($field->getName());
                if (method_exists($this, $getter)) {
                    $result[$field->getName()] = call_user_func_array([$this, $getter], []);
                }
            } else {
                $result[$field->getName()] = $field->getValue($this);
            }
        }

        return $result;
    }
}
