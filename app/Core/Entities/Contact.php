<?php

namespace App\Core\Entities;

use App\Core\Doctrine\AsArray;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use App\Core\Entities\Address;

/**
 * @ODM\EmbeddedDocument
 */
class Contact
{
    use AsArray;

    /**
     * @ODM\Field(type="string", nullable=false)
     *
     * Email(required). Should be a valid email address.
     *
     * @var string
     */
    public $email;

    /**
     * @ODM\Field(type="string", nullable=false)
     *
     * Phone number(required). Should be a valid phone number
     *
     * @var string
     */
    public $phone;

    /**
     * @ODM\Field(type="string", nullable=true)
     *
     * Website(optional). Should be a valid URL
     *
     * @var string
     */
    public $website;

    /**
     * @ODM\Field(type="string", nullable=true, name="additional_info")
     *
     * Additional information(optional, from 0 to 400 chars)
     *
     * @var string
     */
    public $additionalInfo;

    /**
     * @ODM\EmbedOne(targetDocument="App\Core\Entities\Address", nullable=false)
     *
     * @var Address
     */
    public $address;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param string $website
     */
    public function setWebsite(string $website)
    {
        $this->website = $website;
    }

    /**
     * @return string
     */
    public function getAdditionalInfo()
    {
        return $this->additionalInfo;
    }

    /**
     * @param string $additionalInfo
     */
    public function setAdditionalInfo(string $additionalInfo)
    {
        $this->additionalInfo = $additionalInfo;
    }

    /**
     * Set address to this contact.
     *
     * @param Address $address
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
    }

    public function getAddress()
    {
        return $this->address;
    }
}
