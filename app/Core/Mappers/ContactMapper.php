<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/8/16
 * Time: 1:12 PM.
 */

namespace App\Core\Mappers;

use App\Core\Entities\Contact;
use App\Core\Services\AddressService;
use App\Core\Services\ContactService;
use Illuminate\Http\Request;

class ContactMapper extends AbstractRequestMapper
{
    /**
     * @var AddressService
     */
    protected $addressService;

    /**
     * @var ContactService
     */
    protected $contactService;

    /**
     * ContactMapper constructor.
     * @param AddressService $addressService
     * @param ContactService $contactService
     */
    public function __construct(AddressService $addressService, ContactService $contactService)
    {
        $this->addressService = $addressService;
        $this->contactService = $contactService;
    }

    /**
     * Make contact instance from request.
     * @param Request $request
     * @return Contact
     */
    public function to(Request $request)
    {
        $contactData = $request->get('contact');
        $aData = $contactData['address'];
        $address = $this->addressService
            ->create((string) $aData['house'], $aData['street'], $aData['coordinates'], $aData['district'], $aData['city'], $aData['country']);
        $contact = $this->contactService->create(
            $address, $contactData['email'], $contactData['phone'],
            $contactData['website'], $contactData['additional_info']
        );

        return $contact;
    }

    /**
     * @param array $request
     * @return Contact
     */
    public function fromArray(array $request)
    {
        $contactData = $request['contact'];
        $aData = $contactData['address'];
        $address = $this->addressService
            ->create($aData['house'], $aData['street'], $aData['coordinates'], $aData['district'], $aData['city'], $aData['country']);
        $contact = $this->contactService->create(
            $address, $contactData['email'], $contactData['phone'],
            $contactData['website'], $contactData['additional_info']
        );

        return $contact;
    }
}
