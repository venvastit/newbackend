<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/9/16
 * Time: 8:34 AM.
 */

namespace App\Core\Mappers;

use App\Core\Services\AddressService;
use App\Core\Services\ContactService;
use Illuminate\Http\Request;

class DraftContactMapper extends AbstractRequestMapper
{
    /**
     * @var AddressService
     */
    protected $addressService;

    /**
     * @var ContactService
     */
    protected $contactService;

    /**
     * ContactMapper constructor.
     * @param AddressService $addressService
     * @param ContactService $contactService
     */
    public function __construct(AddressService $addressService, ContactService $contactService)
    {
        $this->addressService = $addressService;
        $this->contactService = $contactService;
    }

    public function to(Request $request)
    {
        $contactData = $request->get('contact');
        $aData = $contactData['address'];
        $aData['coordinates'] = $this->convertCoordinates($aData);
        $address = $this->addressService->draft($aData);
        $contact = $this->contactService->draft($address, $contactData);

        return $contact;
    }

    private function convertCoordinates(array $data)
    {
        $newCoordinates = [];
        if (array_key_exists('coordinates', $data)) {
            $coordinates = $data['coordinates'];
            if (array_key_exists('lat', $coordinates) && $coordinates['lat'] !== null) {
                $newCoordinates[1] = $coordinates['lat'];
            }
            if (array_key_exists('lng', $coordinates) && $coordinates['lng'] !== null) {
                $newCoordinates[0] = $coordinates['lng'];
            }
        }
        if (count($newCoordinates) === 2) {
            return $newCoordinates;
        }

        return [];
    }
}
