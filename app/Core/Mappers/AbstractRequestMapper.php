<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/8/16
 * Time: 1:13 PM.
 */

namespace App\Core\Mappers;

use Illuminate\Http\Request;

abstract class AbstractRequestMapper
{
    public function __construct()
    {
    }

    abstract public function to(Request $request);
}
