<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/8/16
 * Time: 10:14 AM.
 */

namespace App\Core\Services;

use Carbon\Carbon;

class SaveImageService
{
    public function __construct()
    {
    }

    /**
     * Save image/images provided as base64
     * returns paths to image/images.
     *
     * @param array|string $images
     * @param string $folder
     * @return array
     */
    public function saveB64($images, $folder)
    {
        $paths = [];
        if (is_array($images) && !empty($images)) {
            foreach ($images as $image) {
                if (substr($image, 0, 4) === 'http') {
                    $paths[] = $image;
                } else {
                    $paths[] = $this->saveBase64Image($image, $folder);
                }
            }

            return $paths;
        }
        if (is_string($images)) {
            return [$this->saveBase64Image($images, $folder)];
        }
    }

    public function saveBase64Image($image, $folder)
    {
        if (!is_string($image) || strlen($image) < 128) {
            return false;
        }
        $saveFolder = public_path('images/'.$folder.'/').Carbon::now()->format('dm');
        $assetFolder = 'images/'.$folder.'/'.Carbon::now()->format('dm');
        $title = str_random(16).'.png';
        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $image));
        while (file_exists($saveFolder.'/'.$title)) {
            $title = str_random(16).'.png';
        }
        if (!file_exists($saveFolder)) {
            mkdir($saveFolder);
        }
        $path = $saveFolder.'/'.$title;
        file_put_contents($path, $data);

        return $assetFolder.'/'.$title;
    }
}
