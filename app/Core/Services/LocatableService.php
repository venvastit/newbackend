<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/12/16
 * Time: 3:05 PM.
 */

namespace App\Core\Services;

use App\Core\Entities\BaseEntity;
use App\Core\Repositories\LocatableRepository;
use App\Domains\Category\Entities\Category;
use App\Domains\Category\Repositories\CategoryRepository;
use App\Domains\User\Entities\User;
use Illuminate\Support\Collection;

abstract class LocatableService
{
    const DEFAULT_RADIUS = 5.0;
    const PER_PAGE = 20;

    /**
     * @var LocatableRepository
     */
    public $repository;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepo;

    /**
     * @var SaveImageService
     */
    protected $imageService;

    /**
     * VenueService constructor.
     *
     * @param LocatableRepository $repository
     * @param CategoryRepository $categoryRepo
     * @param SaveImageService $imageService
     */
    public function __construct(LocatableRepository $repository, CategoryRepository $categoryRepo, SaveImageService $imageService)
    {
        $this->repository = $repository;
        $this->categoryRepo = $categoryRepo;
        $this->imageService = $imageService;
    }

    /**
     * Save base64-encoded images to disk and return paths.
     *
     * @param array $images
     * @param string $folder
     *
     * @return array
     */
    public function makeImages(array $images, string $folder)
    {
        if (!empty($images) && !in_array(null, $images)) {
            $imagePaths = $this->imageService->saveB64($images, $folder);

            return $imagePaths;
        }

        return [];
    }

    /**
     * Add location query.
     *
     * @param array $config
     */
    public function addLocationQuery(array $config)
    {
        $radius = array_key_exists('radius', $config) ? (float) $config['radius'] : static::DEFAULT_RADIUS;

        if (array_key_exists('latitude', $config) && array_key_exists('longitude', $config)) {
            $this->repository->addNearQuery($config['latitude'], $config['longitude'], $radius);
        }
    }

    /**
     * Add category query.
     *
     * @param array $config
     */
    public function addCategoryQuery(array $config)
    {
        if (array_key_exists('category', $config) && !empty($config['category'])) {
            /** @var Category $category */
            $category = $this->categoryRepo->findOneBy([
                'slug' => $config['category'],
                'type' => $this->getCategoryType(),
            ]);
            $this->repository->addCategoryQuery($category);
        }

        return $this->repository;
    }

    /**
     * Add text query.
     *
     * @param array $config
     */
    public function addTextQuery(array $config)
    {
        if (array_key_exists('text', $config) && !empty($config['text'])) {
            $this->repository->addNameQuery($config['text']);
        }
    }

    /**
     * Paginate.
     *
     * @param Collection $venues
     * @param array $config
     * @return Collection
     */
    public function paginate(Collection $venues, array $config)
    {
        if (array_key_exists('page', $config) && !empty($config['page'])) {
            $venues = $venues->forPage((int) $config['page'] + 1, self::PER_PAGE);
        } else {
            if (array_key_exists('web', $config) && ($config['web'] === 'true' || $config['web'] === true)) {
                $venues = $venues->forPage((int) $config['page'] + 1, self::PER_PAGE);
            } else {
                $venues = $venues->take(config('database.default_page_limit'));
            }
        }

        return $venues;
    }

    /**
     * @param Collection $entities
     * @param string $presenterClass
     * @param array $config
     * @return Collection
     */
    public function presentCollection(Collection $entities, string $presenterClass, array $config = [])
    {
        $entities = $entities->map(function ($entity, $key) use ($presenterClass, $config) {
            $presenter = new $presenterClass($entity);
            $entity = $presenter->toJson();
            if (array_key_exists('timezone', $config) && !empty($config['timezone'])) {
                if (method_exists($presenter, 'open')) {
                    $entity['open'] = $presenter->open($config['timezone']);
                }
            }

            return $entity;
        });

        return $entities;
    }

    /**
     * Is user provided in second param one of organizers of
     * the venue.
     *
     * @param BaseEntity $entity
     * @param User $user
     * @return bool
     */
    public function isOrganizer(BaseEntity $entity, User $user)
    {
        $organizers = $entity->getOrganizers();
        /** @var User $organizer */
        foreach ($organizers as $organizer) {
            if ($organizer->getId() === $user->getId()) {
                return true;
            }
        }

        return false;
    }

    public function addCityQuery(array $config)
    {
        if (array_key_exists('city', $config) && !empty($config['city'])) {
            $this->repository->addTextFieldQuery('contact.address.city', $config['city']);
        }
    }

    public function addEmailQuery(array $config)
    {
        if (array_key_exists('email', $config) && !empty($config['email'])) {
            $this->repository->addTextFieldQuery('contact.email', $config['email']);
        }
    }

    public function addPhoneQuery(array $config)
    {
        if (array_key_exists('phone', $config) && !empty($config['phone'])) {
            $this->repository->addTextFieldQuery('contact.phone', $config['phone']);
        }
    }

    /**
     * @return CategoryRepository
     */
    public function getCategoryRepo()
    {
        return $this->categoryRepo;
    }

    /**
     * Returns venue validator.
     *
     * @param BaseEntity $entity
     * @return \Illuminate\Validation\Validator
     */
    abstract protected function getValidator(BaseEntity $entity);

    /**
     * Lists entities by criteria from configuration array.
     *
     * @param array $config
     * @param bool $paginate
     *
     * @return Collection
     */
    abstract public function getList(array $config, bool $paginate);

    /**
     * Organizer list.
     *
     * @param array $config
     * @param User $user
     * @return Collection
     */
    abstract public function organizerList(array $config, User $user);

    /**
     * List drafts of entities.
     *
     * @param array $config
     * @param User $user
     * @return Collection
     */
    abstract public function draftsList(array $config, User $user);

    /**
     * @param BaseEntity $entity
     * @param bool $draft
     * @return mixed
     */
    abstract public function store(BaseEntity $entity, bool $draft = false);

    /**
     * @return int
     */
    abstract protected function getCategoryType();
}
