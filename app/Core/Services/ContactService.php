<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/5/16
 * Time: 7:02 AM.
 */

namespace App\Core\Services;

use App\Core\Entities\Address;
use App\Core\Entities\Contact;

class ContactService
{
    /**
     * Create new Contact instance.
     *
     * @param Address $address
     * @param string $email
     * @param string $phone
     * @param null $web
     * @param null $info
     *
     * @return Contact
     */
    public function create(Address $address, string $email, string $phone, $web = null, $info = null)
    {
        $contact = new Contact();
        $contact->setAddress($address);
        $contact->setEmail($email);
        $contact->setPhone($phone);
        if ($web) {
            $contact->setWebsite($web);
        }
        if ($info) {
            $contact->setAdditionalInfo($info);
        }

        return $contact;
    }

    /**
     * Create new Contact instance.
     *
     * @param Address $address
     * @param array $params
     *
     * @return Contact
     */
    public function draft(Address $address, array $params)
    {
        $contact = new Contact();
        $contact->setAddress($address);
        if (array_key_exists('email', $params) && $params['email'] !== null) {
            $contact->setEmail($params['email']);
        }
        if (array_key_exists('phone', $params) && $params['phone'] !== null) {
            $contact->setPhone($params['phone']);
        }
        if (array_key_exists('website', $params) && $params['website']) {
            $contact->setWebsite($params['website']);
        }
        if (array_key_exists('info', $params) && $params['info']) {
            $contact->setAdditionalInfo($params['info']);
        }

        return $contact;
    }
}
