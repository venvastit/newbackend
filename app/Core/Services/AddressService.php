<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/5/16
 * Time: 6:55 AM.
 */

namespace App\Core\Services;

use App\Core\Entities\Address;

class AddressService
{
    /**
     * @param string $house
     * @param string $street
     * @param array $coordinates
     * @param null $district
     * @param null $city
     * @param null $country
     *
     * @return Address
     */
    public function create(string $house, string $street, array $coordinates, $district = null, $city = null, $country = null)
    {
        if (array_key_exists('lng', $coordinates)) {
            $coordinates[0] = $coordinates['lng'];
            $coordinates[1] = $coordinates['lat'];
        }
        $address = new Address();
        $address->setHouse($house);
        $address->setStreet($street);
        $address->makeCoordinates($coordinates[0], $coordinates[1]);
        if ($district !== null) {
            $address->setDistrict($district);
        }
        if ($city !== null) {
            $address->setCity($city);
        }
        if ($country !== null) {
            $address->setCountry($country);
        }

        return $address;
    }

    /**
     * Just allows to create empty addresses for drafts.
     *
     * @param array $params
     * @return Address
     */
    public function draft(array $params)
    {
        $address = new Address();
        if (array_key_exists('house', $params) && $params['house'] !== null) {
            $address->setHouse($params['house']);
        }
        if (array_key_exists('street', $params) && $params['street'] !== null) {
            $address->setStreet($params['street']);
        }
        if (array_key_exists('coordinates', $params) && !empty($params['coordinates'])) {
            $address->makeCoordinates($params['coordinates'][0], $params['coordinates'][1]);
        }
        if (array_key_exists('district', $params) && $params['district'] !== null) {
            $address->setDistrict($params['district']);
        }
        if (array_key_exists('city', $params) && $params['city'] !== null) {
            $address->setCity($params['city']);
        }
        if (array_key_exists('country', $params) && $params['country'] !== null) {
            $address->setCountry($params['country']);
        }

        return $address;
    }
}
