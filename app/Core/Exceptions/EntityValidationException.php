<?php

namespace App\Core\Exceptions;

use Exception;

class EntityValidationException extends Exception
{
}
