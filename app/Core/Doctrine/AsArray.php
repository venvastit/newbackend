<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/6/16
 * Time: 6:07 AM.
 */

namespace App\Core\Doctrine;

use ReflectionClass;

trait AsArray
{
    public function toArray()
    {
        $reflection = new ReflectionClass($this);
        $fields = $reflection->getProperties();
        $result = [];
        foreach ($fields as $field) {
            if ($field->isPrivate() || $field->isProtected()) {
                $getter = 'get'.ucfirst($field->getName());
                if (method_exists($this, $getter)) {
                    $result[$field->getName()] = call_user_func_array([$this, $getter], []);
                }
            } else {
                $result[$field->getName()] = $field->getValue($this);
            }
        }

        return $result;
    }
}
