<?php
/**
 * Copyright (c) 2016.  Venvast Limited - All rights reserved.
 *
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/5/16
 * Time: 1:32 PM.
 */

namespace App\Core\Presenters;

class TypeAheadPresenter extends BasePresenter
{
    protected $entity;

    protected $publicFields = [
        'id', 'name', 'category', 'image', 'slug',
    ];

    /**
     * @return string
     */
    public function id()
    {
        return $this->entity->getId();
    }

    public function slug()
    {
        return $this->entity->getSlug();
    }

    /**
     * @return string
     */
    public function name()
    {
        return $this->entity->getName();
    }

    /**
     * @return string|null
     */
    public function category()
    {
        if (env('APP_DEBUG') === true) {
            return $this->entity->getCategories()->first() ? $this->entity->getCategories()->first()->getName() : 'Undefined';
        }

        return $this->entity->getCategories()->first()->getName();
    }

    public function image()
    {
        $images = $this->entity->getImages();
        $output = [];

        foreach ($images as $image) {
            if (starts_with('http', $image)) {
                $output[] = $image;
            } else {
                $output[] = \URL::asset($image);
            }
        }

        if (count($output) === 0) {
            return;
        }

        return $output[0];
    }
}
