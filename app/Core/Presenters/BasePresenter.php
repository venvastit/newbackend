<?php

namespace App\Core\Presenters;

use Caffeinated\Presenter\Presenter;

abstract class BasePresenter extends Presenter
{
    /**
     * Field names which will be availavle in public representation.
     *
     * @var array
     */
    protected $publicFields;

    public function toJson()
    {
        $json = [];
        foreach ($this->publicFields as $field) {
            $json[$field] = $this->{$field}();
        }

        return $json;
    }

    public function toArray()
    {
        $json = [];
        foreach ($this->publicFields as $field) {
            $json[$field] = $this->{$field}();
        }

        return $json;
    }
}
