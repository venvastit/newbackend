<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/5/16
 * Time: 1:32 PM.
 */

namespace App\Core\Presenters;

class MarkerPresenter extends BasePresenter
{
    protected $entity;

    protected $publicFields = [
        'id', 'title', 'category',
        'icon', 'image', 'center',
        'slug',
    ];

    /**
     * @return string
     */
    public function id()
    {
        return $this->entity->getId();
    }

    /**
     * @return string
     */
    public function title()
    {
        return $this->entity->getName();
    }

    public function slug()
    {
        return $this->entity->getSlug();
    }

    /**
     * @return string|null
     */
    public function category()
    {
        if (env('APP_DEBUG') === true) {
            return $this->entity->getCategories()->first() ? $this->entity->getCategories()->first()->getName() : 'Undefined';
        }

        return $this->entity->getCategories()->first()->getName();
    }

    public function icon()
    {
        if (env('APP_DEBUG') === true) { //TODO: remove this shit
            $category = $this->entity->getCategories()->first();
            if ($category) {
                return $category->present()->icon();
            } else {
                return 'https://api.venvast.com/images/categories/8NzFa7cguM.png';
            }
        }

        return $this->entity->getCategories()->first()->present()->icon();
    }

    public function image()
    {
        $images = $this->entity->getImages();
        $output = [];

        foreach ($images as $image) {
            if (starts_with('http', $image)) {
                $output[] = $image;
            } else {
                $output[] = \URL::asset($image);
            }
        }

        if (count($output) === 0) {
            return;
        }

        return $output[0];
    }

    /**
     * @return array
     */
    public function center()
    {
        $point = $this->entity->getContact()->getAddress()->getCoordinates();

        return [$point['coordinates'][1], $point['coordinates'][0]];
    }
}
