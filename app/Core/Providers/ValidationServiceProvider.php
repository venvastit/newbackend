<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/6/16
 * Time: 8:56 AM.
 */

namespace App\Core\Providers;

use App\Core\Entities\Address;
use App\Core\Entities\Contact;
use App\Core\Entities\Schedule\Schedule;
use App\Core\Validation\Rules\AddressRules;
use App\Core\Validation\Rules\ContactRules;
use App\Domains\Category\Entities\Category;
use App\Domains\Venue\Entities\Venue;
use Doctrine\Common\Collections\ArrayCollection;
use GeoJson\Geometry\Point;
use Illuminate\Support\ServiceProvider;
use Validator;

class ValidationServiceProvider extends ServiceProvider
{
    public function register()
    {
        // TODO: Implement register() method.
    }

    public function boot()
    {
        Validator::extend('address', function ($attribute, $value, $parameters, $validator) {
            if (!$value instanceof Address) {
                return false;
            }

            return $this->validateAddress($value);
        });

        Validator::extend('contact', function ($attribute, $value, $parameters, $validator) {
            if (!$value instanceof Contact) {
                return false;
            }

            return $this->validateContact($value);
        });

        Validator::extend('schedule_week', function ($attribute, $value, $parameters, $validator) {
            if (!$value instanceof Schedule) {
                return false;
            }

            return $this->validateScheduleWeek($value);
        });

        Validator::extend('coordinates', function ($attribute, $value, $parameters, $validator) {
            if (!array_key_exists('coordinates', $value)) {
                return false;
            }
            try {
                new Point($value['coordinates']);

                return true;
            } catch (\Exception $e) {
                return false;
            }
        });

        Validator::extend('categories', function ($attribute, $value, $parameters, $validator) {
            return $this->validateCategoriesCollection($value);
        });

        Validator::extend('category', function ($attribute, $value, $parameters, $validator) {
            if (!$value instanceof Category) {
                return false;
            }

            return $this->validateCategory($value);
        });

        Validator::extend('has_venue', function ($attribute, $value, $parameters, $validator) {
            if (!$value instanceof Venue) {
                return false;
            }
            if (env('APP_DEBUG') === false) {
                return $value->isPublished();
            } else {
                return true;
            }
        });
    }

    /**
     * Address entity validation.
     *
     * @param Address $address
     * @return bool
     */
    private function validateAddress(Address $address)
    {
        /** @var \Illuminate\Contracts\Validation\Validator $validator */
        $validator = Validator::make($address->toArray(), AddressRules::entity());

        return !$validator->fails();
    }

    /**
     * Contact validation.
     *
     * @param Contact $contact
     * @return bool
     */
    private function validateContact(Contact $contact)
    {
        /** @var \Illuminate\Contracts\Validation\Validator $validator */
        $validator = Validator::make($contact->toArray(), ContactRules::entity());

        return !$validator->fails();
    }

    /**
     * Make validator for week scheduling.
     *
     * @param Schedule $schedule
     * @return bool
     */
    private function validateScheduleWeek(Schedule $schedule)
    {
        if (count($schedule->getDays()) !== 7) {
            return false;
        }
        $openDays = 0;
        foreach ($schedule->getDays() as $day) {
            if ($day->start !== null && $day->start !== false) {
                $openDays++;
            }
        }

        return $openDays > 1;
    }

    /**
     * Categories list should not be empty and
     * category should relate to saved category.
     *
     * @param ArrayCollection $categories
     * @return bool
     */
    private function validateCategoriesCollection($categories)
    {
        if (count($categories) > 0) {
            /** @var Category $category */
            $category = $categories->first();
            if ($category instanceof Category) {
                return $category->getId() !== null;
            }

            return false;
        }

        return false;
    }

    /**
     * TODO: category instance validation.
     *
     * @param Category $category
     * @return bool
     */
    private function validateCategory(Category $category)
    {
        return true;
    }
}
