<?php

namespace App\Core\Providers;

use App\Core\Entities\Address;
use App\Core\Entities\Contact;
use App\Core\Entities\Schedule\Schedule;
use App\Core\Entities\Schedule\ScheduleDay;
use App\Core\Mappers\ContactMapper;
use App\Core\Mappers\DraftContactMapper;
use App\Core\Services\AddressService;
use App\Core\Services\ContactService;
use App\Core\Validation\Rules\AddressRules;
use App\Core\Validation\Rules\ContactRules;
use Doctrine\ODM\MongoDB\Types\Type;
use GeoJson\Geometry\Point;
use Illuminate\Support\ServiceProvider;
use Doctrine\MongoDB\Connection;
use Doctrine\ODM\MongoDB\Configuration;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\EventManager;
use Gedmo\Tree\TreeListener;
use Gedmo\Timestampable\TimestampableListener;
use Gedmo\Sluggable\SluggableListener;
use Gedmo\Loggable\LoggableListener;
use Gedmo\Sortable\SortableListener;
use Gedmo\Translatable\TranslatableListener;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    private $subscribers = [
        TreeListener::class,
        TimestampableListener::class,
        SluggableListener::class,
        LoggableListener::class,
        SortableListener::class,
        TranslatableListener::class,
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerDoctrine();

        $this->app->singleton('ContactRequestMapper', function ($app) {
            return new ContactMapper(new AddressService(), new ContactService());
        });
        $this->app->singleton('DraftContactRequestMapper', function ($app) {
            return new DraftContactMapper(new AddressService(), new ContactService());
        });
    }

    /**
     * @throws \Doctrine\ODM\MongoDB\Mapping\MappingException
     */
    public function registerDoctrine()
    {
        $connection = new Connection();
        $config = new Configuration();
        $config->setProxyDir(app_path('DoctrineProxies'));
        $config->setProxyNamespace('DoctrineProxies');
        $config->setHydratorDir(app_path('DoctrineHydrators'));
        $config->setHydratorNamespace('DoctrineHydrators');
        $config->setDefaultDB(config('database.connections.mongodb.database'));
        $config->setMetadataDriverImpl(AnnotationDriver::create(config('doctrine.managers.default.paths')));
        AnnotationDriver::registerAnnotationClasses();
        $evm = $this->getDoctrineEventManager();
        $this->app->bind('Doctrine\ODM\MongoDB\DocumentManager', function ($app) use ($connection, $config, $evm) {
            return DocumentManager::create($connection, $config, $evm);
        });
        Type::overrideType('collection', 'App\Core\Doctrine\Types\Collection');
    }

    /**
     * Configure doctrine event manager and return an instance.
     * @return EventManager [description]
     */
    private function getDoctrineEventManager() : EventManager
    {
        $evm = new EventManager();
        foreach ($this->subscribers as $subscriber) {
            $evm->addEventSubscriber(new $subscriber);
        }

        return $evm;
    }
}
