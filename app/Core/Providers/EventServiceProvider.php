<?php

namespace App\Core\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Domains\User\Events\Login;
use App\Domains\User\Events\Registration\AccountActivated;
use App\Domains\User\Events\Registration\CredentialsRegistered;
use App\Domains\User\Events\Registration\OnlyEmailRegistered;
use App\Domains\User\Listeners\Email\ActivationLinkEmail;
use App\Domains\User\Listeners\Email\CompleteRegistrationEmail;
use App\Domains\User\Listeners\Email\SetUpPasswordEmail;
use App\Domains\User\Listeners\Stats\CompleteRegistrationStats;
use App\Domains\User\Listeners\Stats\LoginStats;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        CredentialsRegistered::class => [
            ActivationLinkEmail::class,
        ],
        OnlyEmailRegistered::class => [
            SetUpPasswordEmail::class,
        ],
        AccountActivated::class => [
            CompleteRegistrationStats::class,
            CompleteRegistrationEmail::class,
        ],
        Login::class => [
            LoginStats::class,
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
