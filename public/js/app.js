(function(){
    "use strict";
    var app = angular.module('app',
        [
            'app.controllers',
            'app.filters',
            'app.services',
            'app.directives',
            'app.routes',
            'app.config',
            'facebook',
            'geolocation',
            'djds4rce.angular-socialshare',
            'uiGmapgoogle-maps',
            'jkuri.datepicker',
            'ui.date',
            'ui.timepicker',
            'ui.utils.masks',
            'ngStorage',
            'satellizer'
        ], ["$interpolateProvider", "$locationProvider", "FacebookProvider", "$httpProvider", "$authProvider", function($interpolateProvider, $locationProvider, FacebookProvider, $httpProvider, $authProvider){
            $interpolateProvider.startSymbol('{%');
            $interpolateProvider.endSymbol('%}');
            FacebookProvider.init($('#fb_app_id').text());//localhost: 1631594013727920 //1621403594746962
            $httpProvider.interceptors.push(['$q', '$location', '$localStorage', function ($q, $location, $localStorage) {
                return {
                    'request': function (config) {
                        config.headers = config.headers || {};
                        config.params = config.params || {};
                        if ($localStorage.token) {
                            // config.headers.Authorization = 'Bearer ' + $localStorage.token;
                            config.params.token = $localStorage.token;
                        }
                        return config;
                    }
                };
            }]);

        }]);

    angular.module('app.routes', ['ui.router']);
    angular.module('app.controllers', ['ui.router', 'restangular', 'ui.bootstrap', 'facebook', 'uiGmapgoogle-maps', 'geolocation', 'google.places', 'ngFileUpload', 'ngImgCrop']);
    angular.module('app.filters', []);
    angular.module('app.services', ['restangular', 'ui.router', 'facebook', 'geolocation']);
    angular.module('app.directives', []);
    angular.module('app.config', ['ui.bootstrap']);
})();
(function(){
    "use strict";

    angular.module('app.routes').config( ["$stateProvider", "$urlRouterProvider", "$locationProvider", function($stateProvider, $urlRouterProvider, $locationProvider ) {

        var getView = function( viewName ){
            return '/views/app/' + viewName + '/' + viewName + '.html';
        };
        
        var getOrgView = function(viewName) {
            return '/views/app/org/' + viewName + '/' + viewName + '.html';
        };
        $locationProvider.html5Mode(true).hashPrefix('!');
        $urlRouterProvider.otherwise('/!/events');
        $stateProvider
            .state('home', {
                url: '/!',
                views: {
                    main: {
                        templateUrl: getView('home')
                    }
                },
                abstract: true
            })
            .state('google_token', {
                template: '',
                //https://venvast.com/#access_token=ya29.Ci8bAyk4PdagtFGaaOymcxDmCo-u91AvQGf1FjFIPLT34Bpj4QPkAI8un3KFHRenlg&token_type=Bearer&expires_in=3600
                url: '/#access_token=:accessToken',
                controller: ["$location", "$rootScope", function ($location, $rootScope) {
                    var hash = $location.path().substr(1);
                    var splitted = hash.split('&');
                    var params = {};
                    console.log("TOKEEEN!!");
                    // for (var i = 0; i < splitted.length; i++) {
                    //     var param  = splitted[i].split('=');
                    //     var key    = param[0];
                    //     var value  = param[1];
                    //     params[key] = value;
                    //     $rootScope.accesstoken=params;
                    // }
                    // $location.path("/about");
                }]
            })
            .state('home.events', {
                url: '/events',
                views: {
                    main: {
                        templateUrl: getView('home')
                    },
                    catalogue: {
                        templateUrl: getView('events')
                    },
                    r_sidebar: {
                        templateUrl: getView('sidebar')
                    },
                    filters: {
                        templateUrl: getView('event_filter')
                    }
                }
            })
            .state('home.venues', {
                url: '/venues',
                views: {
                    main: {
                        templateUrl: getView('home')
                    },
                    catalogue: {
                        templateUrl: getView('venues')
                    },
                    r_sidebar: {
                        templateUrl: getView('sidebar')
                    },
                    filters: {
                        templateUrl: getView('venue_filter')
                    }
                }
            })
            .state('home.deals', {
                url: '/deals',
                views: {
                    main: {
                        templateUrl: getView('home')
                    },
                    catalogue: {
                        templateUrl: getView('catalogue')
                    },
                    r_sidebar: {
                        templateUrl: getView('sidebar')
                    }
                }
            })
            .state('organizer', {
                url: '/!',
                views: {
                    main: {
                        templateUrl: getOrgView('home')
                    }
                },
                abstract: true
            })
            .state('organizer.events', {
                url: '/organize',
                views: {
                    main: {
                        templateUrl: getOrgView('home')
                    },
                    catalogue: {
                        templateUrl: getOrgView('events')
                    },
                    r_sidebar: {
                        templateUrl: getOrgView('sidebar')
                    },
                    filters: {
                        templateUrl: getView('event_filter')
                    }
                }
            })
            .state('organizer.create_event', {
                url: '/create_event',
                views: {
                    main: {
                        templateUrl: getOrgView('home')
                    },
                    catalogue: {
                        templateUrl: getOrgView('create_event')
                    }
                }
            })
            .state('organizer.venues', {
                url: '/organize_venues',
                views: {
                    main: {
                        templateUrl: getOrgView('home')
                    },
                    catalogue: {
                        templateUrl: getOrgView('venues')
                    },
                    r_sidebar: {
                        templateUrl: getOrgView('sidebar')
                    },
                    filters: {
                        templateUrl: getView('venue_filter')
                    }
                }
            })
            .state('organizer.hosts', {
                url: '/hosts',
                views: {
                    main: {
                        templateUrl: getOrgView('home')
                    },
                    catalogue: {
                        templateUrl: getOrgView('hosts')
                    },
                    r_sidebar: {
                        templateUrl: getOrgView('sidebar')
                    }
                    // filters: {
                    //     templateUrl: getView('venue_filter')
                    // }
                }
            })
            .state('organizer.create_venue', {
                url: '/create_venue',
                views: {
                    main: {
                        templateUrl: getOrgView('home')
                    },
                    catalogue: {
                        templateUrl: getOrgView('create_venue')
                    }
                }
            })
            .state('organizer.deals', {
                url: '/organize_deals',
                views: {
                    main: {
                        templateUrl: getOrgView('home')
                    },
                    catalogue: {
                        templateUrl: getOrgView('deals')
                    },
                    r_sidebar: {
                        templateUrl: getOrgView('sidebar')
                    },
                    filters: {
                        templateUrl: getView('deal_filter')
                    }
                }
            });
    }] );
})();
(function(){
    "use strict";


    angular.module('app.services').factory('CategoriesStorage', ["$http", "Category", function($http, Category){

        /**
         *
         * @param {Array} categoriesArray
         * @constructor
         */
        function CategoriesStorage(categoriesArray) {
            if(categoriesArray && categoriesArray.length > 0){
                for (var i = 0; i < categoriesArray.length; i++) {
                    if ( !(categoriesArray[i] instanceof Category) ) {
                        categoriesArray[i] = new Category(categoriesArray[i]);
                    }
                }
                this.loadArray(categoriesArray);
            }
        };

        CategoriesStorage.prototype = {
            TYPE_EVENT: "event",
            TYPE_VENUE: "venue",
            TYPE_DEAL: "deal",
            data: [],
            loading: false,
            
            loadArray: function(categoriesArray) {
                this.data = categoriesArray;
            },
            /**
             *
             * @param {object} requestObject
             */
            load: function(requestObject) {
                var scope = this;
                if(this.loading === false) {
                    this.loading = true;
                    return $http.get('category', {params: requestObject}).success(function(categoryData) {
                        for(var i = 0; i < categoryData.length; i++) {
                            categoryData[i] = new Category(categoryData[i]);
                        }
                        scope.loadArray(categoryData);
                        scope.loading = false;
                        return scope;
                    });
                }
            },
            getImageUrl: function(width, height) {
                return 'our/image/service/' + this.category.id + '/width/height';
            }
        };

        return CategoriesStorage;

    }])})();
(function(){
    "use strict";


angular.module('app.services').factory('Category', ["$state", "$http", function($state, $http){

    function Category(categoryData) {
        if (categoryData) {
            this.setData(categoryData);
        }
        //что-то, что еще нужно для инициализации книги
    };

    Category.prototype = {
        setData: function(categoryData) {
            angular.extend(this, categoryData);
        },

        /**
         * 
         * @param categoryId
         * @returns {Promise}
         */
        load: function(categoryId) {
            var scope = this;
            return $http.get('category/' + categoryId).success(function(categoryData) {
                scope.setData(categoryData);
                return scope;
            });
        },
        
        getImageUrl: function() {
            return 'https://venvast.com/img/cache/original/categories/' + this.image
        }
    };
    return Category;

}])})();
(function() {
    "use strict";

    angular.module('app.services').factory('ChangesNotifier', ["$rootScope", function($rootScope) {
        return {
            
            SUBJ_EVENTS_LOADED: 'EventsLoaded',
            SUBJ_VENUES_LOADED: 'VenuesLoaded',
            SUBJ_DEALS_LOADED: 'DealsLoaded',
            
            SUBJ_EVENTS_RELOADED: 'EventsReloaded',
            SUBJ_VENUES_RELOADED: 'VenuesReloaded',
            SUBJ_DEALS_RELOADED: 'DealsReloaded',
            
            SUBJ_EVENTS_ADDED: 'EventsAdded',
            SUBJ_VENUES_ADDED: 'VenuesAdded',
            SUBJ_DEALS_ADDED: 'DealsAdded',


            SUBJ_ORG_EVENTS_LOADED: 'EventsLoaded',
            SUBJ_ORG_VENUES_LOADED: 'VenuesLoaded',
            SUBJ_ORG_DEALS_LOADED: 'DealsLoaded',

            SUBJ_ORG_EVENTS_RELOADED: 'EventsReloaded',
            SUBJ_ORG_VENUES_RELOADED: 'VenuesReloaded',
            SUBJ_ORG_DEALS_RELOADED: 'DealsReloaded',

            SUBJ_ORG_EVENTS_ADDED: 'EventsAdded',
            SUBJ_ORG_VENUES_ADDED: 'VenuesAdded',
            SUBJ_ORG_DEALS_ADDED: 'DealsAdded',
            
            value: null,
            org_value: null,
            org_venues: null,
            venues_value: null,
            
            subscribe: function(subject, scope, callback) {
                var handler = $rootScope.$on(subject, callback);
                scope.$on('$destroy', handler);
            },

            notify: function(subject, value) {
                $rootScope.$emit(subject);
                if(value)
                    this.value = value;
            }
        };
    }]);
    
})();
(function(){
    "use strict";
    angular.module('app.services').factory('DateService', function(){
        return {
            diff: {
                inHours: function (d1, d2) {
                    var t2 = d2.getTime();
                    var t1 = d1.getTime();
                    return parseInt((t2-t1)/(3600*1000));
                },
                inDays: function(d1, d2) {
                    var t2 = d2.getTime();
                    var t1 = d1.getTime();

                    return parseInt((t2-t1)/(24*3600*1000));
                },
                inWeeks: function(d1, d2) {
                    var t2 = d2.getTime();
                    var t1 = d1.getTime();

                    return parseInt((t2-t1)/(24*3600*1000*7));
                },
                inMonths: function(d1, d2) {
                    var d1Y = d1.getFullYear();
                    var d2Y = d2.getFullYear();
                    var d1M = d1.getMonth();
                    var d2M = d2.getMonth();
                    return (d2M+12*d2Y)-(d1M+12*d1Y);
                },
                inYears: function(d1, d2) {
                    return d2.getFullYear()-d1.getFullYear();
                },
                getDiff: function(d1, d2) {
                    if(this.inHours(d1, d2) > 48){
                        if(this.inDays > 14){
                            return "Starts in " + this.inWeeks(d1, d2) + " weeks";
                        } else {
                            return "Starts in " + this.inDays(d1, d2) + " days";
                        }
                    } else {
                        return "Starts in " + this.inHours(d1, d2) + " hours";
                    }
                }
            }
        };
    });

})();
(function(){
    "use strict";


    angular.module('app.services').factory('Event', ["Upload", "$http", function(Upload, $http) {


        function Event(eventData) {
            if(eventData) {
                this.loadData(eventData);
            }
        };

        Event.prototype = {

            _id: null,
            title: null,
            details: null,
            address: null,
            start_at: null,
            end_at: null,
            location: null,
            categories: [],
            venue: null,
            picture: null,
            className: 'Event',

            attendees: [],
            organizers: [],
            likes: 0,

            loadData: function (data) {
                angular.extend(this, data);
                if(typeof this.venue_relation !== 'undefined' && this.veue_relation !== null) {
                    this.venue = this.venue_relation;
                }
                this.icon = this.getIcon();
                return this;
            },
            getCoordinates: function () {
                if(typeof this.location === "undefined" || this.location === null) {
                    this.location = this.venue.location;
                    this.updateOnBackend();
                }
                return {
                    longitude: this.location.coordinates[0],
                    latitude: this.location.coordinates[1]
                }
            },

            getIcon: function () {
                if(this.categories.length === 0 || !this.categories || typeof this.categories === 'undefined') {
                    return '/img/question.png';
                } else {
                    if(typeof this.categories[0] !== 'undefined' &&  this.categories[0] !== null && this.categories[0].image) {
                        return "/img/cache/original/categories/" + this.categories[0].image;
                    } else {
                        return '/img/question.png';
                    }
                }
            },

            getPicture: function() {
                if(this.picture === null || typeof this.picture === 'undefined' || this.picture === ''){
                    return 'https://placeholdit.imgix.net/~text?txtsize=33&txt=' + this.title + '&w=350&h=150';
                }
                if(typeof this.picture !== 'string'){
                    return this.picture
                }
                if(!this.picture.startsWith('http')) {
                    return '/img/cache/original/events/' + this.picture;
                } else {
                    return this.picture;
                }
            },

            getMarker: function() {
                var scope = this;
                var coordinates = scope.getCoordinates();
                return new google.maps.Marker({
                    icon: new google.maps.MarkerImage(scope.getIcon(), null, null, null, new google.maps.Size(32, 32)),
                    position: {lat: coordinates.latitude, lng: coordinates.longitude},
                    id: 'event-' + scope._id
                });
            },

            saveOnBackend: function () {
                if(this.picture !== null) {
                    return Upload.upload({url: 'event/create', data: this})
                        .then(function (resp) {
                            return resp;
                        });
                }
                return $http.post('event/create', this).then(function(response) {
                    return response;
                });
            },

            updateOnBackend: function() {
                if(typeof this.picture === 'object') {
                    return Upload.upload({url: 'event/update/'+this._id, data: this})
                        .then(function (resp) {
                            return resp;
                        });
                } else {
                    return $http.post('event/update/'+this._id, this).then(function(response) {
                        return response;
                    });
                }
            },

            /**
             *
             * @returns {moment.Moment|*}
             */
            getStartAt: function() {
                return moment(this.start_at);
            },

            /**
             *
             * @returns {moment.Moment|*}
             */
            getEndAt: function() {
                if(this.end_at === null)
                    return null;
                return moment(this.end_at);
            },

            /**
             *
             * @returns {string}
             */
            getStartHours: function () {
                return this.getStartAt().format('h:mm a');
            },

            getEndHours: function() {
                if(this.getEndAt() === null)
                    return "unknown";
                return this.getEndAt().format('h:mm a');
            },

            /**
             *
             * @returns {string}
             */
            getStartDate: function () {
                return this.getStartAt().format('YYYY-MM-DD');
            },

            getEndDate: function() {
                if(this.getEndAt() === null)
                    return "unknown";
                return this.getEndAt().format('YYYY-MM-DD');
            },

            /**
             *
             * @returns {string}
             */
            getStartsIn: function() {
                return moment().to(this.getStartAt());
            },

            isAfter: function() {
                return (this.getStartAt() && this.getStartAt().isAfter(moment())) || (this.getEndAt() && this.getEndAt().isAfter(moment()))
            },

            /**
             * Returns distance in km between point specified and
             * this Event
             *
             * @param {{latitude: float, longitude: float}} point
             * @returns {number}
             */
            computeDistanceTo: function (point) {
                var eventCoordinates = this.getCoordinates();
                var pointLatLng = new google.maps.LatLng({lat: point.latitude, lng: point.longitude});
                var EventLatLng = new google.maps.LatLng({lat: eventCoordinates.latitude, lng: eventCoordinates.longitude});
                return parseFloat((google.maps.geometry.spherical.computeDistanceBetween(pointLatLng, EventLatLng) / 1000).toFixed(2));
            },

            deleteFromBackend: function () {
                return $http.get('event/delete/'+this._id).then(function(response) {
                    return response;
                });
            }

        };
        
        return Event;
    }]);

})();
(function(){
    
    // "use strict";
    
    angular.module('app.services').factory('EventsStorage', ["$http", "$timeout", "Event", "ChangesNotifier", function($http, $timeout, Event, ChangesNotifier) {

        /**
         *
         * @param {Array} eventsArray
         * @constructor
         */
        function EventsStorage(eventsArray) {
            if(eventsArray){
                for(var i = 0; i < eventsArray.length; i++) {
                    if(!(eventsArray[i] instanceof Event)) {
                        eventsArray[i] = new Event(eventsArray[i]);
                    }
                }
                this.loadArray(eventsArray);
            }
        }

        EventsStorage.prototype = {
            data: [],
            loading: false,
            hasNextPage: false,
            _timeout: null,

            loadArray: function (eventsArray) {
                this.data = eventsArray;
            },

            /**
             *
             * @param {VenVastRequest} request
             * @returns {*}
             */
            load: function (request) {
                var scope = this;

                if(this._timeout){ //if there is already a timeout in process cancel it
                    $timeout.cancel(scope._timeout);
                }
                return this._timeout = $timeout(function(){
                    if(scope.loading === false) {
                        scope.loading = true;
                        if(request.hasMorePages === false) {
                            return [];
                        }
                        return $http.get('event', {params: request.getRequestObject()}).success(function(eventData) {
                            for(var i = 0; i < eventData.length; i++) {
                                eventData[i] = new Event(eventData[i]);
                            }
                            request.requestPerformed(eventData);
                            scope.loadArray(eventData);
                            scope.loading = false;
                            return scope;
                        });
                    }
                    scope._timeout = null;
                }, 500);
            }



        };
        
        return EventsStorage;
    }]);
    
})();
(function(){
    "use strict";
    angular.module('app.services').factory('HtmlHelper', ["$state", "$rootScope", "Restangular", function($state, $rootScope, Restangular){

        return {

            body: null,
            overlay: null,
            link :null,
            content: null,
            popups:null,
            document:null,


            init: function() {
                $(window).scroll(function(){
                    var scroll = $(window).scrollTop();
                    var category = $('div.category').eq(0);
                    if (scroll > 200) {
                        category.addClass('category--fixed');
                    }
                    else {
                        category.removeClass('category--fixed');
                    }
                });
                this.getLink().on('click', this.render_tab);
                $('.container ul li .active').removeClass('active');
            },

            getCoords: function (elem) {
                if(elem) {
                    var box = elem.getBoundingClientRect();
                    return box.top + pageYOffset;
                }
            },

            closePopups: function() {
                $('html').addClass('scroll');
                $('.popup-overlay').addClass('hide');
                for(var i = 0; i < $('.popup-simple').length; i++){
                    $($('.popup-simple')[i]).addClass('hide');
                }
                $('.category').removeClass('hide');
                $('.loader-wrapper').hide();
            },

            showPopUp: function(popupId) {
                // $('html').removeClass('scroll');
                $('.popup-overlay').removeClass('hide');
                $("#"+popupId).removeClass('hide');
                $('.category').addClass('hide');
                $('.loader-wrapper').hide('hide');
            },

            render_tab: function (event, tabId) {
                $('.tab-link li').removeClass('active');
                $('.tab-content li').removeClass('active');
                $('#'+tabId).addClass('active');
                $(event.target).closest('li').addClass('active');

            },

            getBody: function() {
                if(this.body === null)
                    this.body = $('body');
                return this.body;
            },
            getOverlay: function() {
                if(this.overlay === null)
                    this.overlay = $('.popup-overlay');
                return this.overlay;
            },
            getLink: function() {
                if(this.link === null)
                    this.link = $('.tab-link');
                return this.link;
            },
            getContent: function() {
                if(this.content === null)
                    this.content = $('.tab-content li');
                return this.content
            },
            getPopups: function() {
                if(this.popups === null)
                    this.popups = $('.popup-simple');
                return this.popups
            },

            getDocument: function() {
                return document;
            },

            StyledDropDown: function(selector, model) {
                this.dd = $(selector);
                this.placeholder = this.dd.children('span');
                this.opts = this.dd.find('ul.dropdown li');
                this.val = '';
                this.index = -1;

                this.initEvents = function() {
                    var obj = this;
                    obj.dd.on('click', function(event){
                        $(this).toggleClass('active');
                        return false;
                    });
                    obj.opts.on('click',function(){
                        var opt = $(this);
                        var cat_id = opt.find('a').data('value');
                        obj.val = opt.text();
                        obj.index = opt.index();
                        obj.placeholder.text(obj.val);
                    });

                    $('body').on('click', function () {
                        obj.dd.removeClass('active');
                    });
                };
                
                this.getValue = function() {
                    return this.val;
                };
                this.getIndex= function() {
                    return this.index;
                };

                this.initEvents();
            },

            /**
             * Show loader animation
             */
            showLoader: function () {
                $('html').removeClass('scroll');
                $('#loader-wrapper').show();
                $('.catalog-wrapper').addClass('hide');
            },

            /**
             * Hide loader animation
             */
            hideLoader: function () {
                $('html').addClass('scroll');
                $('#loader-wrapper').hide();
                $('.catalog-wrapper').removeClass('hide');
            }
        };

    }]);

})();
(function(){
    "use strict";


    angular.module('app.services').factory('MapService', ["$q", "UserService", "VenueStorage", "EventsStorage", "VenVastRequest", "AnchorSmoothScroll", function($q, UserService, VenueStorage, EventsStorage, VenVastRequest, AnchorSmoothScroll){

        return {

            KEY_EVENTS: 'events',
            KEY_DEALS: 'deals',
            KEY_VENUES: 'venues',

            ZOOM_INIT: 14,
            ZOOM_DETAIL: 17,

            request: null,
            markers: {
                events: [],
                venues: [],
                deals: []
            },
            currentMarkersKey: 'events',
            map: null,
            mapOptions: null,
            userMarker: null,

            prevState: null,
            prevRequest: null,

            /**
             * Extends an instance with an object provided
             * @param options
             */
            setOptions: function (options) {
                angular.extend(this, options);
            },

            /**
             * Set markers from object given
             * @param markers
             * @returns {MapService}
             */
            setMarkers: function(markers) {
                this.markers = markers;
                return this;
            },

            /**
             * Set event markers from given array of Events
             *
             * @param {Event[]} events
             */
            setEventMarkers: function(events) {
                var markers = [];
                var scope = this;
                if(events) {
                    for (var i = 0; i < events.length; i++) {
                        var marker = events[i].getMarker();
                        marker.addListener('click', function() {
                            var ma = this;
                            if(scope.mapOptions.zoom !== scope.ZOOM_DETAIL){
                                scope.prevState = scope.mapOptions;
                            }
                            scope.mapOptions = {
                                center: ma.getPosition(),
                                zoom: scope.ZOOM_DETAIL
                            };
                            scope.map.setCenter(scope.mapOptions.center);
                            scope.map.setZoom(scope.mapOptions.zoom);
                            $('.catalog-wrapper').removeClass('catalog-wrapper--full', {duration: 500});
                            AnchorSmoothScroll.scrollTo(ma.id);
                            $('#' + ma.id).addClass('catalog-wrapper--full', {duration: 500});
                        });
                        marker.setMap(scope.map);
                        markers.push(marker);
                    }
                }
                this.markers.events = markers;
            },

            addEventMarkers: function(events) {
                var markers = [];
                var scope = this;
                if(events) {
                    for (var i = 0; i < events.length; i++) {
                        var marker = events[i].getMarker();
                        marker.addListener('click', function() {
                            var ma = this;
                            if(scope.mapOptions.zoom !== scope.ZOOM_DETAIL){
                                scope.prevState = scope.mapOptions;
                            }
                            scope.mapOptions = {
                                center: ma.getPosition(),
                                zoom: scope.ZOOM_DETAIL
                            };
                            scope.map.setCenter(scope.mapOptions.center);
                            scope.map.setZoom(scope.mapOptions.zoom);
                            $('.catalog-wrapper').removeClass('catalog-wrapper--full', {duration: 500});
                            AnchorSmoothScroll.scrollTo(ma.id);
                            $('#' + ma.id).addClass('catalog-wrapper--full', {duration: 500});
                        });
                        marker.setMap(scope.map);
                        markers.push(marker);
                        this.markers.events.push(marker);
                    }
                }
            },


            /**
             * Set venue markers from given array of Venues
             * @param {Venue[]} venues
             */
            setVenueMarkers: function(venues) {
                var markers = [];
                var scope = this;
                if(venues) {
                    console.log(venues);
                    for (var i = 0; i < venues.length; i++) {
                        var marker = venues[i].getMarker();
                        marker.addListener('click', function() {
                            var ma = this;
                            if(scope.mapOptions.zoom !== scope.ZOOM_DETAIL){
                                scope.prevState = scope.mapOptions;
                            }
                            scope.mapOptions = {
                                center: ma.getPosition(),
                                zoom: scope.ZOOM_DETAIL
                            };
                            scope.map.setCenter(scope.mapOptions.center);
                            scope.map.setZoom(scope.mapOptions.zoom);
                            $('.catalog-wrapper').removeClass('catalog-wrapper--full', {duration: 500});
                            AnchorSmoothScroll.scrollTo(ma.id);
                            $('#' + ma.id).addClass('catalog-wrapper--full', {duration: 500});
                        });
                        marker.setMap(scope.map);
                        markers.push(marker);
                    }
                }
                this.markers.venues = markers;
            },

            addVenueMarkers: function(venues) {
                var markers = [];
                var scope = this;
                if(venues) {
                    for (var i = 0; i < venues.length; i++) {
                        var marker = venues[i].getMarker();
                        marker.addListener('click', function() {
                            var ma = this;
                            if(scope.mapOptions.zoom !== scope.ZOOM_DETAIL){
                                scope.prevState = scope.mapOptions;
                            }
                            scope.mapOptions = {
                                center: ma.getPosition(),
                                zoom: scope.ZOOM_DETAIL
                            };
                            scope.map.setCenter(scope.mapOptions.center);
                            scope.map.setZoom(scope.mapOptions.zoom);
                            $('.catalog-wrapper').removeClass('catalog-wrapper--full', {duration: 500});
                            AnchorSmoothScroll.scrollTo(ma.id);
                            $('#' + ma.id).addClass('catalog-wrapper--full', {duration: 500});
                        });
                        marker.setMap(scope.map);
                        markers.push(marker);
                        this.markers.venues.push(marker);
                    }
                }
            },

            /**
             * Set deals markers from given array of deals
             *
             * TODO: implement after implementing deals
             * @param markers
             */
            setDealsMarkers: function(markers) {
                this.markers.deals = markers;
            },

            /**
             * Hide all markers from the map and delete them from this request
             */
            deleteAllMarkers: function() {
                var scope = this;
                var events = this.markers.events;
                var venues = this.markers.venues;
                var deals = this.markers.deals;
                this.deleteMarkers(events);
                this.deleteMarkers(venues);
                this.deleteMarkers(deals);
                this.setAllMarkersEmpty();
            },

            /**
             * Set all markers in this object as an empty array
             * It clearing all info about markers in this object
             * but not actually removes them from the map
             */
            setAllMarkersEmpty: function() {
              this.markers = {
                  events: [],
                  venues: [],
                  deals: []
              };
            },

            /**
             * Delete all markers from the map by setting
             * map reference in Marker objects t0 null
             * @param markers
             */
            deleteMarkers: function(markers) {
                if(markers && markers.length > 0) {
                    for(var i = 0; i < markers.length; i++) {
                        var marker = markers[i];
                        marker.setMap(null);
                        delete markers[i];
                    }
                }
            },

            /**
             * Create user marker, attach it to map
             * and save in current MapService instance
             */
            showUserMarker: function() {
                var position = this.request.getUserPosition();
                if(typeof position.latitude !== "udefined" && position.latitude) {
                    var userMarker = new google.maps.Marker({
                        icon: new google.maps.MarkerImage('/img/gpsloc.png', null, null, null, new google.maps.Size(16, 16)),
                        position: {lat: position.latitude, lng: position.longitude},
                        id: "userPos"
                    });
                    userMarker.setMap(this.map);
                    this.userMarker  = userMarker;
                }
            },

            /**
             * Hides user marker on the map and remove it from MapService
             * instance
             */
            hideUserMarker: function() {
                this.userMarker.setMap(null);
                this.userMarker = null;
            },

            /**
             *
             * Initialize map. Sets center as a current user position, we got from request
             *
             * @param {VenVastRequest} request
             * @param {string} elementId
             */
            initialize: function(request, elementId) {

                var scope = this;
                return $q.when(request.updateCoordinates()).then(function (coordinates) {
                    position = request.getPosition();
                    if(typeof position === "undefined") {
                        var position = request.mockPosition();
                    }
                    if(typeof coordinates !== "undefined") {
                        position = coordinates;
                    }
                    if(request.where !== null && typeof request.where.latitude !== 'undefined') {
                        console.log("Reuest where is not null");
                        position = request.where;
                    }
                    var element;
                    if(elementId) {
                        element = document.getElementById(elementId);
                    } else {
                        element = document.getElementById('map-canvas');
                    }
                    var mapOptions = {
                        center: new google.maps.LatLng(position.latitude, position.longitude),
                        zoom: position.zoom ? position.zoom : scope.ZOOM_INIT,
                        zoomControl: true,
                        scaleControl: true,
                        disableDefaultUI: true,
                        styles: [
                            {
                                featureType: "poi",
                                stylers: [
                                    { visibility: "off" }
                                ]
                            }
                        ]
                    };
                    scope.map = new google.maps.Map(element, mapOptions);
                    scope.mapOptions = mapOptions;
                    scope.request = request;
                    return scope;
                });
            },

            /**
             * Check if current request is not equels previous one
             * So we can update markers
             * @returns {boolean}
             */
            requestUpdated: function() {
                return !angular.equals(this.request, this.prevRequest);
            },

            /**
             * Get markers that should be displayed according to current state
             * @returns {*}
             */
            getCurrentStateMarkers: function() {
                return this.markers[this.currentMarkersKey];
            },

            /**
             * Center map on object given
             *
             * TODO: extend with deals
             *
             * @param {Event|Venue} obj
             * @returns {null}
             */
            centerOnObject: function(obj) {
                var scope = this;
                var coordinates = obj.getCoordinates();
                this.prevState = this.mapOptions;
                this.mapOptions = {
                    center: {lat: coordinates.latitude, lng: coordinates.longitude},
                    zoom: scope.ZOOM_DETAIL,
                };
                this.map.setCenter(this.mapOptions.center);
                this.map.setZoom(this.mapOptions.zoom);
                return this.map;
            },

            /**
             * Restore map to previous saved state
             *
             * @returns {null}
             */
            restoreMap: function() {
                var tmp;
                angular.copy(this.mapOptions, tmp);
                this.mapOptions = this.prevState;
                this.prevState = tmp;
                this.map.setCenter(this.mapOptions.center);
                this.map.setZoom(this.mapOptions.zoom);
                return this.map;
            }

        };

    }]);

})();
(function() {
    "use strict";

    angular.module('app.services').factory('AnchorSmoothScroll', function(){

        return {
            scrollTo: function (eID) {
                // This scrolling function
                // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript
                var startY = currentYPosition();
                var stopY = elmYPosition(eID);
                var distance = stopY > startY ? stopY - startY : startY - stopY;
                if (distance < 100) {
                    scrollTo(0, stopY);
                    return;
                }
                var speed = Math.round(distance / 100);
                if (speed >= 20) speed = 20;
                var step = Math.round(distance / 25);
                var leapY = stopY > startY ? startY + step : startY - step;
                var timer = 0;
                if (stopY > startY) {
                    for (var i = startY; i < stopY; i += step) {
                        setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                        leapY += step;
                        if (leapY > stopY) leapY = stopY;
                        timer++;
                    }
                    return;
                }
                for (var i = startY; i > stopY; i -= step) {
                    setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                    leapY -= step;
                    if (leapY < stopY) leapY = stopY;
                    timer++;
                }

                function currentYPosition() {
                    // Firefox, Chrome, Opera, Safari
                    if (self.pageYOffset) return self.pageYOffset;
                    // Internet Explorer 6 - standards mode
                    if (document.documentElement && document.documentElement.scrollTop)
                        return document.documentElement.scrollTop;
                    // Internet Explorer 6, 7 and 8
                    if (document.body.scrollTop) return document.body.scrollTop;
                    return 0;
                }

                function elmYPosition(eID) {
                    var elm = document.getElementById(eID) ? document.getElementById(eID) : $('[ng-id="'+ eID +'"]');
                    var y = elm.offsetTop;
                    var node = elm;
                    while (node.offsetParent && node.offsetParent != document.body) {
                        node = node.offsetParent;
                        y += node.offsetTop;
                    }
                    return y - 80;
                }

            }
        };
    });
})();
(function(){
    "use strict";

    angular.module('app.services').factory('UserModel', ["$state", "$http", "VenVastRequest", function($state, $http, VenVastRequest){

        /**
         *
         * @param userData
         * @constructor
         */
        function User(userData) {
            if(userData) {
                this.loadData(userData);
            }
        }


        /**
         *
         * @type {{
         *
         * GENDER_WOMAN: number,
         * GENDER_MAN: number,
         * TYPE_ATTENDEE: number,
         * TYPE_ORGANIZER: number,
         * login: string|null,
         * email: string|null,
         * first_name: string|null,
         * last_name: string|null,
         * gender: int|null,
         * hide_phone: number,
         * phone: string|null,
         * created_at: string|null,
         * updated_at: string|null,
         * type: number,
         * last_lat: number|null,
         * last_lng: number|null,
         * fb_id: string|null,
         * google_id: string|null,
         * fb_picture: string|null,
         * google_picture: string|null,
         * fb_access_token: string|null,
         * old_id: string|null,
         * _id: string|null,
         * loadData: User.loadData,
         * makeLogin: User.makeLogin,
         * isOrganizer: User.isOrganizer,
         * updateLogin: User.updateLogin,
         * setGenderFromData: User.setGenderFromData,
         * setTypeFromData: User.setTypeFromData,
         * prepareForUpdateRequest: User.prepareForUpdateRequest
         *
         * }}
         */
        User.prototype = {

            GENDER_WOMAN: 0,
            GENDER_MAN: 1,

            TYPE_ATTENDEE: 0,
            TYPE_ORGANIZER: 1,

            login: null,
            email: null,
            first_name: null,
            last_name: null,
            gender: null,
            hide_phone: 0,
            phone: null,
            created_at: null,
            updated_at: null,
            type: this.TYPE_ATTENDEE,
            last_lat: null,
            last_lng: null,
            fb_id: null,
            google_id: null,
            fb_picture: null,
            google_picture: null,
            fb_access_token: null,
            old_id: null,
            _id: null,
            
            initialized: false,


            /**
             * Create object from given data
             * @param userData
             */
            loadData: function (userData) {
                angular.extend(this, userData);
                this.makeLogin(userData);
                this.setGenderFromData(userData);
                this.setTypeFromData(userData);
                this.initialized = true;

            },

            find: function (id) {
                return $http.get('user/' + id).then(function(response) {
                    if(response.success === true && typeof response.user !== "undefined") {
                        return new User(response.user);
                    }
                    return null;
                });
            },

            /**
             * Makes login if it's not set
             *
             * @param userData
             * @returns {*}
             */
            makeLogin: function (userData) {
                    return userData.email;
            },

            /**
             * Can this user create venues, events or deals
             * @returns {boolean}
             */
            isOrganizer: function () {
                return this.type === this.TYPE_ORGANIZER;
            },


            /**
             * Updates login on server
             *
             * @param {string} login
             * @returns {*}
             */
            updateLogin: function(login) {
                var reqObj = this.prepareForUpdateRequest(this);
                reqObj.login = login;
                console.log("before Calling update: ", reqObj);
                return $http.post('user/update', reqObj).then(function(response) {
                    return response.success;
                }, function(error) {
                    console.log("Error occured", error);
                });
            },
            
            updateLatLng: function() {
                var reqObj = this.prepareForUpdateRequest(this);
                return $q.when(VenVastRequest.updateCoordinates()).then(function (position) {
                    reqObj.last_lat = position.latitude;
                    reqObj.last_lng = position.longitude;
                    return $http.post('user/update', {params: reqObj}).then(function(response) {
                        return response.success;
                    }, function(error) {
                        console.log("Error occured", error);
                    });
                });
            },

            /**
             * Updates user on server
             * 
             * @returns {*}
             */
            updateUser: function () {
                var reqObj = this.prepareForUpdateRequest(this);
                return $http.post('user/update', {params: reqObj}).then(function(response) {
                    return response.success;
                }, function(error) {
                    console.log("Error occured", error);
                });
            },


            /**
             * Set gender from data presented
             *
             * @param userData
             * @returns {User}
             */
            setGenderFromData: function (userData) {
                this.gender = parseInt(userData.gender);
                return this;
            },

            /**
             * Set type from data presented
             * @param userData
             * @returns {User}
             */
            setTypeFromData: function (userData) {
                var scope = this;
                if (typeof userData.type === "undefined" || userData.type === "") {
                    this.type = this.TYPE_ATTENDEE;
                } else {
                    userData.type = parseInt(userData.type);
                    switch (userData.type) {
                        case scope.TYPE_ATTENDEE:
                            scope.type = scope.TYPE_ATTENDEE;
                            break;
                        case scope.TYPE_ORGANIZER:
                            scope.type = scope.TYPE_ORGANIZER;
                            break;
                        default:
                            scope.type = scope.TYPE_ATTENDEE;
                            break;
                    }
                }
                return this;
            },
            
            isConnectedWithFacebook: function () {
               return this.fb_id.length > 2; 
            },
            
            isConnectedWithGoogle: function () {
                return this.google_id.length > 2; 
            },

            /**
             * Prepare oject for performing POST update request
             *
             * @param userObject
             * @returns {*}
             */
            prepareForUpdateRequest: function(userObject) {
                var reqObj = {};
                angular.copy(reqObj, userObject);
                delete reqObj.password;
                delete reqObj.created_at;
                delete reqObj._id;
                angular.copy(reqObj.id, userObject._id);
                return reqObj;
            },
        };

        return User;
    }]);

})();
(function(){
    "use strict";

    
    angular.module('app.services').factory('UserService', ["$http", "UserModel", "$localStorage", "$q", "$timeout", function($http, UserModel, $localStorage, $q, $timeout){
        return {
            user: null,
            formFields: {},
            loginError: null,
            homeTimeout: null,

            /**
             * get current user(no matters how)
             * @returns {*}
             */
            getCurrentUser: function() {
                if(this.user !== null && this.user.__id){
                    return this.user;
                }else{
                    return this.requestCurrentUser();
                }
            },

            /**
             * Request current user from server
             *
             * @returns {*}
             */
            requestCurrentUser: function() {
                var scope = this;

                if(this.homeTimeout){ //if there is already a timeout in process cancel it
                    $timeout.cancel(scope.homeTimeout);
                }

                return this.homeTimeout = $timeout(function () {
                    if(!$localStorage.token)
                        return null;
                    return $http.get('user/home').then(function(response){
                        if(typeof response === "undefined" || typeof response.data === 'undefined') {
                            scope.user = null;
                            return scope.user;
                        }
                        response = response.data;
                        if(response == null || response.length === 0 || angular.equals({}, response)){
                            scope.user = null;
                        } else {
                            scope.user = new UserModel(response);
                        }
                        scope.homeTimeout = null;
                        return scope.user;
                    });
                }, 5000);
            },

            /**
             * Do login by user credentials
             * @param {Object} formData
             */
            login: function(formData) {
                var scope = this;
                return $http.post('api/authenticate', formData).then(function(response){
                    response = response.data;
                    if(response.token !== null && typeof response.token !== 'undefined') {
                        $localStorage.token = response.token;
                        if(response.token) {
                            return $http.get('user/home').then(function(userResponse) {
                                userResponse = userResponse.data;
                                if(userResponse !== null && typeof userResponse !== "undefined") {
                                    scope.user = new UserModel(userResponse);
                                }
                                return scope.user;
                            });
                        } else {
                            scope.user = null;
                            return scope.user;
                        }
                    }
                }, function(error) {
                    scope.loginError = error;
                });
            },

            /**
             * Set email and password typed in login form
             *
             * @param email
             * @param password
             */
            setLoginFormFields: function(email, password) {
                this.formFields.email = email;
                this.formFields.password = password;
            },

            /**
             * Login to facebook
             *
             * @param Facebook
             * @returns {User}
             */
            loginFacebook: function(Facebook) {
                var scope = this;
                scope.user = {};
                return Facebook.getLoginStatus(function(fbStatusResponse) {
                    if(fbStatusResponse.status === 'connected') {
                        return fbStatusResponse.authResponse.accessToken;
                        
                    } else {
                        return Facebook.login(function(loginResponse) {
                            return loginResponse.authResponse.accessToken;
                        });
                    }
                });
            },

            /**
             * Updates user model with facebook credentials
             *
             * @param user
             * @returns {*}
             */
            backendFacebookLogin: function(user) {
                var scope = this;
                return $http.post('user/facebook', user).then(function(response){
                    if(response.success === true){
                        scope.user = response.user;
                        scope.user.initialized = true;
                        return scope.user;
                    }
                });
            },

            /**
             * Set data we got from facebook
             * to current user
             *
             * @param fbUserDetails
             * @returns {updateWithFbData}
             */
            updateWithFbData: function(fbUserDetails) {
                var formEmail = typeof this.formFields.email;
                if(typeof formEmail !== "undefined" && formEmail.length > 6) {
                    this.user.email = this.formFields.email;
                } else {
                    this.user.email = fbUserDetails.email;
                }
                this.user.fb_id = fbUserDetails.id;
                this.user.gender = fbUserDetails.gender.toLowerCase() === "male" ? 1 : 0;
                this.user.first_name = fbUserDetails.first_name;
                this.user.last_name = fbUserDetails.last_name;
                this.user.fb_picture = fbUserDetails.picture.data.url;
                return this;
            },

            /**
             * Perform logging out
             * @returns {*}
             */
            logout: function() {
                var scope = this;
                return $http.get('user/logout').then(function(response){
                    response = response.data;
                    if(response.success === true){
                        scope.user = null;
                        $localStorage.token = null;
                        return scope.user;
                    }
                });
            },

            /**
             * Perform register action
             * @param userData
             * @returns {*}
             */
            register: function(userData){
                var scope = this;
                return $http.post('user/register', userData).then(function(response){
                    response = response.data;
                    console.log("Got response: ", response);
                    if(response.success === true){
                        scope.user = new UserModel(response.user);
                        return scope.user;
                    }
                });
            }
        };
    }]);

})();
(function(){
    "use strict";


    angular.module('app.services').factory('Venue', ["$http", "VenVastRequest", "Event", "Upload", function($http, VenVastRequest, Event, Upload){

        function Venue(venueData) {
            if(venueData) {
                this.setData(venueData);
            }
        }

        Venue.prototype = {

            name: null,
            birthday: null,
            cover_image: null,
            description: null,
            general_info: null,
            street: null,
            zip: null,
            phone: null,
            website : null,
            public_transit : null,
            published : false,
            location : null,
            categories : [],
            upcomingEvents: [],
            currentDeals: [],
            upcomingDeals: [],
            _id : null,
            className: 'Venue',


            setData: function (venueData) {
                angular.extend(this, venueData);
                var scope = this;
                if(typeof scope.events !== 'undefined' && scope.events.length > 0) {
                    for(var i = 0; i < scope.events.length; i++) {
                        scope.events[i] = new Event(scope.events[i]);
                    }
                }
                this.icon = this.getIcon();
                return this;
            },

            find: function(id) {
                return $http.get('venue/' + id).then(function (response) {
                    if(response !== null)
                        return new Venue(response);
                });
            },

            getIcon: function () {
                if(this.categories.length === 0 || !this.categories || typeof this.categories === 'undefined') {
                    return '/img/question.png';
                } else {
                    if(typeof this.categories[0] !== 'undefined' &&  this.categories[0] !== null && this.categories[0].image) {
                        return "/img/cache/original/categories/" + this.categories[0].image;
                    } else {
                        return '/img/question.png';
                    }
                }
            },
            
            getPicture: function() {
                if(this.cover_image === null || typeof this.cover_image === 'undefined' || this.cover_image === ''){
                    return 'https://placeholdit.imgix.net/~text?txtsize=33&txt=' + this.name + '&w=350&h=150';
                }

                if(typeof this.cover_image !== 'string'){
                    return this.cover_image
                }
                if(!this.cover_image.startsWith('http')) {
                    return '/img/cache/original/venues/' + this.cover_image;
                } else {
                        return this.cover_image;
                }
            },


            saveOnBackend: function () {
                if(this.cover_image !== null) {
                    return Upload.upload({url: 'venue/create', data: this})
                        .then(function (resp) {
                            return resp;
                        });
                }
                return $http.post('venue/create', this).then(function(response) {
                    return response;
                });
            },
            
            deleteFromBackend: function () {
                return $http.get('venue/delete/'+this._id).then(function(response) {
                    return response;
                });
            },

            getCoordinates: function () {
                return {
                    longitude: this.location.coordinates[0],
                    latitude: this.location.coordinates[1]
                }
            },

            isPublished: function() {
                return this.published;
            },

            createOpenHours: function(openHours) {
                for(var i = 0; i < openHours.length; i++) {
                    var dayObject = openHours[i];
                    dayObject.day = parseInt(dayObject.day);
                    if(typeof dayObject.open === 'object') {
                        dayObject.open = {
                            day: dayObject.day,
                            time: moment(dayObject.open.time).format('HHmm')
                        }
                    } else {
                        dayObject.open = false;
                    }
                    if(typeof dayObject.close === 'object') {
                        dayObject.close = {
                            day: dayObject.day,
                            time: moment(dayObject.close.time).format('HHmm')
                        }
                    } else {
                        dayObject.open = false;
                    }
                    openHours[i] = dayObject;
                }
                return openHours;
            },

            createOpeningHoursTemplate: function() {
                var hours = [];
                var scope = this;
                for(var i = 0; i < 7; i++) {
                    if(i < 1 || i > 5) {
                        var closed = scope.getClosedDayObject(i);
                        hours.push(closed);
                    } else {
                        var day = scope.getOpenHoursDayObject(i, "0900", "2000");
                        hours.push(day);
                    }
                }
                return hours;
            },

            getOpenHoursDayObject: function(day, open, close) {
                return {
                    day: day,
                    open: {
                        day: day,
                        time: open
                    },
                    close: {
                        day: day,
                        time: close
                    }
                };
            },

            getClosedDayObject: function(day) {
                return {
                    day: day
                }
            },

            getMarker: function() {
                var scope = this;
                var coordinates = scope.getCoordinates();
                return new google.maps.Marker({
                    icon: new google.maps.MarkerImage(scope.getIcon(), null, null, null, new google.maps.Size(32, 32)),
                    position: {lat: coordinates.latitude, lng: coordinates.longitude},
                    id: 'venue-' + scope._id
                });
            },

            /**
             * Returns distance in meter between point specified and
             * this Venue
             *
             * @param {{latitude: float, longitude: float}} point
             * @returns {number}
             */
            computeDistanceTo: function (point) {
                var venueCoordinates = this.getCoordinates();
                var pointLatLng = new google.maps.LatLng({lat: point.latitude, lng: point.longitude});
                var VenueLatLng = new google.maps.LatLng({lat: venueCoordinates.latitude, lng: venueCoordinates.longitude});

                return parseFloat((google.maps.geometry.spherical.computeDistanceBetween(pointLatLng, VenueLatLng) / 1000).toFixed(2));
            },

            /**
             * Return string name of day of week by its int code
             * @param code
             * @returns {*}
             */
            getDayOfWeekByCode: function (code) {
                switch (code) {
                    case 0:
                        return 'sunday';
                    case 1:
                        return 'monday';
                    case 2:
                        return 'tuesday';
                    case 3:
                        return 'wednesday';
                    case 4:
                        return 'thursday';
                    case 5:
                        return 'friday';
                    case 6:
                        return 'saturday';
                }
            }
            
        };

        return Venue;

    }]);

})();
(function(){
    "use strict";


    angular.module('app.services').factory('VenueStorage', ["$q", "$http", "VenVastRequest", "Event", "EventsStorage", "Venue", function($q, $http, VenVastRequest, Event, EventsStorage, Venue){

        /**
         * 
         * @param {Array} venuesArray
         * @constructor
         */
        function VenueStorage(venuesArray) {
            if(venuesArray) {
                for(var i = 0; i < venuesArray.length; i++) {
                    if(!(venuesArray[i] instanceof Venue)) {
                        venuesArray[i] = new Venue(venuesArray[i]);
                    }
                }
                this.loadArray(venuesArray);
            }
        }

        /**
         * 
         * @type {{
         * 
         * data: Array, 
         * loading: boolean, 
         * loadArray: VenueStorage.loadArray, 
         * list: VenueStorage.list, 
         * create: VenueStorage.create, 
         * getData: VenueStorage.getData
         * 
         * }}
         */
        VenueStorage.prototype = {
            data: [],
            loading: false,

            /**
             * Load array data to storage
             * 
             * @param venuesArray
             * @returns {VenueStorage}
             */
            loadArray: function (venuesArray) {
                this.data = venuesArray;
         
                return this;
            },

            /**
             *
             * @param {VenVastRequest} request
             * @returns {*}
             */
            load: function (request) {
                var scope = this;
                if(this.loading === false) {
                    scope.loading = true;
                    if(request.hasMorePages === false) {
                        return [];
                    }
                    return $http.get('venue', {params: request.getRequestObject()}).success(function(venueData) {
                        for(var i = 0; i < venueData.length; i++) {
                            venueData[i] = new Venue(venueData[i]);
                            if(!venueData[i].d || typeof venueData[i].d === 'undefined') {
                                venueData[i].d = venueData[i].computeDistanceTo(request.getUserPosition());
                            }
                            venueData[i].d = parseFloat(venueData[i].d);
                            venueData[i].distance = venueData[i].d ? venueData[i].d + ' km away' : '';
                        }
                        request.requestPerformed(venueData);
                        scope.loadArray(venueData);
                        scope.loading = false;
                        return scope;
                    });
                }
            },

            find: function(params) {
                var scope = this;
                if(this.loading === false) {
                    scope.loading = true;
                    return $http.get('venue/find', {params: params}).success(function(venueData) {
                        scope.loading = false;
                        return venueData.data;
                    });
                }
            },

            /**
             * Create new venue
             * @param venue
             * @returns {*}
             */
            create: function(venue) {
                return $http.post('venue/create', {params: venue}).success(function(response){
                    return response;
                });
            },

            /**
             * Returns all objects in storage
             * 
             * @returns {Array}
             */
            getData: function () {
                return this.data;
            }
        };

        return VenueStorage;
    }]);

})();
(function(){
    "use strict";


    angular.module('app.services').factory('VenVastRequest', ["$q", function($q){

        /**
         * @param requestData
         * @constructor
         */
        function VenVastRequest(requestData) {
            if(requestData) {
                this.loadData(requestData);
                if(!this.where) {
                    this.updateCoordinates();
                }
            }
        }

        function updateCoordinates() {
            var __self = this;
            if(navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    return __self.setPosition(position).getPosition();
                }, function(error) {
                    if(error) {
                        return __self.mockPosition().getPosition();
                    }
                });
            }
        }

        /**
         *
         * @type {{
         * when: string|null,
         * where: string|null,
         * category: string|null,
         * latitude: float|null,
         * longitude: float|null,
         * page: number,
         * web: string,
         * radius: number,
         * hasMorePages: boolean,
         * loadData: VenVastRequest.loadData,
         * updateCoordinates: VenVastRequest.updateCoordinates,
         * requestPerformed: VenVastRequest.requestPerformed,
         * mockPosition: VenVastRequest.mockPosition,
         * setPosition: VenVastRequest.setPosition,
         * getPosition: VenVastRequest.getPosition
         * }}
         */
        VenVastRequest.prototype = {

            WHEN_TODAY: 'today',
            WHEN_TOMORROW: 'tomorrow',
            WHEN_WEEK: 'week',
            WHEN_MONTH: 'month',

            when: null,
            where: null,
            category: null,
            latitude: null,
            longitude: null,
            hosts: false,
            page: 0,
            web: 'true',
            radius: 25,
            perPage: 60,
            text: null,
            date: null,

            hasMorePages: true,

            /**
             * Load data to this object
             * @param data
             */
            loadData: function(data) {
                angular.extend(this, data);
            },
            
            
            setWhere: function(latitude, longitude, name) {
                this.where = {
                    latitude: latitude,
                    longitude: longitude,
                    name: name
                };
                return this;
            },



            /**
             * Updates coordinates and returns updated position object
             *
             * @return Promise
             */
            updateCoordinates: function () {
                var __self = this;
                if(navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        return __self.setPosition(position).getPosition();
                    }, function(error) {
                        if(error) {
                            return __self.mockPosition().getPosition();
                        }
                    });
                }
            },

            requestPerformed: function(response) {
                if(response.length === 0 || typeof response === "undefined" || response.length < this.perPage) {
                    this.hasMorePages = false;
                    return;
                }
                this.page++;
            },

            /**
             *
             * @returns {VenVastRequest}
             */
            mockPosition: function() {
                this.latitude = 13.7252764;
                this.longitude = 100.587646;
                return this;
            },

            /**
             * @param position
             * @returns {VenVastRequest}
             */
            setPosition: function(position) {
                this.latitude = position.coords.latitude;
                this.longitude = position.coords.longitude;
                return this;
            },

            /**
             * Get position of request
             * @returns {{latitude: float, longitude: float}}
             */
            getPosition: function() {
                var scope = this;
                if(scope.where !== null) {
                    return {
                        latitude: scope.where.latitude,
                        longitude: scope.where.longitude,
                        zoom: 10
                    };
                } else {
                    return {
                        latitude: scope.latitude,
                        longitude: scope.longitude
                    };
                }
            },

            /**
             * Get position of user
             *
             * @returns {{latitude: *, longitude: *}}
             */
            getUserPosition: function() {
                var scope = this;
                return {
                    latitude: scope.latitude,
                    longitude: scope.longitude
                };  
            },

            isValidWhenValue: function(value) {
                switch(value) {
                    case this.WHEN_WEEK:
                        return true;
                    case this.WHEN_TODAY:
                        return true;
                    case this.WHEN_TOMORROW:
                        return true;
                    case this.WHEN_MONTH:
                        return true;
                }
                return false;
            },

            getRequestObject: function () {
                var obj = {};
                var scope = this;
                if(this.when !== null && this.isValidWhenValue(this.when)) {
                    obj.when = this.when;
                }
                if(this.organizer === null || typeof this.organizer === 'undefined') {
                    var pos = this.getPosition();
                    if(pos.latitude === null && pos.longitude === null) {
                        scope.mockPosition();
                        obj.latitude = this.latitude;
                        obj.longitude = this.longitude;
                    } else {
                        obj.latitude = pos.latitude;
                        obj.longitude = pos.longitude;
                    }

                } else {
                    $q.when(this.updateCoordinates()).then(function(coordinates) {
                        scope.mockPosition();
                    });
                }
                if(this.hosts === true) {
                    obj.hosts = 'true';
                }
                if(this.category !== null) {
                    obj.category = this.category;
                }
                if(this.where && typeof this.where.latitude !== 'undefined') {
                    obj.latitude = this.where.latitude;
                    obj.longitude = this.where.longitude;
                }
                if(this.where === null && this.latitude !== null && this.longitude !== null) {
                    obj.latitude = this.latitude;
                    obj.longitude = this.longitude;
                }
                if(this.type !== null) {
                    obj.type = this.type;
                }
                if(this.text !== null) {
                    obj.text = this.text;
                }
                if(this.date !== null) {
                    obj.date = this.date;
                }
                if(this.organizer !== null) {
                    obj.organizer = this.organizer;
                }
                obj.web = this.web;
                obj.radius = this.radius;
                obj.page = this.page;
                obj.timezone = jstz.determine().name();
                return obj;
            }

        };

        return VenVastRequest;
    }]);

})();
/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";

    /**
     * This controller is dealing with events list view
     */
    angular.module('app.controllers').controller('CalendarCtrl', ["$q", "$scope", "$timeout", "$state", "HtmlHelper", "EventsStorage", "ChangesNotifier", function($q, $scope, $timeout, $state, HtmlHelper, EventsStorage, ChangesNotifier){

        var state = $state.current.name;
        switch (state) {
            case 'home.events':
                $("#calendar-filter").ionCalendar({
                    lang: "en",
                    sundayFirst: false,
                    years: "2015-2018",
                    format: "YYYY-MM-DD",
                    onClick: function(date){
                        $('#when_search').children('span').text(date);
                        $timeout(function() {
                            $scope.eventRequest.when = date;
                        });
                        $scope.eventRequest.date = date;
                        $scope.search();
                    }
                });
                break;
            case 'home.venues':
                $("#calendar-filter").addClass('hidden');
                break;
        }

        $scope.search = function() {
            HtmlHelper.showLoader();
            $scope.eventRequest.page = 0;
            $scope.events = [];
            $scope.eventRequest.type = 'events';
            $scope.eventRequest.hasMorePages = true;
            var events = new EventsStorage();
            return $q.when(events.load($scope.eventRequest)).then(function(eventStorage) {
                $scope.events = eventStorage.data;
                ChangesNotifier.value = $scope.events;
                ChangesNotifier.notify(ChangesNotifier.SUBJ_EVENTS_LOADED, $scope.events);
                HtmlHelper.hideLoader();
            });
            // $scope.eventRequest.getRequestObject()
        };
    }]);
})();
/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";


    angular.module('app.controllers').controller('DealsCtrl', ["$scope", "$state", "UserService", "EventService", "MapService", function($scope, $state, UserService, EventService, MapService){
        //$scope.showingDescription = null;
        //$scope.showingEvent = null;
        $scope.init = function(){
            $scope.events = {};
            UserService.getCurrentUser(function(user) {
                $scope.user = user;
            });
            EventService.list(function(events){
                $scope.events.objects = events;
            });
            MapService.requestMap(function(map) {
                $scope.map = map;
            });
        };

        //$scope.showInfo = function(element, event){
        //    if($scope.showingDescription === null && $scope.showingEvent === null){
        //        $('.event-description').removeClass('active');
        //        $('.event').hide();
        //        var eventBlock = $(element.target).closest('.event').eq(0);
        //        eventBlock.show();
        //        $('#venue-'+event.id).addClass('active');
        //        $scope.showingDescription = true;
        //        $scope.showingEvent = true;
        //    } else {
        //        $('.event-description').removeClass('active');
        //        $('.event').show('slow');
        //        $scope.showingDescription = null;
        //        $scope.showingEvent = null;
        //    }
        //
        //};
        $scope.init();
    }]);
})();
/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";


    angular.module('app.controllers').controller('EventCreateCtrl', ["$scope", "$state", "UserService", "EventService", "VenueService", "MapService", "CategoryService", function($scope, $state, UserService, EventService, VenueService, MapService, CategoryService) {
        $scope.newEvent = {};
        $scope.datePicker = {
            opened: false
        };

        $scope.init = function(){

            Date.prototype.addHours = function(h) {
                this.setTime(this.getTime() + (h*60*60*1000));
                return this;
            };
            var startDateInit = new Date();
            $scope.newEvent.start_date =
                startDateInit.getFullYear() +"."+
                ("0" + (startDateInit.getMonth()+1)).slice(-2) +"."+
                ("0" + startDateInit.getDate()).slice(-2);/* + " " +*/
                //m.getHours() + ':' + m.getMinutes();
            var endsInInit = new Date().addHours(2);
            $scope.newEvent.ends_in = endsInInit.getFullYear() +"."+
                ("0" + (endsInInit.getMonth()+1)).slice(-2) +"."+
                ("0" + endsInInit.getDate()).slice(-2);
            //$scope.newEvent.duration = 30;
            $scope.newEvent.source_id = 3;
            $scope.newEvent.category = "";

            VenueService.requestListVenues(function(venues) {
                $scope.venues = venues;
            });
            CategoryService.listCategories(function(categories) {
                $scope.categories = categories;
            });

            $('#category').select2();

            $('#eventDate').datetimepicker({
                timepicker:true,
                format: "Y.d.m H:i",
                startDate: new Date(),
                minDate: new Date()
            });

            $('#eventDate').change(function() {
                $scope.newEvent.durationHint = $scope.getEventDurationHint();
                $scope.newEvent.start_date = $('#eventDate').val();
            });

            $('#ends_in').datetimepicker({
                timepicker:true,
                format: "Y.d.m H:i",
                startDate: new Date().addHours(2),
                minDate: new Date().addHours(2)
            });

            $('#ends_in').change(function() {
                $scope.newEvent.durationHint = $scope.getEventDurationHint();
                $scope.newEvent.ends_in = $('#ends_in').val();
            });
            //UserService.getCurrentUser($scope, $state);
        };

        $scope.toggleAddressSearch = function() {
            if($scope.newEvent.venue === "" || typeof $scope.newEvent.venue === "undefined"){
                $('#address-search').show('slow');
            } else {
                $('#address-search').hide('slow');
            }

        };

        $scope.createEvent = function($event){
            $event.preventDefault();
            $scope.newEvent.category = $('#category').val();
            if(typeof $scope.newEvent.address === "object" && typeof $scope.newEvent.venue === "undefined") {
                var addressObj = $scope.newEvent.address;
                var lat = addressObj.geometry.location.lat();
                var lng = addressObj.geometry.location.lng();
                $scope.newEvent.address = addressObj.formatted_address;
                $scope.newEvent.latitude = lat;
                $scope.newEvent.longitude = lng;

                VenueService.find({
                    source: 2,
                    latitude: lat,
                    longitude: lng,
                    name: addressObj.name
                }, function(venue) {
                    if(venue.length === 0){
                        $scope.createVenueFromAddress(addressObj, function(venue) {
                            $scope.newEvent.venue_id = venue.id;
                            $scope.newEvent.latitude = venue.latitude;
                            $scope.newEvent.longitude = venue.longitude;
                            $scope.newEvent.address = venue.street;
                            $scope.newEvent.venue_id = venue.id;
                            EventService.create($scope.newEvent, function() {
                                $state.go('organizer_home');
                            });
                        });
                    } else {
                        venue = venue[0];
                        $scope.newEvent.latitude = venue.latitude;
                        $scope.newEvent.longitude = venue.longitude;
                        $scope.newEvent.address = venue.street;
                        $scope.newEvent.venue_id = venue.id;
                        EventService.create($scope.newEvent, function() {
                            $state.go('organizer_home');
                        });
                    }
                });
            } else {
                VenueService.find({id: $scope.newEvent.venue}, function(venue){
                    if(venue.length === 0)
                        return false;
                    venue = venue[0];
                    $scope.newEvent.latitude = venue.latitude;
                    $scope.newEvent.longitude = venue.longitude;
                    $scope.newEvent.address = venue.street;
                    $scope.newEvent.venue_id = venue.id;
                    EventService.create($scope.newEvent, function() {
                        $state.go('organizer_home');
                    });
                });
            }
        };

        $scope.createVenueFromAddress = function(addressObj, callback) {
            var venue = {};
            venue.name = addressObj.name;
            venue.source_id = 2;
            venue.description = "";
            venue.general_info = "";
            venue.phone = addressObj.formatted_phone_number;
            venue.phone = addressObj.website;
            venue.street = addressObj.vicinity;
            venue.latitude = addressObj.geometry.location.lat();
            venue.longitude = addressObj.geometry.location.lng();
            VenueService.create(venue, callback);
        };

        $scope.addressSelected = function(){
            if(typeof $scope.newEvent.address === "object") {
                $scope.getVenueByLocation($scope.newEvent.address);
                $scope.map = MapService.centerOnEvent({
                    latitude: $scope.newEvent.address.geometry.location.lat(),
                    longitude: $scope.newEvent.address.geometry.location.lng()
                });
                $scope.map.latitude = $scope.newEvent.address.geometry.location.lat();
                $scope.map.longitude = $scope.newEvent.address.geometry.location.lng();
            }
        };

        $scope.getVenueByLocation = function(googleResponse) {
            var location = {
                latitude: googleResponse.geometry.location.lat(),
                longitude: googleResponse.geometry.location.lng()
            };
            VenueService.getByLocation(location, function(venue) {
                if(venue !== null){
                    $scope.newEvent.venue = venue.id;
                }
            });
        };

        $scope.getEventDurationHint = function() {
            var duration = $scope.getEventDuration();
            if(duration.days > 0) {
                return "Duration: " + duration.days + " days";
            }
            return "Duration: " + duration.minutes + " minutes";
        };

        $scope.getEventDuration = function() {
            var startingAt = $scope.parseDate($scope.newEvent.start_date);
            var finishAt = $scope.parseDate($scope.newEvent.ends_in);
            var diffMs = (startingAt - finishAt); // milliseconds between now & Christmas
            var diffDays = Math.round(diffMs / 86400000); // days
            var diffHrs = Math.round((diffMs % 86400000) / 3600000); // hours
            var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes

            return {
                days: diffDays,
                hours: diffHrs,
                minutes: diffMins
            };
        };

        $scope.parseDate = function(dateString) {
            var dateParts = dateString.split(' ');
            var date = dateParts[0].split('.');
            if(typeof dateParts[1] !== "undefined"){
                var time = dateParts[1].split(':');
                return new Date(date[0], date[1], date[2]);
            }
            return new Date(date[0], date[1], date[2]);
        };

        $scope.durationDefined = function() {
            return typeof $scope.newEvent.duration !== "undefined";
        };

        $scope.init();
    }]);
})();
/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";

    /**
     * This controller is dealing with events list view
     */
    angular.module('app.controllers').controller('EventFilterCtrl', ["$q", "$timeout", "$scope", "$rootScope", "$state", "HtmlHelper", "ChangesNotifier", "EventsStorage", function($q, $timeout, $scope, $rootScope, $state, HtmlHelper, ChangesNotifier, EventsStorage){

        var _timeout;


        $scope.getEventRequest = function() {
            var state = $state.current.name;
            switch (state) {
                case 'home.events':
                    return $scope.eventRequest;
                    break;
                case 'organizer.events':
                    return $scope.orgEventRequest;
                    break;
            }
        };


        angular.element(document).ready(function () {
            HtmlHelper.init();
            var category = $scope.getEventRequest().category;
            var categories = $scope.eventCategories;
            var categoryName = null;
            if(category !== null && typeof categories !== 'undefined') {
                for(var i = 0; i < categories.length; i++) {
                    if(categories[i]._id == category){
                        categoryName = categories[i].name;
                    }
                }
            }
            if(categoryName !== null) {
                $timeout(function() {
                    $('#category_search').children('span').text(categoryName);
                });
            }
            new HtmlHelper.StyledDropDown('#category_search', category);
            new HtmlHelper.StyledDropDown('#when_search', $scope.getEventRequest().when);
            var input = $('.location-input').first();
            $scope.autocomplete = new google.maps.places.Autocomplete(input[0], {types: ['(cities)']});
            $scope.autocomplete.addListener('place_changed', function () {
                var place = $scope.autocomplete.getPlace();
                $scope.getEventRequest().setWhere(place.geometry.location.lat(), place.geometry.location.lng(), place.name);
                $scope.search();
            });
        });

        $scope.searchByText = function () {
            if(_timeout){ //if there is already a timeout in process cancel it
                $timeout.cancel(_timeout);
            }
            _timeout = $timeout(function(){
                $scope.search();
                _timeout = null;
            }, 500);
        };


        $scope.selectCategory = function(category) {
            if(category === null){
                $scope.getEventRequest().category = null;
                $('#category_search').children('span').text('-- All --');
            } else {
                $scope.getEventRequest().category = category._id;
                $('#category_search').children('span').text(category.name);
            }
            $scope.search();
        };

        $scope.selectWhen = function(value) {
            $scope.getEventRequest().date = null;
            $scope.getEventRequest().when = value;
            if(value === null) {
                $('#when_search').children('span').text('-- All --');
            }
            else {
                $('#when_search').children('span').text(value);
            }
            $scope.search();
        };

        $scope.organizerSearch = function() {
            HtmlHelper.showLoader();
            $scope.orgEventRequest.page = 0;
            $scope.events = [];
            $scope.orgEventRequest.type = 'events';
            $scope.orgEventRequest.hasMorePages = true;
            var events = new EventsStorage();
            return $q.when(events.load($scope.orgEventRequest)).then(function(eventStorage) {
                $scope.orgEvents = eventStorage.data;
                ChangesNotifier.org_value = $scope.orgEvents;
                ChangesNotifier.notify(ChangesNotifier.SUBJ_ORG_EVENTS_LOADED);
                HtmlHelper.hideLoader();
            });
        };

        $scope.userSearch = function() {
            HtmlHelper.showLoader();
            $scope.eventRequest.page = 0;
            $scope.events = [];
            $scope.eventRequest.type = 'events';
            $scope.eventRequest.hasMorePages = true;
            var events = new EventsStorage();
            return $q.when(events.load($scope.eventRequest)).then(function(eventStorage) {
                $scope.events = eventStorage.data;
                ChangesNotifier.value = $scope.events;
                ChangesNotifier.notify(ChangesNotifier.SUBJ_EVENTS_LOADED, $scope.events);
                HtmlHelper.hideLoader();
            });
        };

        $scope.search = function() {
            var state = $state.current.name;
            switch (state) {
                case 'home.events':
                    $scope.userSearch();
                    break;
                case 'organizer.events':
                    $scope.organizerSearch();
                    break;
            }
        };
    }]);
})();
/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";

    /**
     * This controller is dealing with events list view
     */
    angular.module('app.controllers').controller('EventsCtrl', ["$q", "$scope", "$state", "HtmlHelper", "ChangesNotifier", function($q, $scope, $state, HtmlHelper, ChangesNotifier){


        $scope.$watch('events', function() {
            if($scope.events) {
                for(var i = 0; i < $scope.events.length; i++) {
                    var distance = $scope.events[i].computeDistanceTo($scope.eventRequest.getUserPosition());
                    if(distance > 30) {
                        distance = '> 30';
                    }
                    $scope.events[i].distance = distance ? distance + ' km away' : '';
                }
                HtmlHelper.hideLoader();
            }
        });
    }]);
})();
/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";
    angular.module('app.controllers').controller('HeaderCtrl', ["$scope", "$rootScope", "$localStorage", "$timeout", "$q", "$http", "$state", "$location", "$auth", "HtmlHelper", "UserService", "Facebook", function($scope, $rootScope, $localStorage, $timeout, $q, $http, $state, $location, $auth, HtmlHelper, UserService, Facebook){

        $scope.render_tab = HtmlHelper.render_tab;
        $scope.closePopups = HtmlHelper.closePopups;
        $scope.showPopUp = HtmlHelper.showPopUp;

        $scope.loginForm = {
            email: "",
            password: ""
        };

        $scope.registerForm = {
            type: 0
        };

        $('body').click(function(event) {
            if(
                !$(event.target).is('a')
                &&
                !$(event.target).is('input')
                &&
                !$(event.target).is('label')
            ){
                $scope.closePopups();
            }
        });


        $rootScope.$on('$stateChangeSuccess', function(){
            var eventLi = $('#menu-events').closest('li');
            var venueLi = $('#menu-venues').closest('li');
            var dealLi = $('#menu-deals').closest('li');
            switch($location.path().substring(3)){
                case 'events':
                    venueLi.removeClass('active');
                    dealLi.removeClass('active');
                    eventLi.addClass('active');
                    break;
                case 'venues':
                    dealLi.removeClass('active');
                    eventLi.removeClass('active');
                    venueLi.addClass('active');
                    break;
                case  'deals':
                    eventLi.removeClass('active');
                    venueLi.removeClass('active');
                    dealLi.addClass('active');
                    break;
            }
        });

        /**
         * Log current user out
         */
        $scope.logout = function() {
            $localStorage.token = null;
            $q.when(UserService.logout()).then(function() {
                $timeout(function() {
                    $scope.user = null;
                });
            });
        };

        /**
         * Log user in
         */
        $scope.submitLogin = function() {
            $scope.togglePopUpLoading();
            UserService.login($scope.loginForm).then(function(response) {
                if(typeof response._id !== "undefined") {
                    $scope.user = response;
                    $scope.togglePopUpLoading();
                    $scope.closePopups();
                } else {
                        $scope.loginError = 'Error occured, please try reload the page and repeat your action';
                        $('#login-error').removeClass('hidden');
                }
            });
        };

        /**
         * Login with facebook
         */
        $scope.loginFacebook = function () {
            $q.when(UserService.loginFacebook(Facebook)).then(function(us) {
                if(us.authResponse && us.authResponse.accessToken) {
                    $scope.loginForm = {facebook_access_token: us.authResponse.accessToken};
                    $scope.submitLogin();
                }  else {
                    $scope.loginError = 'Error occured, please try reload the page and repeat your action';
                    $('#login-error').removeClass('hidden');
                }
            });
        };

        /**
         * Login with facebook
         */
        $scope.loginGoogle = function () {
            $http.post('https://venvast.com/auth/google').success(function (response, status, headers) {
                console.log("Some resp: ", response);
                console.log("Status: ", status);
                console.log("Headers: ", headers);
            });
        };

        $scope.togglePopUpLoading = function() {
            $('#login_form').toggleClass('hidden');
            $('#register_form').toggleClass('hidden');
            $('.loader-wrapper').toggle();
        };

        /**
         * Register new user
         */
        $scope.register = function() {
            $scope.submitLogin();
        };

        /**
         * Connect facebook account on registration
         */
        $scope.registerFacebook = function() {
            $scope.loginFacebook();
        };
        




        switch($location.path().substring(3)){
            case '':
                $('#menu-events').closest('li').addClass('active');
                break;
            case 'events':
                $('#menu-events').closest('li').addClass('active');
                break;
            case 'venues':
                $('#menu-venues').closest('li').addClass('active');
                break;
            case  'deals':
                $('#menu-deals').closest('li').addClass('active');
                break;
        }
    }]);
})();
/**
 * Created by oem on 3/19/16.
 */
(function(){
    // "use strict";


    angular.module('app.controllers').controller('HomeCtrl', ["$q", "$scope", "$timeout", "$window", "$anchorScroll", "$state", "AnchorSmoothScroll", "HtmlHelper", "EventsStorage", "VenueStorage", "Event", "UserService", "VenVastRequest", "MapService", "ChangesNotifier", "CategoriesStorage", function($q, $scope, $timeout, $window, $anchorScroll, $state, AnchorSmoothScroll, HtmlHelper, EventsStorage, VenueStorage, Event, UserService, VenVastRequest, MapService, ChangesNotifier, CategoriesStorage){


        $scope.init = function(){
            $scope.win_height_padded = $(window).height() * 1.34;
            $window.onscroll = $scope.revealOnScroll;

            $scope.eventCategoriesRequest = new VenVastRequest({type: 'events'});
            if(typeof $scope.eventRequest === 'undefined' || $scope.eventRequest === null) {
                $scope.eventRequest = new VenVastRequest({when: "week"});
            }
            $scope.venueCategoriesRequest = new VenVastRequest({type: 'venues'});
            if(typeof $scope.venuesRequest === 'undefined' || $scope.venuesRequest === null) {
                $scope.venuesRequest = new VenVastRequest({type: 'venues'});
            }
            HtmlHelper.showLoader();
            $scope.initUser();
            $scope.initEvents();
            $scope.initVenues();
            ChangesNotifier.subscribe(ChangesNotifier.SUBJ_VENUES_LOADED, $scope, function() {
                $timeout(function() {
                    if(typeof  ChangesNotifier.venues_value !== "undefined" && ChangesNotifier.venues_value !== null) {
                        $scope.venues = ChangesNotifier.venues_value;
                    }
                });
            });
            ChangesNotifier.subscribe(ChangesNotifier.SUBJ_EVENTS_LOADED, $scope, function () {
                $timeout(function () {
                    if(typeof  ChangesNotifier.value !== "undefined" && ChangesNotifier.value !== null) {
                        $scope.events = ChangesNotifier.value;
                    }
                });
            });

            ChangesNotifier.subscribe(ChangesNotifier.SUBJ_EVENTS_ADDED, $scope, function() {
                var es = new EventsStorage();
                $scope.addScrollHandler(es, $scope.eventRequest, 'events');
            });
            ChangesNotifier.subscribe(ChangesNotifier.SUBJ_VENUES_ADDED, $scope, function() {
                var vs = new VenueStorage();
                $scope.addScrollHandler(vs, $scope.venuesRequest, 'venues');
            });
        };

        $scope.initEvents = function() {
            var events = new EventsStorage();
            var categories = new CategoriesStorage();
            return $q.when(events.load($scope.eventRequest)).then(function(eventStorage) {
                return $q.when(categories.load($scope.eventCategoriesRequest)).then(function(categoryStorage) {
                    if(typeof categoryStorage === 'undefined' || categoryStorage === null){
                        return false;
                    }
                    $scope.events = eventStorage.data;
                    $scope.eventCategories = categoryStorage.data;
                    ChangesNotifier.notify(ChangesNotifier.SUBJ_EVENTS_LOADED);
                    $scope.addScrollHandler(events, $scope.eventRequest, 'events');
                    return true;
                });
            });
        };

        $scope.initVenues = function() {
            var venues = new VenueStorage();
            var categories = new CategoriesStorage();
            return $q.when(venues.load($scope.venuesRequest)).then(function(venueStorage) {
                return $q.when(categories.load($scope.venueCategoriesRequest)).then(function(categoryStorage) {
                    $scope.venues = venueStorage.data;
                    $scope.venueCategories = categoryStorage.data;
                    ChangesNotifier.notify(ChangesNotifier.SUBJ_VENUES_LOADED);
                    $scope.addScrollHandler(venues, $scope.venuesRequest, 'venues');
                    return true;
                });
            });
        };

        $scope.initUser = function() {
            $q.when(UserService.getCurrentUser()).then(function(user) {
                $scope.user = user;
            });
        };

        $scope.addScrollHandler = function(storageInstance, requestInstance, scopeStorage) {
            $(window).scroll(function(){
                var state = $state.current.name;
                var lastElement = HtmlHelper.getCoords(document.querySelector('.catalog-wrapper:last-child'));
                var lastElementHeight = $('.catalog-wrapper:last-child').eq(0).outerHeight();
                if(window.pageYOffset + window.screen.height >= lastElement - (lastElementHeight + 50)) {
                    console.log("Reach bottom");
                    if(state === 'home.'+scopeStorage){
                        $q.when(storageInstance.load(requestInstance)).then(function(storage) {
                            if(storage && storage.data) {
                                for(var i= 0; i < storage.data.length; i++) {
                                    $scope[scopeStorage].push(storage.data[i]);
                                }
                                $scope.notifyMoreItemsLoaded(scopeStorage, storage);
                            }
                        });
                    }
                }
            });
        };
        
        $scope.notifyMoreItemsLoaded = function(scopeStorage, storage) {
            switch(scopeStorage) {
                case 'events':
                    ChangesNotifier.value = storage.data;
                    ChangesNotifier.notify(ChangesNotifier.SUBJ_EVENTS_ADDED);
                    break;

                case 'venues':
                    ChangesNotifier.venues_value = storage.data;
                    ChangesNotifier.notify(ChangesNotifier.SUBJ_VENUES_ADDED);
                    break;
            }
        };

        $scope.revealOnScroll = function () {
            var scrolled = $(window).scrollTop();

           $('.revealOnScroll.animated').each(function(i, element) {
               var __self = $(this);
               var offsetTop = __self.offset().top;
               if (scrolled + $scope.win_height_padded < offsetTop) {
                       __self.removeClass('animated');
                       __self.removeClass(__self.data('animation'));
                   }
            });
            $(".revealOnScroll:not(.animated)").each(function (i, element) {
                var __self = $(this);
                var offsetTop = __self.offset().top;
                if (scrolled + $scope.win_height_padded > offsetTop) {
                    if(__self.data('timeout') && i > 5) {
                        //window.setTimeout(function() {
                            __self.addClass('animated');
                            __self.addClass(__self.data('animation'));
                        //}, 0);
                    }
                }
            });
        };
        
        $scope.toggleInfo = function(object) {
            var element = $('#'+ object.className.toLowerCase() + '-' + object._id);
            if(element.hasClass('catalog-wrapper--full')) {
                $scope.closeInfo();
                // $scope.closeClick(event);
            } else {
                $scope.showInfo(object);
                // $scope.onClick(event);
            }
        };

        $scope.showInfo = function(object) {
            var title = "VenVast " + object.className + " - " + object.title;
            var domIdentfier = object.className.toLowerCase() +'-'+ object._id;
            $('#meta-title').attr('content', title);
            $('title').text(title);
            $('#meta-type').attr('content', "website");
            $('#meta-image').attr('content', object.picture);
            $('#meta-description').attr('content', object.description);
            $('#meta-url').attr('content', 'http://venvast.com/!/'+ domIdentfier);
            $('.catalog-wrapper').removeClass('catalog-wrapper--full', {duration: 500});
            MapService.centerOnObject(object);
            AnchorSmoothScroll.scrollTo(domIdentfier);
            $('#' + domIdentfier).addClass('catalog-wrapper--full', {duration: 500});
        };

        $scope.closeInfo = function() {
            $('.catalog-wrapper').removeClass('catalog-wrapper--full', {duration: 500});
            $scope.map = MapService.restoreMap();
        };
        
        
        $scope.init();
    }]);
})();
/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";

    /**
     * This controller is dealing with events list view
     */
    angular.module('app.controllers').controller('MapCtrl', ["$q", "$scope", "$state", "UserService", "VenVastRequest", "MapService", "ChangesNotifier", "uiGmapGoogleMapApi", "HtmlHelper", function($q, $scope, $state, UserService, VenVastRequest, MapService, ChangesNotifier, uiGmapGoogleMapApi, HtmlHelper){

        // angular.element(document).ready(function () {
            var category = $('.category');
            var stickySelector = '.sticky';
            var el = $(stickySelector);
            var stickyTop = $(stickySelector).offset().top; // returns number
            var stickyHeight = $(stickySelector).height();

            $(window).scroll(function(){
                if($('footer').offset()) {
                    var limit = $('footer').offset().top - stickyHeight - 20 - 200;
                    var windowTop = $(window).scrollTop(); // returns number
                    if (windowTop > 200 && stickyTop < windowTop){
                        el.css({ position: 'fixed', top: 0, "margin-left": "72%"});
                    }
                    else {
                        el.css({ position: '', top: "", "margin-left": ""});
                    }

                    if (limit < windowTop) {
                        var diff = limit - windowTop;
                        el.css({top: diff});
                    }
                }
            });
        // });

        $scope.initializeMap = function() {
            $q.when(MapService.initialize($scope.getStateRequest())).then(function(mapService) {
                $scope.mapService = mapService;
                uiGmapGoogleMapApi.then(function(maps) {
                    $scope.loadMarkers();
                });
            });
        };

        $scope.loadMarkers = function(objects) {
            var state = $state.current.name;
            switch (state) {
                case 'home.events':
                    $scope.loadEventMarkers(objects);
                    break;
                case 'home.venues':
                    $scope.loadVenueMarkers(objects);
                    break;
            }
       };
        
        $scope.loadEventMarkers = function(objects) {
            if(objects)
                $scope.mapService.setEventMarkers(objects);
            else
                $scope.mapService.setEventMarkers($scope.events);

            $scope.mapService.showUserMarker();
        };

        $scope.loadVenueMarkers = function(objects) {
            if(objects)
                $scope.mapService.setVenueMarkers(objects);
            else
                $scope.mapService.setVenueMarkers($scope.venues);

            $scope.mapService.showUserMarker();
        };
        

        $scope.reloadMarkers = function() {
            var state = $state.current.name;
            switch (state) {
                case 'home.events':
                    $scope.reloadEventMarkers();
                    break;
                case 'home.venues':
                    $scope.reloadVenueMarkers();
                    break;
            }
        };
        
        $scope.reloadEventMarkers = function() {
            $q.when(MapService.initialize($scope.eventRequest)).then(function(mapService) {
                $scope.mapService = mapService;
                $scope.mapService.deleteAllMarkers();
                if(typeof ChangesNotifier.value !== "undefined" && ChangesNotifier.value !== null){
                    $scope.loadMarkers(ChangesNotifier.value);
                } else {
                    $scope.loadMarkers();
                }
            });
        };
        
        $scope.reloadVenueMarkers = function() {
            $q.when(MapService.initialize($scope.venuesRequest)).then(function(mapService) {
                $scope.mapService = mapService;
                $scope.mapService.deleteAllMarkers();
                if(typeof ChangesNotifier.venues_value !== "undefined" && ChangesNotifier.venues_value !== null){
                    $scope.loadMarkers(ChangesNotifier.venues_value);
                } else {
                    $scope.loadMarkers();
                }
            });
        };
        
        $scope.addMarkers = function() {
            var state = $state.current.name;
            switch (state) {
                case 'home.events':
                    $scope.addEventMarkers();
                    break;
                case 'home.venues':
                    $scope.addVenueMarkers();
                    break;
            }
        };

        $scope.getStateRequest = function() {
            var state = $state.current.name;
            switch (state) {
                case 'home.events':
                    return $scope.eventRequest;
                    break;
                case 'home.venues':
                    return $scope.venuesRequest;
                    break;
            }
        };
        
        
        $scope.addVenueMarkers = function() {
            if(ChangesNotifier.venues_value)
                $scope.mapService.addVenueMarkers(ChangesNotifier.venues_value);
        };
        
        $scope.addEventMarkers = function() {
            if(ChangesNotifier.value)
                $scope.mapService.addEventMarkers(ChangesNotifier.value);
        };

        angular.element(document).ready(function () {
            $scope.initializeMap();
        });


        ChangesNotifier.subscribe(ChangesNotifier.SUBJ_EVENTS_LOADED, $scope, $scope.reloadMarkers);
        ChangesNotifier.subscribe(ChangesNotifier.SUBJ_VENUES_LOADED, $scope, $scope.reloadMarkers);

        ChangesNotifier.subscribe(ChangesNotifier.SUBJ_EVENTS_ADDED, $scope, $scope.addMarkers);
        ChangesNotifier.subscribe(ChangesNotifier.SUBJ_VENUES_ADDED, $scope, $scope.addMarkers);
    }]);
})();
/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";

    /**
     * This controller is dealing with venues list view
     */
    angular.module('app.controllers').controller('VenueFilterCtrl', ["$q", "$timeout", "$scope", "$rootScope", "$state", "HtmlHelper", "ChangesNotifier", "VenueStorage", function($q, $timeout, $scope, $rootScope, $state, HtmlHelper, ChangesNotifier, VenueStorage){

        var _timeout;
        $scope.textSearchInAction = false;
        $scope.textSearchfirstCall = true;

        $scope.getVenueRequest = function() {
            var state = $state.current.name;
            switch (state) {
                case 'home.venues':
                    return $scope.venuesRequest;
                    break;
                case 'organizer.venues':
                    return $scope.orgVenuesRequest;
                    break;
            }
        };

        angular.element(document).ready(function () {
            HtmlHelper.init();

            var category = $scope.getVenueRequest().category;
            var categories = $scope.venueCategories;
            var categoryName = null;
            if(category !== null && typeof categories !== 'undefined') {
                for(var i = 0; i < categories.length; i++) {
                    if(categories[i]._id == category){
                        categoryName = categories[i].name;
                    }
                }
            }
            if(categoryName !== null) {
                $timeout(function() {
                    $('#category_search').children('span').text(categoryName);
                });
            }

            new HtmlHelper.StyledDropDown('#category_search', $scope.venuesRequest.category);
            new HtmlHelper.StyledDropDown('#when_search', $scope.venuesRequest.category);
            var input = $('.location-input').first();
            $scope.autocomplete = new google.maps.places.Autocomplete(input[0], {types: ['(cities)']});
            $scope.autocomplete.addListener('place_changed', function () {
                var place = $scope.autocomplete.getPlace();
                $scope.getVenueRequest().setWhere(place.geometry.location.lat(), place.geometry.location.lng());
                $scope.search();
            });
        });

        $scope.searchByText = function () {
            if(_timeout){ //if there is already a timeout in process cancel it
                $timeout.cancel(_timeout);
            }
            _timeout = $timeout(function(){
                $scope.search();
                _timeout = null;
            }, 500);
        };


        $scope.selectCategory = function(category) {
            if(category === null){
                $scope.getVenueRequest().category = null;
                $('#category_search').children('span').text('-- All --');
            } else {
                $scope.getVenueRequest().category = category._id;
                $('#category_search').children('span').text(category.name);
            }
            $scope.search();
        };

        $scope.organizerSearch = function() {
            HtmlHelper.showLoader();
            $scope.orgVenuesRequest.page = 0;
            $scope.orgVenues = [];
            $scope.orgVenuesRequest.type = 'venues';
            $scope.orgVenuesRequest.hasMorePages = true;
            $scope.orgVenuesRequest.organizer = true;
            var venues = new VenueStorage();
            return $q.when(venues.load($scope.orgVenuesRequest)).then(function(venueStorage) {
                $scope.orgVenues = venueStorage.data;
                ChangesNotifier.org_venues = $scope.orgVenues;
                ChangesNotifier.notify(ChangesNotifier.SUBJ_ORG_VENUES_LOADED);
                HtmlHelper.hideLoader();
            });
        };

        $scope.userSearch = function() {
            HtmlHelper.showLoader();
            $scope.venuesRequest.page = 0;
            $scope.venues = [];
            $scope.venuesRequest.type = 'venues';
            $scope.venuesRequest.hasMorePages = true;
            var venues = new VenueStorage();
            return $q.when(venues.load($scope.venuesRequest)).then(function(venueStorage) {
                $scope.venues = venueStorage.data;
                ChangesNotifier.venues_value = $scope.venues;
                ChangesNotifier.notify(ChangesNotifier.SUBJ_VENUES_LOADED);
                HtmlHelper.hideLoader();
            });
        };


        $scope.search = function() {
            var state = $state.current.name;
            switch (state) {
                case 'home.venues':
                    $scope.userSearch();
                    break;
                case 'organizer.venues':
                    $scope.organizerSearch();
                    break;
            }
        };
    }]);
})();
/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";


    angular.module('app.controllers').controller('VenuesCtrl', ["$scope", "HtmlHelper", function($scope, HtmlHelper){
        $scope.$watch('venues', function() {
            if($scope.venues) {
                HtmlHelper.hideLoader();
            }
        });
    }]);
})();
/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";

    /**
     * This controller is dealing with events list view
     */
    angular.module('app.controllers').controller('OrgCalendarCtrl', ["$q", "$scope", "$timeout", "$state", "HtmlHelper", "EventsStorage", "ChangesNotifier", function($q, $scope, $timeout, $state, HtmlHelper, EventsStorage, ChangesNotifier){

        var state = $state.current.name;
        switch (state) {
            case 'organizer.events':
                $("#calendar-filter").ionCalendar({
                    lang: "en",
                    sundayFirst: false,
                    years: "2015-2018",
                    format: "YYYY-MM-DD",
                    onClick: function(date){
                        $('#when_search').children('span').text(date);
                        $timeout(function() {
                            $scope.orgEventRequest.when = date;
                        });
                        $scope.orgEventRequest.date = date;
                        $scope.search();
                    }
                });
                break;
            case 'organizer.venues':
                $("#calendar-filter").addClass('hidden');
                break;
        }

        $scope.search = function() {
            HtmlHelper.showLoader();
            $scope.orgEventRequest.page = 0;
            $scope.orgEvents = [];
            $scope.orgEventRequest.type = 'events';
            $scope.orgEventRequest.hasMorePages = true;
            var events = new EventsStorage();
            return $q.when(events.load($scope.orgEventRequest)).then(function(eventStorage) {
                console.log("Storage: ", eventStorage);
                $scope.orgEvents = eventStorage.data;
                ChangesNotifier.org_value = $scope.orgEvents;
                ChangesNotifier.notify(ChangesNotifier.SUBJ_ORG_EVENTS_LOADED, $scope.orgEvents);
                HtmlHelper.hideLoader();
            });
            // $scope.orgEventRequest.getRequestObject()
        };
    }]);
})();
(function(){
    "use strict";


    angular.module('app.controllers').controller('EventCreateCtrl', ["$scope", "$rootScope", "$timeout", "$q", "$state", "HtmlHelper", "Upload", "VenVastRequest", "EventsStorage", "VenueStorage", "Event", "Venue", "MapService", function($scope, $rootScope, $timeout, $q, $state, HtmlHelper, Upload, VenVastRequest, EventsStorage, VenueStorage, Event, Venue, MapService) {

        var _timeout;

        $scope.init = function() {
            if(!$rootScope.newEvent) {
                $scope.newEvent = {};
            } else {
                $scope.newEvent = $rootScope.newEvent;
            }
            $scope.errors = [];

            $scope.venueCreated = false;

            if($scope.orgVenues && $scope.orgVenues.length > 0) {
                $scope.newEvent.createVenue = false;
            } else {
                $scope.newEvent.createVenue = true;
            }

            $('#category').select2();
            $('#venue').select2();

            $scope.eventMap = MapService.initialize(new VenVastRequest({where: null}), 'event-map');

            $('#startAt').datetimepicker({
                timepicker:true,
                format: "Y.d.m H:i",
                startDate: moment(),
                minDate: moment(),
                onChangeDateTime: function(current, $input) {
                    $scope.newEvent.start_at = $input.val();
                }
            });



            $('#endAt').datetimepicker({
                timepicker:true,
                format: "Y.d.m H:i",
                startDate: moment().add(30, 'm'),
                minDate: moment().add(30, 'm'),
                onChangeDateTime: function(current, $input) {
                    $scope.newEvent.end_at = $input.val();
                }
            });

            HtmlHelper.hideLoader();
            $('.l-content').width('100%');

        };

        $scope.removePicture = function() {
            $scope.newEvent.picture = null;
        };

        $scope.initOrgVenuesSelect = function() {
            if(typeof $scope.newEvent.venue === 'object' && $scope.newEvent.venue !== null) {
                $('#address-search').hide();
                $scope.$watch('orgVenues', function() {
                    $('#venue').val($scope.newEvent.venue._id);
                });
            }
        };


        $scope.toggleAddressSearch = function() {
            if($scope.newEvent.venue === "" || typeof $scope.newEvent.venue === "undefined"){
                $('#address-search').show('slow');
            } else {
                $('#address-search').hide('slow');
            }
        };

        $scope.createLocation = function(lat, lng) {
            return {
                type: "Point",
                coordinates: [lng, lat]
            }
        };

        $scope.createEvent = function($event){
            $event.preventDefault();
            $scope.newEvent.categories = [$('#category').val()];
            //Means that event were picked after address search
            $q.when($scope.setEventAttributes()).then(function() {
                var event = new Event($scope.newEvent);
                $q.when(event.saveOnBackend()).then(function(response) {
                    $state.go('organizer.events', {}, {reload: true});
                });
            });
        };



        $scope.setEventAttributes = function() {
            if(typeof $scope.newEvent.address === "object" && typeof $scope.newEvent.venue === "undefined") {
                var addressObj = $scope.newEvent.address;
                var lat = addressObj.geometry.location.lat();
                var lng = addressObj.geometry.location.lng();
                $scope.newEvent.address = addressObj.formatted_address;
                $scope.newEvent.location = $scope.createLocation(lat, lng);
                if($scope.newEvent.venue_id === null || typeof $scope.newEvent.venue_id === "undefined") {
                    return $q.when($scope.createVenueFromAddress(addressObj)).then(function(venueCreated) {
                        if(venueCreated === false || venueCreated === -1 || typeof venueCreated === "undefined") {
                            var error = "Venue can't be created. Please, try again later or check again the venue data you entered in the form bellow!";
                            $scope.errors.push(error);
                            return false;
                        } else {
                            $scope.newEvent.venue_id = venueCreated._id;
                            return $scope.newEvent;
                        }
                    });

                }
            } else if(typeof $scope.newEvent.venue !== "undefined") { //event owned by user were picked
                $scope.newEvent.venue_id = $scope.newEvent.venue;
                var venueStorage = new VenueStorage();
                return $q.when(venueStorage.find({
                    _id: $scope.newEvent.venue_id
                })).then(function(existingVenues) {
                    console.log("Got existing venues: ", existingVenues);
                    var venue = existingVenues.data[0];
                    $scope.newEvent.address = venue.street;
                    $scope.newEvent.location = venue.location;
                    return $scope.newEvent;
                });
            }
        };

        //
        $scope.createVenueFromAddress = function(addressObj) {
            var venue = new Venue({
                name: addressObj.name ? addressObj.name : null,
                description: "",
                general_info: "",
                phone: addressObj.formatted_phone_number ? addressObj.formatted_phone_number : null,
                website: addressObj.website ? addressObj.website : null,
                street: addressObj.vicinity,
                location: $scope.createLocation(addressObj.geometry.location.lat(), addressObj.geometry.location.lng())
            });
            return $q.when(venue.saveOnBackend()).then(function (response) {
                if(response.status === 200 && response.data.venue !== "undefined") {
                    $scope.venueCreated = response.data.venue;
                } else {
                    $scope.venueCreated = -1;
                }
                return $scope.venueCreated;
            });
        };



        $scope.addressSelected = function(){
            if(typeof $scope.newEvent.address === "object") {
                MapService.map.setCenter($scope.newEvent.address.geometry.location);
                MapService.map.setZoom(16);
                var venueStorage = new VenueStorage();
                var addressObj = $scope.newEvent.address;
                var lat = addressObj.geometry.location.lat();
                var lng = addressObj.geometry.location.lng();
                $q.when(venueStorage.find({
                    latitude: lat,
                    longitude: lng,
                    name: addressObj.name
                })).then(function(existingVenues) {
                    if(existingVenues.data.length > 0) {
                            $scope.existingVenues = existingVenues.data;
                    }
                });

            }
        };

        $scope.nameChanged = function () {
            if(_timeout){ //if there is already a timeout in process cancel it
                $timeout.cancel(_timeout);
            }
            _timeout = $timeout(function(){
                $scope.addressSelected();
                _timeout = null;
            }, 1000);
        };

        $scope.createNewVenueChanged = function() {
            $('.catalog-item').css('border', '0px');
            if($scope.newEvent.createVenue === false) {
                $scope.newEvent.venue_id = null;
            }
        };

        $scope.pickVenue = function(event, venue) {
            $('.catalog-item').css('border', '0px');
            $scope.newEvent.venue_id = venue._id;
            $scope.newEvent.createVenue = false;
            $(event.target).closest('.catalog-item').css('border', '1px solid blue');
        };


        $scope.init();
    }]);
})();

(function(){
    "use strict";


    angular.module('app.controllers').controller('VenueCreateCtrl', ["$scope", "$rootScope", "$timeout", "$q", "$state", "HtmlHelper", "Upload", "VenVastRequest", "EventsStorage", "VenueStorage", "Event", "Venue", "MapService", function($scope, $rootScope, $timeout, $q, $state, HtmlHelper, Upload, VenVastRequest, EventsStorage, VenueStorage, Event, Venue, MapService) {

        var _timeout;

        $scope.init = function() {
            if(!$rootScope.newVenue) {
                $scope.newVenue = new Venue({});
                $scope.openingHours = $scope.newVenue.createOpeningHoursTemplate();
            } else {
                $scope.newVenue = $rootScope.newVenue;
                if($scope.newVenue.business_hours){
                    $scope.openingHours = $scope.newVenue.createOpenHours($scope.newVenue.business_hours);
                }
                else{
                    $scope.openingHours = $scope.newVenue.createOpeningHoursTemplate();
                }
            }
            $scope.errors = [];
            $('#category').select2();
            if($scope.newVenue.categories.length > 0){
                $scope.newVenue.category = $scope.newVenue.categories[0];
            }

            $scope.eventMap = MapService.initialize(new VenVastRequest({where: null}), 'venue-map');

            HtmlHelper.hideLoader();
            $('.l-content').width('100%');
            
        };

        $scope.businessHoursChecked = function(dayObject) {
            return typeof dayObject.open !== "undefined";
        };

        $scope.businessHoursToggle = function(event, dayObj) {
            var checked = $(event.target)[0].checked;
            if(checked) {
                dayObj = $scope.newVenue.getOpenHoursDayObject(dayObj.day, '0900', '2000');
                $timeout(function() {
                    $scope.openingHours[dayObj.day] = dayObj;
                });
                console.log(dayObj);
            } else {
                dayObj.open = false;
                dayObj.close = false;
            }
        };


        $scope.removePicture = function() {
            $scope.newVenue.cover_image = null;
        };
        

        $scope.createLocation = function(lat, lng) {
            return {
                type: "Point",
                coordinates: [lng, lat]
            }
        };

        $scope.createVenue = function($event){
            $event.preventDefault();
            $scope.newVenue.categories = [$('#category').val()];
            $scope.newVenue.business_hours = $scope.openingHours;
            $scope.newVenue.address = $scope.newVenue.address.formatted_address;
            $scope.newVenue.street = $scope.newVenue.address;
            $q.when($scope.newVenue.saveOnBackend()).then(function(response) {
                $state.go('organizer.venues', {}, {reload: true});
            });
        };


        $scope.addressSelected = function(){
            if(typeof $scope.newVenue.address === "object") {
                MapService.map.setCenter($scope.newVenue.address.geometry.location);
                MapService.map.setZoom(16);
                var venueStorage = new VenueStorage();
                var addressObj = $scope.newVenue.address;
                var lat = addressObj.geometry.location.lat();
                var lng = addressObj.geometry.location.lng();
                $scope.newVenue.location = $scope.createLocation(lat, lng);
            }
        };

        $scope.toggleSelection = function(event, category) {
            var checked = $(event.target)[0].checked;
            var categories = $scope.newVenue.eventCategories;
            if(checked) {
                if(typeof categories === "undefined")
                    categories = [];
                categories.push(category._id);
            } else {
                if(typeof categories !== "undefined" && categories.length > 0) {
                    var index = categories.indexOf(category._id);
                    categories.splice(index, 1);
                }
            }
            $scope.newVenue.eventCategories = categories;
        };

        $scope.init();
    }]);
})();

/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";
    angular.module('app.controllers').controller('OrgHeaderCtrl', ["$scope", "$rootScope", "$timeout", "$q", "$state", "$location", "HtmlHelper", "UserService", "Facebook", function($scope, $rootScope, $timeout, $q, $state, $location, HtmlHelper, UserService, Facebook){


        $scope.highlightMenuItem = function () {
            var state = $state.current.name;
            var eventLi = $('#menu-events').closest('li');
            var venueLi = $('#menu-venues').closest('li');
            var dealLi = $('#menu-deals').closest('li');
            var hostsLi = $('#menu-hosts').closest('li');
            switch(state){
                case 'organizer.events':
                    venueLi.removeClass('active');
                    dealLi.removeClass('active');
                    hostsLi.removeClass('active');
                    eventLi.addClass('active');
                    break;
                case 'organizer.venues':
                    dealLi.removeClass('active');
                    eventLi.removeClass('active');
                    hostsLi.removeClass('active');
                    venueLi.addClass('active');
                    break;
                case  'organizer.deals':
                    eventLi.removeClass('active');
                    venueLi.removeClass('active');
                    hostsLi.removeClass('active');
                    dealLi.addClass('active');
                    break;
                case 'organizer.hosts':
                    eventLi.removeClass('active');
                    venueLi.removeClass('active');
                    dealLi.removeClass('active');
                    hostsLi.addClass('active');
                    break;
            }
        };

        /**
         * Log current user out
         */
        $scope.logout = function() {
            $q.when(UserService.logout()).then(function() {
                $timeout(function() {
                    $scope.user = null;
                    $state.go('home.events');
                });
            });
        };

        $(document).ready($scope.highlightMenuItem);
        $rootScope.$on('$stateChangeSuccess', $scope.highlightMenuItem);
    }]);
})();
/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";


    angular.module('app.controllers').controller('OrgHomeCtrl', ["$q", "$scope", "$timeout", "$window", "$anchorScroll", "$state", "AnchorSmoothScroll", "HtmlHelper", "EventsStorage", "VenueStorage", "Event", "UserService", "VenVastRequest", "MapService", "ChangesNotifier", "CategoriesStorage", function($q, $scope, $timeout, $window, $anchorScroll, $state, AnchorSmoothScroll, HtmlHelper, EventsStorage, VenueStorage, Event, UserService, VenVastRequest, MapService, ChangesNotifier, CategoriesStorage){

        $scope.init = function(){
            console.log("Init called");

            HtmlHelper.showLoader();
            $scope.win_height_padded = $(window).height() * 1.34;
            $window.onscroll = $scope.revealOnScroll;
            $scope.orgEventRequest = new VenVastRequest({when: "week", organizer: true});
            $scope.orgVenuesRequest = new VenVastRequest({organizer: true});
            $scope.hostsRequest = new VenVastRequest({hosts: true});

            $q.when($scope.initUser()).then(function(user) {

                $scope.initEvents();
                $scope.initVenues();
                $scope.initHosts();
            });

            ChangesNotifier.subscribe(ChangesNotifier.SUBJ_ORG_VENUES_LOADED, $scope, function() {
                $timeout(function() {
                    if(typeof  ChangesNotifier.org_venues !== "undefined" && ChangesNotifier.org_venues !== null)
                        $scope.orgVenues = ChangesNotifier.org_venues;
                });
            });
            ChangesNotifier.subscribe(ChangesNotifier.SUBJ_ORG_EVENTS_LOADED, $scope, function () {
                $timeout(function () {
                    if(typeof  ChangesNotifier.org_value !== "undefined" && ChangesNotifier.org_value !== null)
                        $scope.orgEvents = ChangesNotifier.org_value;
                });
            });
        };

        $scope.initEvents = function() {
            var events = new EventsStorage();
            var categories = new CategoriesStorage();
            $scope.eventCategoriesRequest = new VenVastRequest({type: 'events'});
            return $q.when(events.load($scope.orgEventRequest)).then(function(eventStorage) {
                return $q.when(categories.load($scope.eventCategoriesRequest)).then(function(categoryStorage) {
                    $scope.orgEvents = eventStorage.data;
                    $scope.eventCategories = categoryStorage.data;
                    ChangesNotifier.org_value = $scope.orgEvents;
                    ChangesNotifier.notify(ChangesNotifier.SUBJ_ORG_EVENTS_LOADED);
                    $scope.addScrollHandler(events, $scope.orgEventRequest, 'orgEvents');
                    return true;
                });
            });
        };

        $scope.initVenues = function() {
            var venues = new VenueStorage();
            var categories = new CategoriesStorage();
            $scope.venueCategoriesRequest = new VenVastRequest({type: 'venues'});
            return $q.when(venues.load($scope.orgVenuesRequest)).then(function(venueStorage) {
                return $q.when(categories.load($scope.venueCategoriesRequest)).then(function(categoryStorage) {
                    $scope.orgVenues = venueStorage.data;
                    $scope.venueCategories = categoryStorage.data;
                    ChangesNotifier.org_venues = $scope.orgVenues;
                    ChangesNotifier.notify(ChangesNotifier.SUBJ_ORG_VENUES_LOADED);
                    $scope.addScrollHandler(venues, $scope.orgVenuesRequest, 'orgVenues');
                    return true;
                });
            });
        };

        $scope.initHosts = function() {
            var hosts = new VenueStorage();
            var categories = new CategoriesStorage();
            return $q.when(hosts.load($scope.hostsRequest)).then(function(venueStorage) {
                $scope.hosts = venueStorage.data;
                ChangesNotifier.hosts = $scope.hosts;
                // ChangesNotifier.notify(ChangesNotifier.SUBJ_ORG_VENUES_LOADED);
                $scope.addScrollHandler(hosts, $scope.hostsRequest, 'hosts');
                return true;
            });
        };

        $scope.initUser = function() {
            $q.when(UserService.getCurrentUser()).then(function(user) {
                if(user === null || typeof user === "undefined")
                    return $state.go('home.events', {}, {reload: true});
                
                $scope.user = user;
            });
        };

        $scope.addScrollHandler = function(storageInstance, requestInstance, scopeStorage) {
            $(window).scroll(function(){
                var lastElement = HtmlHelper.getCoords(document.querySelector('.catalog-wrapper:last-child'));
                if(window.pageYOffset + window.screen.height >= lastElement - 800) {
                    $q.when(storageInstance.load(requestInstance)).then(function(storage) {
                        if(storage && storage.data) {
                            for(var i= 0; i < storage.data.length; i++) {
                                $scope[scopeStorage].push(storage.data[i]);
                            }
                            $scope.notifyMoreItemsLoaded(scopeStorage, storage);
                        }
                    });
                }
            });
        };
        
        $scope.notifyMoreItemsLoaded = function(scopeStorage, storage) {
            switch(scopeStorage) {
                case 'orgEvents':
                    ChangesNotifier.org_value = storage.data;
                    ChangesNotifier.notify(ChangesNotifier.SUBJ_EVENTS_ADDED);
                    break;

                case 'orgVenues':
                    ChangesNotifier.org_venues = storage.data;
                    ChangesNotifier.notify(ChangesNotifier.SUBJ_VENUES_ADDED);
                    break;
            }
        };

        $scope.revealOnScroll = function () {
            var scrolled = $(window).scrollTop();

           $('.revealOnScroll.animated').each(function(i, element) {
               var __self = $(this);
               var offsetTop = __self.offset().top;
               if (scrolled + $scope.win_height_padded < offsetTop) {
                       __self.removeClass('animated');
                       __self.removeClass(__self.data('animation'));
                   }
            });

            $(".revealOnScroll:not(.animated)").each(function (i, element) {
                var __self = $(this);
                var offsetTop = __self.offset().top;
                if (scrolled + $scope.win_height_padded > offsetTop) {
                    if(__self.data('timeout') && i > 5) {
                        __self.addClass('animated');
                        __self.addClass(__self.data('animation'));
                    }
                }
            });
        };
        
        $scope.toggleInfo = function(object) {
            var element = $('#'+ object.className.toLowerCase() + '-' + object._id);
            if(element.hasClass('catalog-wrapper--full')) {
                $scope.closeInfo();
            } else {
                $scope.showInfo(object);
            }
        };

        $scope.showInfo = function(object) {
            var title = "VenVast " + object.className + " - " + object.title;
            var domIdentfier = object.className.toLowerCase() +'-'+ object._id;
            $('#meta-title').attr('content', title);
            $('title').text(title);
            $('#meta-type').attr('content', "website");
            $('#meta-image').attr('content', object.picture);
            $('#meta-description').attr('content', object.description);
            $('#meta-url').attr('content', 'http://venvast.com/!/'+ domIdentfier);
            $('.catalog-wrapper').removeClass('catalog-wrapper--full', {duration: 500});
            MapService.centerOnObject(object);
            AnchorSmoothScroll.scrollTo(domIdentfier);
            $('#' + domIdentfier).addClass('catalog-wrapper--full', {duration: 500});
        };

        $scope.closeInfo = function() {
            $('.catalog-wrapper').removeClass('catalog-wrapper--full', {duration: 500});
            $scope.map = MapService.restoreMap();
        };
        
        
        $scope.init();
    }]);
})();
/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";


    angular.module('app.controllers').controller('HostsCtrl', ["$scope", "$state", "HtmlHelper", function($scope, $state, HtmlHelper){

        $scope.openVenueCreation = function() {
            $state.go('organizer.create_venue');
        };
        
        $scope.$watch('hosts', function() {
            if($scope.hosts) {
                console.log("Hosts: ", $scope.hosts);
                for(var i = 0; i < $scope.hosts.length; i++) {
                    var distance = $scope.hosts[i].computeDistanceTo($scope.hostsRequest.getUserPosition());
                    var d = $scope.hosts[i].computeDistanceTo($scope.hostsRequest.getUserPosition());
                    if(distance > 30) {
                        distance = '> 30';
                    }
                    $scope.hosts[i].distance = distance ? distance + ' km away' : '';
                    $scope.hosts[i].d = d;
                }
                $scope.hosts.sort(function(a,b) {
                    return a.d - b.d;
                });

                HtmlHelper.hideLoader();
            }
        });
    }]);
})();
/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";

    /**
     * This controller is dealing with events list view
     */
    angular.module('app.controllers').controller('OrgMapCtrl', ["$q", "$scope", "$state", "UserService", "VenVastRequest", "MapService", "ChangesNotifier", "uiGmapGoogleMapApi", function($q, $scope, $state, UserService, VenVastRequest, MapService, ChangesNotifier, uiGmapGoogleMapApi){


        var category = $('.category');
        var stickySelector = '.sticky';
        var el = $(stickySelector);
        var stickyTop = $(stickySelector).offset().top; // returns number
        var stickyHeight = $(stickySelector).height();

        $(window).scroll(function(){
            var limit = $('footer').offset().top - stickyHeight - 20 - 200;
            var windowTop = $(window).scrollTop(); // returns number
            if (windowTop > 200 && stickyTop < windowTop){
                el.css({ position: 'fixed', top: 0, "margin-left": "72%"});
            }
            else {
                el.css({ position: '', top: "", "margin-left": ""});
            }

            if (limit < windowTop) {
                var diff = limit - windowTop;
                el.css({top: diff});
            }
        });

        $scope.initializeMap = function() {
            $q.when(MapService.initialize($scope.getStateRequest(), 'org-map-canvas')).then(function(mapService) {
                $scope.orgMapService = mapService;
                uiGmapGoogleMapApi.then(function(maps) {
                    $scope.loadMarkers();
                });
            });
        };

        $scope.loadMarkers = function(objects) {
            var state = $state.current.name;
            switch (state) {
                case 'organizer.events':
                    $scope.loadEventMarkers(objects);
                    break;
                case 'organizer.venues':
                    $scope.loadVenueMarkers(objects);
                    break;
                case 'organizer.hosts':
                    $scope.loadHostsMarkers(objects);
                    break;
            }
       };

        $scope.loadEventMarkers = function(objects) {
            if(objects && typeof objects !== "undefined"){
                $scope.orgMapService.setEventMarkers(objects);
            } else {
                $scope.orgMapService.setEventMarkers($scope.orgEvents);
            }

            $scope.orgMapService.showUserMarker();
        };

        $scope.loadVenueMarkers = function(objects) {
            if(objects)
                $scope.orgMapService.setVenueMarkers(objects);
            else
                $scope.orgMapService.setVenueMarkers($scope.orgVenues);

            $scope.orgMapService.showUserMarker();
        };

        $scope.loadHostsMarkers = function(objects) {
            if(objects)
                $scope.orgMapService.setVenueMarkers(objects);
            else
                $scope.orgMapService.setVenueMarkers($scope.hosts);

            $scope.orgMapService.showUserMarker();
        };
        

        $scope.reloadMarkers = function() {
            var state = $state.current.name;
            switch (state) {
                case 'organizer.events':
                    $scope.reloadEventMarkers();
                    break;
                case 'organizer.venues':
                    $scope.reloadVenueMarkers();
                    break;
            }
        };
        
        $scope.reloadEventMarkers = function() {
             $q.when(MapService.initialize($scope.getStateRequest(), 'org-map-canvas')).then(function(mapService) {
                 $scope.orgMapService = mapService;
                $scope.orgMapService.deleteAllMarkers();
                if(typeof ChangesNotifier.org_value !== "undefined" && ChangesNotifier.org_value !== null){
                    $scope.loadMarkers(ChangesNotifier.org_value);
                } else {
                    $scope.loadMarkers();
                }
             });
        };
        
        $scope.reloadVenueMarkers = function() {
            $q.when(MapService.initialize($scope.getStateRequest(), 'org-map-canvas')).then(function(mapService) {
                $scope.orgMapService = mapService;
                $scope.orgMapService.deleteAllMarkers();
                if(typeof ChangesNotifier.org_venues !== "undefined" && ChangesNotifier.org_venues !== null){
                    $scope.loadMarkers(ChangesNotifier.org_venues);
                } else {
                    $scope.loadMarkers();
                }
            });
        };
        
        $scope.addMarkers = function() {
            var state = $state.current.name;
            switch (state) {
                case 'organizer.events':
                    $scope.addEventMarkers();
                    break;
                case 'organizer.venues':
                    $scope.addVenueMarkers();
                    break;
            }
        };

        $scope.getStateRequest = function() {
            var state = $state.current.name;
            switch (state) {
                case 'organizer.events':
                    return $scope.orgEventRequest;
                    break;
                case 'organizer.venues':
                    return $scope.orgVenuesRequest;
                    break;
                case 'organizer.hosts':
                    return $scope.hostsRequest;
                    break;
            }
        };
        
        
        $scope.addVenueMarkers = function() {
            if(ChangesNotifier.org_venues)
                $scope.orgMapService.addVenueMarkers(ChangesNotifier.org_venues);
        };
        
        $scope.addEventMarkers = function() {
            if(ChangesNotifier.org_value)
                $scope.orgMapService.addEventMarkers(ChangesNotifier.org_venues);
        };

        angular.element(document).ready(function () {
            $scope.initializeMap();
        });


        ChangesNotifier.subscribe(ChangesNotifier.SUBJ_ORG_EVENTS_LOADED, $scope, $scope.reloadMarkers);
        ChangesNotifier.subscribe(ChangesNotifier.SUBJ_ORG_VENUES_LOADED, $scope, $scope.reloadMarkers);

        ChangesNotifier.subscribe(ChangesNotifier.SUBJ_ORG_EVENTS_ADDED, $scope, $scope.addMarkers);
        ChangesNotifier.subscribe(ChangesNotifier.SUBJ_ORG_VENUES_ADDED, $scope, $scope.addMarkers);
    }]);
})();
/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";


    angular.module('app.controllers').controller('DealsCtrl', ["$scope", "$state", "UserService", "EventService", "MapService", function($scope, $state, UserService, EventService, MapService){
        //$scope.showingDescription = null;
        //$scope.showingEvent = null;
        $scope.init = function(){
            $scope.events = {};
            UserService.getCurrentUser(function(user) {
                $scope.user = user;
            });
            EventService.list(function(events){
                $scope.events.objects = events;
            });
            MapService.requestMap(function(map) {
                $scope.map = map;
            });
        };

        //$scope.showInfo = function(element, event){
        //    if($scope.showingDescription === null && $scope.showingEvent === null){
        //        $('.event-description').removeClass('active');
        //        $('.event').hide();
        //        var eventBlock = $(element.target).closest('.event').eq(0);
        //        eventBlock.show();
        //        $('#venue-'+event.id).addClass('active');
        //        $scope.showingDescription = true;
        //        $scope.showingEvent = true;
        //    } else {
        //        $('.event-description').removeClass('active');
        //        $('.event').show('slow');
        //        $scope.showingDescription = null;
        //        $scope.showingEvent = null;
        //    }
        //
        //};
        $scope.init();
    }]);
})();
/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";

    /**
     * This controller is dealing with events list view
     */
    angular.module('app.controllers').controller('OrgEventsCtrl', ["$q", "$rootScope", "$scope", "$state", "HtmlHelper", "ChangesNotifier", function($q, $rootScope, $scope, $state, HtmlHelper, ChangesNotifier){

        // $('.l-content').width('72%');
        
        $scope.editEvent = function ($event, event) {
            $event.stopPropagation();
            $rootScope.newEvent = event;
            $state.go('organizer.create_event', {});
        };

        $scope.deleteEvent = function ($event, event) {
            $event.stopPropagation();
            for(var i = 0; i < $scope.orgEvents.length; i++) {
                if($scope.orgEvents[i]._id === event._id) {
                    $scope.orgEvents.splice(i, 1);
                }
            }
            event.deleteFromBackend();
        };
        
        $scope.openEventCreation = function() {
            $state.go('organizer.create_event');
        };
        

        $scope.$watch('events', function() {
            if($scope.events) {
                for(var i = 0; i < $scope.events.length; i++) {
                    var distance = $scope.events[i].computeDistanceTo($scope.eventRequest.getUserPosition());
                    if(distance > 30) {
                        distance = '> 30';
                    }
                    $scope.events[i].distance = distance ? distance + ' km away' : '';
                }
                HtmlHelper.hideLoader();
            }
        });
    }]);
})();
/**
 * Created by oem on 3/19/16.
 */
(function(){
    "use strict";


    angular.module('app.controllers').controller('OrgVenuesCtrl', ["$scope", "$rootScope", "$state", "HtmlHelper", function($scope, $rootScope, $state, HtmlHelper){

        // $('.l-content').width('72%');

        $rootScope.newVenue = null;
        
        $scope.openVenueCreation = function() {
            $state.go('organizer.create_venue');
        };

        $scope.editVenue = function(event, venue) {
            event.stopPropagation();
            $rootScope.newVenue = venue;
            $state.go('organizer.create_venue', {});
        };

        $scope.deleteVenue = function(event, venue) {
            event.stopPropagation();
            for(var i = 0; i < $scope.orgVenues.length; i++) {
                if($scope.orgVenues[i]._id === venue._id) {
                    $scope.orgVenues.splice(i, 1);
                }
            }
            venue.deleteFromBackend();
        };
        
        $scope.$watch('orgVenues', function() {
            if($scope.orgVenues) {
                for(var i = 0; i < $scope.orgVenues.length; i++) {
                    var distance = $scope.orgVenues[i].computeDistanceTo($scope.orgVenuesRequest.getUserPosition());
                    var d = $scope.orgVenues[i].computeDistanceTo($scope.orgVenuesRequest.getUserPosition());
                    if(distance > 30) {
                        distance = '> 30';
                    }
                    $scope.orgVenues[i].distance = distance ? distance + ' km away' : '';
                    $scope.orgVenues[i].d = d;
                }
                $scope.orgVenues.sort(function(a,b) {
                    return a.d - b.d;
                });

                HtmlHelper.hideLoader();
            }
        });
    }]);
})();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4uanMiLCJyb3V0ZXMuanMiLCJzZXJ2aWNlcy9jYXRlZ29yaWVzX3N0b3JhZ2UuanMiLCJzZXJ2aWNlcy9jYXRlZ29yeS5qcyIsInNlcnZpY2VzL2NoYW5nZXNfbm90aWZpZXIuanMiLCJzZXJ2aWNlcy9kYXRlLmpzIiwic2VydmljZXMvZXZlbnQuanMiLCJzZXJ2aWNlcy9ldmVudF9zdG9yYWdlLmpzIiwic2VydmljZXMvaHRtbF9oZWxwZXIuanMiLCJzZXJ2aWNlcy9tYXAuanMiLCJzZXJ2aWNlcy9zbW9vdGhfc2Nyb2xsLmpzIiwic2VydmljZXMvdXNlci5qcyIsInNlcnZpY2VzL3VzZXJfc2VydmljZS5qcyIsInNlcnZpY2VzL3ZlbnVlLmpzIiwic2VydmljZXMvdmVudWVfc3RvcmFnZS5qcyIsInNlcnZpY2VzL3ZlbnZhc3RfcmVxdWVzdC5qcyIsImFwcC9jYWxlbmRhci9jYWxlbmRhci5qcyIsImFwcC9kZWFscy9kZWFscy5qcyIsImFwcC9ldmVudF9jcmVhdGUvZXZlbnRfY3JlYXRlLmpzIiwiYXBwL2V2ZW50X2ZpbHRlci9ldmVudF9maWx0ZXIuanMiLCJhcHAvZXZlbnRzL2V2ZW50cy5qcyIsImFwcC9oZWFkZXIvaGVhZGVyLmpzIiwiYXBwL2hvbWUvaG9tZS5qcyIsImFwcC9tYXAvbWFwLmpzIiwiYXBwL3ZlbnVlX2ZpbHRlci92ZW51ZV9maWx0ZXIuanMiLCJhcHAvdmVudWVzL3ZlbnVlcy5qcyIsImFwcC9vcmcvY2FsZW5kYXIvY2FsZW5kYXIuanMiLCJhcHAvb3JnL2NyZWF0ZV9ldmVudC9jcmVhdGVfZXZlbnQuanMiLCJhcHAvb3JnL2NyZWF0ZV92ZW51ZS9jcmVhdGVfdmVudWUuanMiLCJhcHAvb3JnL2hlYWRlci9oZWFkZXIuanMiLCJhcHAvb3JnL2hvbWUvaG9tZS5qcyIsImFwcC9vcmcvaG9zdHMvaG9zdHMuanMiLCJhcHAvb3JnL21hcC9tYXAuanMiLCJhcHAvb3JnL2RlYWxzL2RlYWxzLmpzIiwiYXBwL29yZy9ldmVudHMvZXZlbnRzLmpzIiwiYXBwL29yZy92ZW51ZXMvdmVudWVzLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLENBQUEsVUFBQTtJQUNBO0lBQ0EsSUFBQSxNQUFBLFFBQUEsT0FBQTtRQUNBO1lBQ0E7WUFDQTtZQUNBO1lBQ0E7WUFDQTtZQUNBO1lBQ0E7WUFDQTtZQUNBO1lBQ0E7WUFDQTtZQUNBO1lBQ0E7WUFDQTtZQUNBO1lBQ0E7K0dBQ0EsU0FBQSxzQkFBQSxtQkFBQSxrQkFBQSxlQUFBLGNBQUE7WUFDQSxxQkFBQSxZQUFBO1lBQ0EscUJBQUEsVUFBQTtZQUNBLGlCQUFBLEtBQUEsRUFBQSxjQUFBO1lBQ0EsY0FBQSxhQUFBLEtBQUEsQ0FBQSxNQUFBLGFBQUEsaUJBQUEsVUFBQSxJQUFBLFdBQUEsZUFBQTtnQkFDQSxPQUFBO29CQUNBLFdBQUEsVUFBQSxRQUFBO3dCQUNBLE9BQUEsVUFBQSxPQUFBLFdBQUE7d0JBQ0EsT0FBQSxTQUFBLE9BQUEsVUFBQTt3QkFDQSxJQUFBLGNBQUEsT0FBQTs7NEJBRUEsT0FBQSxPQUFBLFFBQUEsY0FBQTs7d0JBRUEsT0FBQTs7Ozs7OztJQU9BLFFBQUEsT0FBQSxjQUFBLENBQUE7SUFDQSxRQUFBLE9BQUEsbUJBQUEsQ0FBQSxhQUFBLGVBQUEsZ0JBQUEsWUFBQSxxQkFBQSxlQUFBLGlCQUFBLGdCQUFBO0lBQ0EsUUFBQSxPQUFBLGVBQUE7SUFDQSxRQUFBLE9BQUEsZ0JBQUEsQ0FBQSxlQUFBLGFBQUEsWUFBQTtJQUNBLFFBQUEsT0FBQSxrQkFBQTtJQUNBLFFBQUEsT0FBQSxjQUFBLENBQUE7O0FDN0NBLENBQUEsVUFBQTtJQUNBOztJQUVBLFFBQUEsT0FBQSxjQUFBLHNFQUFBLFNBQUEsZ0JBQUEsb0JBQUEsb0JBQUE7O1FBRUEsSUFBQSxVQUFBLFVBQUEsVUFBQTtZQUNBLE9BQUEsZ0JBQUEsV0FBQSxNQUFBLFdBQUE7OztRQUdBLElBQUEsYUFBQSxTQUFBLFVBQUE7WUFDQSxPQUFBLG9CQUFBLFdBQUEsTUFBQSxXQUFBOztRQUVBLGtCQUFBLFVBQUEsTUFBQSxXQUFBO1FBQ0EsbUJBQUEsVUFBQTtRQUNBO2FBQ0EsTUFBQSxRQUFBO2dCQUNBLEtBQUE7Z0JBQ0EsT0FBQTtvQkFDQSxNQUFBO3dCQUNBLGFBQUEsUUFBQTs7O2dCQUdBLFVBQUE7O2FBRUEsTUFBQSxnQkFBQTtnQkFDQSxVQUFBOztnQkFFQSxLQUFBO2dCQUNBLHdDQUFBLFVBQUEsV0FBQSxZQUFBO29CQUNBLElBQUEsT0FBQSxVQUFBLE9BQUEsT0FBQTtvQkFDQSxJQUFBLFdBQUEsS0FBQSxNQUFBO29CQUNBLElBQUEsU0FBQTtvQkFDQSxRQUFBLElBQUE7Ozs7Ozs7Ozs7O2FBV0EsTUFBQSxlQUFBO2dCQUNBLEtBQUE7Z0JBQ0EsT0FBQTtvQkFDQSxNQUFBO3dCQUNBLGFBQUEsUUFBQTs7b0JBRUEsV0FBQTt3QkFDQSxhQUFBLFFBQUE7O29CQUVBLFdBQUE7d0JBQ0EsYUFBQSxRQUFBOztvQkFFQSxTQUFBO3dCQUNBLGFBQUEsUUFBQTs7OzthQUlBLE1BQUEsZUFBQTtnQkFDQSxLQUFBO2dCQUNBLE9BQUE7b0JBQ0EsTUFBQTt3QkFDQSxhQUFBLFFBQUE7O29CQUVBLFdBQUE7d0JBQ0EsYUFBQSxRQUFBOztvQkFFQSxXQUFBO3dCQUNBLGFBQUEsUUFBQTs7b0JBRUEsU0FBQTt3QkFDQSxhQUFBLFFBQUE7Ozs7YUFJQSxNQUFBLGNBQUE7Z0JBQ0EsS0FBQTtnQkFDQSxPQUFBO29CQUNBLE1BQUE7d0JBQ0EsYUFBQSxRQUFBOztvQkFFQSxXQUFBO3dCQUNBLGFBQUEsUUFBQTs7b0JBRUEsV0FBQTt3QkFDQSxhQUFBLFFBQUE7Ozs7YUFJQSxNQUFBLGFBQUE7Z0JBQ0EsS0FBQTtnQkFDQSxPQUFBO29CQUNBLE1BQUE7d0JBQ0EsYUFBQSxXQUFBOzs7Z0JBR0EsVUFBQTs7YUFFQSxNQUFBLG9CQUFBO2dCQUNBLEtBQUE7Z0JBQ0EsT0FBQTtvQkFDQSxNQUFBO3dCQUNBLGFBQUEsV0FBQTs7b0JBRUEsV0FBQTt3QkFDQSxhQUFBLFdBQUE7O29CQUVBLFdBQUE7d0JBQ0EsYUFBQSxXQUFBOztvQkFFQSxTQUFBO3dCQUNBLGFBQUEsUUFBQTs7OzthQUlBLE1BQUEsMEJBQUE7Z0JBQ0EsS0FBQTtnQkFDQSxPQUFBO29CQUNBLE1BQUE7d0JBQ0EsYUFBQSxXQUFBOztvQkFFQSxXQUFBO3dCQUNBLGFBQUEsV0FBQTs7OzthQUlBLE1BQUEsb0JBQUE7Z0JBQ0EsS0FBQTtnQkFDQSxPQUFBO29CQUNBLE1BQUE7d0JBQ0EsYUFBQSxXQUFBOztvQkFFQSxXQUFBO3dCQUNBLGFBQUEsV0FBQTs7b0JBRUEsV0FBQTt3QkFDQSxhQUFBLFdBQUE7O29CQUVBLFNBQUE7d0JBQ0EsYUFBQSxRQUFBOzs7O2FBSUEsTUFBQSxtQkFBQTtnQkFDQSxLQUFBO2dCQUNBLE9BQUE7b0JBQ0EsTUFBQTt3QkFDQSxhQUFBLFdBQUE7O29CQUVBLFdBQUE7d0JBQ0EsYUFBQSxXQUFBOztvQkFFQSxXQUFBO3dCQUNBLGFBQUEsV0FBQTs7Ozs7OzthQU9BLE1BQUEsMEJBQUE7Z0JBQ0EsS0FBQTtnQkFDQSxPQUFBO29CQUNBLE1BQUE7d0JBQ0EsYUFBQSxXQUFBOztvQkFFQSxXQUFBO3dCQUNBLGFBQUEsV0FBQTs7OzthQUlBLE1BQUEsbUJBQUE7Z0JBQ0EsS0FBQTtnQkFDQSxPQUFBO29CQUNBLE1BQUE7d0JBQ0EsYUFBQSxXQUFBOztvQkFFQSxXQUFBO3dCQUNBLGFBQUEsV0FBQTs7b0JBRUEsV0FBQTt3QkFDQSxhQUFBLFdBQUE7O29CQUVBLFNBQUE7d0JBQ0EsYUFBQSxRQUFBOzs7Ozs7QUMxTEEsQ0FBQSxVQUFBO0lBQ0E7OztJQUdBLFFBQUEsT0FBQSxnQkFBQSxRQUFBLDJDQUFBLFNBQUEsT0FBQSxTQUFBOzs7Ozs7O1FBT0EsU0FBQSxrQkFBQSxpQkFBQTtZQUNBLEdBQUEsbUJBQUEsZ0JBQUEsU0FBQSxFQUFBO2dCQUNBLEtBQUEsSUFBQSxJQUFBLEdBQUEsSUFBQSxnQkFBQSxRQUFBLEtBQUE7b0JBQ0EsS0FBQSxFQUFBLGdCQUFBLGNBQUEsWUFBQTt3QkFDQSxnQkFBQSxLQUFBLElBQUEsU0FBQSxnQkFBQTs7O2dCQUdBLEtBQUEsVUFBQTs7U0FFQTs7UUFFQSxrQkFBQSxZQUFBO1lBQ0EsWUFBQTtZQUNBLFlBQUE7WUFDQSxXQUFBO1lBQ0EsTUFBQTtZQUNBLFNBQUE7O1lBRUEsV0FBQSxTQUFBLGlCQUFBO2dCQUNBLEtBQUEsT0FBQTs7Ozs7O1lBTUEsTUFBQSxTQUFBLGVBQUE7Z0JBQ0EsSUFBQSxRQUFBO2dCQUNBLEdBQUEsS0FBQSxZQUFBLE9BQUE7b0JBQ0EsS0FBQSxVQUFBO29CQUNBLE9BQUEsTUFBQSxJQUFBLFlBQUEsQ0FBQSxRQUFBLGdCQUFBLFFBQUEsU0FBQSxjQUFBO3dCQUNBLElBQUEsSUFBQSxJQUFBLEdBQUEsSUFBQSxhQUFBLFFBQUEsS0FBQTs0QkFDQSxhQUFBLEtBQUEsSUFBQSxTQUFBLGFBQUE7O3dCQUVBLE1BQUEsVUFBQTt3QkFDQSxNQUFBLFVBQUE7d0JBQ0EsT0FBQTs7OztZQUlBLGFBQUEsU0FBQSxPQUFBLFFBQUE7Z0JBQ0EsT0FBQSx1QkFBQSxLQUFBLFNBQUEsS0FBQTs7OztRQUlBLE9BQUE7OztBQ3ZEQSxDQUFBLFVBQUE7SUFDQTs7O0FBR0EsUUFBQSxPQUFBLGdCQUFBLFFBQUEsZ0NBQUEsU0FBQSxRQUFBLE1BQUE7O0lBRUEsU0FBQSxTQUFBLGNBQUE7UUFDQSxJQUFBLGNBQUE7WUFDQSxLQUFBLFFBQUE7OztLQUdBOztJQUVBLFNBQUEsWUFBQTtRQUNBLFNBQUEsU0FBQSxjQUFBO1lBQ0EsUUFBQSxPQUFBLE1BQUE7Ozs7Ozs7O1FBUUEsTUFBQSxTQUFBLFlBQUE7WUFDQSxJQUFBLFFBQUE7WUFDQSxPQUFBLE1BQUEsSUFBQSxjQUFBLFlBQUEsUUFBQSxTQUFBLGNBQUE7Z0JBQ0EsTUFBQSxRQUFBO2dCQUNBLE9BQUE7Ozs7UUFJQSxhQUFBLFdBQUE7WUFDQSxPQUFBLHVEQUFBLEtBQUE7OztJQUdBLE9BQUE7OztBQ25DQSxDQUFBLFdBQUE7SUFDQTs7SUFFQSxRQUFBLE9BQUEsZ0JBQUEsUUFBQSxrQ0FBQSxTQUFBLFlBQUE7UUFDQSxPQUFBOztZQUVBLG9CQUFBO1lBQ0Esb0JBQUE7WUFDQSxtQkFBQTs7WUFFQSxzQkFBQTtZQUNBLHNCQUFBO1lBQ0EscUJBQUE7O1lBRUEsbUJBQUE7WUFDQSxtQkFBQTtZQUNBLGtCQUFBOzs7WUFHQSx3QkFBQTtZQUNBLHdCQUFBO1lBQ0EsdUJBQUE7O1lBRUEsMEJBQUE7WUFDQSwwQkFBQTtZQUNBLHlCQUFBOztZQUVBLHVCQUFBO1lBQ0EsdUJBQUE7WUFDQSxzQkFBQTs7WUFFQSxPQUFBO1lBQ0EsV0FBQTtZQUNBLFlBQUE7WUFDQSxjQUFBOztZQUVBLFdBQUEsU0FBQSxTQUFBLE9BQUEsVUFBQTtnQkFDQSxJQUFBLFVBQUEsV0FBQSxJQUFBLFNBQUE7Z0JBQ0EsTUFBQSxJQUFBLFlBQUE7OztZQUdBLFFBQUEsU0FBQSxTQUFBLE9BQUE7Z0JBQ0EsV0FBQSxNQUFBO2dCQUNBLEdBQUE7b0JBQ0EsS0FBQSxRQUFBOzs7Ozs7QUM1Q0EsQ0FBQSxVQUFBO0lBQ0E7SUFDQSxRQUFBLE9BQUEsZ0JBQUEsUUFBQSxlQUFBLFVBQUE7UUFDQSxPQUFBO1lBQ0EsTUFBQTtnQkFDQSxTQUFBLFVBQUEsSUFBQSxJQUFBO29CQUNBLElBQUEsS0FBQSxHQUFBO29CQUNBLElBQUEsS0FBQSxHQUFBO29CQUNBLE9BQUEsU0FBQSxDQUFBLEdBQUEsS0FBQSxLQUFBOztnQkFFQSxRQUFBLFNBQUEsSUFBQSxJQUFBO29CQUNBLElBQUEsS0FBQSxHQUFBO29CQUNBLElBQUEsS0FBQSxHQUFBOztvQkFFQSxPQUFBLFNBQUEsQ0FBQSxHQUFBLEtBQUEsR0FBQSxLQUFBOztnQkFFQSxTQUFBLFNBQUEsSUFBQSxJQUFBO29CQUNBLElBQUEsS0FBQSxHQUFBO29CQUNBLElBQUEsS0FBQSxHQUFBOztvQkFFQSxPQUFBLFNBQUEsQ0FBQSxHQUFBLEtBQUEsR0FBQSxLQUFBLEtBQUE7O2dCQUVBLFVBQUEsU0FBQSxJQUFBLElBQUE7b0JBQ0EsSUFBQSxNQUFBLEdBQUE7b0JBQ0EsSUFBQSxNQUFBLEdBQUE7b0JBQ0EsSUFBQSxNQUFBLEdBQUE7b0JBQ0EsSUFBQSxNQUFBLEdBQUE7b0JBQ0EsT0FBQSxDQUFBLElBQUEsR0FBQSxNQUFBLElBQUEsR0FBQTs7Z0JBRUEsU0FBQSxTQUFBLElBQUEsSUFBQTtvQkFDQSxPQUFBLEdBQUEsY0FBQSxHQUFBOztnQkFFQSxTQUFBLFNBQUEsSUFBQSxJQUFBO29CQUNBLEdBQUEsS0FBQSxRQUFBLElBQUEsTUFBQSxHQUFBO3dCQUNBLEdBQUEsS0FBQSxTQUFBLEdBQUE7NEJBQ0EsT0FBQSxlQUFBLEtBQUEsUUFBQSxJQUFBLE1BQUE7K0JBQ0E7NEJBQ0EsT0FBQSxlQUFBLEtBQUEsT0FBQSxJQUFBLE1BQUE7OzJCQUVBO3dCQUNBLE9BQUEsZUFBQSxLQUFBLFFBQUEsSUFBQSxNQUFBOzs7Ozs7OztBQ3hDQSxDQUFBLFVBQUE7SUFDQTs7O0lBR0EsUUFBQSxPQUFBLGdCQUFBLFFBQUEsNkJBQUEsU0FBQSxRQUFBLE9BQUE7OztRQUdBLFNBQUEsTUFBQSxXQUFBO1lBQ0EsR0FBQSxXQUFBO2dCQUNBLEtBQUEsU0FBQTs7U0FFQTs7UUFFQSxNQUFBLFlBQUE7O1lBRUEsS0FBQTtZQUNBLE9BQUE7WUFDQSxTQUFBO1lBQ0EsU0FBQTtZQUNBLFVBQUE7WUFDQSxRQUFBO1lBQ0EsVUFBQTtZQUNBLFlBQUE7WUFDQSxPQUFBO1lBQ0EsU0FBQTtZQUNBLFdBQUE7O1lBRUEsV0FBQTtZQUNBLFlBQUE7WUFDQSxPQUFBOztZQUVBLFVBQUEsVUFBQSxNQUFBO2dCQUNBLFFBQUEsT0FBQSxNQUFBO2dCQUNBLEdBQUEsT0FBQSxLQUFBLG1CQUFBLGVBQUEsS0FBQSxrQkFBQSxNQUFBO29CQUNBLEtBQUEsUUFBQSxLQUFBOztnQkFFQSxLQUFBLE9BQUEsS0FBQTtnQkFDQSxPQUFBOztZQUVBLGdCQUFBLFlBQUE7Z0JBQ0EsR0FBQSxPQUFBLEtBQUEsYUFBQSxlQUFBLEtBQUEsYUFBQSxNQUFBO29CQUNBLEtBQUEsV0FBQSxLQUFBLE1BQUE7b0JBQ0EsS0FBQTs7Z0JBRUEsT0FBQTtvQkFDQSxXQUFBLEtBQUEsU0FBQSxZQUFBO29CQUNBLFVBQUEsS0FBQSxTQUFBLFlBQUE7Ozs7WUFJQSxTQUFBLFlBQUE7Z0JBQ0EsR0FBQSxLQUFBLFdBQUEsV0FBQSxLQUFBLENBQUEsS0FBQSxjQUFBLE9BQUEsS0FBQSxlQUFBLGFBQUE7b0JBQ0EsT0FBQTt1QkFDQTtvQkFDQSxHQUFBLE9BQUEsS0FBQSxXQUFBLE9BQUEsZ0JBQUEsS0FBQSxXQUFBLE9BQUEsUUFBQSxLQUFBLFdBQUEsR0FBQSxPQUFBO3dCQUNBLE9BQUEsb0NBQUEsS0FBQSxXQUFBLEdBQUE7MkJBQ0E7d0JBQ0EsT0FBQTs7Ozs7WUFLQSxZQUFBLFdBQUE7Z0JBQ0EsR0FBQSxLQUFBLFlBQUEsUUFBQSxPQUFBLEtBQUEsWUFBQSxlQUFBLEtBQUEsWUFBQSxHQUFBO29CQUNBLE9BQUEsd0RBQUEsS0FBQSxRQUFBOztnQkFFQSxHQUFBLE9BQUEsS0FBQSxZQUFBLFNBQUE7b0JBQ0EsT0FBQSxLQUFBOztnQkFFQSxHQUFBLENBQUEsS0FBQSxRQUFBLFdBQUEsU0FBQTtvQkFDQSxPQUFBLGdDQUFBLEtBQUE7dUJBQ0E7b0JBQ0EsT0FBQSxLQUFBOzs7O1lBSUEsV0FBQSxXQUFBO2dCQUNBLElBQUEsUUFBQTtnQkFDQSxJQUFBLGNBQUEsTUFBQTtnQkFDQSxPQUFBLElBQUEsT0FBQSxLQUFBLE9BQUE7b0JBQ0EsTUFBQSxJQUFBLE9BQUEsS0FBQSxZQUFBLE1BQUEsV0FBQSxNQUFBLE1BQUEsTUFBQSxJQUFBLE9BQUEsS0FBQSxLQUFBLElBQUE7b0JBQ0EsVUFBQSxDQUFBLEtBQUEsWUFBQSxVQUFBLEtBQUEsWUFBQTtvQkFDQSxJQUFBLFdBQUEsTUFBQTs7OztZQUlBLGVBQUEsWUFBQTtnQkFDQSxHQUFBLEtBQUEsWUFBQSxNQUFBO29CQUNBLE9BQUEsT0FBQSxPQUFBLENBQUEsS0FBQSxnQkFBQSxNQUFBO3lCQUNBLEtBQUEsVUFBQSxNQUFBOzRCQUNBLE9BQUE7OztnQkFHQSxPQUFBLE1BQUEsS0FBQSxnQkFBQSxNQUFBLEtBQUEsU0FBQSxVQUFBO29CQUNBLE9BQUE7Ozs7WUFJQSxpQkFBQSxXQUFBO2dCQUNBLEdBQUEsT0FBQSxLQUFBLFlBQUEsVUFBQTtvQkFDQSxPQUFBLE9BQUEsT0FBQSxDQUFBLEtBQUEsZ0JBQUEsS0FBQSxLQUFBLE1BQUE7eUJBQ0EsS0FBQSxVQUFBLE1BQUE7NEJBQ0EsT0FBQTs7dUJBRUE7b0JBQ0EsT0FBQSxNQUFBLEtBQUEsZ0JBQUEsS0FBQSxLQUFBLE1BQUEsS0FBQSxTQUFBLFVBQUE7d0JBQ0EsT0FBQTs7Ozs7Ozs7O1lBU0EsWUFBQSxXQUFBO2dCQUNBLE9BQUEsT0FBQSxLQUFBOzs7Ozs7O1lBT0EsVUFBQSxXQUFBO2dCQUNBLEdBQUEsS0FBQSxXQUFBO29CQUNBLE9BQUE7Z0JBQ0EsT0FBQSxPQUFBLEtBQUE7Ozs7Ozs7WUFPQSxlQUFBLFlBQUE7Z0JBQ0EsT0FBQSxLQUFBLGFBQUEsT0FBQTs7O1lBR0EsYUFBQSxXQUFBO2dCQUNBLEdBQUEsS0FBQSxlQUFBO29CQUNBLE9BQUE7Z0JBQ0EsT0FBQSxLQUFBLFdBQUEsT0FBQTs7Ozs7OztZQU9BLGNBQUEsWUFBQTtnQkFDQSxPQUFBLEtBQUEsYUFBQSxPQUFBOzs7WUFHQSxZQUFBLFdBQUE7Z0JBQ0EsR0FBQSxLQUFBLGVBQUE7b0JBQ0EsT0FBQTtnQkFDQSxPQUFBLEtBQUEsV0FBQSxPQUFBOzs7Ozs7O1lBT0EsYUFBQSxXQUFBO2dCQUNBLE9BQUEsU0FBQSxHQUFBLEtBQUE7OztZQUdBLFNBQUEsV0FBQTtnQkFDQSxPQUFBLENBQUEsS0FBQSxnQkFBQSxLQUFBLGFBQUEsUUFBQSxlQUFBLEtBQUEsY0FBQSxLQUFBLFdBQUEsUUFBQTs7Ozs7Ozs7OztZQVVBLG1CQUFBLFVBQUEsT0FBQTtnQkFDQSxJQUFBLG1CQUFBLEtBQUE7Z0JBQ0EsSUFBQSxjQUFBLElBQUEsT0FBQSxLQUFBLE9BQUEsQ0FBQSxLQUFBLE1BQUEsVUFBQSxLQUFBLE1BQUE7Z0JBQ0EsSUFBQSxjQUFBLElBQUEsT0FBQSxLQUFBLE9BQUEsQ0FBQSxLQUFBLGlCQUFBLFVBQUEsS0FBQSxpQkFBQTtnQkFDQSxPQUFBLFdBQUEsQ0FBQSxPQUFBLEtBQUEsU0FBQSxVQUFBLHVCQUFBLGFBQUEsZUFBQSxNQUFBLFFBQUE7OztZQUdBLG1CQUFBLFlBQUE7Z0JBQ0EsT0FBQSxNQUFBLElBQUEsZ0JBQUEsS0FBQSxLQUFBLEtBQUEsU0FBQSxVQUFBO29CQUNBLE9BQUE7Ozs7OztRQU1BLE9BQUE7Ozs7QUMvTEEsQ0FBQSxVQUFBOzs7O0lBSUEsUUFBQSxPQUFBLGdCQUFBLFFBQUEsbUVBQUEsU0FBQSxPQUFBLFVBQUEsT0FBQSxpQkFBQTs7Ozs7OztRQU9BLFNBQUEsY0FBQSxhQUFBO1lBQ0EsR0FBQSxZQUFBO2dCQUNBLElBQUEsSUFBQSxJQUFBLEdBQUEsSUFBQSxZQUFBLFFBQUEsS0FBQTtvQkFDQSxHQUFBLEVBQUEsWUFBQSxjQUFBLFFBQUE7d0JBQ0EsWUFBQSxLQUFBLElBQUEsTUFBQSxZQUFBOzs7Z0JBR0EsS0FBQSxVQUFBOzs7O1FBSUEsY0FBQSxZQUFBO1lBQ0EsTUFBQTtZQUNBLFNBQUE7WUFDQSxhQUFBO1lBQ0EsVUFBQTs7WUFFQSxXQUFBLFVBQUEsYUFBQTtnQkFDQSxLQUFBLE9BQUE7Ozs7Ozs7O1lBUUEsTUFBQSxVQUFBLFNBQUE7Z0JBQ0EsSUFBQSxRQUFBOztnQkFFQSxHQUFBLEtBQUEsU0FBQTtvQkFDQSxTQUFBLE9BQUEsTUFBQTs7Z0JBRUEsT0FBQSxLQUFBLFdBQUEsU0FBQSxVQUFBO29CQUNBLEdBQUEsTUFBQSxZQUFBLE9BQUE7d0JBQ0EsTUFBQSxVQUFBO3dCQUNBLEdBQUEsUUFBQSxpQkFBQSxPQUFBOzRCQUNBLE9BQUE7O3dCQUVBLE9BQUEsTUFBQSxJQUFBLFNBQUEsQ0FBQSxRQUFBLFFBQUEscUJBQUEsUUFBQSxTQUFBLFdBQUE7NEJBQ0EsSUFBQSxJQUFBLElBQUEsR0FBQSxJQUFBLFVBQUEsUUFBQSxLQUFBO2dDQUNBLFVBQUEsS0FBQSxJQUFBLE1BQUEsVUFBQTs7NEJBRUEsUUFBQSxpQkFBQTs0QkFDQSxNQUFBLFVBQUE7NEJBQ0EsTUFBQSxVQUFBOzRCQUNBLE9BQUE7OztvQkFHQSxNQUFBLFdBQUE7bUJBQ0E7Ozs7Ozs7UUFPQSxPQUFBOzs7O0FDbkVBLENBQUEsVUFBQTtJQUNBO0lBQ0EsUUFBQSxPQUFBLGdCQUFBLFFBQUEsc0RBQUEsU0FBQSxRQUFBLFlBQUEsWUFBQTs7UUFFQSxPQUFBOztZQUVBLE1BQUE7WUFDQSxTQUFBO1lBQ0EsTUFBQTtZQUNBLFNBQUE7WUFDQSxPQUFBO1lBQ0EsU0FBQTs7O1lBR0EsTUFBQSxXQUFBO2dCQUNBLEVBQUEsUUFBQSxPQUFBLFVBQUE7b0JBQ0EsSUFBQSxTQUFBLEVBQUEsUUFBQTtvQkFDQSxJQUFBLFdBQUEsRUFBQSxnQkFBQSxHQUFBO29CQUNBLElBQUEsU0FBQSxLQUFBO3dCQUNBLFNBQUEsU0FBQTs7eUJBRUE7d0JBQ0EsU0FBQSxZQUFBOzs7Z0JBR0EsS0FBQSxVQUFBLEdBQUEsU0FBQSxLQUFBO2dCQUNBLEVBQUEsNEJBQUEsWUFBQTs7O1lBR0EsV0FBQSxVQUFBLE1BQUE7Z0JBQ0EsR0FBQSxNQUFBO29CQUNBLElBQUEsTUFBQSxLQUFBO29CQUNBLE9BQUEsSUFBQSxNQUFBOzs7O1lBSUEsYUFBQSxXQUFBO2dCQUNBLEVBQUEsUUFBQSxTQUFBO2dCQUNBLEVBQUEsa0JBQUEsU0FBQTtnQkFDQSxJQUFBLElBQUEsSUFBQSxHQUFBLElBQUEsRUFBQSxpQkFBQSxRQUFBLElBQUE7b0JBQ0EsRUFBQSxFQUFBLGlCQUFBLElBQUEsU0FBQTs7Z0JBRUEsRUFBQSxhQUFBLFlBQUE7Z0JBQ0EsRUFBQSxtQkFBQTs7O1lBR0EsV0FBQSxTQUFBLFNBQUE7O2dCQUVBLEVBQUEsa0JBQUEsWUFBQTtnQkFDQSxFQUFBLElBQUEsU0FBQSxZQUFBO2dCQUNBLEVBQUEsYUFBQSxTQUFBO2dCQUNBLEVBQUEsbUJBQUEsS0FBQTs7O1lBR0EsWUFBQSxVQUFBLE9BQUEsT0FBQTtnQkFDQSxFQUFBLGdCQUFBLFlBQUE7Z0JBQ0EsRUFBQSxtQkFBQSxZQUFBO2dCQUNBLEVBQUEsSUFBQSxPQUFBLFNBQUE7Z0JBQ0EsRUFBQSxNQUFBLFFBQUEsUUFBQSxNQUFBLFNBQUE7Ozs7WUFJQSxTQUFBLFdBQUE7Z0JBQ0EsR0FBQSxLQUFBLFNBQUE7b0JBQ0EsS0FBQSxPQUFBLEVBQUE7Z0JBQ0EsT0FBQSxLQUFBOztZQUVBLFlBQUEsV0FBQTtnQkFDQSxHQUFBLEtBQUEsWUFBQTtvQkFDQSxLQUFBLFVBQUEsRUFBQTtnQkFDQSxPQUFBLEtBQUE7O1lBRUEsU0FBQSxXQUFBO2dCQUNBLEdBQUEsS0FBQSxTQUFBO29CQUNBLEtBQUEsT0FBQSxFQUFBO2dCQUNBLE9BQUEsS0FBQTs7WUFFQSxZQUFBLFdBQUE7Z0JBQ0EsR0FBQSxLQUFBLFlBQUE7b0JBQ0EsS0FBQSxVQUFBLEVBQUE7Z0JBQ0EsT0FBQSxLQUFBOztZQUVBLFdBQUEsV0FBQTtnQkFDQSxHQUFBLEtBQUEsV0FBQTtvQkFDQSxLQUFBLFNBQUEsRUFBQTtnQkFDQSxPQUFBLEtBQUE7OztZQUdBLGFBQUEsV0FBQTtnQkFDQSxPQUFBOzs7WUFHQSxnQkFBQSxTQUFBLFVBQUEsT0FBQTtnQkFDQSxLQUFBLEtBQUEsRUFBQTtnQkFDQSxLQUFBLGNBQUEsS0FBQSxHQUFBLFNBQUE7Z0JBQ0EsS0FBQSxPQUFBLEtBQUEsR0FBQSxLQUFBO2dCQUNBLEtBQUEsTUFBQTtnQkFDQSxLQUFBLFFBQUEsQ0FBQTs7Z0JBRUEsS0FBQSxhQUFBLFdBQUE7b0JBQ0EsSUFBQSxNQUFBO29CQUNBLElBQUEsR0FBQSxHQUFBLFNBQUEsU0FBQSxNQUFBO3dCQUNBLEVBQUEsTUFBQSxZQUFBO3dCQUNBLE9BQUE7O29CQUVBLElBQUEsS0FBQSxHQUFBLFFBQUEsVUFBQTt3QkFDQSxJQUFBLE1BQUEsRUFBQTt3QkFDQSxJQUFBLFNBQUEsSUFBQSxLQUFBLEtBQUEsS0FBQTt3QkFDQSxJQUFBLE1BQUEsSUFBQTt3QkFDQSxJQUFBLFFBQUEsSUFBQTt3QkFDQSxJQUFBLFlBQUEsS0FBQSxJQUFBOzs7b0JBR0EsRUFBQSxRQUFBLEdBQUEsU0FBQSxZQUFBO3dCQUNBLElBQUEsR0FBQSxZQUFBOzs7O2dCQUlBLEtBQUEsV0FBQSxXQUFBO29CQUNBLE9BQUEsS0FBQTs7Z0JBRUEsS0FBQSxVQUFBLFdBQUE7b0JBQ0EsT0FBQSxLQUFBOzs7Z0JBR0EsS0FBQTs7Ozs7O1lBTUEsWUFBQSxZQUFBO2dCQUNBLEVBQUEsUUFBQSxZQUFBO2dCQUNBLEVBQUEsbUJBQUE7Z0JBQ0EsRUFBQSxvQkFBQSxTQUFBOzs7Ozs7WUFNQSxZQUFBLFlBQUE7Z0JBQ0EsRUFBQSxRQUFBLFNBQUE7Z0JBQ0EsRUFBQSxtQkFBQTtnQkFDQSxFQUFBLG9CQUFBLFlBQUE7Ozs7Ozs7QUMvSUEsQ0FBQSxVQUFBO0lBQ0E7OztJQUdBLFFBQUEsT0FBQSxnQkFBQSxRQUFBLDZHQUFBLFNBQUEsSUFBQSxhQUFBLGNBQUEsZUFBQSxnQkFBQSxtQkFBQTs7UUFFQSxPQUFBOztZQUVBLFlBQUE7WUFDQSxXQUFBO1lBQ0EsWUFBQTs7WUFFQSxXQUFBO1lBQ0EsYUFBQTs7WUFFQSxTQUFBO1lBQ0EsU0FBQTtnQkFDQSxRQUFBO2dCQUNBLFFBQUE7Z0JBQ0EsT0FBQTs7WUFFQSxtQkFBQTtZQUNBLEtBQUE7WUFDQSxZQUFBO1lBQ0EsWUFBQTs7WUFFQSxXQUFBO1lBQ0EsYUFBQTs7Ozs7O1lBTUEsWUFBQSxVQUFBLFNBQUE7Z0JBQ0EsUUFBQSxPQUFBLE1BQUE7Ozs7Ozs7O1lBUUEsWUFBQSxTQUFBLFNBQUE7Z0JBQ0EsS0FBQSxVQUFBO2dCQUNBLE9BQUE7Ozs7Ozs7O1lBUUEsaUJBQUEsU0FBQSxRQUFBO2dCQUNBLElBQUEsVUFBQTtnQkFDQSxJQUFBLFFBQUE7Z0JBQ0EsR0FBQSxRQUFBO29CQUNBLEtBQUEsSUFBQSxJQUFBLEdBQUEsSUFBQSxPQUFBLFFBQUEsS0FBQTt3QkFDQSxJQUFBLFNBQUEsT0FBQSxHQUFBO3dCQUNBLE9BQUEsWUFBQSxTQUFBLFdBQUE7NEJBQ0EsSUFBQSxLQUFBOzRCQUNBLEdBQUEsTUFBQSxXQUFBLFNBQUEsTUFBQSxZQUFBO2dDQUNBLE1BQUEsWUFBQSxNQUFBOzs0QkFFQSxNQUFBLGFBQUE7Z0NBQ0EsUUFBQSxHQUFBO2dDQUNBLE1BQUEsTUFBQTs7NEJBRUEsTUFBQSxJQUFBLFVBQUEsTUFBQSxXQUFBOzRCQUNBLE1BQUEsSUFBQSxRQUFBLE1BQUEsV0FBQTs0QkFDQSxFQUFBLG9CQUFBLFlBQUEseUJBQUEsQ0FBQSxVQUFBOzRCQUNBLG1CQUFBLFNBQUEsR0FBQTs0QkFDQSxFQUFBLE1BQUEsR0FBQSxJQUFBLFNBQUEseUJBQUEsQ0FBQSxVQUFBOzt3QkFFQSxPQUFBLE9BQUEsTUFBQTt3QkFDQSxRQUFBLEtBQUE7OztnQkFHQSxLQUFBLFFBQUEsU0FBQTs7O1lBR0EsaUJBQUEsU0FBQSxRQUFBO2dCQUNBLElBQUEsVUFBQTtnQkFDQSxJQUFBLFFBQUE7Z0JBQ0EsR0FBQSxRQUFBO29CQUNBLEtBQUEsSUFBQSxJQUFBLEdBQUEsSUFBQSxPQUFBLFFBQUEsS0FBQTt3QkFDQSxJQUFBLFNBQUEsT0FBQSxHQUFBO3dCQUNBLE9BQUEsWUFBQSxTQUFBLFdBQUE7NEJBQ0EsSUFBQSxLQUFBOzRCQUNBLEdBQUEsTUFBQSxXQUFBLFNBQUEsTUFBQSxZQUFBO2dDQUNBLE1BQUEsWUFBQSxNQUFBOzs0QkFFQSxNQUFBLGFBQUE7Z0NBQ0EsUUFBQSxHQUFBO2dDQUNBLE1BQUEsTUFBQTs7NEJBRUEsTUFBQSxJQUFBLFVBQUEsTUFBQSxXQUFBOzRCQUNBLE1BQUEsSUFBQSxRQUFBLE1BQUEsV0FBQTs0QkFDQSxFQUFBLG9CQUFBLFlBQUEseUJBQUEsQ0FBQSxVQUFBOzRCQUNBLG1CQUFBLFNBQUEsR0FBQTs0QkFDQSxFQUFBLE1BQUEsR0FBQSxJQUFBLFNBQUEseUJBQUEsQ0FBQSxVQUFBOzt3QkFFQSxPQUFBLE9BQUEsTUFBQTt3QkFDQSxRQUFBLEtBQUE7d0JBQ0EsS0FBQSxRQUFBLE9BQUEsS0FBQTs7Ozs7Ozs7OztZQVVBLGlCQUFBLFNBQUEsUUFBQTtnQkFDQSxJQUFBLFVBQUE7Z0JBQ0EsSUFBQSxRQUFBO2dCQUNBLEdBQUEsUUFBQTtvQkFDQSxRQUFBLElBQUE7b0JBQ0EsS0FBQSxJQUFBLElBQUEsR0FBQSxJQUFBLE9BQUEsUUFBQSxLQUFBO3dCQUNBLElBQUEsU0FBQSxPQUFBLEdBQUE7d0JBQ0EsT0FBQSxZQUFBLFNBQUEsV0FBQTs0QkFDQSxJQUFBLEtBQUE7NEJBQ0EsR0FBQSxNQUFBLFdBQUEsU0FBQSxNQUFBLFlBQUE7Z0NBQ0EsTUFBQSxZQUFBLE1BQUE7OzRCQUVBLE1BQUEsYUFBQTtnQ0FDQSxRQUFBLEdBQUE7Z0NBQ0EsTUFBQSxNQUFBOzs0QkFFQSxNQUFBLElBQUEsVUFBQSxNQUFBLFdBQUE7NEJBQ0EsTUFBQSxJQUFBLFFBQUEsTUFBQSxXQUFBOzRCQUNBLEVBQUEsb0JBQUEsWUFBQSx5QkFBQSxDQUFBLFVBQUE7NEJBQ0EsbUJBQUEsU0FBQSxHQUFBOzRCQUNBLEVBQUEsTUFBQSxHQUFBLElBQUEsU0FBQSx5QkFBQSxDQUFBLFVBQUE7O3dCQUVBLE9BQUEsT0FBQSxNQUFBO3dCQUNBLFFBQUEsS0FBQTs7O2dCQUdBLEtBQUEsUUFBQSxTQUFBOzs7WUFHQSxpQkFBQSxTQUFBLFFBQUE7Z0JBQ0EsSUFBQSxVQUFBO2dCQUNBLElBQUEsUUFBQTtnQkFDQSxHQUFBLFFBQUE7b0JBQ0EsS0FBQSxJQUFBLElBQUEsR0FBQSxJQUFBLE9BQUEsUUFBQSxLQUFBO3dCQUNBLElBQUEsU0FBQSxPQUFBLEdBQUE7d0JBQ0EsT0FBQSxZQUFBLFNBQUEsV0FBQTs0QkFDQSxJQUFBLEtBQUE7NEJBQ0EsR0FBQSxNQUFBLFdBQUEsU0FBQSxNQUFBLFlBQUE7Z0NBQ0EsTUFBQSxZQUFBLE1BQUE7OzRCQUVBLE1BQUEsYUFBQTtnQ0FDQSxRQUFBLEdBQUE7Z0NBQ0EsTUFBQSxNQUFBOzs0QkFFQSxNQUFBLElBQUEsVUFBQSxNQUFBLFdBQUE7NEJBQ0EsTUFBQSxJQUFBLFFBQUEsTUFBQSxXQUFBOzRCQUNBLEVBQUEsb0JBQUEsWUFBQSx5QkFBQSxDQUFBLFVBQUE7NEJBQ0EsbUJBQUEsU0FBQSxHQUFBOzRCQUNBLEVBQUEsTUFBQSxHQUFBLElBQUEsU0FBQSx5QkFBQSxDQUFBLFVBQUE7O3dCQUVBLE9BQUEsT0FBQSxNQUFBO3dCQUNBLFFBQUEsS0FBQTt3QkFDQSxLQUFBLFFBQUEsT0FBQSxLQUFBOzs7Ozs7Ozs7OztZQVdBLGlCQUFBLFNBQUEsU0FBQTtnQkFDQSxLQUFBLFFBQUEsUUFBQTs7Ozs7O1lBTUEsa0JBQUEsV0FBQTtnQkFDQSxJQUFBLFFBQUE7Z0JBQ0EsSUFBQSxTQUFBLEtBQUEsUUFBQTtnQkFDQSxJQUFBLFNBQUEsS0FBQSxRQUFBO2dCQUNBLElBQUEsUUFBQSxLQUFBLFFBQUE7Z0JBQ0EsS0FBQSxjQUFBO2dCQUNBLEtBQUEsY0FBQTtnQkFDQSxLQUFBLGNBQUE7Z0JBQ0EsS0FBQTs7Ozs7Ozs7WUFRQSxvQkFBQSxXQUFBO2NBQ0EsS0FBQSxVQUFBO2tCQUNBLFFBQUE7a0JBQ0EsUUFBQTtrQkFDQSxPQUFBOzs7Ozs7Ozs7WUFTQSxlQUFBLFNBQUEsU0FBQTtnQkFDQSxHQUFBLFdBQUEsUUFBQSxTQUFBLEdBQUE7b0JBQ0EsSUFBQSxJQUFBLElBQUEsR0FBQSxJQUFBLFFBQUEsUUFBQSxLQUFBO3dCQUNBLElBQUEsU0FBQSxRQUFBO3dCQUNBLE9BQUEsT0FBQTt3QkFDQSxPQUFBLFFBQUE7Ozs7Ozs7OztZQVNBLGdCQUFBLFdBQUE7Z0JBQ0EsSUFBQSxXQUFBLEtBQUEsUUFBQTtnQkFDQSxHQUFBLE9BQUEsU0FBQSxhQUFBLGNBQUEsU0FBQSxVQUFBO29CQUNBLElBQUEsYUFBQSxJQUFBLE9BQUEsS0FBQSxPQUFBO3dCQUNBLE1BQUEsSUFBQSxPQUFBLEtBQUEsWUFBQSxtQkFBQSxNQUFBLE1BQUEsTUFBQSxJQUFBLE9BQUEsS0FBQSxLQUFBLElBQUE7d0JBQ0EsVUFBQSxDQUFBLEtBQUEsU0FBQSxVQUFBLEtBQUEsU0FBQTt3QkFDQSxJQUFBOztvQkFFQSxXQUFBLE9BQUEsS0FBQTtvQkFDQSxLQUFBLGNBQUE7Ozs7Ozs7O1lBUUEsZ0JBQUEsV0FBQTtnQkFDQSxLQUFBLFdBQUEsT0FBQTtnQkFDQSxLQUFBLGFBQUE7Ozs7Ozs7Ozs7WUFVQSxZQUFBLFNBQUEsU0FBQSxXQUFBOztnQkFFQSxJQUFBLFFBQUE7Z0JBQ0EsT0FBQSxHQUFBLEtBQUEsUUFBQSxxQkFBQSxLQUFBLFVBQUEsYUFBQTtvQkFDQSxXQUFBLFFBQUE7b0JBQ0EsR0FBQSxPQUFBLGFBQUEsYUFBQTt3QkFDQSxJQUFBLFdBQUEsUUFBQTs7b0JBRUEsR0FBQSxPQUFBLGdCQUFBLGFBQUE7d0JBQ0EsV0FBQTs7b0JBRUEsR0FBQSxRQUFBLFVBQUEsUUFBQSxPQUFBLFFBQUEsTUFBQSxhQUFBLGFBQUE7d0JBQ0EsUUFBQSxJQUFBO3dCQUNBLFdBQUEsUUFBQTs7b0JBRUEsSUFBQTtvQkFDQSxHQUFBLFdBQUE7d0JBQ0EsVUFBQSxTQUFBLGVBQUE7MkJBQ0E7d0JBQ0EsVUFBQSxTQUFBLGVBQUE7O29CQUVBLElBQUEsYUFBQTt3QkFDQSxRQUFBLElBQUEsT0FBQSxLQUFBLE9BQUEsU0FBQSxVQUFBLFNBQUE7d0JBQ0EsTUFBQSxTQUFBLE9BQUEsU0FBQSxPQUFBLE1BQUE7d0JBQ0EsYUFBQTt3QkFDQSxjQUFBO3dCQUNBLGtCQUFBO3dCQUNBLFFBQUE7NEJBQ0E7Z0NBQ0EsYUFBQTtnQ0FDQSxTQUFBO29DQUNBLEVBQUEsWUFBQTs7Ozs7b0JBS0EsTUFBQSxNQUFBLElBQUEsT0FBQSxLQUFBLElBQUEsU0FBQTtvQkFDQSxNQUFBLGFBQUE7b0JBQ0EsTUFBQSxVQUFBO29CQUNBLE9BQUE7Ozs7Ozs7OztZQVNBLGdCQUFBLFdBQUE7Z0JBQ0EsT0FBQSxDQUFBLFFBQUEsT0FBQSxLQUFBLFNBQUEsS0FBQTs7Ozs7OztZQU9BLHdCQUFBLFdBQUE7Z0JBQ0EsT0FBQSxLQUFBLFFBQUEsS0FBQTs7Ozs7Ozs7Ozs7WUFXQSxnQkFBQSxTQUFBLEtBQUE7Z0JBQ0EsSUFBQSxRQUFBO2dCQUNBLElBQUEsY0FBQSxJQUFBO2dCQUNBLEtBQUEsWUFBQSxLQUFBO2dCQUNBLEtBQUEsYUFBQTtvQkFDQSxRQUFBLENBQUEsS0FBQSxZQUFBLFVBQUEsS0FBQSxZQUFBO29CQUNBLE1BQUEsTUFBQTs7Z0JBRUEsS0FBQSxJQUFBLFVBQUEsS0FBQSxXQUFBO2dCQUNBLEtBQUEsSUFBQSxRQUFBLEtBQUEsV0FBQTtnQkFDQSxPQUFBLEtBQUE7Ozs7Ozs7O1lBUUEsWUFBQSxXQUFBO2dCQUNBLElBQUE7Z0JBQ0EsUUFBQSxLQUFBLEtBQUEsWUFBQTtnQkFDQSxLQUFBLGFBQUEsS0FBQTtnQkFDQSxLQUFBLFlBQUE7Z0JBQ0EsS0FBQSxJQUFBLFVBQUEsS0FBQSxXQUFBO2dCQUNBLEtBQUEsSUFBQSxRQUFBLEtBQUEsV0FBQTtnQkFDQSxPQUFBLEtBQUE7Ozs7Ozs7O0FDNVZBLENBQUEsV0FBQTtJQUNBOztJQUVBLFFBQUEsT0FBQSxnQkFBQSxRQUFBLHNCQUFBLFVBQUE7O1FBRUEsT0FBQTtZQUNBLFVBQUEsVUFBQSxLQUFBOzs7Z0JBR0EsSUFBQSxTQUFBO2dCQUNBLElBQUEsUUFBQSxhQUFBO2dCQUNBLElBQUEsV0FBQSxRQUFBLFNBQUEsUUFBQSxTQUFBLFNBQUE7Z0JBQ0EsSUFBQSxXQUFBLEtBQUE7b0JBQ0EsU0FBQSxHQUFBO29CQUNBOztnQkFFQSxJQUFBLFFBQUEsS0FBQSxNQUFBLFdBQUE7Z0JBQ0EsSUFBQSxTQUFBLElBQUEsUUFBQTtnQkFDQSxJQUFBLE9BQUEsS0FBQSxNQUFBLFdBQUE7Z0JBQ0EsSUFBQSxRQUFBLFFBQUEsU0FBQSxTQUFBLE9BQUEsU0FBQTtnQkFDQSxJQUFBLFFBQUE7Z0JBQ0EsSUFBQSxRQUFBLFFBQUE7b0JBQ0EsS0FBQSxJQUFBLElBQUEsUUFBQSxJQUFBLE9BQUEsS0FBQSxNQUFBO3dCQUNBLFdBQUEsd0JBQUEsUUFBQSxLQUFBLFFBQUE7d0JBQ0EsU0FBQTt3QkFDQSxJQUFBLFFBQUEsT0FBQSxRQUFBO3dCQUNBOztvQkFFQTs7Z0JBRUEsS0FBQSxJQUFBLElBQUEsUUFBQSxJQUFBLE9BQUEsS0FBQSxNQUFBO29CQUNBLFdBQUEsd0JBQUEsUUFBQSxLQUFBLFFBQUE7b0JBQ0EsU0FBQTtvQkFDQSxJQUFBLFFBQUEsT0FBQSxRQUFBO29CQUNBOzs7Z0JBR0EsU0FBQSxtQkFBQTs7b0JBRUEsSUFBQSxLQUFBLGFBQUEsT0FBQSxLQUFBOztvQkFFQSxJQUFBLFNBQUEsbUJBQUEsU0FBQSxnQkFBQTt3QkFDQSxPQUFBLFNBQUEsZ0JBQUE7O29CQUVBLElBQUEsU0FBQSxLQUFBLFdBQUEsT0FBQSxTQUFBLEtBQUE7b0JBQ0EsT0FBQTs7O2dCQUdBLFNBQUEsYUFBQSxLQUFBO29CQUNBLElBQUEsTUFBQSxTQUFBLGVBQUEsT0FBQSxTQUFBLGVBQUEsT0FBQSxFQUFBLFlBQUEsS0FBQTtvQkFDQSxJQUFBLElBQUEsSUFBQTtvQkFDQSxJQUFBLE9BQUE7b0JBQ0EsT0FBQSxLQUFBLGdCQUFBLEtBQUEsZ0JBQUEsU0FBQSxNQUFBO3dCQUNBLE9BQUEsS0FBQTt3QkFDQSxLQUFBLEtBQUE7O29CQUVBLE9BQUEsSUFBQTs7Ozs7OztBQ3hEQSxDQUFBLFVBQUE7SUFDQTs7SUFFQSxRQUFBLE9BQUEsZ0JBQUEsUUFBQSxtREFBQSxTQUFBLFFBQUEsT0FBQSxlQUFBOzs7Ozs7O1FBT0EsU0FBQSxLQUFBLFVBQUE7WUFDQSxHQUFBLFVBQUE7Z0JBQ0EsS0FBQSxTQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7UUEwQ0EsS0FBQSxZQUFBOztZQUVBLGNBQUE7WUFDQSxZQUFBOztZQUVBLGVBQUE7WUFDQSxnQkFBQTs7WUFFQSxPQUFBO1lBQ0EsT0FBQTtZQUNBLFlBQUE7WUFDQSxXQUFBO1lBQ0EsUUFBQTtZQUNBLFlBQUE7WUFDQSxPQUFBO1lBQ0EsWUFBQTtZQUNBLFlBQUE7WUFDQSxNQUFBLEtBQUE7WUFDQSxVQUFBO1lBQ0EsVUFBQTtZQUNBLE9BQUE7WUFDQSxXQUFBO1lBQ0EsWUFBQTtZQUNBLGdCQUFBO1lBQ0EsaUJBQUE7WUFDQSxRQUFBO1lBQ0EsS0FBQTs7WUFFQSxhQUFBOzs7Ozs7O1lBT0EsVUFBQSxVQUFBLFVBQUE7Z0JBQ0EsUUFBQSxPQUFBLE1BQUE7Z0JBQ0EsS0FBQSxVQUFBO2dCQUNBLEtBQUEsa0JBQUE7Z0JBQ0EsS0FBQSxnQkFBQTtnQkFDQSxLQUFBLGNBQUE7Ozs7WUFJQSxNQUFBLFVBQUEsSUFBQTtnQkFDQSxPQUFBLE1BQUEsSUFBQSxVQUFBLElBQUEsS0FBQSxTQUFBLFVBQUE7b0JBQ0EsR0FBQSxTQUFBLFlBQUEsUUFBQSxPQUFBLFNBQUEsU0FBQSxhQUFBO3dCQUNBLE9BQUEsSUFBQSxLQUFBLFNBQUE7O29CQUVBLE9BQUE7Ozs7Ozs7Ozs7WUFVQSxXQUFBLFVBQUEsVUFBQTtvQkFDQSxPQUFBLFNBQUE7Ozs7Ozs7WUFPQSxhQUFBLFlBQUE7Z0JBQ0EsT0FBQSxLQUFBLFNBQUEsS0FBQTs7Ozs7Ozs7OztZQVVBLGFBQUEsU0FBQSxPQUFBO2dCQUNBLElBQUEsU0FBQSxLQUFBLHdCQUFBO2dCQUNBLE9BQUEsUUFBQTtnQkFDQSxRQUFBLElBQUEsMkJBQUE7Z0JBQ0EsT0FBQSxNQUFBLEtBQUEsZUFBQSxRQUFBLEtBQUEsU0FBQSxVQUFBO29CQUNBLE9BQUEsU0FBQTttQkFDQSxTQUFBLE9BQUE7b0JBQ0EsUUFBQSxJQUFBLGlCQUFBOzs7O1lBSUEsY0FBQSxXQUFBO2dCQUNBLElBQUEsU0FBQSxLQUFBLHdCQUFBO2dCQUNBLE9BQUEsR0FBQSxLQUFBLGVBQUEscUJBQUEsS0FBQSxVQUFBLFVBQUE7b0JBQ0EsT0FBQSxXQUFBLFNBQUE7b0JBQ0EsT0FBQSxXQUFBLFNBQUE7b0JBQ0EsT0FBQSxNQUFBLEtBQUEsZUFBQSxDQUFBLFFBQUEsU0FBQSxLQUFBLFNBQUEsVUFBQTt3QkFDQSxPQUFBLFNBQUE7dUJBQ0EsU0FBQSxPQUFBO3dCQUNBLFFBQUEsSUFBQSxpQkFBQTs7Ozs7Ozs7OztZQVVBLFlBQUEsWUFBQTtnQkFDQSxJQUFBLFNBQUEsS0FBQSx3QkFBQTtnQkFDQSxPQUFBLE1BQUEsS0FBQSxlQUFBLENBQUEsUUFBQSxTQUFBLEtBQUEsU0FBQSxVQUFBO29CQUNBLE9BQUEsU0FBQTttQkFDQSxTQUFBLE9BQUE7b0JBQ0EsUUFBQSxJQUFBLGlCQUFBOzs7Ozs7Ozs7OztZQVdBLG1CQUFBLFVBQUEsVUFBQTtnQkFDQSxLQUFBLFNBQUEsU0FBQSxTQUFBO2dCQUNBLE9BQUE7Ozs7Ozs7O1lBUUEsaUJBQUEsVUFBQSxVQUFBO2dCQUNBLElBQUEsUUFBQTtnQkFDQSxJQUFBLE9BQUEsU0FBQSxTQUFBLGVBQUEsU0FBQSxTQUFBLElBQUE7b0JBQ0EsS0FBQSxPQUFBLEtBQUE7dUJBQ0E7b0JBQ0EsU0FBQSxPQUFBLFNBQUEsU0FBQTtvQkFDQSxRQUFBLFNBQUE7d0JBQ0EsS0FBQSxNQUFBOzRCQUNBLE1BQUEsT0FBQSxNQUFBOzRCQUNBO3dCQUNBLEtBQUEsTUFBQTs0QkFDQSxNQUFBLE9BQUEsTUFBQTs0QkFDQTt3QkFDQTs0QkFDQSxNQUFBLE9BQUEsTUFBQTs0QkFDQTs7O2dCQUdBLE9BQUE7OztZQUdBLHlCQUFBLFlBQUE7ZUFDQSxPQUFBLEtBQUEsTUFBQSxTQUFBOzs7WUFHQSx1QkFBQSxZQUFBO2dCQUNBLE9BQUEsS0FBQSxVQUFBLFNBQUE7Ozs7Ozs7OztZQVNBLHlCQUFBLFNBQUEsWUFBQTtnQkFDQSxJQUFBLFNBQUE7Z0JBQ0EsUUFBQSxLQUFBLFFBQUE7Z0JBQ0EsT0FBQSxPQUFBO2dCQUNBLE9BQUEsT0FBQTtnQkFDQSxPQUFBLE9BQUE7Z0JBQ0EsUUFBQSxLQUFBLE9BQUEsSUFBQSxXQUFBO2dCQUNBLE9BQUE7Ozs7UUFJQSxPQUFBOzs7O0FDek9BLENBQUEsVUFBQTtJQUNBOzs7SUFHQSxRQUFBLE9BQUEsZ0JBQUEsUUFBQSx5RUFBQSxTQUFBLE9BQUEsV0FBQSxlQUFBLElBQUEsU0FBQTtRQUNBLE9BQUE7WUFDQSxNQUFBO1lBQ0EsWUFBQTtZQUNBLFlBQUE7WUFDQSxhQUFBOzs7Ozs7WUFNQSxnQkFBQSxXQUFBO2dCQUNBLEdBQUEsS0FBQSxTQUFBLFFBQUEsS0FBQSxLQUFBLEtBQUE7b0JBQ0EsT0FBQSxLQUFBO3FCQUNBO29CQUNBLE9BQUEsS0FBQTs7Ozs7Ozs7O1lBU0Esb0JBQUEsV0FBQTtnQkFDQSxJQUFBLFFBQUE7O2dCQUVBLEdBQUEsS0FBQSxZQUFBO29CQUNBLFNBQUEsT0FBQSxNQUFBOzs7Z0JBR0EsT0FBQSxLQUFBLGNBQUEsU0FBQSxZQUFBO29CQUNBLEdBQUEsQ0FBQSxjQUFBO3dCQUNBLE9BQUE7b0JBQ0EsT0FBQSxNQUFBLElBQUEsYUFBQSxLQUFBLFNBQUEsU0FBQTt3QkFDQSxHQUFBLE9BQUEsYUFBQSxlQUFBLE9BQUEsU0FBQSxTQUFBLGFBQUE7NEJBQ0EsTUFBQSxPQUFBOzRCQUNBLE9BQUEsTUFBQTs7d0JBRUEsV0FBQSxTQUFBO3dCQUNBLEdBQUEsWUFBQSxRQUFBLFNBQUEsV0FBQSxLQUFBLFFBQUEsT0FBQSxJQUFBLFVBQUE7NEJBQ0EsTUFBQSxPQUFBOytCQUNBOzRCQUNBLE1BQUEsT0FBQSxJQUFBLFVBQUE7O3dCQUVBLE1BQUEsY0FBQTt3QkFDQSxPQUFBLE1BQUE7O21CQUVBOzs7Ozs7O1lBT0EsT0FBQSxTQUFBLFVBQUE7Z0JBQ0EsSUFBQSxRQUFBO2dCQUNBLE9BQUEsTUFBQSxLQUFBLG9CQUFBLFVBQUEsS0FBQSxTQUFBLFNBQUE7b0JBQ0EsV0FBQSxTQUFBO29CQUNBLEdBQUEsU0FBQSxVQUFBLFFBQUEsT0FBQSxTQUFBLFVBQUEsYUFBQTt3QkFDQSxjQUFBLFFBQUEsU0FBQTt3QkFDQSxHQUFBLFNBQUEsT0FBQTs0QkFDQSxPQUFBLE1BQUEsSUFBQSxhQUFBLEtBQUEsU0FBQSxjQUFBO2dDQUNBLGVBQUEsYUFBQTtnQ0FDQSxHQUFBLGlCQUFBLFFBQUEsT0FBQSxpQkFBQSxhQUFBO29DQUNBLE1BQUEsT0FBQSxJQUFBLFVBQUE7O2dDQUVBLE9BQUEsTUFBQTs7K0JBRUE7NEJBQ0EsTUFBQSxPQUFBOzRCQUNBLE9BQUEsTUFBQTs7O21CQUdBLFNBQUEsT0FBQTtvQkFDQSxNQUFBLGFBQUE7Ozs7Ozs7Ozs7WUFVQSxvQkFBQSxTQUFBLE9BQUEsVUFBQTtnQkFDQSxLQUFBLFdBQUEsUUFBQTtnQkFDQSxLQUFBLFdBQUEsV0FBQTs7Ozs7Ozs7O1lBU0EsZUFBQSxTQUFBLFVBQUE7Z0JBQ0EsSUFBQSxRQUFBO2dCQUNBLE1BQUEsT0FBQTtnQkFDQSxPQUFBLFNBQUEsZUFBQSxTQUFBLGtCQUFBO29CQUNBLEdBQUEsaUJBQUEsV0FBQSxhQUFBO3dCQUNBLE9BQUEsaUJBQUEsYUFBQTs7MkJBRUE7d0JBQ0EsT0FBQSxTQUFBLE1BQUEsU0FBQSxlQUFBOzRCQUNBLE9BQUEsY0FBQSxhQUFBOzs7Ozs7Ozs7Ozs7WUFZQSxzQkFBQSxTQUFBLE1BQUE7Z0JBQ0EsSUFBQSxRQUFBO2dCQUNBLE9BQUEsTUFBQSxLQUFBLGlCQUFBLE1BQUEsS0FBQSxTQUFBLFNBQUE7b0JBQ0EsR0FBQSxTQUFBLFlBQUEsS0FBQTt3QkFDQSxNQUFBLE9BQUEsU0FBQTt3QkFDQSxNQUFBLEtBQUEsY0FBQTt3QkFDQSxPQUFBLE1BQUE7Ozs7Ozs7Ozs7OztZQVlBLGtCQUFBLFNBQUEsZUFBQTtnQkFDQSxJQUFBLFlBQUEsT0FBQSxLQUFBLFdBQUE7Z0JBQ0EsR0FBQSxPQUFBLGNBQUEsZUFBQSxVQUFBLFNBQUEsR0FBQTtvQkFDQSxLQUFBLEtBQUEsUUFBQSxLQUFBLFdBQUE7dUJBQ0E7b0JBQ0EsS0FBQSxLQUFBLFFBQUEsY0FBQTs7Z0JBRUEsS0FBQSxLQUFBLFFBQUEsY0FBQTtnQkFDQSxLQUFBLEtBQUEsU0FBQSxjQUFBLE9BQUEsa0JBQUEsU0FBQSxJQUFBO2dCQUNBLEtBQUEsS0FBQSxhQUFBLGNBQUE7Z0JBQ0EsS0FBQSxLQUFBLFlBQUEsY0FBQTtnQkFDQSxLQUFBLEtBQUEsYUFBQSxjQUFBLFFBQUEsS0FBQTtnQkFDQSxPQUFBOzs7Ozs7O1lBT0EsUUFBQSxXQUFBO2dCQUNBLElBQUEsUUFBQTtnQkFDQSxPQUFBLE1BQUEsSUFBQSxlQUFBLEtBQUEsU0FBQSxTQUFBO29CQUNBLFdBQUEsU0FBQTtvQkFDQSxHQUFBLFNBQUEsWUFBQSxLQUFBO3dCQUNBLE1BQUEsT0FBQTt3QkFDQSxjQUFBLFFBQUE7d0JBQ0EsT0FBQSxNQUFBOzs7Ozs7Ozs7O1lBVUEsVUFBQSxTQUFBLFNBQUE7Z0JBQ0EsSUFBQSxRQUFBO2dCQUNBLE9BQUEsTUFBQSxLQUFBLGlCQUFBLFVBQUEsS0FBQSxTQUFBLFNBQUE7b0JBQ0EsV0FBQSxTQUFBO29CQUNBLFFBQUEsSUFBQSxrQkFBQTtvQkFDQSxHQUFBLFNBQUEsWUFBQSxLQUFBO3dCQUNBLE1BQUEsT0FBQSxJQUFBLFVBQUEsU0FBQTt3QkFDQSxPQUFBLE1BQUE7Ozs7Ozs7O0FDdExBLENBQUEsVUFBQTtJQUNBOzs7SUFHQSxRQUFBLE9BQUEsZ0JBQUEsUUFBQSx3REFBQSxTQUFBLE9BQUEsZ0JBQUEsT0FBQSxPQUFBOztRQUVBLFNBQUEsTUFBQSxXQUFBO1lBQ0EsR0FBQSxXQUFBO2dCQUNBLEtBQUEsUUFBQTs7OztRQUlBLE1BQUEsWUFBQTs7WUFFQSxNQUFBO1lBQ0EsVUFBQTtZQUNBLGFBQUE7WUFDQSxhQUFBO1lBQ0EsY0FBQTtZQUNBLFFBQUE7WUFDQSxLQUFBO1lBQ0EsT0FBQTtZQUNBLFVBQUE7WUFDQSxpQkFBQTtZQUNBLFlBQUE7WUFDQSxXQUFBO1lBQ0EsYUFBQTtZQUNBLGdCQUFBO1lBQ0EsY0FBQTtZQUNBLGVBQUE7WUFDQSxNQUFBO1lBQ0EsV0FBQTs7O1lBR0EsU0FBQSxVQUFBLFdBQUE7Z0JBQ0EsUUFBQSxPQUFBLE1BQUE7Z0JBQ0EsSUFBQSxRQUFBO2dCQUNBLEdBQUEsT0FBQSxNQUFBLFdBQUEsZUFBQSxNQUFBLE9BQUEsU0FBQSxHQUFBO29CQUNBLElBQUEsSUFBQSxJQUFBLEdBQUEsSUFBQSxNQUFBLE9BQUEsUUFBQSxLQUFBO3dCQUNBLE1BQUEsT0FBQSxLQUFBLElBQUEsTUFBQSxNQUFBLE9BQUE7OztnQkFHQSxLQUFBLE9BQUEsS0FBQTtnQkFDQSxPQUFBOzs7WUFHQSxNQUFBLFNBQUEsSUFBQTtnQkFDQSxPQUFBLE1BQUEsSUFBQSxXQUFBLElBQUEsS0FBQSxVQUFBLFVBQUE7b0JBQ0EsR0FBQSxhQUFBO3dCQUNBLE9BQUEsSUFBQSxNQUFBOzs7O1lBSUEsU0FBQSxZQUFBO2dCQUNBLEdBQUEsS0FBQSxXQUFBLFdBQUEsS0FBQSxDQUFBLEtBQUEsY0FBQSxPQUFBLEtBQUEsZUFBQSxhQUFBO29CQUNBLE9BQUE7dUJBQ0E7b0JBQ0EsR0FBQSxPQUFBLEtBQUEsV0FBQSxPQUFBLGdCQUFBLEtBQUEsV0FBQSxPQUFBLFFBQUEsS0FBQSxXQUFBLEdBQUEsT0FBQTt3QkFDQSxPQUFBLG9DQUFBLEtBQUEsV0FBQSxHQUFBOzJCQUNBO3dCQUNBLE9BQUE7Ozs7O1lBS0EsWUFBQSxXQUFBO2dCQUNBLEdBQUEsS0FBQSxnQkFBQSxRQUFBLE9BQUEsS0FBQSxnQkFBQSxlQUFBLEtBQUEsZ0JBQUEsR0FBQTtvQkFDQSxPQUFBLHdEQUFBLEtBQUEsT0FBQTs7O2dCQUdBLEdBQUEsT0FBQSxLQUFBLGdCQUFBLFNBQUE7b0JBQ0EsT0FBQSxLQUFBOztnQkFFQSxHQUFBLENBQUEsS0FBQSxZQUFBLFdBQUEsU0FBQTtvQkFDQSxPQUFBLGdDQUFBLEtBQUE7dUJBQ0E7d0JBQ0EsT0FBQSxLQUFBOzs7OztZQUtBLGVBQUEsWUFBQTtnQkFDQSxHQUFBLEtBQUEsZ0JBQUEsTUFBQTtvQkFDQSxPQUFBLE9BQUEsT0FBQSxDQUFBLEtBQUEsZ0JBQUEsTUFBQTt5QkFDQSxLQUFBLFVBQUEsTUFBQTs0QkFDQSxPQUFBOzs7Z0JBR0EsT0FBQSxNQUFBLEtBQUEsZ0JBQUEsTUFBQSxLQUFBLFNBQUEsVUFBQTtvQkFDQSxPQUFBOzs7O1lBSUEsbUJBQUEsWUFBQTtnQkFDQSxPQUFBLE1BQUEsSUFBQSxnQkFBQSxLQUFBLEtBQUEsS0FBQSxTQUFBLFVBQUE7b0JBQ0EsT0FBQTs7OztZQUlBLGdCQUFBLFlBQUE7Z0JBQ0EsT0FBQTtvQkFDQSxXQUFBLEtBQUEsU0FBQSxZQUFBO29CQUNBLFVBQUEsS0FBQSxTQUFBLFlBQUE7Ozs7WUFJQSxhQUFBLFdBQUE7Z0JBQ0EsT0FBQSxLQUFBOzs7WUFHQSxpQkFBQSxTQUFBLFdBQUE7Z0JBQ0EsSUFBQSxJQUFBLElBQUEsR0FBQSxJQUFBLFVBQUEsUUFBQSxLQUFBO29CQUNBLElBQUEsWUFBQSxVQUFBO29CQUNBLFVBQUEsTUFBQSxTQUFBLFVBQUE7b0JBQ0EsR0FBQSxPQUFBLFVBQUEsU0FBQSxVQUFBO3dCQUNBLFVBQUEsT0FBQTs0QkFDQSxLQUFBLFVBQUE7NEJBQ0EsTUFBQSxPQUFBLFVBQUEsS0FBQSxNQUFBLE9BQUE7OzJCQUVBO3dCQUNBLFVBQUEsT0FBQTs7b0JBRUEsR0FBQSxPQUFBLFVBQUEsVUFBQSxVQUFBO3dCQUNBLFVBQUEsUUFBQTs0QkFDQSxLQUFBLFVBQUE7NEJBQ0EsTUFBQSxPQUFBLFVBQUEsTUFBQSxNQUFBLE9BQUE7OzJCQUVBO3dCQUNBLFVBQUEsT0FBQTs7b0JBRUEsVUFBQSxLQUFBOztnQkFFQSxPQUFBOzs7WUFHQSw0QkFBQSxXQUFBO2dCQUNBLElBQUEsUUFBQTtnQkFDQSxJQUFBLFFBQUE7Z0JBQ0EsSUFBQSxJQUFBLElBQUEsR0FBQSxJQUFBLEdBQUEsS0FBQTtvQkFDQSxHQUFBLElBQUEsS0FBQSxJQUFBLEdBQUE7d0JBQ0EsSUFBQSxTQUFBLE1BQUEsbUJBQUE7d0JBQ0EsTUFBQSxLQUFBOzJCQUNBO3dCQUNBLElBQUEsTUFBQSxNQUFBLHNCQUFBLEdBQUEsUUFBQTt3QkFDQSxNQUFBLEtBQUE7OztnQkFHQSxPQUFBOzs7WUFHQSx1QkFBQSxTQUFBLEtBQUEsTUFBQSxPQUFBO2dCQUNBLE9BQUE7b0JBQ0EsS0FBQTtvQkFDQSxNQUFBO3dCQUNBLEtBQUE7d0JBQ0EsTUFBQTs7b0JBRUEsT0FBQTt3QkFDQSxLQUFBO3dCQUNBLE1BQUE7Ozs7O1lBS0Esb0JBQUEsU0FBQSxLQUFBO2dCQUNBLE9BQUE7b0JBQ0EsS0FBQTs7OztZQUlBLFdBQUEsV0FBQTtnQkFDQSxJQUFBLFFBQUE7Z0JBQ0EsSUFBQSxjQUFBLE1BQUE7Z0JBQ0EsT0FBQSxJQUFBLE9BQUEsS0FBQSxPQUFBO29CQUNBLE1BQUEsSUFBQSxPQUFBLEtBQUEsWUFBQSxNQUFBLFdBQUEsTUFBQSxNQUFBLE1BQUEsSUFBQSxPQUFBLEtBQUEsS0FBQSxJQUFBO29CQUNBLFVBQUEsQ0FBQSxLQUFBLFlBQUEsVUFBQSxLQUFBLFlBQUE7b0JBQ0EsSUFBQSxXQUFBLE1BQUE7Ozs7Ozs7Ozs7O1lBV0EsbUJBQUEsVUFBQSxPQUFBO2dCQUNBLElBQUEsbUJBQUEsS0FBQTtnQkFDQSxJQUFBLGNBQUEsSUFBQSxPQUFBLEtBQUEsT0FBQSxDQUFBLEtBQUEsTUFBQSxVQUFBLEtBQUEsTUFBQTtnQkFDQSxJQUFBLGNBQUEsSUFBQSxPQUFBLEtBQUEsT0FBQSxDQUFBLEtBQUEsaUJBQUEsVUFBQSxLQUFBLGlCQUFBOztnQkFFQSxPQUFBLFdBQUEsQ0FBQSxPQUFBLEtBQUEsU0FBQSxVQUFBLHVCQUFBLGFBQUEsZUFBQSxNQUFBLFFBQUE7Ozs7Ozs7O1lBUUEsb0JBQUEsVUFBQSxNQUFBO2dCQUNBLFFBQUE7b0JBQ0EsS0FBQTt3QkFDQSxPQUFBO29CQUNBLEtBQUE7d0JBQ0EsT0FBQTtvQkFDQSxLQUFBO3dCQUNBLE9BQUE7b0JBQ0EsS0FBQTt3QkFDQSxPQUFBO29CQUNBLEtBQUE7d0JBQ0EsT0FBQTtvQkFDQSxLQUFBO3dCQUNBLE9BQUE7b0JBQ0EsS0FBQTt3QkFDQSxPQUFBOzs7Ozs7UUFNQSxPQUFBOzs7OztBQzdOQSxDQUFBLFVBQUE7SUFDQTs7O0lBR0EsUUFBQSxPQUFBLGdCQUFBLFFBQUEscUZBQUEsU0FBQSxJQUFBLE9BQUEsZ0JBQUEsT0FBQSxlQUFBLE1BQUE7Ozs7Ozs7UUFPQSxTQUFBLGFBQUEsYUFBQTtZQUNBLEdBQUEsYUFBQTtnQkFDQSxJQUFBLElBQUEsSUFBQSxHQUFBLElBQUEsWUFBQSxRQUFBLEtBQUE7b0JBQ0EsR0FBQSxFQUFBLFlBQUEsY0FBQSxRQUFBO3dCQUNBLFlBQUEsS0FBQSxJQUFBLE1BQUEsWUFBQTs7O2dCQUdBLEtBQUEsVUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7UUFpQkEsYUFBQSxZQUFBO1lBQ0EsTUFBQTtZQUNBLFNBQUE7Ozs7Ozs7O1lBUUEsV0FBQSxVQUFBLGFBQUE7Z0JBQ0EsS0FBQSxPQUFBOztnQkFFQSxPQUFBOzs7Ozs7OztZQVFBLE1BQUEsVUFBQSxTQUFBO2dCQUNBLElBQUEsUUFBQTtnQkFDQSxHQUFBLEtBQUEsWUFBQSxPQUFBO29CQUNBLE1BQUEsVUFBQTtvQkFDQSxHQUFBLFFBQUEsaUJBQUEsT0FBQTt3QkFDQSxPQUFBOztvQkFFQSxPQUFBLE1BQUEsSUFBQSxTQUFBLENBQUEsUUFBQSxRQUFBLHFCQUFBLFFBQUEsU0FBQSxXQUFBO3dCQUNBLElBQUEsSUFBQSxJQUFBLEdBQUEsSUFBQSxVQUFBLFFBQUEsS0FBQTs0QkFDQSxVQUFBLEtBQUEsSUFBQSxNQUFBLFVBQUE7NEJBQ0EsR0FBQSxDQUFBLFVBQUEsR0FBQSxLQUFBLE9BQUEsVUFBQSxHQUFBLE1BQUEsYUFBQTtnQ0FDQSxVQUFBLEdBQUEsSUFBQSxVQUFBLEdBQUEsa0JBQUEsUUFBQTs7NEJBRUEsVUFBQSxHQUFBLElBQUEsV0FBQSxVQUFBLEdBQUE7NEJBQ0EsVUFBQSxHQUFBLFdBQUEsVUFBQSxHQUFBLElBQUEsVUFBQSxHQUFBLElBQUEsYUFBQTs7d0JBRUEsUUFBQSxpQkFBQTt3QkFDQSxNQUFBLFVBQUE7d0JBQ0EsTUFBQSxVQUFBO3dCQUNBLE9BQUE7Ozs7O1lBS0EsTUFBQSxTQUFBLFFBQUE7Z0JBQ0EsSUFBQSxRQUFBO2dCQUNBLEdBQUEsS0FBQSxZQUFBLE9BQUE7b0JBQ0EsTUFBQSxVQUFBO29CQUNBLE9BQUEsTUFBQSxJQUFBLGNBQUEsQ0FBQSxRQUFBLFNBQUEsUUFBQSxTQUFBLFdBQUE7d0JBQ0EsTUFBQSxVQUFBO3dCQUNBLE9BQUEsVUFBQTs7Ozs7Ozs7OztZQVVBLFFBQUEsU0FBQSxPQUFBO2dCQUNBLE9BQUEsTUFBQSxLQUFBLGdCQUFBLENBQUEsUUFBQSxRQUFBLFFBQUEsU0FBQSxTQUFBO29CQUNBLE9BQUE7Ozs7Ozs7OztZQVNBLFNBQUEsWUFBQTtnQkFDQSxPQUFBLEtBQUE7Ozs7UUFJQSxPQUFBOzs7O0FDaEhBLENBQUEsVUFBQTtJQUNBOzs7SUFHQSxRQUFBLE9BQUEsZ0JBQUEsUUFBQSx5QkFBQSxTQUFBLEdBQUE7Ozs7OztRQU1BLFNBQUEsZUFBQSxhQUFBO1lBQ0EsR0FBQSxhQUFBO2dCQUNBLEtBQUEsU0FBQTtnQkFDQSxHQUFBLENBQUEsS0FBQSxPQUFBO29CQUNBLEtBQUE7Ozs7O1FBS0EsU0FBQSxvQkFBQTtZQUNBLElBQUEsU0FBQTtZQUNBLEdBQUEsVUFBQSxhQUFBO2dCQUNBLFVBQUEsWUFBQSxtQkFBQSxTQUFBLFVBQUE7b0JBQ0EsT0FBQSxPQUFBLFlBQUEsVUFBQTttQkFDQSxTQUFBLE9BQUE7b0JBQ0EsR0FBQSxPQUFBO3dCQUNBLE9BQUEsT0FBQSxlQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztRQTBCQSxlQUFBLFlBQUE7O1lBRUEsWUFBQTtZQUNBLGVBQUE7WUFDQSxXQUFBO1lBQ0EsWUFBQTs7WUFFQSxNQUFBO1lBQ0EsT0FBQTtZQUNBLFVBQUE7WUFDQSxVQUFBO1lBQ0EsV0FBQTtZQUNBLE9BQUE7WUFDQSxNQUFBO1lBQ0EsS0FBQTtZQUNBLFFBQUE7WUFDQSxTQUFBO1lBQ0EsTUFBQTtZQUNBLE1BQUE7O1lBRUEsY0FBQTs7Ozs7O1lBTUEsVUFBQSxTQUFBLE1BQUE7Z0JBQ0EsUUFBQSxPQUFBLE1BQUE7Ozs7WUFJQSxVQUFBLFNBQUEsVUFBQSxXQUFBLE1BQUE7Z0JBQ0EsS0FBQSxRQUFBO29CQUNBLFVBQUE7b0JBQ0EsV0FBQTtvQkFDQSxNQUFBOztnQkFFQSxPQUFBOzs7Ozs7Ozs7O1lBVUEsbUJBQUEsWUFBQTtnQkFDQSxJQUFBLFNBQUE7Z0JBQ0EsR0FBQSxVQUFBLGFBQUE7b0JBQ0EsVUFBQSxZQUFBLG1CQUFBLFNBQUEsVUFBQTt3QkFDQSxPQUFBLE9BQUEsWUFBQSxVQUFBO3VCQUNBLFNBQUEsT0FBQTt3QkFDQSxHQUFBLE9BQUE7NEJBQ0EsT0FBQSxPQUFBLGVBQUE7Ozs7OztZQU1BLGtCQUFBLFNBQUEsVUFBQTtnQkFDQSxHQUFBLFNBQUEsV0FBQSxLQUFBLE9BQUEsYUFBQSxlQUFBLFNBQUEsU0FBQSxLQUFBLFNBQUE7b0JBQ0EsS0FBQSxlQUFBO29CQUNBOztnQkFFQSxLQUFBOzs7Ozs7O1lBT0EsY0FBQSxXQUFBO2dCQUNBLEtBQUEsV0FBQTtnQkFDQSxLQUFBLFlBQUE7Z0JBQ0EsT0FBQTs7Ozs7OztZQU9BLGFBQUEsU0FBQSxVQUFBO2dCQUNBLEtBQUEsV0FBQSxTQUFBLE9BQUE7Z0JBQ0EsS0FBQSxZQUFBLFNBQUEsT0FBQTtnQkFDQSxPQUFBOzs7Ozs7O1lBT0EsYUFBQSxXQUFBO2dCQUNBLElBQUEsUUFBQTtnQkFDQSxHQUFBLE1BQUEsVUFBQSxNQUFBO29CQUNBLE9BQUE7d0JBQ0EsVUFBQSxNQUFBLE1BQUE7d0JBQ0EsV0FBQSxNQUFBLE1BQUE7d0JBQ0EsTUFBQTs7dUJBRUE7b0JBQ0EsT0FBQTt3QkFDQSxVQUFBLE1BQUE7d0JBQ0EsV0FBQSxNQUFBOzs7Ozs7Ozs7O1lBVUEsaUJBQUEsV0FBQTtnQkFDQSxJQUFBLFFBQUE7Z0JBQ0EsT0FBQTtvQkFDQSxVQUFBLE1BQUE7b0JBQ0EsV0FBQSxNQUFBOzs7O1lBSUEsa0JBQUEsU0FBQSxPQUFBO2dCQUNBLE9BQUE7b0JBQ0EsS0FBQSxLQUFBO3dCQUNBLE9BQUE7b0JBQ0EsS0FBQSxLQUFBO3dCQUNBLE9BQUE7b0JBQ0EsS0FBQSxLQUFBO3dCQUNBLE9BQUE7b0JBQ0EsS0FBQSxLQUFBO3dCQUNBLE9BQUE7O2dCQUVBLE9BQUE7OztZQUdBLGtCQUFBLFlBQUE7Z0JBQ0EsSUFBQSxNQUFBO2dCQUNBLElBQUEsUUFBQTtnQkFDQSxHQUFBLEtBQUEsU0FBQSxRQUFBLEtBQUEsaUJBQUEsS0FBQSxPQUFBO29CQUNBLElBQUEsT0FBQSxLQUFBOztnQkFFQSxHQUFBLEtBQUEsY0FBQSxRQUFBLE9BQUEsS0FBQSxjQUFBLGFBQUE7b0JBQ0EsSUFBQSxNQUFBLEtBQUE7b0JBQ0EsR0FBQSxJQUFBLGFBQUEsUUFBQSxJQUFBLGNBQUEsTUFBQTt3QkFDQSxNQUFBO3dCQUNBLElBQUEsV0FBQSxLQUFBO3dCQUNBLElBQUEsWUFBQSxLQUFBOzJCQUNBO3dCQUNBLElBQUEsV0FBQSxJQUFBO3dCQUNBLElBQUEsWUFBQSxJQUFBOzs7dUJBR0E7b0JBQ0EsR0FBQSxLQUFBLEtBQUEscUJBQUEsS0FBQSxTQUFBLGFBQUE7d0JBQ0EsTUFBQTs7O2dCQUdBLEdBQUEsS0FBQSxVQUFBLE1BQUE7b0JBQ0EsSUFBQSxRQUFBOztnQkFFQSxHQUFBLEtBQUEsYUFBQSxNQUFBO29CQUNBLElBQUEsV0FBQSxLQUFBOztnQkFFQSxHQUFBLEtBQUEsU0FBQSxPQUFBLEtBQUEsTUFBQSxhQUFBLGFBQUE7b0JBQ0EsSUFBQSxXQUFBLEtBQUEsTUFBQTtvQkFDQSxJQUFBLFlBQUEsS0FBQSxNQUFBOztnQkFFQSxHQUFBLEtBQUEsVUFBQSxRQUFBLEtBQUEsYUFBQSxRQUFBLEtBQUEsY0FBQSxNQUFBO29CQUNBLElBQUEsV0FBQSxLQUFBO29CQUNBLElBQUEsWUFBQSxLQUFBOztnQkFFQSxHQUFBLEtBQUEsU0FBQSxNQUFBO29CQUNBLElBQUEsT0FBQSxLQUFBOztnQkFFQSxHQUFBLEtBQUEsU0FBQSxNQUFBO29CQUNBLElBQUEsT0FBQSxLQUFBOztnQkFFQSxHQUFBLEtBQUEsU0FBQSxNQUFBO29CQUNBLElBQUEsT0FBQSxLQUFBOztnQkFFQSxHQUFBLEtBQUEsY0FBQSxNQUFBO29CQUNBLElBQUEsWUFBQSxLQUFBOztnQkFFQSxJQUFBLE1BQUEsS0FBQTtnQkFDQSxJQUFBLFNBQUEsS0FBQTtnQkFDQSxJQUFBLE9BQUEsS0FBQTtnQkFDQSxJQUFBLFdBQUEsS0FBQSxZQUFBO2dCQUNBLE9BQUE7Ozs7O1FBS0EsT0FBQTs7Ozs7OztBQ2pQQSxDQUFBLFVBQUE7SUFDQTs7Ozs7SUFLQSxRQUFBLE9BQUEsbUJBQUEsV0FBQSx5R0FBQSxTQUFBLElBQUEsUUFBQSxVQUFBLFFBQUEsWUFBQSxlQUFBLGdCQUFBOztRQUVBLElBQUEsUUFBQSxPQUFBLFFBQUE7UUFDQSxRQUFBO1lBQ0EsS0FBQTtnQkFDQSxFQUFBLG9CQUFBLFlBQUE7b0JBQ0EsTUFBQTtvQkFDQSxhQUFBO29CQUNBLE9BQUE7b0JBQ0EsUUFBQTtvQkFDQSxTQUFBLFNBQUEsS0FBQTt3QkFDQSxFQUFBLGdCQUFBLFNBQUEsUUFBQSxLQUFBO3dCQUNBLFNBQUEsV0FBQTs0QkFDQSxPQUFBLGFBQUEsT0FBQTs7d0JBRUEsT0FBQSxhQUFBLE9BQUE7d0JBQ0EsT0FBQTs7O2dCQUdBO1lBQ0EsS0FBQTtnQkFDQSxFQUFBLG9CQUFBLFNBQUE7Z0JBQ0E7OztRQUdBLE9BQUEsU0FBQSxXQUFBO1lBQ0EsV0FBQTtZQUNBLE9BQUEsYUFBQSxPQUFBO1lBQ0EsT0FBQSxTQUFBO1lBQ0EsT0FBQSxhQUFBLE9BQUE7WUFDQSxPQUFBLGFBQUEsZUFBQTtZQUNBLElBQUEsU0FBQSxJQUFBO1lBQ0EsT0FBQSxHQUFBLEtBQUEsT0FBQSxLQUFBLE9BQUEsZUFBQSxLQUFBLFNBQUEsY0FBQTtnQkFDQSxPQUFBLFNBQUEsYUFBQTtnQkFDQSxnQkFBQSxRQUFBLE9BQUE7Z0JBQ0EsZ0JBQUEsT0FBQSxnQkFBQSxvQkFBQSxPQUFBO2dCQUNBLFdBQUE7Ozs7Ozs7OztBQzFDQSxDQUFBLFVBQUE7SUFDQTs7O0lBR0EsUUFBQSxPQUFBLG1CQUFBLFdBQUEsK0VBQUEsU0FBQSxRQUFBLFFBQUEsYUFBQSxjQUFBLFdBQUE7OztRQUdBLE9BQUEsT0FBQSxVQUFBO1lBQ0EsT0FBQSxTQUFBO1lBQ0EsWUFBQSxlQUFBLFNBQUEsTUFBQTtnQkFDQSxPQUFBLE9BQUE7O1lBRUEsYUFBQSxLQUFBLFNBQUEsT0FBQTtnQkFDQSxPQUFBLE9BQUEsVUFBQTs7WUFFQSxXQUFBLFdBQUEsU0FBQSxLQUFBO2dCQUNBLE9BQUEsTUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1FBcUJBLE9BQUE7Ozs7OztBQ3JDQSxDQUFBLFVBQUE7SUFDQTs7O0lBR0EsUUFBQSxPQUFBLG1CQUFBLFdBQUEsd0hBQUEsU0FBQSxRQUFBLFFBQUEsYUFBQSxjQUFBLGNBQUEsWUFBQSxpQkFBQTtRQUNBLE9BQUEsV0FBQTtRQUNBLE9BQUEsYUFBQTtZQUNBLFFBQUE7OztRQUdBLE9BQUEsT0FBQSxVQUFBOztZQUVBLEtBQUEsVUFBQSxXQUFBLFNBQUEsR0FBQTtnQkFDQSxLQUFBLFFBQUEsS0FBQSxhQUFBLEVBQUEsR0FBQSxHQUFBO2dCQUNBLE9BQUE7O1lBRUEsSUFBQSxnQkFBQSxJQUFBO1lBQ0EsT0FBQSxTQUFBO2dCQUNBLGNBQUEsZUFBQTtnQkFDQSxDQUFBLE9BQUEsY0FBQSxXQUFBLElBQUEsTUFBQSxDQUFBLElBQUE7Z0JBQ0EsQ0FBQSxNQUFBLGNBQUEsV0FBQSxNQUFBLENBQUE7O1lBRUEsSUFBQSxhQUFBLElBQUEsT0FBQSxTQUFBO1lBQ0EsT0FBQSxTQUFBLFVBQUEsV0FBQSxlQUFBO2dCQUNBLENBQUEsT0FBQSxXQUFBLFdBQUEsSUFBQSxNQUFBLENBQUEsSUFBQTtnQkFDQSxDQUFBLE1BQUEsV0FBQSxXQUFBLE1BQUEsQ0FBQTs7WUFFQSxPQUFBLFNBQUEsWUFBQTtZQUNBLE9BQUEsU0FBQSxXQUFBOztZQUVBLGFBQUEsa0JBQUEsU0FBQSxRQUFBO2dCQUNBLE9BQUEsU0FBQTs7WUFFQSxnQkFBQSxlQUFBLFNBQUEsWUFBQTtnQkFDQSxPQUFBLGFBQUE7OztZQUdBLEVBQUEsYUFBQTs7WUFFQSxFQUFBLGNBQUEsZUFBQTtnQkFDQSxXQUFBO2dCQUNBLFFBQUE7Z0JBQ0EsV0FBQSxJQUFBO2dCQUNBLFNBQUEsSUFBQTs7O1lBR0EsRUFBQSxjQUFBLE9BQUEsV0FBQTtnQkFDQSxPQUFBLFNBQUEsZUFBQSxPQUFBO2dCQUNBLE9BQUEsU0FBQSxhQUFBLEVBQUEsY0FBQTs7O1lBR0EsRUFBQSxZQUFBLGVBQUE7Z0JBQ0EsV0FBQTtnQkFDQSxRQUFBO2dCQUNBLFdBQUEsSUFBQSxPQUFBLFNBQUE7Z0JBQ0EsU0FBQSxJQUFBLE9BQUEsU0FBQTs7O1lBR0EsRUFBQSxZQUFBLE9BQUEsV0FBQTtnQkFDQSxPQUFBLFNBQUEsZUFBQSxPQUFBO2dCQUNBLE9BQUEsU0FBQSxVQUFBLEVBQUEsWUFBQTs7Ozs7UUFLQSxPQUFBLHNCQUFBLFdBQUE7WUFDQSxHQUFBLE9BQUEsU0FBQSxVQUFBLE1BQUEsT0FBQSxPQUFBLFNBQUEsVUFBQSxZQUFBO2dCQUNBLEVBQUEsbUJBQUEsS0FBQTttQkFDQTtnQkFDQSxFQUFBLG1CQUFBLEtBQUE7Ozs7O1FBS0EsT0FBQSxjQUFBLFNBQUEsT0FBQTtZQUNBLE9BQUE7WUFDQSxPQUFBLFNBQUEsV0FBQSxFQUFBLGFBQUE7WUFDQSxHQUFBLE9BQUEsT0FBQSxTQUFBLFlBQUEsWUFBQSxPQUFBLE9BQUEsU0FBQSxVQUFBLGFBQUE7Z0JBQ0EsSUFBQSxhQUFBLE9BQUEsU0FBQTtnQkFDQSxJQUFBLE1BQUEsV0FBQSxTQUFBLFNBQUE7Z0JBQ0EsSUFBQSxNQUFBLFdBQUEsU0FBQSxTQUFBO2dCQUNBLE9BQUEsU0FBQSxVQUFBLFdBQUE7Z0JBQ0EsT0FBQSxTQUFBLFdBQUE7Z0JBQ0EsT0FBQSxTQUFBLFlBQUE7O2dCQUVBLGFBQUEsS0FBQTtvQkFDQSxRQUFBO29CQUNBLFVBQUE7b0JBQ0EsV0FBQTtvQkFDQSxNQUFBLFdBQUE7bUJBQ0EsU0FBQSxPQUFBO29CQUNBLEdBQUEsTUFBQSxXQUFBLEVBQUE7d0JBQ0EsT0FBQSx1QkFBQSxZQUFBLFNBQUEsT0FBQTs0QkFDQSxPQUFBLFNBQUEsV0FBQSxNQUFBOzRCQUNBLE9BQUEsU0FBQSxXQUFBLE1BQUE7NEJBQ0EsT0FBQSxTQUFBLFlBQUEsTUFBQTs0QkFDQSxPQUFBLFNBQUEsVUFBQSxNQUFBOzRCQUNBLE9BQUEsU0FBQSxXQUFBLE1BQUE7NEJBQ0EsYUFBQSxPQUFBLE9BQUEsVUFBQSxXQUFBO2dDQUNBLE9BQUEsR0FBQTs7OzJCQUdBO3dCQUNBLFFBQUEsTUFBQTt3QkFDQSxPQUFBLFNBQUEsV0FBQSxNQUFBO3dCQUNBLE9BQUEsU0FBQSxZQUFBLE1BQUE7d0JBQ0EsT0FBQSxTQUFBLFVBQUEsTUFBQTt3QkFDQSxPQUFBLFNBQUEsV0FBQSxNQUFBO3dCQUNBLGFBQUEsT0FBQSxPQUFBLFVBQUEsV0FBQTs0QkFDQSxPQUFBLEdBQUE7Ozs7bUJBSUE7Z0JBQ0EsYUFBQSxLQUFBLENBQUEsSUFBQSxPQUFBLFNBQUEsUUFBQSxTQUFBLE1BQUE7b0JBQ0EsR0FBQSxNQUFBLFdBQUE7d0JBQ0EsT0FBQTtvQkFDQSxRQUFBLE1BQUE7b0JBQ0EsT0FBQSxTQUFBLFdBQUEsTUFBQTtvQkFDQSxPQUFBLFNBQUEsWUFBQSxNQUFBO29CQUNBLE9BQUEsU0FBQSxVQUFBLE1BQUE7b0JBQ0EsT0FBQSxTQUFBLFdBQUEsTUFBQTtvQkFDQSxhQUFBLE9BQUEsT0FBQSxVQUFBLFdBQUE7d0JBQ0EsT0FBQSxHQUFBOzs7Ozs7UUFNQSxPQUFBLHlCQUFBLFNBQUEsWUFBQSxVQUFBO1lBQ0EsSUFBQSxRQUFBO1lBQ0EsTUFBQSxPQUFBLFdBQUE7WUFDQSxNQUFBLFlBQUE7WUFDQSxNQUFBLGNBQUE7WUFDQSxNQUFBLGVBQUE7WUFDQSxNQUFBLFFBQUEsV0FBQTtZQUNBLE1BQUEsUUFBQSxXQUFBO1lBQ0EsTUFBQSxTQUFBLFdBQUE7WUFDQSxNQUFBLFdBQUEsV0FBQSxTQUFBLFNBQUE7WUFDQSxNQUFBLFlBQUEsV0FBQSxTQUFBLFNBQUE7WUFDQSxhQUFBLE9BQUEsT0FBQTs7O1FBR0EsT0FBQSxrQkFBQSxVQUFBO1lBQ0EsR0FBQSxPQUFBLE9BQUEsU0FBQSxZQUFBLFVBQUE7Z0JBQ0EsT0FBQSxtQkFBQSxPQUFBLFNBQUE7Z0JBQ0EsT0FBQSxNQUFBLFdBQUEsY0FBQTtvQkFDQSxVQUFBLE9BQUEsU0FBQSxRQUFBLFNBQUEsU0FBQTtvQkFDQSxXQUFBLE9BQUEsU0FBQSxRQUFBLFNBQUEsU0FBQTs7Z0JBRUEsT0FBQSxJQUFBLFdBQUEsT0FBQSxTQUFBLFFBQUEsU0FBQSxTQUFBO2dCQUNBLE9BQUEsSUFBQSxZQUFBLE9BQUEsU0FBQSxRQUFBLFNBQUEsU0FBQTs7OztRQUlBLE9BQUEscUJBQUEsU0FBQSxnQkFBQTtZQUNBLElBQUEsV0FBQTtnQkFDQSxVQUFBLGVBQUEsU0FBQSxTQUFBO2dCQUNBLFdBQUEsZUFBQSxTQUFBLFNBQUE7O1lBRUEsYUFBQSxjQUFBLFVBQUEsU0FBQSxPQUFBO2dCQUNBLEdBQUEsVUFBQSxLQUFBO29CQUNBLE9BQUEsU0FBQSxRQUFBLE1BQUE7Ozs7O1FBS0EsT0FBQSx1QkFBQSxXQUFBO1lBQ0EsSUFBQSxXQUFBLE9BQUE7WUFDQSxHQUFBLFNBQUEsT0FBQSxHQUFBO2dCQUNBLE9BQUEsZUFBQSxTQUFBLE9BQUE7O1lBRUEsT0FBQSxlQUFBLFNBQUEsVUFBQTs7O1FBR0EsT0FBQSxtQkFBQSxXQUFBO1lBQ0EsSUFBQSxhQUFBLE9BQUEsVUFBQSxPQUFBLFNBQUE7WUFDQSxJQUFBLFdBQUEsT0FBQSxVQUFBLE9BQUEsU0FBQTtZQUNBLElBQUEsVUFBQSxhQUFBO1lBQ0EsSUFBQSxXQUFBLEtBQUEsTUFBQSxTQUFBO1lBQ0EsSUFBQSxVQUFBLEtBQUEsTUFBQSxDQUFBLFNBQUEsWUFBQTtZQUNBLElBQUEsV0FBQSxLQUFBLE1BQUEsQ0FBQSxDQUFBLFNBQUEsWUFBQSxXQUFBOztZQUVBLE9BQUE7Z0JBQ0EsTUFBQTtnQkFDQSxPQUFBO2dCQUNBLFNBQUE7Ozs7UUFJQSxPQUFBLFlBQUEsU0FBQSxZQUFBO1lBQ0EsSUFBQSxZQUFBLFdBQUEsTUFBQTtZQUNBLElBQUEsT0FBQSxVQUFBLEdBQUEsTUFBQTtZQUNBLEdBQUEsT0FBQSxVQUFBLE9BQUEsWUFBQTtnQkFDQSxJQUFBLE9BQUEsVUFBQSxHQUFBLE1BQUE7Z0JBQ0EsT0FBQSxJQUFBLEtBQUEsS0FBQSxJQUFBLEtBQUEsSUFBQSxLQUFBOztZQUVBLE9BQUEsSUFBQSxLQUFBLEtBQUEsSUFBQSxLQUFBLElBQUEsS0FBQTs7O1FBR0EsT0FBQSxrQkFBQSxXQUFBO1lBQ0EsT0FBQSxPQUFBLE9BQUEsU0FBQSxhQUFBOzs7UUFHQSxPQUFBOzs7Ozs7QUM1TUEsQ0FBQSxVQUFBO0lBQ0E7Ozs7O0lBS0EsUUFBQSxPQUFBLG1CQUFBLFdBQUEsMEhBQUEsU0FBQSxJQUFBLFVBQUEsUUFBQSxZQUFBLFFBQUEsWUFBQSxpQkFBQSxjQUFBOztRQUVBLElBQUE7OztRQUdBLE9BQUEsa0JBQUEsV0FBQTtZQUNBLElBQUEsUUFBQSxPQUFBLFFBQUE7WUFDQSxRQUFBO2dCQUNBLEtBQUE7b0JBQ0EsT0FBQSxPQUFBO29CQUNBO2dCQUNBLEtBQUE7b0JBQ0EsT0FBQSxPQUFBO29CQUNBOzs7OztRQUtBLFFBQUEsUUFBQSxVQUFBLE1BQUEsWUFBQTtZQUNBLFdBQUE7WUFDQSxJQUFBLFdBQUEsT0FBQSxrQkFBQTtZQUNBLElBQUEsYUFBQSxPQUFBO1lBQ0EsSUFBQSxlQUFBO1lBQ0EsR0FBQSxhQUFBLFFBQUEsT0FBQSxlQUFBLGFBQUE7Z0JBQ0EsSUFBQSxJQUFBLElBQUEsR0FBQSxJQUFBLFdBQUEsUUFBQSxLQUFBO29CQUNBLEdBQUEsV0FBQSxHQUFBLE9BQUEsU0FBQTt3QkFDQSxlQUFBLFdBQUEsR0FBQTs7OztZQUlBLEdBQUEsaUJBQUEsTUFBQTtnQkFDQSxTQUFBLFdBQUE7b0JBQ0EsRUFBQSxvQkFBQSxTQUFBLFFBQUEsS0FBQTs7O1lBR0EsSUFBQSxXQUFBLGVBQUEsb0JBQUE7WUFDQSxJQUFBLFdBQUEsZUFBQSxnQkFBQSxPQUFBLGtCQUFBO1lBQ0EsSUFBQSxRQUFBLEVBQUEsbUJBQUE7WUFDQSxPQUFBLGVBQUEsSUFBQSxPQUFBLEtBQUEsT0FBQSxhQUFBLE1BQUEsSUFBQSxDQUFBLE9BQUEsQ0FBQTtZQUNBLE9BQUEsYUFBQSxZQUFBLGlCQUFBLFlBQUE7Z0JBQ0EsSUFBQSxRQUFBLE9BQUEsYUFBQTtnQkFDQSxPQUFBLGtCQUFBLFNBQUEsTUFBQSxTQUFBLFNBQUEsT0FBQSxNQUFBLFNBQUEsU0FBQSxPQUFBLE1BQUE7Z0JBQ0EsT0FBQTs7OztRQUlBLE9BQUEsZUFBQSxZQUFBO1lBQ0EsR0FBQSxTQUFBO2dCQUNBLFNBQUEsT0FBQTs7WUFFQSxXQUFBLFNBQUEsVUFBQTtnQkFDQSxPQUFBO2dCQUNBLFdBQUE7ZUFDQTs7OztRQUlBLE9BQUEsaUJBQUEsU0FBQSxVQUFBO1lBQ0EsR0FBQSxhQUFBLEtBQUE7Z0JBQ0EsT0FBQSxrQkFBQSxXQUFBO2dCQUNBLEVBQUEsb0JBQUEsU0FBQSxRQUFBLEtBQUE7bUJBQ0E7Z0JBQ0EsT0FBQSxrQkFBQSxXQUFBLFNBQUE7Z0JBQ0EsRUFBQSxvQkFBQSxTQUFBLFFBQUEsS0FBQSxTQUFBOztZQUVBLE9BQUE7OztRQUdBLE9BQUEsYUFBQSxTQUFBLE9BQUE7WUFDQSxPQUFBLGtCQUFBLE9BQUE7WUFDQSxPQUFBLGtCQUFBLE9BQUE7WUFDQSxHQUFBLFVBQUEsTUFBQTtnQkFDQSxFQUFBLGdCQUFBLFNBQUEsUUFBQSxLQUFBOztpQkFFQTtnQkFDQSxFQUFBLGdCQUFBLFNBQUEsUUFBQSxLQUFBOztZQUVBLE9BQUE7OztRQUdBLE9BQUEsa0JBQUEsV0FBQTtZQUNBLFdBQUE7WUFDQSxPQUFBLGdCQUFBLE9BQUE7WUFDQSxPQUFBLFNBQUE7WUFDQSxPQUFBLGdCQUFBLE9BQUE7WUFDQSxPQUFBLGdCQUFBLGVBQUE7WUFDQSxJQUFBLFNBQUEsSUFBQTtZQUNBLE9BQUEsR0FBQSxLQUFBLE9BQUEsS0FBQSxPQUFBLGtCQUFBLEtBQUEsU0FBQSxjQUFBO2dCQUNBLE9BQUEsWUFBQSxhQUFBO2dCQUNBLGdCQUFBLFlBQUEsT0FBQTtnQkFDQSxnQkFBQSxPQUFBLGdCQUFBO2dCQUNBLFdBQUE7Ozs7UUFJQSxPQUFBLGFBQUEsV0FBQTtZQUNBLFdBQUE7WUFDQSxPQUFBLGFBQUEsT0FBQTtZQUNBLE9BQUEsU0FBQTtZQUNBLE9BQUEsYUFBQSxPQUFBO1lBQ0EsT0FBQSxhQUFBLGVBQUE7WUFDQSxJQUFBLFNBQUEsSUFBQTtZQUNBLE9BQUEsR0FBQSxLQUFBLE9BQUEsS0FBQSxPQUFBLGVBQUEsS0FBQSxTQUFBLGNBQUE7Z0JBQ0EsT0FBQSxTQUFBLGFBQUE7Z0JBQ0EsZ0JBQUEsUUFBQSxPQUFBO2dCQUNBLGdCQUFBLE9BQUEsZ0JBQUEsb0JBQUEsT0FBQTtnQkFDQSxXQUFBOzs7O1FBSUEsT0FBQSxTQUFBLFdBQUE7WUFDQSxJQUFBLFFBQUEsT0FBQSxRQUFBO1lBQ0EsUUFBQTtnQkFDQSxLQUFBO29CQUNBLE9BQUE7b0JBQ0E7Z0JBQ0EsS0FBQTtvQkFDQSxPQUFBO29CQUNBOzs7Ozs7OztBQzVIQSxDQUFBLFVBQUE7SUFDQTs7Ozs7SUFLQSxRQUFBLE9BQUEsbUJBQUEsV0FBQSwwRUFBQSxTQUFBLElBQUEsUUFBQSxRQUFBLFlBQUEsZ0JBQUE7OztRQUdBLE9BQUEsT0FBQSxVQUFBLFdBQUE7WUFDQSxHQUFBLE9BQUEsUUFBQTtnQkFDQSxJQUFBLElBQUEsSUFBQSxHQUFBLElBQUEsT0FBQSxPQUFBLFFBQUEsS0FBQTtvQkFDQSxJQUFBLFdBQUEsT0FBQSxPQUFBLEdBQUEsa0JBQUEsT0FBQSxhQUFBO29CQUNBLEdBQUEsV0FBQSxJQUFBO3dCQUNBLFdBQUE7O29CQUVBLE9BQUEsT0FBQSxHQUFBLFdBQUEsV0FBQSxXQUFBLGFBQUE7O2dCQUVBLFdBQUE7Ozs7Ozs7O0FDbEJBLENBQUEsVUFBQTtJQUNBO0lBQ0EsUUFBQSxPQUFBLG1CQUFBLFdBQUEsNEpBQUEsU0FBQSxRQUFBLFlBQUEsZUFBQSxVQUFBLElBQUEsT0FBQSxRQUFBLFdBQUEsT0FBQSxZQUFBLGFBQUEsU0FBQTs7UUFFQSxPQUFBLGFBQUEsV0FBQTtRQUNBLE9BQUEsY0FBQSxXQUFBO1FBQ0EsT0FBQSxZQUFBLFdBQUE7O1FBRUEsT0FBQSxZQUFBO1lBQ0EsT0FBQTtZQUNBLFVBQUE7OztRQUdBLE9BQUEsZUFBQTtZQUNBLE1BQUE7OztRQUdBLEVBQUEsUUFBQSxNQUFBLFNBQUEsT0FBQTtZQUNBO2dCQUNBLENBQUEsRUFBQSxNQUFBLFFBQUEsR0FBQTs7Z0JBRUEsQ0FBQSxFQUFBLE1BQUEsUUFBQSxHQUFBOztnQkFFQSxDQUFBLEVBQUEsTUFBQSxRQUFBLEdBQUE7YUFDQTtnQkFDQSxPQUFBOzs7OztRQUtBLFdBQUEsSUFBQSx1QkFBQSxVQUFBO1lBQ0EsSUFBQSxVQUFBLEVBQUEsZ0JBQUEsUUFBQTtZQUNBLElBQUEsVUFBQSxFQUFBLGdCQUFBLFFBQUE7WUFDQSxJQUFBLFNBQUEsRUFBQSxlQUFBLFFBQUE7WUFDQSxPQUFBLFVBQUEsT0FBQSxVQUFBO2dCQUNBLEtBQUE7b0JBQ0EsUUFBQSxZQUFBO29CQUNBLE9BQUEsWUFBQTtvQkFDQSxRQUFBLFNBQUE7b0JBQ0E7Z0JBQ0EsS0FBQTtvQkFDQSxPQUFBLFlBQUE7b0JBQ0EsUUFBQSxZQUFBO29CQUNBLFFBQUEsU0FBQTtvQkFDQTtnQkFDQSxNQUFBO29CQUNBLFFBQUEsWUFBQTtvQkFDQSxRQUFBLFlBQUE7b0JBQ0EsT0FBQSxTQUFBO29CQUNBOzs7Ozs7O1FBT0EsT0FBQSxTQUFBLFdBQUE7WUFDQSxjQUFBLFFBQUE7WUFDQSxHQUFBLEtBQUEsWUFBQSxVQUFBLEtBQUEsV0FBQTtnQkFDQSxTQUFBLFdBQUE7b0JBQ0EsT0FBQSxPQUFBOzs7Ozs7OztRQVFBLE9BQUEsY0FBQSxXQUFBO1lBQ0EsT0FBQTtZQUNBLFlBQUEsTUFBQSxPQUFBLFdBQUEsS0FBQSxTQUFBLFVBQUE7Z0JBQ0EsR0FBQSxPQUFBLFNBQUEsUUFBQSxhQUFBO29CQUNBLE9BQUEsT0FBQTtvQkFDQSxPQUFBO29CQUNBLE9BQUE7dUJBQ0E7d0JBQ0EsT0FBQSxhQUFBO3dCQUNBLEVBQUEsZ0JBQUEsWUFBQTs7Ozs7Ozs7UUFRQSxPQUFBLGdCQUFBLFlBQUE7WUFDQSxHQUFBLEtBQUEsWUFBQSxjQUFBLFdBQUEsS0FBQSxTQUFBLElBQUE7Z0JBQ0EsR0FBQSxHQUFBLGdCQUFBLEdBQUEsYUFBQSxhQUFBO29CQUNBLE9BQUEsWUFBQSxDQUFBLHVCQUFBLEdBQUEsYUFBQTtvQkFDQSxPQUFBO3dCQUNBO29CQUNBLE9BQUEsYUFBQTtvQkFDQSxFQUFBLGdCQUFBLFlBQUE7Ozs7Ozs7O1FBUUEsT0FBQSxjQUFBLFlBQUE7WUFDQSxNQUFBLEtBQUEsbUNBQUEsUUFBQSxVQUFBLFVBQUEsUUFBQSxTQUFBO2dCQUNBLFFBQUEsSUFBQSxlQUFBO2dCQUNBLFFBQUEsSUFBQSxZQUFBO2dCQUNBLFFBQUEsSUFBQSxhQUFBOzs7O1FBSUEsT0FBQSxxQkFBQSxXQUFBO1lBQ0EsRUFBQSxlQUFBLFlBQUE7WUFDQSxFQUFBLGtCQUFBLFlBQUE7WUFDQSxFQUFBLG1CQUFBOzs7Ozs7UUFNQSxPQUFBLFdBQUEsV0FBQTtZQUNBLE9BQUE7Ozs7OztRQU1BLE9BQUEsbUJBQUEsV0FBQTtZQUNBLE9BQUE7Ozs7Ozs7UUFPQSxPQUFBLFVBQUEsT0FBQSxVQUFBO1lBQ0EsS0FBQTtnQkFDQSxFQUFBLGdCQUFBLFFBQUEsTUFBQSxTQUFBO2dCQUNBO1lBQ0EsS0FBQTtnQkFDQSxFQUFBLGdCQUFBLFFBQUEsTUFBQSxTQUFBO2dCQUNBO1lBQ0EsS0FBQTtnQkFDQSxFQUFBLGdCQUFBLFFBQUEsTUFBQSxTQUFBO2dCQUNBO1lBQ0EsTUFBQTtnQkFDQSxFQUFBLGVBQUEsUUFBQSxNQUFBLFNBQUE7Z0JBQ0E7Ozs7Ozs7QUNoSkEsQ0FBQSxVQUFBOzs7O0lBSUEsUUFBQSxPQUFBLG1CQUFBLFdBQUEsb1BBQUEsU0FBQSxJQUFBLFFBQUEsVUFBQSxTQUFBLGVBQUEsUUFBQSxvQkFBQSxZQUFBLGVBQUEsY0FBQSxPQUFBLGFBQUEsZ0JBQUEsWUFBQSxpQkFBQSxrQkFBQTs7O1FBR0EsT0FBQSxPQUFBLFVBQUE7WUFDQSxPQUFBLG9CQUFBLEVBQUEsUUFBQSxXQUFBO1lBQ0EsUUFBQSxXQUFBLE9BQUE7O1lBRUEsT0FBQSx5QkFBQSxJQUFBLGVBQUEsQ0FBQSxNQUFBO1lBQ0EsR0FBQSxPQUFBLE9BQUEsaUJBQUEsZUFBQSxPQUFBLGlCQUFBLE1BQUE7Z0JBQ0EsT0FBQSxlQUFBLElBQUEsZUFBQSxDQUFBLE1BQUE7O1lBRUEsT0FBQSx5QkFBQSxJQUFBLGVBQUEsQ0FBQSxNQUFBO1lBQ0EsR0FBQSxPQUFBLE9BQUEsa0JBQUEsZUFBQSxPQUFBLGtCQUFBLE1BQUE7Z0JBQ0EsT0FBQSxnQkFBQSxJQUFBLGVBQUEsQ0FBQSxNQUFBOztZQUVBLFdBQUE7WUFDQSxPQUFBO1lBQ0EsT0FBQTtZQUNBLE9BQUE7WUFDQSxnQkFBQSxVQUFBLGdCQUFBLG9CQUFBLFFBQUEsV0FBQTtnQkFDQSxTQUFBLFdBQUE7b0JBQ0EsR0FBQSxRQUFBLGdCQUFBLGlCQUFBLGVBQUEsZ0JBQUEsaUJBQUEsTUFBQTt3QkFDQSxPQUFBLFNBQUEsZ0JBQUE7Ozs7WUFJQSxnQkFBQSxVQUFBLGdCQUFBLG9CQUFBLFFBQUEsWUFBQTtnQkFDQSxTQUFBLFlBQUE7b0JBQ0EsR0FBQSxRQUFBLGdCQUFBLFVBQUEsZUFBQSxnQkFBQSxVQUFBLE1BQUE7d0JBQ0EsT0FBQSxTQUFBLGdCQUFBOzs7OztZQUtBLGdCQUFBLFVBQUEsZ0JBQUEsbUJBQUEsUUFBQSxXQUFBO2dCQUNBLElBQUEsS0FBQSxJQUFBO2dCQUNBLE9BQUEsaUJBQUEsSUFBQSxPQUFBLGNBQUE7O1lBRUEsZ0JBQUEsVUFBQSxnQkFBQSxtQkFBQSxRQUFBLFdBQUE7Z0JBQ0EsSUFBQSxLQUFBLElBQUE7Z0JBQ0EsT0FBQSxpQkFBQSxJQUFBLE9BQUEsZUFBQTs7OztRQUlBLE9BQUEsYUFBQSxXQUFBO1lBQ0EsSUFBQSxTQUFBLElBQUE7WUFDQSxJQUFBLGFBQUEsSUFBQTtZQUNBLE9BQUEsR0FBQSxLQUFBLE9BQUEsS0FBQSxPQUFBLGVBQUEsS0FBQSxTQUFBLGNBQUE7Z0JBQ0EsT0FBQSxHQUFBLEtBQUEsV0FBQSxLQUFBLE9BQUEseUJBQUEsS0FBQSxTQUFBLGlCQUFBO29CQUNBLEdBQUEsT0FBQSxvQkFBQSxlQUFBLG9CQUFBLEtBQUE7d0JBQ0EsT0FBQTs7b0JBRUEsT0FBQSxTQUFBLGFBQUE7b0JBQ0EsT0FBQSxrQkFBQSxnQkFBQTtvQkFDQSxnQkFBQSxPQUFBLGdCQUFBO29CQUNBLE9BQUEsaUJBQUEsUUFBQSxPQUFBLGNBQUE7b0JBQ0EsT0FBQTs7Ozs7UUFLQSxPQUFBLGFBQUEsV0FBQTtZQUNBLElBQUEsU0FBQSxJQUFBO1lBQ0EsSUFBQSxhQUFBLElBQUE7WUFDQSxPQUFBLEdBQUEsS0FBQSxPQUFBLEtBQUEsT0FBQSxnQkFBQSxLQUFBLFNBQUEsY0FBQTtnQkFDQSxPQUFBLEdBQUEsS0FBQSxXQUFBLEtBQUEsT0FBQSx5QkFBQSxLQUFBLFNBQUEsaUJBQUE7b0JBQ0EsT0FBQSxTQUFBLGFBQUE7b0JBQ0EsT0FBQSxrQkFBQSxnQkFBQTtvQkFDQSxnQkFBQSxPQUFBLGdCQUFBO29CQUNBLE9BQUEsaUJBQUEsUUFBQSxPQUFBLGVBQUE7b0JBQ0EsT0FBQTs7Ozs7UUFLQSxPQUFBLFdBQUEsV0FBQTtZQUNBLEdBQUEsS0FBQSxZQUFBLGtCQUFBLEtBQUEsU0FBQSxNQUFBO2dCQUNBLE9BQUEsT0FBQTs7OztRQUlBLE9BQUEsbUJBQUEsU0FBQSxpQkFBQSxpQkFBQSxjQUFBO1lBQ0EsRUFBQSxRQUFBLE9BQUEsVUFBQTtnQkFDQSxJQUFBLFFBQUEsT0FBQSxRQUFBO2dCQUNBLElBQUEsY0FBQSxXQUFBLFVBQUEsU0FBQSxjQUFBO2dCQUNBLElBQUEsb0JBQUEsRUFBQSwrQkFBQSxHQUFBLEdBQUE7Z0JBQ0EsR0FBQSxPQUFBLGNBQUEsT0FBQSxPQUFBLFVBQUEsZUFBQSxvQkFBQSxLQUFBO29CQUNBLFFBQUEsSUFBQTtvQkFDQSxHQUFBLFVBQUEsUUFBQSxhQUFBO3dCQUNBLEdBQUEsS0FBQSxnQkFBQSxLQUFBLGtCQUFBLEtBQUEsU0FBQSxTQUFBOzRCQUNBLEdBQUEsV0FBQSxRQUFBLE1BQUE7Z0NBQ0EsSUFBQSxJQUFBLEdBQUEsR0FBQSxJQUFBLFFBQUEsS0FBQSxRQUFBLEtBQUE7b0NBQ0EsT0FBQSxjQUFBLEtBQUEsUUFBQSxLQUFBOztnQ0FFQSxPQUFBLHNCQUFBLGNBQUE7Ozs7Ozs7O1FBUUEsT0FBQSx3QkFBQSxTQUFBLGNBQUEsU0FBQTtZQUNBLE9BQUE7Z0JBQ0EsS0FBQTtvQkFDQSxnQkFBQSxRQUFBLFFBQUE7b0JBQ0EsZ0JBQUEsT0FBQSxnQkFBQTtvQkFDQTs7Z0JBRUEsS0FBQTtvQkFDQSxnQkFBQSxlQUFBLFFBQUE7b0JBQ0EsZ0JBQUEsT0FBQSxnQkFBQTtvQkFDQTs7OztRQUlBLE9BQUEsaUJBQUEsWUFBQTtZQUNBLElBQUEsV0FBQSxFQUFBLFFBQUE7O1dBRUEsRUFBQSw0QkFBQSxLQUFBLFNBQUEsR0FBQSxTQUFBO2VBQ0EsSUFBQSxTQUFBLEVBQUE7ZUFDQSxJQUFBLFlBQUEsT0FBQSxTQUFBO2VBQ0EsSUFBQSxXQUFBLE9BQUEsb0JBQUEsV0FBQTt1QkFDQSxPQUFBLFlBQUE7dUJBQ0EsT0FBQSxZQUFBLE9BQUEsS0FBQTs7O1lBR0EsRUFBQSxrQ0FBQSxLQUFBLFVBQUEsR0FBQSxTQUFBO2dCQUNBLElBQUEsU0FBQSxFQUFBO2dCQUNBLElBQUEsWUFBQSxPQUFBLFNBQUE7Z0JBQ0EsSUFBQSxXQUFBLE9BQUEsb0JBQUEsV0FBQTtvQkFDQSxHQUFBLE9BQUEsS0FBQSxjQUFBLElBQUEsR0FBQTs7NEJBRUEsT0FBQSxTQUFBOzRCQUNBLE9BQUEsU0FBQSxPQUFBLEtBQUE7Ozs7Ozs7UUFPQSxPQUFBLGFBQUEsU0FBQSxRQUFBO1lBQ0EsSUFBQSxVQUFBLEVBQUEsS0FBQSxPQUFBLFVBQUEsZ0JBQUEsTUFBQSxPQUFBO1lBQ0EsR0FBQSxRQUFBLFNBQUEsMEJBQUE7Z0JBQ0EsT0FBQTs7bUJBRUE7Z0JBQ0EsT0FBQSxTQUFBOzs7OztRQUtBLE9BQUEsV0FBQSxTQUFBLFFBQUE7WUFDQSxJQUFBLFFBQUEsYUFBQSxPQUFBLFlBQUEsUUFBQSxPQUFBO1lBQ0EsSUFBQSxlQUFBLE9BQUEsVUFBQSxlQUFBLEtBQUEsT0FBQTtZQUNBLEVBQUEsZUFBQSxLQUFBLFdBQUE7WUFDQSxFQUFBLFNBQUEsS0FBQTtZQUNBLEVBQUEsY0FBQSxLQUFBLFdBQUE7WUFDQSxFQUFBLGVBQUEsS0FBQSxXQUFBLE9BQUE7WUFDQSxFQUFBLHFCQUFBLEtBQUEsV0FBQSxPQUFBO1lBQ0EsRUFBQSxhQUFBLEtBQUEsV0FBQSx5QkFBQTtZQUNBLEVBQUEsb0JBQUEsWUFBQSx5QkFBQSxDQUFBLFVBQUE7WUFDQSxXQUFBLGVBQUE7WUFDQSxtQkFBQSxTQUFBO1lBQ0EsRUFBQSxNQUFBLGNBQUEsU0FBQSx5QkFBQSxDQUFBLFVBQUE7OztRQUdBLE9BQUEsWUFBQSxXQUFBO1lBQ0EsRUFBQSxvQkFBQSxZQUFBLHlCQUFBLENBQUEsVUFBQTtZQUNBLE9BQUEsTUFBQSxXQUFBOzs7O1FBSUEsT0FBQTs7Ozs7O0FDakxBLENBQUEsVUFBQTtJQUNBOzs7OztJQUtBLFFBQUEsT0FBQSxtQkFBQSxXQUFBLDRJQUFBLFNBQUEsSUFBQSxRQUFBLFFBQUEsYUFBQSxnQkFBQSxZQUFBLGlCQUFBLG9CQUFBLFdBQUE7OztZQUdBLElBQUEsV0FBQSxFQUFBO1lBQ0EsSUFBQSxpQkFBQTtZQUNBLElBQUEsS0FBQSxFQUFBO1lBQ0EsSUFBQSxZQUFBLEVBQUEsZ0JBQUEsU0FBQTtZQUNBLElBQUEsZUFBQSxFQUFBLGdCQUFBOztZQUVBLEVBQUEsUUFBQSxPQUFBLFVBQUE7Z0JBQ0EsR0FBQSxFQUFBLFVBQUEsVUFBQTtvQkFDQSxJQUFBLFFBQUEsRUFBQSxVQUFBLFNBQUEsTUFBQSxlQUFBLEtBQUE7b0JBQ0EsSUFBQSxZQUFBLEVBQUEsUUFBQTtvQkFDQSxJQUFBLFlBQUEsT0FBQSxZQUFBLFVBQUE7d0JBQ0EsR0FBQSxJQUFBLEVBQUEsVUFBQSxTQUFBLEtBQUEsR0FBQSxlQUFBOzt5QkFFQTt3QkFDQSxHQUFBLElBQUEsRUFBQSxVQUFBLElBQUEsS0FBQSxJQUFBLGVBQUE7OztvQkFHQSxJQUFBLFFBQUEsV0FBQTt3QkFDQSxJQUFBLE9BQUEsUUFBQTt3QkFDQSxHQUFBLElBQUEsQ0FBQSxLQUFBOzs7Ozs7UUFNQSxPQUFBLGdCQUFBLFdBQUE7WUFDQSxHQUFBLEtBQUEsV0FBQSxXQUFBLE9BQUEsb0JBQUEsS0FBQSxTQUFBLFlBQUE7Z0JBQ0EsT0FBQSxhQUFBO2dCQUNBLG1CQUFBLEtBQUEsU0FBQSxNQUFBO29CQUNBLE9BQUE7Ozs7O1FBS0EsT0FBQSxjQUFBLFNBQUEsU0FBQTtZQUNBLElBQUEsUUFBQSxPQUFBLFFBQUE7WUFDQSxRQUFBO2dCQUNBLEtBQUE7b0JBQ0EsT0FBQSxpQkFBQTtvQkFDQTtnQkFDQSxLQUFBO29CQUNBLE9BQUEsaUJBQUE7b0JBQ0E7Ozs7UUFJQSxPQUFBLG1CQUFBLFNBQUEsU0FBQTtZQUNBLEdBQUE7Z0JBQ0EsT0FBQSxXQUFBLGdCQUFBOztnQkFFQSxPQUFBLFdBQUEsZ0JBQUEsT0FBQTs7WUFFQSxPQUFBLFdBQUE7OztRQUdBLE9BQUEsbUJBQUEsU0FBQSxTQUFBO1lBQ0EsR0FBQTtnQkFDQSxPQUFBLFdBQUEsZ0JBQUE7O2dCQUVBLE9BQUEsV0FBQSxnQkFBQSxPQUFBOztZQUVBLE9BQUEsV0FBQTs7OztRQUlBLE9BQUEsZ0JBQUEsV0FBQTtZQUNBLElBQUEsUUFBQSxPQUFBLFFBQUE7WUFDQSxRQUFBO2dCQUNBLEtBQUE7b0JBQ0EsT0FBQTtvQkFDQTtnQkFDQSxLQUFBO29CQUNBLE9BQUE7b0JBQ0E7Ozs7UUFJQSxPQUFBLHFCQUFBLFdBQUE7WUFDQSxHQUFBLEtBQUEsV0FBQSxXQUFBLE9BQUEsZUFBQSxLQUFBLFNBQUEsWUFBQTtnQkFDQSxPQUFBLGFBQUE7Z0JBQ0EsT0FBQSxXQUFBO2dCQUNBLEdBQUEsT0FBQSxnQkFBQSxVQUFBLGVBQUEsZ0JBQUEsVUFBQSxLQUFBO29CQUNBLE9BQUEsWUFBQSxnQkFBQTt1QkFDQTtvQkFDQSxPQUFBOzs7OztRQUtBLE9BQUEscUJBQUEsV0FBQTtZQUNBLEdBQUEsS0FBQSxXQUFBLFdBQUEsT0FBQSxnQkFBQSxLQUFBLFNBQUEsWUFBQTtnQkFDQSxPQUFBLGFBQUE7Z0JBQ0EsT0FBQSxXQUFBO2dCQUNBLEdBQUEsT0FBQSxnQkFBQSxpQkFBQSxlQUFBLGdCQUFBLGlCQUFBLEtBQUE7b0JBQ0EsT0FBQSxZQUFBLGdCQUFBO3VCQUNBO29CQUNBLE9BQUE7Ozs7O1FBS0EsT0FBQSxhQUFBLFdBQUE7WUFDQSxJQUFBLFFBQUEsT0FBQSxRQUFBO1lBQ0EsUUFBQTtnQkFDQSxLQUFBO29CQUNBLE9BQUE7b0JBQ0E7Z0JBQ0EsS0FBQTtvQkFDQSxPQUFBO29CQUNBOzs7O1FBSUEsT0FBQSxrQkFBQSxXQUFBO1lBQ0EsSUFBQSxRQUFBLE9BQUEsUUFBQTtZQUNBLFFBQUE7Z0JBQ0EsS0FBQTtvQkFDQSxPQUFBLE9BQUE7b0JBQ0E7Z0JBQ0EsS0FBQTtvQkFDQSxPQUFBLE9BQUE7b0JBQ0E7Ozs7O1FBS0EsT0FBQSxrQkFBQSxXQUFBO1lBQ0EsR0FBQSxnQkFBQTtnQkFDQSxPQUFBLFdBQUEsZ0JBQUEsZ0JBQUE7OztRQUdBLE9BQUEsa0JBQUEsV0FBQTtZQUNBLEdBQUEsZ0JBQUE7Z0JBQ0EsT0FBQSxXQUFBLGdCQUFBLGdCQUFBOzs7UUFHQSxRQUFBLFFBQUEsVUFBQSxNQUFBLFlBQUE7WUFDQSxPQUFBOzs7O1FBSUEsZ0JBQUEsVUFBQSxnQkFBQSxvQkFBQSxRQUFBLE9BQUE7UUFDQSxnQkFBQSxVQUFBLGdCQUFBLG9CQUFBLFFBQUEsT0FBQTs7UUFFQSxnQkFBQSxVQUFBLGdCQUFBLG1CQUFBLFFBQUEsT0FBQTtRQUNBLGdCQUFBLFVBQUEsZ0JBQUEsbUJBQUEsUUFBQSxPQUFBOzs7Ozs7QUMxSkEsQ0FBQSxVQUFBO0lBQ0E7Ozs7O0lBS0EsUUFBQSxPQUFBLG1CQUFBLFdBQUEseUhBQUEsU0FBQSxJQUFBLFVBQUEsUUFBQSxZQUFBLFFBQUEsWUFBQSxpQkFBQSxhQUFBOztRQUVBLElBQUE7UUFDQSxPQUFBLHFCQUFBO1FBQ0EsT0FBQSxzQkFBQTs7UUFFQSxPQUFBLGtCQUFBLFdBQUE7WUFDQSxJQUFBLFFBQUEsT0FBQSxRQUFBO1lBQ0EsUUFBQTtnQkFDQSxLQUFBO29CQUNBLE9BQUEsT0FBQTtvQkFDQTtnQkFDQSxLQUFBO29CQUNBLE9BQUEsT0FBQTtvQkFDQTs7OztRQUlBLFFBQUEsUUFBQSxVQUFBLE1BQUEsWUFBQTtZQUNBLFdBQUE7O1lBRUEsSUFBQSxXQUFBLE9BQUEsa0JBQUE7WUFDQSxJQUFBLGFBQUEsT0FBQTtZQUNBLElBQUEsZUFBQTtZQUNBLEdBQUEsYUFBQSxRQUFBLE9BQUEsZUFBQSxhQUFBO2dCQUNBLElBQUEsSUFBQSxJQUFBLEdBQUEsSUFBQSxXQUFBLFFBQUEsS0FBQTtvQkFDQSxHQUFBLFdBQUEsR0FBQSxPQUFBLFNBQUE7d0JBQ0EsZUFBQSxXQUFBLEdBQUE7Ozs7WUFJQSxHQUFBLGlCQUFBLE1BQUE7Z0JBQ0EsU0FBQSxXQUFBO29CQUNBLEVBQUEsb0JBQUEsU0FBQSxRQUFBLEtBQUE7Ozs7WUFJQSxJQUFBLFdBQUEsZUFBQSxvQkFBQSxPQUFBLGNBQUE7WUFDQSxJQUFBLFdBQUEsZUFBQSxnQkFBQSxPQUFBLGNBQUE7WUFDQSxJQUFBLFFBQUEsRUFBQSxtQkFBQTtZQUNBLE9BQUEsZUFBQSxJQUFBLE9BQUEsS0FBQSxPQUFBLGFBQUEsTUFBQSxJQUFBLENBQUEsT0FBQSxDQUFBO1lBQ0EsT0FBQSxhQUFBLFlBQUEsaUJBQUEsWUFBQTtnQkFDQSxJQUFBLFFBQUEsT0FBQSxhQUFBO2dCQUNBLE9BQUEsa0JBQUEsU0FBQSxNQUFBLFNBQUEsU0FBQSxPQUFBLE1BQUEsU0FBQSxTQUFBO2dCQUNBLE9BQUE7Ozs7UUFJQSxPQUFBLGVBQUEsWUFBQTtZQUNBLEdBQUEsU0FBQTtnQkFDQSxTQUFBLE9BQUE7O1lBRUEsV0FBQSxTQUFBLFVBQUE7Z0JBQ0EsT0FBQTtnQkFDQSxXQUFBO2VBQ0E7Ozs7UUFJQSxPQUFBLGlCQUFBLFNBQUEsVUFBQTtZQUNBLEdBQUEsYUFBQSxLQUFBO2dCQUNBLE9BQUEsa0JBQUEsV0FBQTtnQkFDQSxFQUFBLG9CQUFBLFNBQUEsUUFBQSxLQUFBO21CQUNBO2dCQUNBLE9BQUEsa0JBQUEsV0FBQSxTQUFBO2dCQUNBLEVBQUEsb0JBQUEsU0FBQSxRQUFBLEtBQUEsU0FBQTs7WUFFQSxPQUFBOzs7UUFHQSxPQUFBLGtCQUFBLFdBQUE7WUFDQSxXQUFBO1lBQ0EsT0FBQSxpQkFBQSxPQUFBO1lBQ0EsT0FBQSxZQUFBO1lBQ0EsT0FBQSxpQkFBQSxPQUFBO1lBQ0EsT0FBQSxpQkFBQSxlQUFBO1lBQ0EsT0FBQSxpQkFBQSxZQUFBO1lBQ0EsSUFBQSxTQUFBLElBQUE7WUFDQSxPQUFBLEdBQUEsS0FBQSxPQUFBLEtBQUEsT0FBQSxtQkFBQSxLQUFBLFNBQUEsY0FBQTtnQkFDQSxPQUFBLFlBQUEsYUFBQTtnQkFDQSxnQkFBQSxhQUFBLE9BQUE7Z0JBQ0EsZ0JBQUEsT0FBQSxnQkFBQTtnQkFDQSxXQUFBOzs7O1FBSUEsT0FBQSxhQUFBLFdBQUE7WUFDQSxXQUFBO1lBQ0EsT0FBQSxjQUFBLE9BQUE7WUFDQSxPQUFBLFNBQUE7WUFDQSxPQUFBLGNBQUEsT0FBQTtZQUNBLE9BQUEsY0FBQSxlQUFBO1lBQ0EsSUFBQSxTQUFBLElBQUE7WUFDQSxPQUFBLEdBQUEsS0FBQSxPQUFBLEtBQUEsT0FBQSxnQkFBQSxLQUFBLFNBQUEsY0FBQTtnQkFDQSxPQUFBLFNBQUEsYUFBQTtnQkFDQSxnQkFBQSxlQUFBLE9BQUE7Z0JBQ0EsZ0JBQUEsT0FBQSxnQkFBQTtnQkFDQSxXQUFBOzs7OztRQUtBLE9BQUEsU0FBQSxXQUFBO1lBQ0EsSUFBQSxRQUFBLE9BQUEsUUFBQTtZQUNBLFFBQUE7Z0JBQ0EsS0FBQTtvQkFDQSxPQUFBO29CQUNBO2dCQUNBLEtBQUE7b0JBQ0EsT0FBQTtvQkFDQTs7Ozs7Ozs7QUNwSEEsQ0FBQSxVQUFBO0lBQ0E7OztJQUdBLFFBQUEsT0FBQSxtQkFBQSxXQUFBLHVDQUFBLFNBQUEsUUFBQSxXQUFBO1FBQ0EsT0FBQSxPQUFBLFVBQUEsV0FBQTtZQUNBLEdBQUEsT0FBQSxRQUFBO2dCQUNBLFdBQUE7Ozs7Ozs7O0FDUEEsQ0FBQSxVQUFBO0lBQ0E7Ozs7O0lBS0EsUUFBQSxPQUFBLG1CQUFBLFdBQUEsNEdBQUEsU0FBQSxJQUFBLFFBQUEsVUFBQSxRQUFBLFlBQUEsZUFBQSxnQkFBQTs7UUFFQSxJQUFBLFFBQUEsT0FBQSxRQUFBO1FBQ0EsUUFBQTtZQUNBLEtBQUE7Z0JBQ0EsRUFBQSxvQkFBQSxZQUFBO29CQUNBLE1BQUE7b0JBQ0EsYUFBQTtvQkFDQSxPQUFBO29CQUNBLFFBQUE7b0JBQ0EsU0FBQSxTQUFBLEtBQUE7d0JBQ0EsRUFBQSxnQkFBQSxTQUFBLFFBQUEsS0FBQTt3QkFDQSxTQUFBLFdBQUE7NEJBQ0EsT0FBQSxnQkFBQSxPQUFBOzt3QkFFQSxPQUFBLGdCQUFBLE9BQUE7d0JBQ0EsT0FBQTs7O2dCQUdBO1lBQ0EsS0FBQTtnQkFDQSxFQUFBLG9CQUFBLFNBQUE7Z0JBQ0E7OztRQUdBLE9BQUEsU0FBQSxXQUFBO1lBQ0EsV0FBQTtZQUNBLE9BQUEsZ0JBQUEsT0FBQTtZQUNBLE9BQUEsWUFBQTtZQUNBLE9BQUEsZ0JBQUEsT0FBQTtZQUNBLE9BQUEsZ0JBQUEsZUFBQTtZQUNBLElBQUEsU0FBQSxJQUFBO1lBQ0EsT0FBQSxHQUFBLEtBQUEsT0FBQSxLQUFBLE9BQUEsa0JBQUEsS0FBQSxTQUFBLGNBQUE7Z0JBQ0EsUUFBQSxJQUFBLGFBQUE7Z0JBQ0EsT0FBQSxZQUFBLGFBQUE7Z0JBQ0EsZ0JBQUEsWUFBQSxPQUFBO2dCQUNBLGdCQUFBLE9BQUEsZ0JBQUEsd0JBQUEsT0FBQTtnQkFDQSxXQUFBOzs7Ozs7QUM5Q0EsQ0FBQSxVQUFBO0lBQ0E7OztJQUdBLFFBQUEsT0FBQSxtQkFBQSxXQUFBLG1MQUFBLFNBQUEsUUFBQSxZQUFBLFVBQUEsSUFBQSxRQUFBLFlBQUEsUUFBQSxnQkFBQSxlQUFBLGNBQUEsT0FBQSxPQUFBLFlBQUE7O1FBRUEsSUFBQTs7UUFFQSxPQUFBLE9BQUEsV0FBQTtZQUNBLEdBQUEsQ0FBQSxXQUFBLFVBQUE7Z0JBQ0EsT0FBQSxXQUFBO21CQUNBO2dCQUNBLE9BQUEsV0FBQSxXQUFBOztZQUVBLE9BQUEsU0FBQTs7WUFFQSxPQUFBLGVBQUE7O1lBRUEsR0FBQSxPQUFBLGFBQUEsT0FBQSxVQUFBLFNBQUEsR0FBQTtnQkFDQSxPQUFBLFNBQUEsY0FBQTttQkFDQTtnQkFDQSxPQUFBLFNBQUEsY0FBQTs7O1lBR0EsRUFBQSxhQUFBO1lBQ0EsRUFBQSxVQUFBOztZQUVBLE9BQUEsV0FBQSxXQUFBLFdBQUEsSUFBQSxlQUFBLENBQUEsT0FBQSxRQUFBOztZQUVBLEVBQUEsWUFBQSxlQUFBO2dCQUNBLFdBQUE7Z0JBQ0EsUUFBQTtnQkFDQSxXQUFBO2dCQUNBLFNBQUE7Z0JBQ0Esa0JBQUEsU0FBQSxTQUFBLFFBQUE7b0JBQ0EsT0FBQSxTQUFBLFdBQUEsT0FBQTs7Ozs7O1lBTUEsRUFBQSxVQUFBLGVBQUE7Z0JBQ0EsV0FBQTtnQkFDQSxRQUFBO2dCQUNBLFdBQUEsU0FBQSxJQUFBLElBQUE7Z0JBQ0EsU0FBQSxTQUFBLElBQUEsSUFBQTtnQkFDQSxrQkFBQSxTQUFBLFNBQUEsUUFBQTtvQkFDQSxPQUFBLFNBQUEsU0FBQSxPQUFBOzs7O1lBSUEsV0FBQTtZQUNBLEVBQUEsY0FBQSxNQUFBOzs7O1FBSUEsT0FBQSxnQkFBQSxXQUFBO1lBQ0EsT0FBQSxTQUFBLFVBQUE7OztRQUdBLE9BQUEsc0JBQUEsV0FBQTtZQUNBLEdBQUEsT0FBQSxPQUFBLFNBQUEsVUFBQSxZQUFBLE9BQUEsU0FBQSxVQUFBLE1BQUE7Z0JBQ0EsRUFBQSxtQkFBQTtnQkFDQSxPQUFBLE9BQUEsYUFBQSxXQUFBO29CQUNBLEVBQUEsVUFBQSxJQUFBLE9BQUEsU0FBQSxNQUFBOzs7Ozs7UUFNQSxPQUFBLHNCQUFBLFdBQUE7WUFDQSxHQUFBLE9BQUEsU0FBQSxVQUFBLE1BQUEsT0FBQSxPQUFBLFNBQUEsVUFBQSxZQUFBO2dCQUNBLEVBQUEsbUJBQUEsS0FBQTttQkFDQTtnQkFDQSxFQUFBLG1CQUFBLEtBQUE7Ozs7UUFJQSxPQUFBLGlCQUFBLFNBQUEsS0FBQSxLQUFBO1lBQ0EsT0FBQTtnQkFDQSxNQUFBO2dCQUNBLGFBQUEsQ0FBQSxLQUFBOzs7O1FBSUEsT0FBQSxjQUFBLFNBQUEsT0FBQTtZQUNBLE9BQUE7WUFDQSxPQUFBLFNBQUEsYUFBQSxDQUFBLEVBQUEsYUFBQTs7WUFFQSxHQUFBLEtBQUEsT0FBQSxzQkFBQSxLQUFBLFdBQUE7Z0JBQ0EsSUFBQSxRQUFBLElBQUEsTUFBQSxPQUFBO2dCQUNBLEdBQUEsS0FBQSxNQUFBLGlCQUFBLEtBQUEsU0FBQSxVQUFBO29CQUNBLE9BQUEsR0FBQSxvQkFBQSxJQUFBLENBQUEsUUFBQTs7Ozs7OztRQU9BLE9BQUEscUJBQUEsV0FBQTtZQUNBLEdBQUEsT0FBQSxPQUFBLFNBQUEsWUFBQSxZQUFBLE9BQUEsT0FBQSxTQUFBLFVBQUEsYUFBQTtnQkFDQSxJQUFBLGFBQUEsT0FBQSxTQUFBO2dCQUNBLElBQUEsTUFBQSxXQUFBLFNBQUEsU0FBQTtnQkFDQSxJQUFBLE1BQUEsV0FBQSxTQUFBLFNBQUE7Z0JBQ0EsT0FBQSxTQUFBLFVBQUEsV0FBQTtnQkFDQSxPQUFBLFNBQUEsV0FBQSxPQUFBLGVBQUEsS0FBQTtnQkFDQSxHQUFBLE9BQUEsU0FBQSxhQUFBLFFBQUEsT0FBQSxPQUFBLFNBQUEsYUFBQSxhQUFBO29CQUNBLE9BQUEsR0FBQSxLQUFBLE9BQUEsdUJBQUEsYUFBQSxLQUFBLFNBQUEsY0FBQTt3QkFDQSxHQUFBLGlCQUFBLFNBQUEsaUJBQUEsQ0FBQSxLQUFBLE9BQUEsaUJBQUEsYUFBQTs0QkFDQSxJQUFBLFFBQUE7NEJBQ0EsT0FBQSxPQUFBLEtBQUE7NEJBQ0EsT0FBQTsrQkFDQTs0QkFDQSxPQUFBLFNBQUEsV0FBQSxhQUFBOzRCQUNBLE9BQUEsT0FBQTs7Ozs7bUJBS0EsR0FBQSxPQUFBLE9BQUEsU0FBQSxVQUFBLGFBQUE7Z0JBQ0EsT0FBQSxTQUFBLFdBQUEsT0FBQSxTQUFBO2dCQUNBLElBQUEsZUFBQSxJQUFBO2dCQUNBLE9BQUEsR0FBQSxLQUFBLGFBQUEsS0FBQTtvQkFDQSxLQUFBLE9BQUEsU0FBQTtvQkFDQSxLQUFBLFNBQUEsZ0JBQUE7b0JBQ0EsUUFBQSxJQUFBLHlCQUFBO29CQUNBLElBQUEsUUFBQSxlQUFBLEtBQUE7b0JBQ0EsT0FBQSxTQUFBLFVBQUEsTUFBQTtvQkFDQSxPQUFBLFNBQUEsV0FBQSxNQUFBO29CQUNBLE9BQUEsT0FBQTs7Ozs7O1FBTUEsT0FBQSx5QkFBQSxTQUFBLFlBQUE7WUFDQSxJQUFBLFFBQUEsSUFBQSxNQUFBO2dCQUNBLE1BQUEsV0FBQSxPQUFBLFdBQUEsT0FBQTtnQkFDQSxhQUFBO2dCQUNBLGNBQUE7Z0JBQ0EsT0FBQSxXQUFBLHlCQUFBLFdBQUEseUJBQUE7Z0JBQ0EsU0FBQSxXQUFBLFVBQUEsV0FBQSxVQUFBO2dCQUNBLFFBQUEsV0FBQTtnQkFDQSxVQUFBLE9BQUEsZUFBQSxXQUFBLFNBQUEsU0FBQSxPQUFBLFdBQUEsU0FBQSxTQUFBOztZQUVBLE9BQUEsR0FBQSxLQUFBLE1BQUEsaUJBQUEsS0FBQSxVQUFBLFVBQUE7Z0JBQ0EsR0FBQSxTQUFBLFdBQUEsT0FBQSxTQUFBLEtBQUEsVUFBQSxhQUFBO29CQUNBLE9BQUEsZUFBQSxTQUFBLEtBQUE7dUJBQ0E7b0JBQ0EsT0FBQSxlQUFBLENBQUE7O2dCQUVBLE9BQUEsT0FBQTs7Ozs7O1FBTUEsT0FBQSxrQkFBQSxVQUFBO1lBQ0EsR0FBQSxPQUFBLE9BQUEsU0FBQSxZQUFBLFVBQUE7Z0JBQ0EsV0FBQSxJQUFBLFVBQUEsT0FBQSxTQUFBLFFBQUEsU0FBQTtnQkFDQSxXQUFBLElBQUEsUUFBQTtnQkFDQSxJQUFBLGVBQUEsSUFBQTtnQkFDQSxJQUFBLGFBQUEsT0FBQSxTQUFBO2dCQUNBLElBQUEsTUFBQSxXQUFBLFNBQUEsU0FBQTtnQkFDQSxJQUFBLE1BQUEsV0FBQSxTQUFBLFNBQUE7Z0JBQ0EsR0FBQSxLQUFBLGFBQUEsS0FBQTtvQkFDQSxVQUFBO29CQUNBLFdBQUE7b0JBQ0EsTUFBQSxXQUFBO29CQUNBLEtBQUEsU0FBQSxnQkFBQTtvQkFDQSxHQUFBLGVBQUEsS0FBQSxTQUFBLEdBQUE7NEJBQ0EsT0FBQSxpQkFBQSxlQUFBOzs7Ozs7O1FBT0EsT0FBQSxjQUFBLFlBQUE7WUFDQSxHQUFBLFNBQUE7Z0JBQ0EsU0FBQSxPQUFBOztZQUVBLFdBQUEsU0FBQSxVQUFBO2dCQUNBLE9BQUE7Z0JBQ0EsV0FBQTtlQUNBOzs7UUFHQSxPQUFBLHdCQUFBLFdBQUE7WUFDQSxFQUFBLGlCQUFBLElBQUEsVUFBQTtZQUNBLEdBQUEsT0FBQSxTQUFBLGdCQUFBLE9BQUE7Z0JBQ0EsT0FBQSxTQUFBLFdBQUE7Ozs7UUFJQSxPQUFBLFlBQUEsU0FBQSxPQUFBLE9BQUE7WUFDQSxFQUFBLGlCQUFBLElBQUEsVUFBQTtZQUNBLE9BQUEsU0FBQSxXQUFBLE1BQUE7WUFDQSxPQUFBLFNBQUEsY0FBQTtZQUNBLEVBQUEsTUFBQSxRQUFBLFFBQUEsaUJBQUEsSUFBQSxVQUFBOzs7O1FBSUEsT0FBQTs7OztBQzNNQSxDQUFBLFVBQUE7SUFDQTs7O0lBR0EsUUFBQSxPQUFBLG1CQUFBLFdBQUEsbUxBQUEsU0FBQSxRQUFBLFlBQUEsVUFBQSxJQUFBLFFBQUEsWUFBQSxRQUFBLGdCQUFBLGVBQUEsY0FBQSxPQUFBLE9BQUEsWUFBQTs7UUFFQSxJQUFBOztRQUVBLE9BQUEsT0FBQSxXQUFBO1lBQ0EsR0FBQSxDQUFBLFdBQUEsVUFBQTtnQkFDQSxPQUFBLFdBQUEsSUFBQSxNQUFBO2dCQUNBLE9BQUEsZUFBQSxPQUFBLFNBQUE7bUJBQ0E7Z0JBQ0EsT0FBQSxXQUFBLFdBQUE7Z0JBQ0EsR0FBQSxPQUFBLFNBQUEsZUFBQTtvQkFDQSxPQUFBLGVBQUEsT0FBQSxTQUFBLGdCQUFBLE9BQUEsU0FBQTs7b0JBRUE7b0JBQ0EsT0FBQSxlQUFBLE9BQUEsU0FBQTs7O1lBR0EsT0FBQSxTQUFBO1lBQ0EsRUFBQSxhQUFBO1lBQ0EsR0FBQSxPQUFBLFNBQUEsV0FBQSxTQUFBLEVBQUE7Z0JBQ0EsT0FBQSxTQUFBLFdBQUEsT0FBQSxTQUFBLFdBQUE7OztZQUdBLE9BQUEsV0FBQSxXQUFBLFdBQUEsSUFBQSxlQUFBLENBQUEsT0FBQSxRQUFBOztZQUVBLFdBQUE7WUFDQSxFQUFBLGNBQUEsTUFBQTs7OztRQUlBLE9BQUEsdUJBQUEsU0FBQSxXQUFBO1lBQ0EsT0FBQSxPQUFBLFVBQUEsU0FBQTs7O1FBR0EsT0FBQSxzQkFBQSxTQUFBLE9BQUEsUUFBQTtZQUNBLElBQUEsVUFBQSxFQUFBLE1BQUEsUUFBQSxHQUFBO1lBQ0EsR0FBQSxTQUFBO2dCQUNBLFNBQUEsT0FBQSxTQUFBLHNCQUFBLE9BQUEsS0FBQSxRQUFBO2dCQUNBLFNBQUEsV0FBQTtvQkFDQSxPQUFBLGFBQUEsT0FBQSxPQUFBOztnQkFFQSxRQUFBLElBQUE7bUJBQ0E7Z0JBQ0EsT0FBQSxPQUFBO2dCQUNBLE9BQUEsUUFBQTs7Ozs7UUFLQSxPQUFBLGdCQUFBLFdBQUE7WUFDQSxPQUFBLFNBQUEsY0FBQTs7OztRQUlBLE9BQUEsaUJBQUEsU0FBQSxLQUFBLEtBQUE7WUFDQSxPQUFBO2dCQUNBLE1BQUE7Z0JBQ0EsYUFBQSxDQUFBLEtBQUE7Ozs7UUFJQSxPQUFBLGNBQUEsU0FBQSxPQUFBO1lBQ0EsT0FBQTtZQUNBLE9BQUEsU0FBQSxhQUFBLENBQUEsRUFBQSxhQUFBO1lBQ0EsT0FBQSxTQUFBLGlCQUFBLE9BQUE7WUFDQSxPQUFBLFNBQUEsVUFBQSxPQUFBLFNBQUEsUUFBQTtZQUNBLE9BQUEsU0FBQSxTQUFBLE9BQUEsU0FBQTtZQUNBLEdBQUEsS0FBQSxPQUFBLFNBQUEsaUJBQUEsS0FBQSxTQUFBLFVBQUE7Z0JBQ0EsT0FBQSxHQUFBLG9CQUFBLElBQUEsQ0FBQSxRQUFBOzs7OztRQUtBLE9BQUEsa0JBQUEsVUFBQTtZQUNBLEdBQUEsT0FBQSxPQUFBLFNBQUEsWUFBQSxVQUFBO2dCQUNBLFdBQUEsSUFBQSxVQUFBLE9BQUEsU0FBQSxRQUFBLFNBQUE7Z0JBQ0EsV0FBQSxJQUFBLFFBQUE7Z0JBQ0EsSUFBQSxlQUFBLElBQUE7Z0JBQ0EsSUFBQSxhQUFBLE9BQUEsU0FBQTtnQkFDQSxJQUFBLE1BQUEsV0FBQSxTQUFBLFNBQUE7Z0JBQ0EsSUFBQSxNQUFBLFdBQUEsU0FBQSxTQUFBO2dCQUNBLE9BQUEsU0FBQSxXQUFBLE9BQUEsZUFBQSxLQUFBOzs7O1FBSUEsT0FBQSxrQkFBQSxTQUFBLE9BQUEsVUFBQTtZQUNBLElBQUEsVUFBQSxFQUFBLE1BQUEsUUFBQSxHQUFBO1lBQ0EsSUFBQSxhQUFBLE9BQUEsU0FBQTtZQUNBLEdBQUEsU0FBQTtnQkFDQSxHQUFBLE9BQUEsZUFBQTtvQkFDQSxhQUFBO2dCQUNBLFdBQUEsS0FBQSxTQUFBO21CQUNBO2dCQUNBLEdBQUEsT0FBQSxlQUFBLGVBQUEsV0FBQSxTQUFBLEdBQUE7b0JBQ0EsSUFBQSxRQUFBLFdBQUEsUUFBQSxTQUFBO29CQUNBLFdBQUEsT0FBQSxPQUFBOzs7WUFHQSxPQUFBLFNBQUEsa0JBQUE7OztRQUdBLE9BQUE7Ozs7Ozs7QUN0R0EsQ0FBQSxVQUFBO0lBQ0E7SUFDQSxRQUFBLE9BQUEsbUJBQUEsV0FBQSw0SEFBQSxTQUFBLFFBQUEsWUFBQSxVQUFBLElBQUEsUUFBQSxXQUFBLFlBQUEsYUFBQSxTQUFBOzs7UUFHQSxPQUFBLG9CQUFBLFlBQUE7WUFDQSxJQUFBLFFBQUEsT0FBQSxRQUFBO1lBQ0EsSUFBQSxVQUFBLEVBQUEsZ0JBQUEsUUFBQTtZQUNBLElBQUEsVUFBQSxFQUFBLGdCQUFBLFFBQUE7WUFDQSxJQUFBLFNBQUEsRUFBQSxlQUFBLFFBQUE7WUFDQSxJQUFBLFVBQUEsRUFBQSxlQUFBLFFBQUE7WUFDQSxPQUFBO2dCQUNBLEtBQUE7b0JBQ0EsUUFBQSxZQUFBO29CQUNBLE9BQUEsWUFBQTtvQkFDQSxRQUFBLFlBQUE7b0JBQ0EsUUFBQSxTQUFBO29CQUNBO2dCQUNBLEtBQUE7b0JBQ0EsT0FBQSxZQUFBO29CQUNBLFFBQUEsWUFBQTtvQkFDQSxRQUFBLFlBQUE7b0JBQ0EsUUFBQSxTQUFBO29CQUNBO2dCQUNBLE1BQUE7b0JBQ0EsUUFBQSxZQUFBO29CQUNBLFFBQUEsWUFBQTtvQkFDQSxRQUFBLFlBQUE7b0JBQ0EsT0FBQSxTQUFBO29CQUNBO2dCQUNBLEtBQUE7b0JBQ0EsUUFBQSxZQUFBO29CQUNBLFFBQUEsWUFBQTtvQkFDQSxPQUFBLFlBQUE7b0JBQ0EsUUFBQSxTQUFBO29CQUNBOzs7Ozs7O1FBT0EsT0FBQSxTQUFBLFdBQUE7WUFDQSxHQUFBLEtBQUEsWUFBQSxVQUFBLEtBQUEsV0FBQTtnQkFDQSxTQUFBLFdBQUE7b0JBQ0EsT0FBQSxPQUFBO29CQUNBLE9BQUEsR0FBQTs7Ozs7UUFLQSxFQUFBLFVBQUEsTUFBQSxPQUFBO1FBQ0EsV0FBQSxJQUFBLHVCQUFBLE9BQUE7Ozs7OztBQ3BEQSxDQUFBLFVBQUE7SUFDQTs7O0lBR0EsUUFBQSxPQUFBLG1CQUFBLFdBQUEsdVBBQUEsU0FBQSxJQUFBLFFBQUEsVUFBQSxTQUFBLGVBQUEsUUFBQSxvQkFBQSxZQUFBLGVBQUEsY0FBQSxPQUFBLGFBQUEsZ0JBQUEsWUFBQSxpQkFBQSxrQkFBQTs7UUFFQSxPQUFBLE9BQUEsVUFBQTtZQUNBLFFBQUEsSUFBQTs7WUFFQSxXQUFBO1lBQ0EsT0FBQSxvQkFBQSxFQUFBLFFBQUEsV0FBQTtZQUNBLFFBQUEsV0FBQSxPQUFBO1lBQ0EsT0FBQSxrQkFBQSxJQUFBLGVBQUEsQ0FBQSxNQUFBLFFBQUEsV0FBQTtZQUNBLE9BQUEsbUJBQUEsSUFBQSxlQUFBLENBQUEsV0FBQTtZQUNBLE9BQUEsZUFBQSxJQUFBLGVBQUEsQ0FBQSxPQUFBOztZQUVBLEdBQUEsS0FBQSxPQUFBLFlBQUEsS0FBQSxTQUFBLE1BQUE7O2dCQUVBLE9BQUE7Z0JBQ0EsT0FBQTtnQkFDQSxPQUFBOzs7WUFHQSxnQkFBQSxVQUFBLGdCQUFBLHdCQUFBLFFBQUEsV0FBQTtnQkFDQSxTQUFBLFdBQUE7b0JBQ0EsR0FBQSxRQUFBLGdCQUFBLGVBQUEsZUFBQSxnQkFBQSxlQUFBO3dCQUNBLE9BQUEsWUFBQSxnQkFBQTs7O1lBR0EsZ0JBQUEsVUFBQSxnQkFBQSx3QkFBQSxRQUFBLFlBQUE7Z0JBQ0EsU0FBQSxZQUFBO29CQUNBLEdBQUEsUUFBQSxnQkFBQSxjQUFBLGVBQUEsZ0JBQUEsY0FBQTt3QkFDQSxPQUFBLFlBQUEsZ0JBQUE7Ozs7O1FBS0EsT0FBQSxhQUFBLFdBQUE7WUFDQSxJQUFBLFNBQUEsSUFBQTtZQUNBLElBQUEsYUFBQSxJQUFBO1lBQ0EsT0FBQSx5QkFBQSxJQUFBLGVBQUEsQ0FBQSxNQUFBO1lBQ0EsT0FBQSxHQUFBLEtBQUEsT0FBQSxLQUFBLE9BQUEsa0JBQUEsS0FBQSxTQUFBLGNBQUE7Z0JBQ0EsT0FBQSxHQUFBLEtBQUEsV0FBQSxLQUFBLE9BQUEseUJBQUEsS0FBQSxTQUFBLGlCQUFBO29CQUNBLE9BQUEsWUFBQSxhQUFBO29CQUNBLE9BQUEsa0JBQUEsZ0JBQUE7b0JBQ0EsZ0JBQUEsWUFBQSxPQUFBO29CQUNBLGdCQUFBLE9BQUEsZ0JBQUE7b0JBQ0EsT0FBQSxpQkFBQSxRQUFBLE9BQUEsaUJBQUE7b0JBQ0EsT0FBQTs7Ozs7UUFLQSxPQUFBLGFBQUEsV0FBQTtZQUNBLElBQUEsU0FBQSxJQUFBO1lBQ0EsSUFBQSxhQUFBLElBQUE7WUFDQSxPQUFBLHlCQUFBLElBQUEsZUFBQSxDQUFBLE1BQUE7WUFDQSxPQUFBLEdBQUEsS0FBQSxPQUFBLEtBQUEsT0FBQSxtQkFBQSxLQUFBLFNBQUEsY0FBQTtnQkFDQSxPQUFBLEdBQUEsS0FBQSxXQUFBLEtBQUEsT0FBQSx5QkFBQSxLQUFBLFNBQUEsaUJBQUE7b0JBQ0EsT0FBQSxZQUFBLGFBQUE7b0JBQ0EsT0FBQSxrQkFBQSxnQkFBQTtvQkFDQSxnQkFBQSxhQUFBLE9BQUE7b0JBQ0EsZ0JBQUEsT0FBQSxnQkFBQTtvQkFDQSxPQUFBLGlCQUFBLFFBQUEsT0FBQSxrQkFBQTtvQkFDQSxPQUFBOzs7OztRQUtBLE9BQUEsWUFBQSxXQUFBO1lBQ0EsSUFBQSxRQUFBLElBQUE7WUFDQSxJQUFBLGFBQUEsSUFBQTtZQUNBLE9BQUEsR0FBQSxLQUFBLE1BQUEsS0FBQSxPQUFBLGVBQUEsS0FBQSxTQUFBLGNBQUE7Z0JBQ0EsT0FBQSxRQUFBLGFBQUE7Z0JBQ0EsZ0JBQUEsUUFBQSxPQUFBOztnQkFFQSxPQUFBLGlCQUFBLE9BQUEsT0FBQSxjQUFBO2dCQUNBLE9BQUE7Ozs7UUFJQSxPQUFBLFdBQUEsV0FBQTtZQUNBLEdBQUEsS0FBQSxZQUFBLGtCQUFBLEtBQUEsU0FBQSxNQUFBO2dCQUNBLEdBQUEsU0FBQSxRQUFBLE9BQUEsU0FBQTtvQkFDQSxPQUFBLE9BQUEsR0FBQSxlQUFBLElBQUEsQ0FBQSxRQUFBOztnQkFFQSxPQUFBLE9BQUE7Ozs7UUFJQSxPQUFBLG1CQUFBLFNBQUEsaUJBQUEsaUJBQUEsY0FBQTtZQUNBLEVBQUEsUUFBQSxPQUFBLFVBQUE7Z0JBQ0EsSUFBQSxjQUFBLFdBQUEsVUFBQSxTQUFBLGNBQUE7Z0JBQ0EsR0FBQSxPQUFBLGNBQUEsT0FBQSxPQUFBLFVBQUEsY0FBQSxLQUFBO29CQUNBLEdBQUEsS0FBQSxnQkFBQSxLQUFBLGtCQUFBLEtBQUEsU0FBQSxTQUFBO3dCQUNBLEdBQUEsV0FBQSxRQUFBLE1BQUE7NEJBQ0EsSUFBQSxJQUFBLEdBQUEsR0FBQSxJQUFBLFFBQUEsS0FBQSxRQUFBLEtBQUE7Z0NBQ0EsT0FBQSxjQUFBLEtBQUEsUUFBQSxLQUFBOzs0QkFFQSxPQUFBLHNCQUFBLGNBQUE7Ozs7Ozs7UUFPQSxPQUFBLHdCQUFBLFNBQUEsY0FBQSxTQUFBO1lBQ0EsT0FBQTtnQkFDQSxLQUFBO29CQUNBLGdCQUFBLFlBQUEsUUFBQTtvQkFDQSxnQkFBQSxPQUFBLGdCQUFBO29CQUNBOztnQkFFQSxLQUFBO29CQUNBLGdCQUFBLGFBQUEsUUFBQTtvQkFDQSxnQkFBQSxPQUFBLGdCQUFBO29CQUNBOzs7O1FBSUEsT0FBQSxpQkFBQSxZQUFBO1lBQ0EsSUFBQSxXQUFBLEVBQUEsUUFBQTs7V0FFQSxFQUFBLDRCQUFBLEtBQUEsU0FBQSxHQUFBLFNBQUE7ZUFDQSxJQUFBLFNBQUEsRUFBQTtlQUNBLElBQUEsWUFBQSxPQUFBLFNBQUE7ZUFDQSxJQUFBLFdBQUEsT0FBQSxvQkFBQSxXQUFBO3VCQUNBLE9BQUEsWUFBQTt1QkFDQSxPQUFBLFlBQUEsT0FBQSxLQUFBOzs7O1lBSUEsRUFBQSxrQ0FBQSxLQUFBLFVBQUEsR0FBQSxTQUFBO2dCQUNBLElBQUEsU0FBQSxFQUFBO2dCQUNBLElBQUEsWUFBQSxPQUFBLFNBQUE7Z0JBQ0EsSUFBQSxXQUFBLE9BQUEsb0JBQUEsV0FBQTtvQkFDQSxHQUFBLE9BQUEsS0FBQSxjQUFBLElBQUEsR0FBQTt3QkFDQSxPQUFBLFNBQUE7d0JBQ0EsT0FBQSxTQUFBLE9BQUEsS0FBQTs7Ozs7O1FBTUEsT0FBQSxhQUFBLFNBQUEsUUFBQTtZQUNBLElBQUEsVUFBQSxFQUFBLEtBQUEsT0FBQSxVQUFBLGdCQUFBLE1BQUEsT0FBQTtZQUNBLEdBQUEsUUFBQSxTQUFBLDBCQUFBO2dCQUNBLE9BQUE7bUJBQ0E7Z0JBQ0EsT0FBQSxTQUFBOzs7O1FBSUEsT0FBQSxXQUFBLFNBQUEsUUFBQTtZQUNBLElBQUEsUUFBQSxhQUFBLE9BQUEsWUFBQSxRQUFBLE9BQUE7WUFDQSxJQUFBLGVBQUEsT0FBQSxVQUFBLGVBQUEsS0FBQSxPQUFBO1lBQ0EsRUFBQSxlQUFBLEtBQUEsV0FBQTtZQUNBLEVBQUEsU0FBQSxLQUFBO1lBQ0EsRUFBQSxjQUFBLEtBQUEsV0FBQTtZQUNBLEVBQUEsZUFBQSxLQUFBLFdBQUEsT0FBQTtZQUNBLEVBQUEscUJBQUEsS0FBQSxXQUFBLE9BQUE7WUFDQSxFQUFBLGFBQUEsS0FBQSxXQUFBLHlCQUFBO1lBQ0EsRUFBQSxvQkFBQSxZQUFBLHlCQUFBLENBQUEsVUFBQTtZQUNBLFdBQUEsZUFBQTtZQUNBLG1CQUFBLFNBQUE7WUFDQSxFQUFBLE1BQUEsY0FBQSxTQUFBLHlCQUFBLENBQUEsVUFBQTs7O1FBR0EsT0FBQSxZQUFBLFdBQUE7WUFDQSxFQUFBLG9CQUFBLFlBQUEseUJBQUEsQ0FBQSxVQUFBO1lBQ0EsT0FBQSxNQUFBLFdBQUE7Ozs7UUFJQSxPQUFBOzs7Ozs7QUM5S0EsQ0FBQSxVQUFBO0lBQ0E7OztJQUdBLFFBQUEsT0FBQSxtQkFBQSxXQUFBLGdEQUFBLFNBQUEsUUFBQSxRQUFBLFdBQUE7O1FBRUEsT0FBQSxvQkFBQSxXQUFBO1lBQ0EsT0FBQSxHQUFBOzs7UUFHQSxPQUFBLE9BQUEsU0FBQSxXQUFBO1lBQ0EsR0FBQSxPQUFBLE9BQUE7Z0JBQ0EsUUFBQSxJQUFBLFdBQUEsT0FBQTtnQkFDQSxJQUFBLElBQUEsSUFBQSxHQUFBLElBQUEsT0FBQSxNQUFBLFFBQUEsS0FBQTtvQkFDQSxJQUFBLFdBQUEsT0FBQSxNQUFBLEdBQUEsa0JBQUEsT0FBQSxhQUFBO29CQUNBLElBQUEsSUFBQSxPQUFBLE1BQUEsR0FBQSxrQkFBQSxPQUFBLGFBQUE7b0JBQ0EsR0FBQSxXQUFBLElBQUE7d0JBQ0EsV0FBQTs7b0JBRUEsT0FBQSxNQUFBLEdBQUEsV0FBQSxXQUFBLFdBQUEsYUFBQTtvQkFDQSxPQUFBLE1BQUEsR0FBQSxJQUFBOztnQkFFQSxPQUFBLE1BQUEsS0FBQSxTQUFBLEVBQUEsR0FBQTtvQkFDQSxPQUFBLEVBQUEsSUFBQSxFQUFBOzs7Z0JBR0EsV0FBQTs7Ozs7Ozs7QUMxQkEsQ0FBQSxVQUFBO0lBQ0E7Ozs7O0lBS0EsUUFBQSxPQUFBLG1CQUFBLFdBQUEsaUlBQUEsU0FBQSxJQUFBLFFBQUEsUUFBQSxhQUFBLGdCQUFBLFlBQUEsaUJBQUEsbUJBQUE7OztRQUdBLElBQUEsV0FBQSxFQUFBO1FBQ0EsSUFBQSxpQkFBQTtRQUNBLElBQUEsS0FBQSxFQUFBO1FBQ0EsSUFBQSxZQUFBLEVBQUEsZ0JBQUEsU0FBQTtRQUNBLElBQUEsZUFBQSxFQUFBLGdCQUFBOztRQUVBLEVBQUEsUUFBQSxPQUFBLFVBQUE7WUFDQSxJQUFBLFFBQUEsRUFBQSxVQUFBLFNBQUEsTUFBQSxlQUFBLEtBQUE7WUFDQSxJQUFBLFlBQUEsRUFBQSxRQUFBO1lBQ0EsSUFBQSxZQUFBLE9BQUEsWUFBQSxVQUFBO2dCQUNBLEdBQUEsSUFBQSxFQUFBLFVBQUEsU0FBQSxLQUFBLEdBQUEsZUFBQTs7aUJBRUE7Z0JBQ0EsR0FBQSxJQUFBLEVBQUEsVUFBQSxJQUFBLEtBQUEsSUFBQSxlQUFBOzs7WUFHQSxJQUFBLFFBQUEsV0FBQTtnQkFDQSxJQUFBLE9BQUEsUUFBQTtnQkFDQSxHQUFBLElBQUEsQ0FBQSxLQUFBOzs7O1FBSUEsT0FBQSxnQkFBQSxXQUFBO1lBQ0EsR0FBQSxLQUFBLFdBQUEsV0FBQSxPQUFBLG1CQUFBLG1CQUFBLEtBQUEsU0FBQSxZQUFBO2dCQUNBLE9BQUEsZ0JBQUE7Z0JBQ0EsbUJBQUEsS0FBQSxTQUFBLE1BQUE7b0JBQ0EsT0FBQTs7Ozs7UUFLQSxPQUFBLGNBQUEsU0FBQSxTQUFBO1lBQ0EsSUFBQSxRQUFBLE9BQUEsUUFBQTtZQUNBLFFBQUE7Z0JBQ0EsS0FBQTtvQkFDQSxPQUFBLGlCQUFBO29CQUNBO2dCQUNBLEtBQUE7b0JBQ0EsT0FBQSxpQkFBQTtvQkFDQTtnQkFDQSxLQUFBO29CQUNBLE9BQUEsaUJBQUE7b0JBQ0E7Ozs7UUFJQSxPQUFBLG1CQUFBLFNBQUEsU0FBQTtZQUNBLEdBQUEsV0FBQSxPQUFBLFlBQUEsWUFBQTtnQkFDQSxPQUFBLGNBQUEsZ0JBQUE7bUJBQ0E7Z0JBQ0EsT0FBQSxjQUFBLGdCQUFBLE9BQUE7OztZQUdBLE9BQUEsY0FBQTs7O1FBR0EsT0FBQSxtQkFBQSxTQUFBLFNBQUE7WUFDQSxHQUFBO2dCQUNBLE9BQUEsY0FBQSxnQkFBQTs7Z0JBRUEsT0FBQSxjQUFBLGdCQUFBLE9BQUE7O1lBRUEsT0FBQSxjQUFBOzs7UUFHQSxPQUFBLG1CQUFBLFNBQUEsU0FBQTtZQUNBLEdBQUE7Z0JBQ0EsT0FBQSxjQUFBLGdCQUFBOztnQkFFQSxPQUFBLGNBQUEsZ0JBQUEsT0FBQTs7WUFFQSxPQUFBLGNBQUE7Ozs7UUFJQSxPQUFBLGdCQUFBLFdBQUE7WUFDQSxJQUFBLFFBQUEsT0FBQSxRQUFBO1lBQ0EsUUFBQTtnQkFDQSxLQUFBO29CQUNBLE9BQUE7b0JBQ0E7Z0JBQ0EsS0FBQTtvQkFDQSxPQUFBO29CQUNBOzs7O1FBSUEsT0FBQSxxQkFBQSxXQUFBO2FBQ0EsR0FBQSxLQUFBLFdBQUEsV0FBQSxPQUFBLG1CQUFBLG1CQUFBLEtBQUEsU0FBQSxZQUFBO2lCQUNBLE9BQUEsZ0JBQUE7Z0JBQ0EsT0FBQSxjQUFBO2dCQUNBLEdBQUEsT0FBQSxnQkFBQSxjQUFBLGVBQUEsZ0JBQUEsY0FBQSxLQUFBO29CQUNBLE9BQUEsWUFBQSxnQkFBQTt1QkFDQTtvQkFDQSxPQUFBOzs7OztRQUtBLE9BQUEscUJBQUEsV0FBQTtZQUNBLEdBQUEsS0FBQSxXQUFBLFdBQUEsT0FBQSxtQkFBQSxtQkFBQSxLQUFBLFNBQUEsWUFBQTtnQkFDQSxPQUFBLGdCQUFBO2dCQUNBLE9BQUEsY0FBQTtnQkFDQSxHQUFBLE9BQUEsZ0JBQUEsZUFBQSxlQUFBLGdCQUFBLGVBQUEsS0FBQTtvQkFDQSxPQUFBLFlBQUEsZ0JBQUE7dUJBQ0E7b0JBQ0EsT0FBQTs7Ozs7UUFLQSxPQUFBLGFBQUEsV0FBQTtZQUNBLElBQUEsUUFBQSxPQUFBLFFBQUE7WUFDQSxRQUFBO2dCQUNBLEtBQUE7b0JBQ0EsT0FBQTtvQkFDQTtnQkFDQSxLQUFBO29CQUNBLE9BQUE7b0JBQ0E7Ozs7UUFJQSxPQUFBLGtCQUFBLFdBQUE7WUFDQSxJQUFBLFFBQUEsT0FBQSxRQUFBO1lBQ0EsUUFBQTtnQkFDQSxLQUFBO29CQUNBLE9BQUEsT0FBQTtvQkFDQTtnQkFDQSxLQUFBO29CQUNBLE9BQUEsT0FBQTtvQkFDQTtnQkFDQSxLQUFBO29CQUNBLE9BQUEsT0FBQTtvQkFDQTs7Ozs7UUFLQSxPQUFBLGtCQUFBLFdBQUE7WUFDQSxHQUFBLGdCQUFBO2dCQUNBLE9BQUEsY0FBQSxnQkFBQSxnQkFBQTs7O1FBR0EsT0FBQSxrQkFBQSxXQUFBO1lBQ0EsR0FBQSxnQkFBQTtnQkFDQSxPQUFBLGNBQUEsZ0JBQUEsZ0JBQUE7OztRQUdBLFFBQUEsUUFBQSxVQUFBLE1BQUEsWUFBQTtZQUNBLE9BQUE7Ozs7UUFJQSxnQkFBQSxVQUFBLGdCQUFBLHdCQUFBLFFBQUEsT0FBQTtRQUNBLGdCQUFBLFVBQUEsZ0JBQUEsd0JBQUEsUUFBQSxPQUFBOztRQUVBLGdCQUFBLFVBQUEsZ0JBQUEsdUJBQUEsUUFBQSxPQUFBO1FBQ0EsZ0JBQUEsVUFBQSxnQkFBQSx1QkFBQSxRQUFBLE9BQUE7Ozs7OztBQ3ZLQSxDQUFBLFVBQUE7SUFDQTs7O0lBR0EsUUFBQSxPQUFBLG1CQUFBLFdBQUEsK0VBQUEsU0FBQSxRQUFBLFFBQUEsYUFBQSxjQUFBLFdBQUE7OztRQUdBLE9BQUEsT0FBQSxVQUFBO1lBQ0EsT0FBQSxTQUFBO1lBQ0EsWUFBQSxlQUFBLFNBQUEsTUFBQTtnQkFDQSxPQUFBLE9BQUE7O1lBRUEsYUFBQSxLQUFBLFNBQUEsT0FBQTtnQkFDQSxPQUFBLE9BQUEsVUFBQTs7WUFFQSxXQUFBLFdBQUEsU0FBQSxLQUFBO2dCQUNBLE9BQUEsTUFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1FBcUJBLE9BQUE7Ozs7OztBQ3JDQSxDQUFBLFVBQUE7SUFDQTs7Ozs7SUFLQSxRQUFBLE9BQUEsbUJBQUEsV0FBQSwyRkFBQSxTQUFBLElBQUEsWUFBQSxRQUFBLFFBQUEsWUFBQSxnQkFBQTs7OztRQUlBLE9BQUEsWUFBQSxVQUFBLFFBQUEsT0FBQTtZQUNBLE9BQUE7WUFDQSxXQUFBLFdBQUE7WUFDQSxPQUFBLEdBQUEsMEJBQUE7OztRQUdBLE9BQUEsY0FBQSxVQUFBLFFBQUEsT0FBQTtZQUNBLE9BQUE7WUFDQSxJQUFBLElBQUEsSUFBQSxHQUFBLElBQUEsT0FBQSxVQUFBLFFBQUEsS0FBQTtnQkFDQSxHQUFBLE9BQUEsVUFBQSxHQUFBLFFBQUEsTUFBQSxLQUFBO29CQUNBLE9BQUEsVUFBQSxPQUFBLEdBQUE7OztZQUdBLE1BQUE7OztRQUdBLE9BQUEsb0JBQUEsV0FBQTtZQUNBLE9BQUEsR0FBQTs7OztRQUlBLE9BQUEsT0FBQSxVQUFBLFdBQUE7WUFDQSxHQUFBLE9BQUEsUUFBQTtnQkFDQSxJQUFBLElBQUEsSUFBQSxHQUFBLElBQUEsT0FBQSxPQUFBLFFBQUEsS0FBQTtvQkFDQSxJQUFBLFdBQUEsT0FBQSxPQUFBLEdBQUEsa0JBQUEsT0FBQSxhQUFBO29CQUNBLEdBQUEsV0FBQSxJQUFBO3dCQUNBLFdBQUE7O29CQUVBLE9BQUEsT0FBQSxHQUFBLFdBQUEsV0FBQSxXQUFBLGFBQUE7O2dCQUVBLFdBQUE7Ozs7Ozs7O0FDeENBLENBQUEsVUFBQTtJQUNBOzs7SUFHQSxRQUFBLE9BQUEsbUJBQUEsV0FBQSxrRUFBQSxTQUFBLFFBQUEsWUFBQSxRQUFBLFdBQUE7Ozs7UUFJQSxXQUFBLFdBQUE7O1FBRUEsT0FBQSxvQkFBQSxXQUFBO1lBQ0EsT0FBQSxHQUFBOzs7UUFHQSxPQUFBLFlBQUEsU0FBQSxPQUFBLE9BQUE7WUFDQSxNQUFBO1lBQ0EsV0FBQSxXQUFBO1lBQ0EsT0FBQSxHQUFBLDBCQUFBOzs7UUFHQSxPQUFBLGNBQUEsU0FBQSxPQUFBLE9BQUE7WUFDQSxNQUFBO1lBQ0EsSUFBQSxJQUFBLElBQUEsR0FBQSxJQUFBLE9BQUEsVUFBQSxRQUFBLEtBQUE7Z0JBQ0EsR0FBQSxPQUFBLFVBQUEsR0FBQSxRQUFBLE1BQUEsS0FBQTtvQkFDQSxPQUFBLFVBQUEsT0FBQSxHQUFBOzs7WUFHQSxNQUFBOzs7UUFHQSxPQUFBLE9BQUEsYUFBQSxXQUFBO1lBQ0EsR0FBQSxPQUFBLFdBQUE7Z0JBQ0EsSUFBQSxJQUFBLElBQUEsR0FBQSxJQUFBLE9BQUEsVUFBQSxRQUFBLEtBQUE7b0JBQ0EsSUFBQSxXQUFBLE9BQUEsVUFBQSxHQUFBLGtCQUFBLE9BQUEsaUJBQUE7b0JBQ0EsSUFBQSxJQUFBLE9BQUEsVUFBQSxHQUFBLGtCQUFBLE9BQUEsaUJBQUE7b0JBQ0EsR0FBQSxXQUFBLElBQUE7d0JBQ0EsV0FBQTs7b0JBRUEsT0FBQSxVQUFBLEdBQUEsV0FBQSxXQUFBLFdBQUEsYUFBQTtvQkFDQSxPQUFBLFVBQUEsR0FBQSxJQUFBOztnQkFFQSxPQUFBLFVBQUEsS0FBQSxTQUFBLEVBQUEsR0FBQTtvQkFDQSxPQUFBLEVBQUEsSUFBQSxFQUFBOzs7Z0JBR0EsV0FBQTs7OztLQUlBIiwiZmlsZSI6ImFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpe1xuICAgIFwidXNlIHN0cmljdFwiO1xuICAgIHZhciBhcHAgPSBhbmd1bGFyLm1vZHVsZSgnYXBwJyxcbiAgICAgICAgW1xuICAgICAgICAgICAgJ2FwcC5jb250cm9sbGVycycsXG4gICAgICAgICAgICAnYXBwLmZpbHRlcnMnLFxuICAgICAgICAgICAgJ2FwcC5zZXJ2aWNlcycsXG4gICAgICAgICAgICAnYXBwLmRpcmVjdGl2ZXMnLFxuICAgICAgICAgICAgJ2FwcC5yb3V0ZXMnLFxuICAgICAgICAgICAgJ2FwcC5jb25maWcnLFxuICAgICAgICAgICAgJ2ZhY2Vib29rJyxcbiAgICAgICAgICAgICdnZW9sb2NhdGlvbicsXG4gICAgICAgICAgICAnZGpkczRyY2UuYW5ndWxhci1zb2NpYWxzaGFyZScsXG4gICAgICAgICAgICAndWlHbWFwZ29vZ2xlLW1hcHMnLFxuICAgICAgICAgICAgJ2prdXJpLmRhdGVwaWNrZXInLFxuICAgICAgICAgICAgJ3VpLmRhdGUnLFxuICAgICAgICAgICAgJ3VpLnRpbWVwaWNrZXInLFxuICAgICAgICAgICAgJ3VpLnV0aWxzLm1hc2tzJyxcbiAgICAgICAgICAgICduZ1N0b3JhZ2UnLFxuICAgICAgICAgICAgJ3NhdGVsbGl6ZXInXG4gICAgICAgIF0sIGZ1bmN0aW9uKCRpbnRlcnBvbGF0ZVByb3ZpZGVyLCAkbG9jYXRpb25Qcm92aWRlciwgRmFjZWJvb2tQcm92aWRlciwgJGh0dHBQcm92aWRlciwgJGF1dGhQcm92aWRlcil7XG4gICAgICAgICAgICAkaW50ZXJwb2xhdGVQcm92aWRlci5zdGFydFN5bWJvbCgneyUnKTtcbiAgICAgICAgICAgICRpbnRlcnBvbGF0ZVByb3ZpZGVyLmVuZFN5bWJvbCgnJX0nKTtcbiAgICAgICAgICAgIEZhY2Vib29rUHJvdmlkZXIuaW5pdCgkKCcjZmJfYXBwX2lkJykudGV4dCgpKTsvL2xvY2FsaG9zdDogMTYzMTU5NDAxMzcyNzkyMCAvLzE2MjE0MDM1OTQ3NDY5NjJcbiAgICAgICAgICAgICRodHRwUHJvdmlkZXIuaW50ZXJjZXB0b3JzLnB1c2goWyckcScsICckbG9jYXRpb24nLCAnJGxvY2FsU3RvcmFnZScsIGZ1bmN0aW9uICgkcSwgJGxvY2F0aW9uLCAkbG9jYWxTdG9yYWdlKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICAgICAgJ3JlcXVlc3QnOiBmdW5jdGlvbiAoY29uZmlnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25maWcuaGVhZGVycyA9IGNvbmZpZy5oZWFkZXJzIHx8IHt9O1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uZmlnLnBhcmFtcyA9IGNvbmZpZy5wYXJhbXMgfHwge307XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoJGxvY2FsU3RvcmFnZS50b2tlbikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGNvbmZpZy5oZWFkZXJzLkF1dGhvcml6YXRpb24gPSAnQmVhcmVyICcgKyAkbG9jYWxTdG9yYWdlLnRva2VuO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbmZpZy5wYXJhbXMudG9rZW4gPSAkbG9jYWxTdG9yYWdlLnRva2VuO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGNvbmZpZztcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICB9XSk7XG5cbiAgICAgICAgfSk7XG5cbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLnJvdXRlcycsIFsndWkucm91dGVyJ10pO1xuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuY29udHJvbGxlcnMnLCBbJ3VpLnJvdXRlcicsICdyZXN0YW5ndWxhcicsICd1aS5ib290c3RyYXAnLCAnZmFjZWJvb2snLCAndWlHbWFwZ29vZ2xlLW1hcHMnLCAnZ2VvbG9jYXRpb24nLCAnZ29vZ2xlLnBsYWNlcycsICduZ0ZpbGVVcGxvYWQnLCAnbmdJbWdDcm9wJ10pO1xuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuZmlsdGVycycsIFtdKTtcbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLnNlcnZpY2VzJywgWydyZXN0YW5ndWxhcicsICd1aS5yb3V0ZXInLCAnZmFjZWJvb2snLCAnZ2VvbG9jYXRpb24nXSk7XG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5kaXJlY3RpdmVzJywgW10pO1xuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuY29uZmlnJywgWyd1aS5ib290c3RyYXAnXSk7XG59KSgpOyIsIihmdW5jdGlvbigpe1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5yb3V0ZXMnKS5jb25maWcoIGZ1bmN0aW9uKCRzdGF0ZVByb3ZpZGVyLCAkdXJsUm91dGVyUHJvdmlkZXIsICRsb2NhdGlvblByb3ZpZGVyICkge1xuXG4gICAgICAgIHZhciBnZXRWaWV3ID0gZnVuY3Rpb24oIHZpZXdOYW1lICl7XG4gICAgICAgICAgICByZXR1cm4gJy92aWV3cy9hcHAvJyArIHZpZXdOYW1lICsgJy8nICsgdmlld05hbWUgKyAnLmh0bWwnO1xuICAgICAgICB9O1xuICAgICAgICBcbiAgICAgICAgdmFyIGdldE9yZ1ZpZXcgPSBmdW5jdGlvbih2aWV3TmFtZSkge1xuICAgICAgICAgICAgcmV0dXJuICcvdmlld3MvYXBwL29yZy8nICsgdmlld05hbWUgKyAnLycgKyB2aWV3TmFtZSArICcuaHRtbCc7XG4gICAgICAgIH07XG4gICAgICAgICRsb2NhdGlvblByb3ZpZGVyLmh0bWw1TW9kZSh0cnVlKS5oYXNoUHJlZml4KCchJyk7XG4gICAgICAgICR1cmxSb3V0ZXJQcm92aWRlci5vdGhlcndpc2UoJy8hL2V2ZW50cycpO1xuICAgICAgICAkc3RhdGVQcm92aWRlclxuICAgICAgICAgICAgLnN0YXRlKCdob21lJywge1xuICAgICAgICAgICAgICAgIHVybDogJy8hJyxcbiAgICAgICAgICAgICAgICB2aWV3czoge1xuICAgICAgICAgICAgICAgICAgICBtYWluOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogZ2V0VmlldygnaG9tZScpXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGFic3RyYWN0OiB0cnVlXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLnN0YXRlKCdnb29nbGVfdG9rZW4nLCB7XG4gICAgICAgICAgICAgICAgdGVtcGxhdGU6ICcnLFxuICAgICAgICAgICAgICAgIC8vaHR0cHM6Ly92ZW52YXN0LmNvbS8jYWNjZXNzX3Rva2VuPXlhMjkuQ2k4YkF5azRQZGFndEZHYWFPeW1jeERtQ28tdTkxQXZRR2YxRmpGSVBMVDM0QnBqNFFQa0FJOHVuM0tGSFJlbmxnJnRva2VuX3R5cGU9QmVhcmVyJmV4cGlyZXNfaW49MzYwMFxuICAgICAgICAgICAgICAgIHVybDogJy8jYWNjZXNzX3Rva2VuPTphY2Nlc3NUb2tlbicsXG4gICAgICAgICAgICAgICAgY29udHJvbGxlcjogZnVuY3Rpb24gKCRsb2NhdGlvbiwgJHJvb3RTY29wZSkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgaGFzaCA9ICRsb2NhdGlvbi5wYXRoKCkuc3Vic3RyKDEpO1xuICAgICAgICAgICAgICAgICAgICB2YXIgc3BsaXR0ZWQgPSBoYXNoLnNwbGl0KCcmJyk7XG4gICAgICAgICAgICAgICAgICAgIHZhciBwYXJhbXMgPSB7fTtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJUT0tFRUVOISFcIik7XG4gICAgICAgICAgICAgICAgICAgIC8vIGZvciAodmFyIGkgPSAwOyBpIDwgc3BsaXR0ZWQubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIHZhciBwYXJhbSAgPSBzcGxpdHRlZFtpXS5zcGxpdCgnPScpO1xuICAgICAgICAgICAgICAgICAgICAvLyAgICAgdmFyIGtleSAgICA9IHBhcmFtWzBdO1xuICAgICAgICAgICAgICAgICAgICAvLyAgICAgdmFyIHZhbHVlICA9IHBhcmFtWzFdO1xuICAgICAgICAgICAgICAgICAgICAvLyAgICAgcGFyYW1zW2tleV0gPSB2YWx1ZTtcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgICRyb290U2NvcGUuYWNjZXNzdG9rZW49cGFyYW1zO1xuICAgICAgICAgICAgICAgICAgICAvLyB9XG4gICAgICAgICAgICAgICAgICAgIC8vICRsb2NhdGlvbi5wYXRoKFwiL2Fib3V0XCIpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAuc3RhdGUoJ2hvbWUuZXZlbnRzJywge1xuICAgICAgICAgICAgICAgIHVybDogJy9ldmVudHMnLFxuICAgICAgICAgICAgICAgIHZpZXdzOiB7XG4gICAgICAgICAgICAgICAgICAgIG1haW46IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiBnZXRWaWV3KCdob21lJylcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgY2F0YWxvZ3VlOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogZ2V0VmlldygnZXZlbnRzJylcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgcl9zaWRlYmFyOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogZ2V0Vmlldygnc2lkZWJhcicpXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIGZpbHRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiBnZXRWaWV3KCdldmVudF9maWx0ZXInKVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5zdGF0ZSgnaG9tZS52ZW51ZXMnLCB7XG4gICAgICAgICAgICAgICAgdXJsOiAnL3ZlbnVlcycsXG4gICAgICAgICAgICAgICAgdmlld3M6IHtcbiAgICAgICAgICAgICAgICAgICAgbWFpbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6IGdldFZpZXcoJ2hvbWUnKVxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBjYXRhbG9ndWU6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiBnZXRWaWV3KCd2ZW51ZXMnKVxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICByX3NpZGViYXI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiBnZXRWaWV3KCdzaWRlYmFyJylcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgZmlsdGVyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6IGdldFZpZXcoJ3ZlbnVlX2ZpbHRlcicpXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLnN0YXRlKCdob21lLmRlYWxzJywge1xuICAgICAgICAgICAgICAgIHVybDogJy9kZWFscycsXG4gICAgICAgICAgICAgICAgdmlld3M6IHtcbiAgICAgICAgICAgICAgICAgICAgbWFpbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6IGdldFZpZXcoJ2hvbWUnKVxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBjYXRhbG9ndWU6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiBnZXRWaWV3KCdjYXRhbG9ndWUnKVxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICByX3NpZGViYXI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiBnZXRWaWV3KCdzaWRlYmFyJylcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAuc3RhdGUoJ29yZ2FuaXplcicsIHtcbiAgICAgICAgICAgICAgICB1cmw6ICcvIScsXG4gICAgICAgICAgICAgICAgdmlld3M6IHtcbiAgICAgICAgICAgICAgICAgICAgbWFpbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6IGdldE9yZ1ZpZXcoJ2hvbWUnKVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBhYnN0cmFjdDogdHJ1ZVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5zdGF0ZSgnb3JnYW5pemVyLmV2ZW50cycsIHtcbiAgICAgICAgICAgICAgICB1cmw6ICcvb3JnYW5pemUnLFxuICAgICAgICAgICAgICAgIHZpZXdzOiB7XG4gICAgICAgICAgICAgICAgICAgIG1haW46IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiBnZXRPcmdWaWV3KCdob21lJylcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgY2F0YWxvZ3VlOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogZ2V0T3JnVmlldygnZXZlbnRzJylcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgcl9zaWRlYmFyOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogZ2V0T3JnVmlldygnc2lkZWJhcicpXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIGZpbHRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiBnZXRWaWV3KCdldmVudF9maWx0ZXInKVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5zdGF0ZSgnb3JnYW5pemVyLmNyZWF0ZV9ldmVudCcsIHtcbiAgICAgICAgICAgICAgICB1cmw6ICcvY3JlYXRlX2V2ZW50JyxcbiAgICAgICAgICAgICAgICB2aWV3czoge1xuICAgICAgICAgICAgICAgICAgICBtYWluOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogZ2V0T3JnVmlldygnaG9tZScpXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIGNhdGFsb2d1ZToge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6IGdldE9yZ1ZpZXcoJ2NyZWF0ZV9ldmVudCcpXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLnN0YXRlKCdvcmdhbml6ZXIudmVudWVzJywge1xuICAgICAgICAgICAgICAgIHVybDogJy9vcmdhbml6ZV92ZW51ZXMnLFxuICAgICAgICAgICAgICAgIHZpZXdzOiB7XG4gICAgICAgICAgICAgICAgICAgIG1haW46IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiBnZXRPcmdWaWV3KCdob21lJylcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgY2F0YWxvZ3VlOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogZ2V0T3JnVmlldygndmVudWVzJylcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgcl9zaWRlYmFyOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogZ2V0T3JnVmlldygnc2lkZWJhcicpXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIGZpbHRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiBnZXRWaWV3KCd2ZW51ZV9maWx0ZXInKVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5zdGF0ZSgnb3JnYW5pemVyLmhvc3RzJywge1xuICAgICAgICAgICAgICAgIHVybDogJy9ob3N0cycsXG4gICAgICAgICAgICAgICAgdmlld3M6IHtcbiAgICAgICAgICAgICAgICAgICAgbWFpbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6IGdldE9yZ1ZpZXcoJ2hvbWUnKVxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBjYXRhbG9ndWU6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiBnZXRPcmdWaWV3KCdob3N0cycpXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIHJfc2lkZWJhcjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6IGdldE9yZ1ZpZXcoJ3NpZGViYXInKVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIC8vIGZpbHRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgLy8gICAgIHRlbXBsYXRlVXJsOiBnZXRWaWV3KCd2ZW51ZV9maWx0ZXInKVxuICAgICAgICAgICAgICAgICAgICAvLyB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5zdGF0ZSgnb3JnYW5pemVyLmNyZWF0ZV92ZW51ZScsIHtcbiAgICAgICAgICAgICAgICB1cmw6ICcvY3JlYXRlX3ZlbnVlJyxcbiAgICAgICAgICAgICAgICB2aWV3czoge1xuICAgICAgICAgICAgICAgICAgICBtYWluOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogZ2V0T3JnVmlldygnaG9tZScpXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIGNhdGFsb2d1ZToge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6IGdldE9yZ1ZpZXcoJ2NyZWF0ZV92ZW51ZScpXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLnN0YXRlKCdvcmdhbml6ZXIuZGVhbHMnLCB7XG4gICAgICAgICAgICAgICAgdXJsOiAnL29yZ2FuaXplX2RlYWxzJyxcbiAgICAgICAgICAgICAgICB2aWV3czoge1xuICAgICAgICAgICAgICAgICAgICBtYWluOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogZ2V0T3JnVmlldygnaG9tZScpXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIGNhdGFsb2d1ZToge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6IGdldE9yZ1ZpZXcoJ2RlYWxzJylcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgcl9zaWRlYmFyOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogZ2V0T3JnVmlldygnc2lkZWJhcicpXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIGZpbHRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiBnZXRWaWV3KCdkZWFsX2ZpbHRlcicpXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICB9ICk7XG59KSgpOyIsIihmdW5jdGlvbigpe1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG5cbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLnNlcnZpY2VzJykuZmFjdG9yeSgnQ2F0ZWdvcmllc1N0b3JhZ2UnLCBmdW5jdGlvbigkaHR0cCwgQ2F0ZWdvcnkpe1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKlxuICAgICAgICAgKiBAcGFyYW0ge0FycmF5fSBjYXRlZ29yaWVzQXJyYXlcbiAgICAgICAgICogQGNvbnN0cnVjdG9yXG4gICAgICAgICAqL1xuICAgICAgICBmdW5jdGlvbiBDYXRlZ29yaWVzU3RvcmFnZShjYXRlZ29yaWVzQXJyYXkpIHtcbiAgICAgICAgICAgIGlmKGNhdGVnb3JpZXNBcnJheSAmJiBjYXRlZ29yaWVzQXJyYXkubGVuZ3RoID4gMCl7XG4gICAgICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBjYXRlZ29yaWVzQXJyYXkubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCAhKGNhdGVnb3JpZXNBcnJheVtpXSBpbnN0YW5jZW9mIENhdGVnb3J5KSApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhdGVnb3JpZXNBcnJheVtpXSA9IG5ldyBDYXRlZ29yeShjYXRlZ29yaWVzQXJyYXlbaV0pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHRoaXMubG9hZEFycmF5KGNhdGVnb3JpZXNBcnJheSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgQ2F0ZWdvcmllc1N0b3JhZ2UucHJvdG90eXBlID0ge1xuICAgICAgICAgICAgVFlQRV9FVkVOVDogXCJldmVudFwiLFxuICAgICAgICAgICAgVFlQRV9WRU5VRTogXCJ2ZW51ZVwiLFxuICAgICAgICAgICAgVFlQRV9ERUFMOiBcImRlYWxcIixcbiAgICAgICAgICAgIGRhdGE6IFtdLFxuICAgICAgICAgICAgbG9hZGluZzogZmFsc2UsXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGxvYWRBcnJheTogZnVuY3Rpb24oY2F0ZWdvcmllc0FycmF5KSB7XG4gICAgICAgICAgICAgICAgdGhpcy5kYXRhID0gY2F0ZWdvcmllc0FycmF5O1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICpcbiAgICAgICAgICAgICAqIEBwYXJhbSB7b2JqZWN0fSByZXF1ZXN0T2JqZWN0XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGxvYWQ6IGZ1bmN0aW9uKHJlcXVlc3RPYmplY3QpIHtcbiAgICAgICAgICAgICAgICB2YXIgc2NvcGUgPSB0aGlzO1xuICAgICAgICAgICAgICAgIGlmKHRoaXMubG9hZGluZyA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICRodHRwLmdldCgnY2F0ZWdvcnknLCB7cGFyYW1zOiByZXF1ZXN0T2JqZWN0fSkuc3VjY2VzcyhmdW5jdGlvbihjYXRlZ29yeURhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvcih2YXIgaSA9IDA7IGkgPCBjYXRlZ29yeURhdGEubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXRlZ29yeURhdGFbaV0gPSBuZXcgQ2F0ZWdvcnkoY2F0ZWdvcnlEYXRhW2ldKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmxvYWRBcnJheShjYXRlZ29yeURhdGEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUubG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHNjb3BlO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgZ2V0SW1hZ2VVcmw6IGZ1bmN0aW9uKHdpZHRoLCBoZWlnaHQpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gJ291ci9pbWFnZS9zZXJ2aWNlLycgKyB0aGlzLmNhdGVnb3J5LmlkICsgJy93aWR0aC9oZWlnaHQnO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuXG4gICAgICAgIHJldHVybiBDYXRlZ29yaWVzU3RvcmFnZTtcblxuICAgIH0pfSkoKTsiLCIoZnVuY3Rpb24oKXtcbiAgICBcInVzZSBzdHJpY3RcIjtcblxuXG5hbmd1bGFyLm1vZHVsZSgnYXBwLnNlcnZpY2VzJykuZmFjdG9yeSgnQ2F0ZWdvcnknLCBmdW5jdGlvbigkc3RhdGUsICRodHRwKXtcblxuICAgIGZ1bmN0aW9uIENhdGVnb3J5KGNhdGVnb3J5RGF0YSkge1xuICAgICAgICBpZiAoY2F0ZWdvcnlEYXRhKSB7XG4gICAgICAgICAgICB0aGlzLnNldERhdGEoY2F0ZWdvcnlEYXRhKTtcbiAgICAgICAgfVxuICAgICAgICAvL9GH0YLQvi3RgtC+LCDRh9GC0L4g0LXRidC1INC90YPQttC90L4g0LTQu9GPINC40L3QuNGG0LjQsNC70LjQt9Cw0YbQuNC4INC60L3QuNCz0LhcbiAgICB9O1xuXG4gICAgQ2F0ZWdvcnkucHJvdG90eXBlID0ge1xuICAgICAgICBzZXREYXRhOiBmdW5jdGlvbihjYXRlZ29yeURhdGEpIHtcbiAgICAgICAgICAgIGFuZ3VsYXIuZXh0ZW5kKHRoaXMsIGNhdGVnb3J5RGF0YSk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFxuICAgICAgICAgKiBAcGFyYW0gY2F0ZWdvcnlJZFxuICAgICAgICAgKiBAcmV0dXJucyB7UHJvbWlzZX1cbiAgICAgICAgICovXG4gICAgICAgIGxvYWQ6IGZ1bmN0aW9uKGNhdGVnb3J5SWQpIHtcbiAgICAgICAgICAgIHZhciBzY29wZSA9IHRoaXM7XG4gICAgICAgICAgICByZXR1cm4gJGh0dHAuZ2V0KCdjYXRlZ29yeS8nICsgY2F0ZWdvcnlJZCkuc3VjY2VzcyhmdW5jdGlvbihjYXRlZ29yeURhdGEpIHtcbiAgICAgICAgICAgICAgICBzY29wZS5zZXREYXRhKGNhdGVnb3J5RGF0YSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHNjb3BlO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0sXG4gICAgICAgIFxuICAgICAgICBnZXRJbWFnZVVybDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gJ2h0dHBzOi8vdmVudmFzdC5jb20vaW1nL2NhY2hlL29yaWdpbmFsL2NhdGVnb3JpZXMvJyArIHRoaXMuaW1hZ2VcbiAgICAgICAgfVxuICAgIH07XG4gICAgcmV0dXJuIENhdGVnb3J5O1xuXG59KX0pKCk7IiwiKGZ1bmN0aW9uKCkge1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5zZXJ2aWNlcycpLmZhY3RvcnkoJ0NoYW5nZXNOb3RpZmllcicsIGZ1bmN0aW9uKCRyb290U2NvcGUpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgU1VCSl9FVkVOVFNfTE9BREVEOiAnRXZlbnRzTG9hZGVkJyxcbiAgICAgICAgICAgIFNVQkpfVkVOVUVTX0xPQURFRDogJ1ZlbnVlc0xvYWRlZCcsXG4gICAgICAgICAgICBTVUJKX0RFQUxTX0xPQURFRDogJ0RlYWxzTG9hZGVkJyxcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgU1VCSl9FVkVOVFNfUkVMT0FERUQ6ICdFdmVudHNSZWxvYWRlZCcsXG4gICAgICAgICAgICBTVUJKX1ZFTlVFU19SRUxPQURFRDogJ1ZlbnVlc1JlbG9hZGVkJyxcbiAgICAgICAgICAgIFNVQkpfREVBTFNfUkVMT0FERUQ6ICdEZWFsc1JlbG9hZGVkJyxcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgU1VCSl9FVkVOVFNfQURERUQ6ICdFdmVudHNBZGRlZCcsXG4gICAgICAgICAgICBTVUJKX1ZFTlVFU19BRERFRDogJ1ZlbnVlc0FkZGVkJyxcbiAgICAgICAgICAgIFNVQkpfREVBTFNfQURERUQ6ICdEZWFsc0FkZGVkJyxcblxuXG4gICAgICAgICAgICBTVUJKX09SR19FVkVOVFNfTE9BREVEOiAnRXZlbnRzTG9hZGVkJyxcbiAgICAgICAgICAgIFNVQkpfT1JHX1ZFTlVFU19MT0FERUQ6ICdWZW51ZXNMb2FkZWQnLFxuICAgICAgICAgICAgU1VCSl9PUkdfREVBTFNfTE9BREVEOiAnRGVhbHNMb2FkZWQnLFxuXG4gICAgICAgICAgICBTVUJKX09SR19FVkVOVFNfUkVMT0FERUQ6ICdFdmVudHNSZWxvYWRlZCcsXG4gICAgICAgICAgICBTVUJKX09SR19WRU5VRVNfUkVMT0FERUQ6ICdWZW51ZXNSZWxvYWRlZCcsXG4gICAgICAgICAgICBTVUJKX09SR19ERUFMU19SRUxPQURFRDogJ0RlYWxzUmVsb2FkZWQnLFxuXG4gICAgICAgICAgICBTVUJKX09SR19FVkVOVFNfQURERUQ6ICdFdmVudHNBZGRlZCcsXG4gICAgICAgICAgICBTVUJKX09SR19WRU5VRVNfQURERUQ6ICdWZW51ZXNBZGRlZCcsXG4gICAgICAgICAgICBTVUJKX09SR19ERUFMU19BRERFRDogJ0RlYWxzQWRkZWQnLFxuICAgICAgICAgICAgXG4gICAgICAgICAgICB2YWx1ZTogbnVsbCxcbiAgICAgICAgICAgIG9yZ192YWx1ZTogbnVsbCxcbiAgICAgICAgICAgIG9yZ192ZW51ZXM6IG51bGwsXG4gICAgICAgICAgICB2ZW51ZXNfdmFsdWU6IG51bGwsXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHN1YnNjcmliZTogZnVuY3Rpb24oc3ViamVjdCwgc2NvcGUsIGNhbGxiYWNrKSB7XG4gICAgICAgICAgICAgICAgdmFyIGhhbmRsZXIgPSAkcm9vdFNjb3BlLiRvbihzdWJqZWN0LCBjYWxsYmFjayk7XG4gICAgICAgICAgICAgICAgc2NvcGUuJG9uKCckZGVzdHJveScsIGhhbmRsZXIpO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgbm90aWZ5OiBmdW5jdGlvbihzdWJqZWN0LCB2YWx1ZSkge1xuICAgICAgICAgICAgICAgICRyb290U2NvcGUuJGVtaXQoc3ViamVjdCk7XG4gICAgICAgICAgICAgICAgaWYodmFsdWUpXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudmFsdWUgPSB2YWx1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICB9KTtcbiAgICBcbn0pKCk7IiwiKGZ1bmN0aW9uKCl7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5zZXJ2aWNlcycpLmZhY3RvcnkoJ0RhdGVTZXJ2aWNlJywgZnVuY3Rpb24oKXtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGRpZmY6IHtcbiAgICAgICAgICAgICAgICBpbkhvdXJzOiBmdW5jdGlvbiAoZDEsIGQyKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciB0MiA9IGQyLmdldFRpbWUoKTtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHQxID0gZDEuZ2V0VGltZSgpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcGFyc2VJbnQoKHQyLXQxKS8oMzYwMCoxMDAwKSk7XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBpbkRheXM6IGZ1bmN0aW9uKGQxLCBkMikge1xuICAgICAgICAgICAgICAgICAgICB2YXIgdDIgPSBkMi5nZXRUaW1lKCk7XG4gICAgICAgICAgICAgICAgICAgIHZhciB0MSA9IGQxLmdldFRpbWUoKTtcblxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcGFyc2VJbnQoKHQyLXQxKS8oMjQqMzYwMCoxMDAwKSk7XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBpbldlZWtzOiBmdW5jdGlvbihkMSwgZDIpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHQyID0gZDIuZ2V0VGltZSgpO1xuICAgICAgICAgICAgICAgICAgICB2YXIgdDEgPSBkMS5nZXRUaW1lKCk7XG5cbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHBhcnNlSW50KCh0Mi10MSkvKDI0KjM2MDAqMTAwMCo3KSk7XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBpbk1vbnRoczogZnVuY3Rpb24oZDEsIGQyKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBkMVkgPSBkMS5nZXRGdWxsWWVhcigpO1xuICAgICAgICAgICAgICAgICAgICB2YXIgZDJZID0gZDIuZ2V0RnVsbFllYXIoKTtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGQxTSA9IGQxLmdldE1vbnRoKCk7XG4gICAgICAgICAgICAgICAgICAgIHZhciBkMk0gPSBkMi5nZXRNb250aCgpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gKGQyTSsxMipkMlkpLShkMU0rMTIqZDFZKTtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGluWWVhcnM6IGZ1bmN0aW9uKGQxLCBkMikge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZDIuZ2V0RnVsbFllYXIoKS1kMS5nZXRGdWxsWWVhcigpO1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgZ2V0RGlmZjogZnVuY3Rpb24oZDEsIGQyKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmKHRoaXMuaW5Ib3VycyhkMSwgZDIpID4gNDgpe1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYodGhpcy5pbkRheXMgPiAxNCl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFwiU3RhcnRzIGluIFwiICsgdGhpcy5pbldlZWtzKGQxLCBkMikgKyBcIiB3ZWVrc1wiO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gXCJTdGFydHMgaW4gXCIgKyB0aGlzLmluRGF5cyhkMSwgZDIpICsgXCIgZGF5c1wiO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFwiU3RhcnRzIGluIFwiICsgdGhpcy5pbkhvdXJzKGQxLCBkMikgKyBcIiBob3Vyc1wiO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgIH0pO1xuXG59KSgpOyIsIihmdW5jdGlvbigpe1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG5cbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLnNlcnZpY2VzJykuZmFjdG9yeSgnRXZlbnQnLCBmdW5jdGlvbihVcGxvYWQsICRodHRwKSB7XG5cblxuICAgICAgICBmdW5jdGlvbiBFdmVudChldmVudERhdGEpIHtcbiAgICAgICAgICAgIGlmKGV2ZW50RGF0YSkge1xuICAgICAgICAgICAgICAgIHRoaXMubG9hZERhdGEoZXZlbnREYXRhKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICBFdmVudC5wcm90b3R5cGUgPSB7XG5cbiAgICAgICAgICAgIF9pZDogbnVsbCxcbiAgICAgICAgICAgIHRpdGxlOiBudWxsLFxuICAgICAgICAgICAgZGV0YWlsczogbnVsbCxcbiAgICAgICAgICAgIGFkZHJlc3M6IG51bGwsXG4gICAgICAgICAgICBzdGFydF9hdDogbnVsbCxcbiAgICAgICAgICAgIGVuZF9hdDogbnVsbCxcbiAgICAgICAgICAgIGxvY2F0aW9uOiBudWxsLFxuICAgICAgICAgICAgY2F0ZWdvcmllczogW10sXG4gICAgICAgICAgICB2ZW51ZTogbnVsbCxcbiAgICAgICAgICAgIHBpY3R1cmU6IG51bGwsXG4gICAgICAgICAgICBjbGFzc05hbWU6ICdFdmVudCcsXG5cbiAgICAgICAgICAgIGF0dGVuZGVlczogW10sXG4gICAgICAgICAgICBvcmdhbml6ZXJzOiBbXSxcbiAgICAgICAgICAgIGxpa2VzOiAwLFxuXG4gICAgICAgICAgICBsb2FkRGF0YTogZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgICAgICBhbmd1bGFyLmV4dGVuZCh0aGlzLCBkYXRhKTtcbiAgICAgICAgICAgICAgICBpZih0eXBlb2YgdGhpcy52ZW51ZV9yZWxhdGlvbiAhPT0gJ3VuZGVmaW5lZCcgJiYgdGhpcy52ZXVlX3JlbGF0aW9uICE9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMudmVudWUgPSB0aGlzLnZlbnVlX3JlbGF0aW9uO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0aGlzLmljb24gPSB0aGlzLmdldEljb24oKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcztcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBnZXRDb29yZGluYXRlczogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGlmKHR5cGVvZiB0aGlzLmxvY2F0aW9uID09PSBcInVuZGVmaW5lZFwiIHx8IHRoaXMubG9jYXRpb24gPT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2NhdGlvbiA9IHRoaXMudmVudWUubG9jYXRpb247XG4gICAgICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlT25CYWNrZW5kKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgICAgIGxvbmdpdHVkZTogdGhpcy5sb2NhdGlvbi5jb29yZGluYXRlc1swXSxcbiAgICAgICAgICAgICAgICAgICAgbGF0aXR1ZGU6IHRoaXMubG9jYXRpb24uY29vcmRpbmF0ZXNbMV1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBnZXRJY29uOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgaWYodGhpcy5jYXRlZ29yaWVzLmxlbmd0aCA9PT0gMCB8fCAhdGhpcy5jYXRlZ29yaWVzIHx8IHR5cGVvZiB0aGlzLmNhdGVnb3JpZXMgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAnL2ltZy9xdWVzdGlvbi5wbmcnO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGlmKHR5cGVvZiB0aGlzLmNhdGVnb3JpZXNbMF0gIT09ICd1bmRlZmluZWQnICYmICB0aGlzLmNhdGVnb3JpZXNbMF0gIT09IG51bGwgJiYgdGhpcy5jYXRlZ29yaWVzWzBdLmltYWdlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gXCIvaW1nL2NhY2hlL29yaWdpbmFsL2NhdGVnb3JpZXMvXCIgKyB0aGlzLmNhdGVnb3JpZXNbMF0uaW1hZ2U7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gJy9pbWcvcXVlc3Rpb24ucG5nJztcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIGdldFBpY3R1cmU6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIGlmKHRoaXMucGljdHVyZSA9PT0gbnVsbCB8fCB0eXBlb2YgdGhpcy5waWN0dXJlID09PSAndW5kZWZpbmVkJyB8fCB0aGlzLnBpY3R1cmUgPT09ICcnKXtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICdodHRwczovL3BsYWNlaG9sZGl0LmltZ2l4Lm5ldC9+dGV4dD90eHRzaXplPTMzJnR4dD0nICsgdGhpcy50aXRsZSArICcmdz0zNTAmaD0xNTAnO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZih0eXBlb2YgdGhpcy5waWN0dXJlICE9PSAnc3RyaW5nJyl7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnBpY3R1cmVcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYoIXRoaXMucGljdHVyZS5zdGFydHNXaXRoKCdodHRwJykpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICcvaW1nL2NhY2hlL29yaWdpbmFsL2V2ZW50cy8nICsgdGhpcy5waWN0dXJlO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnBpY3R1cmU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgZ2V0TWFya2VyOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICB2YXIgc2NvcGUgPSB0aGlzO1xuICAgICAgICAgICAgICAgIHZhciBjb29yZGluYXRlcyA9IHNjb3BlLmdldENvb3JkaW5hdGVzKCk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBnb29nbGUubWFwcy5NYXJrZXIoe1xuICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgZ29vZ2xlLm1hcHMuTWFya2VySW1hZ2Uoc2NvcGUuZ2V0SWNvbigpLCBudWxsLCBudWxsLCBudWxsLCBuZXcgZ29vZ2xlLm1hcHMuU2l6ZSgzMiwgMzIpKSxcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IHtsYXQ6IGNvb3JkaW5hdGVzLmxhdGl0dWRlLCBsbmc6IGNvb3JkaW5hdGVzLmxvbmdpdHVkZX0sXG4gICAgICAgICAgICAgICAgICAgIGlkOiAnZXZlbnQtJyArIHNjb3BlLl9pZFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgc2F2ZU9uQmFja2VuZDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGlmKHRoaXMucGljdHVyZSAhPT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gVXBsb2FkLnVwbG9hZCh7dXJsOiAnZXZlbnQvY3JlYXRlJywgZGF0YTogdGhpc30pXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhlbihmdW5jdGlvbiAocmVzcCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiByZXNwO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJldHVybiAkaHR0cC5wb3N0KCdldmVudC9jcmVhdGUnLCB0aGlzKS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIHVwZGF0ZU9uQmFja2VuZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgaWYodHlwZW9mIHRoaXMucGljdHVyZSA9PT0gJ29iamVjdCcpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFVwbG9hZC51cGxvYWQoe3VybDogJ2V2ZW50L3VwZGF0ZS8nK3RoaXMuX2lkLCBkYXRhOiB0aGlzfSlcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uIChyZXNwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3A7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJGh0dHAucG9zdCgnZXZlbnQvdXBkYXRlLycrdGhpcy5faWQsIHRoaXMpLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKlxuICAgICAgICAgICAgICogQHJldHVybnMge21vbWVudC5Nb21lbnR8Kn1cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgZ2V0U3RhcnRBdDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIG1vbWVudCh0aGlzLnN0YXJ0X2F0KTtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICpcbiAgICAgICAgICAgICAqIEByZXR1cm5zIHttb21lbnQuTW9tZW50fCp9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGdldEVuZEF0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBpZih0aGlzLmVuZF9hdCA9PT0gbnVsbClcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgICAgICAgICAgcmV0dXJuIG1vbWVudCh0aGlzLmVuZF9hdCk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqXG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7c3RyaW5nfVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBnZXRTdGFydEhvdXJzOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0U3RhcnRBdCgpLmZvcm1hdCgnaDptbSBhJyk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBnZXRFbmRIb3VyczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgaWYodGhpcy5nZXRFbmRBdCgpID09PSBudWxsKVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gXCJ1bmtub3duXCI7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0RW5kQXQoKS5mb3JtYXQoJ2g6bW0gYScpO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKlxuICAgICAgICAgICAgICogQHJldHVybnMge3N0cmluZ31cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgZ2V0U3RhcnREYXRlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0U3RhcnRBdCgpLmZvcm1hdCgnWVlZWS1NTS1ERCcpO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgZ2V0RW5kRGF0ZTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgaWYodGhpcy5nZXRFbmRBdCgpID09PSBudWxsKVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gXCJ1bmtub3duXCI7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0RW5kQXQoKS5mb3JtYXQoJ1lZWVktTU0tREQnKTtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICpcbiAgICAgICAgICAgICAqIEByZXR1cm5zIHtzdHJpbmd9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGdldFN0YXJ0c0luOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gbW9tZW50KCkudG8odGhpcy5nZXRTdGFydEF0KCkpO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgaXNBZnRlcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuICh0aGlzLmdldFN0YXJ0QXQoKSAmJiB0aGlzLmdldFN0YXJ0QXQoKS5pc0FmdGVyKG1vbWVudCgpKSkgfHwgKHRoaXMuZ2V0RW5kQXQoKSAmJiB0aGlzLmdldEVuZEF0KCkuaXNBZnRlcihtb21lbnQoKSkpXG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFJldHVybnMgZGlzdGFuY2UgaW4ga20gYmV0d2VlbiBwb2ludCBzcGVjaWZpZWQgYW5kXG4gICAgICAgICAgICAgKiB0aGlzIEV2ZW50XG4gICAgICAgICAgICAgKlxuICAgICAgICAgICAgICogQHBhcmFtIHt7bGF0aXR1ZGU6IGZsb2F0LCBsb25naXR1ZGU6IGZsb2F0fX0gcG9pbnRcbiAgICAgICAgICAgICAqIEByZXR1cm5zIHtudW1iZXJ9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGNvbXB1dGVEaXN0YW5jZVRvOiBmdW5jdGlvbiAocG9pbnQpIHtcbiAgICAgICAgICAgICAgICB2YXIgZXZlbnRDb29yZGluYXRlcyA9IHRoaXMuZ2V0Q29vcmRpbmF0ZXMoKTtcbiAgICAgICAgICAgICAgICB2YXIgcG9pbnRMYXRMbmcgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKHtsYXQ6IHBvaW50LmxhdGl0dWRlLCBsbmc6IHBvaW50LmxvbmdpdHVkZX0pO1xuICAgICAgICAgICAgICAgIHZhciBFdmVudExhdExuZyA9IG5ldyBnb29nbGUubWFwcy5MYXRMbmcoe2xhdDogZXZlbnRDb29yZGluYXRlcy5sYXRpdHVkZSwgbG5nOiBldmVudENvb3JkaW5hdGVzLmxvbmdpdHVkZX0pO1xuICAgICAgICAgICAgICAgIHJldHVybiBwYXJzZUZsb2F0KChnb29nbGUubWFwcy5nZW9tZXRyeS5zcGhlcmljYWwuY29tcHV0ZURpc3RhbmNlQmV0d2Vlbihwb2ludExhdExuZywgRXZlbnRMYXRMbmcpIC8gMTAwMCkudG9GaXhlZCgyKSk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBkZWxldGVGcm9tQmFja2VuZDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHJldHVybiAkaHR0cC5nZXQoJ2V2ZW50L2RlbGV0ZS8nK3RoaXMuX2lkKS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9O1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIEV2ZW50O1xuICAgIH0pO1xuXG59KSgpOyIsIihmdW5jdGlvbigpe1xuICAgIFxuICAgIC8vIFwidXNlIHN0cmljdFwiO1xuICAgIFxuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuc2VydmljZXMnKS5mYWN0b3J5KCdFdmVudHNTdG9yYWdlJywgZnVuY3Rpb24oJGh0dHAsICR0aW1lb3V0LCBFdmVudCwgQ2hhbmdlc05vdGlmaWVyKSB7XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqXG4gICAgICAgICAqIEBwYXJhbSB7QXJyYXl9IGV2ZW50c0FycmF5XG4gICAgICAgICAqIEBjb25zdHJ1Y3RvclxuICAgICAgICAgKi9cbiAgICAgICAgZnVuY3Rpb24gRXZlbnRzU3RvcmFnZShldmVudHNBcnJheSkge1xuICAgICAgICAgICAgaWYoZXZlbnRzQXJyYXkpe1xuICAgICAgICAgICAgICAgIGZvcih2YXIgaSA9IDA7IGkgPCBldmVudHNBcnJheS5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICBpZighKGV2ZW50c0FycmF5W2ldIGluc3RhbmNlb2YgRXZlbnQpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBldmVudHNBcnJheVtpXSA9IG5ldyBFdmVudChldmVudHNBcnJheVtpXSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkQXJyYXkoZXZlbnRzQXJyYXkpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgRXZlbnRzU3RvcmFnZS5wcm90b3R5cGUgPSB7XG4gICAgICAgICAgICBkYXRhOiBbXSxcbiAgICAgICAgICAgIGxvYWRpbmc6IGZhbHNlLFxuICAgICAgICAgICAgaGFzTmV4dFBhZ2U6IGZhbHNlLFxuICAgICAgICAgICAgX3RpbWVvdXQ6IG51bGwsXG5cbiAgICAgICAgICAgIGxvYWRBcnJheTogZnVuY3Rpb24gKGV2ZW50c0FycmF5KSB7XG4gICAgICAgICAgICAgICAgdGhpcy5kYXRhID0gZXZlbnRzQXJyYXk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqXG4gICAgICAgICAgICAgKiBAcGFyYW0ge1ZlblZhc3RSZXF1ZXN0fSByZXF1ZXN0XG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7Kn1cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgbG9hZDogZnVuY3Rpb24gKHJlcXVlc3QpIHtcbiAgICAgICAgICAgICAgICB2YXIgc2NvcGUgPSB0aGlzO1xuXG4gICAgICAgICAgICAgICAgaWYodGhpcy5fdGltZW91dCl7IC8vaWYgdGhlcmUgaXMgYWxyZWFkeSBhIHRpbWVvdXQgaW4gcHJvY2VzcyBjYW5jZWwgaXRcbiAgICAgICAgICAgICAgICAgICAgJHRpbWVvdXQuY2FuY2VsKHNjb3BlLl90aW1lb3V0KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuX3RpbWVvdXQgPSAkdGltZW91dChmdW5jdGlvbigpe1xuICAgICAgICAgICAgICAgICAgICBpZihzY29wZS5sb2FkaW5nID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUubG9hZGluZyA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZihyZXF1ZXN0Lmhhc01vcmVQYWdlcyA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gW107XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gJGh0dHAuZ2V0KCdldmVudCcsIHtwYXJhbXM6IHJlcXVlc3QuZ2V0UmVxdWVzdE9iamVjdCgpfSkuc3VjY2VzcyhmdW5jdGlvbihldmVudERhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IodmFyIGkgPSAwOyBpIDwgZXZlbnREYXRhLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV2ZW50RGF0YVtpXSA9IG5ldyBFdmVudChldmVudERhdGFbaV0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXF1ZXN0LnJlcXVlc3RQZXJmb3JtZWQoZXZlbnREYXRhKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5sb2FkQXJyYXkoZXZlbnREYXRhKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5sb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHNjb3BlO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgc2NvcGUuX3RpbWVvdXQgPSBudWxsO1xuICAgICAgICAgICAgICAgIH0sIDUwMCk7XG4gICAgICAgICAgICB9XG5cblxuXG4gICAgICAgIH07XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gRXZlbnRzU3RvcmFnZTtcbiAgICB9KTtcbiAgICBcbn0pKCk7IiwiKGZ1bmN0aW9uKCl7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5zZXJ2aWNlcycpLmZhY3RvcnkoJ0h0bWxIZWxwZXInLCBmdW5jdGlvbigkc3RhdGUsICRyb290U2NvcGUsIFJlc3Rhbmd1bGFyKXtcblxuICAgICAgICByZXR1cm4ge1xuXG4gICAgICAgICAgICBib2R5OiBudWxsLFxuICAgICAgICAgICAgb3ZlcmxheTogbnVsbCxcbiAgICAgICAgICAgIGxpbmsgOm51bGwsXG4gICAgICAgICAgICBjb250ZW50OiBudWxsLFxuICAgICAgICAgICAgcG9wdXBzOm51bGwsXG4gICAgICAgICAgICBkb2N1bWVudDpudWxsLFxuXG5cbiAgICAgICAgICAgIGluaXQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICQod2luZG93KS5zY3JvbGwoZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICAgICAgdmFyIHNjcm9sbCA9ICQod2luZG93KS5zY3JvbGxUb3AoKTtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGNhdGVnb3J5ID0gJCgnZGl2LmNhdGVnb3J5JykuZXEoMCk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChzY3JvbGwgPiAyMDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhdGVnb3J5LmFkZENsYXNzKCdjYXRlZ29yeS0tZml4ZWQnKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhdGVnb3J5LnJlbW92ZUNsYXNzKCdjYXRlZ29yeS0tZml4ZWQnKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIHRoaXMuZ2V0TGluaygpLm9uKCdjbGljaycsIHRoaXMucmVuZGVyX3RhYik7XG4gICAgICAgICAgICAgICAgJCgnLmNvbnRhaW5lciB1bCBsaSAuYWN0aXZlJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgZ2V0Q29vcmRzOiBmdW5jdGlvbiAoZWxlbSkge1xuICAgICAgICAgICAgICAgIGlmKGVsZW0pIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGJveCA9IGVsZW0uZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBib3gudG9wICsgcGFnZVlPZmZzZXQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgY2xvc2VQb3B1cHM6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICQoJ2h0bWwnKS5hZGRDbGFzcygnc2Nyb2xsJyk7XG4gICAgICAgICAgICAgICAgJCgnLnBvcHVwLW92ZXJsYXknKS5hZGRDbGFzcygnaGlkZScpO1xuICAgICAgICAgICAgICAgIGZvcih2YXIgaSA9IDA7IGkgPCAkKCcucG9wdXAtc2ltcGxlJykubGVuZ3RoOyBpKyspe1xuICAgICAgICAgICAgICAgICAgICAkKCQoJy5wb3B1cC1zaW1wbGUnKVtpXSkuYWRkQ2xhc3MoJ2hpZGUnKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgJCgnLmNhdGVnb3J5JykucmVtb3ZlQ2xhc3MoJ2hpZGUnKTtcbiAgICAgICAgICAgICAgICAkKCcubG9hZGVyLXdyYXBwZXInKS5oaWRlKCk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBzaG93UG9wVXA6IGZ1bmN0aW9uKHBvcHVwSWQpIHtcbiAgICAgICAgICAgICAgICAvLyAkKCdodG1sJykucmVtb3ZlQ2xhc3MoJ3Njcm9sbCcpO1xuICAgICAgICAgICAgICAgICQoJy5wb3B1cC1vdmVybGF5JykucmVtb3ZlQ2xhc3MoJ2hpZGUnKTtcbiAgICAgICAgICAgICAgICAkKFwiI1wiK3BvcHVwSWQpLnJlbW92ZUNsYXNzKCdoaWRlJyk7XG4gICAgICAgICAgICAgICAgJCgnLmNhdGVnb3J5JykuYWRkQ2xhc3MoJ2hpZGUnKTtcbiAgICAgICAgICAgICAgICAkKCcubG9hZGVyLXdyYXBwZXInKS5oaWRlKCdoaWRlJyk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICByZW5kZXJfdGFiOiBmdW5jdGlvbiAoZXZlbnQsIHRhYklkKSB7XG4gICAgICAgICAgICAgICAgJCgnLnRhYi1saW5rIGxpJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgICAgICQoJy50YWItY29udGVudCBsaScpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICAgICAkKCcjJyt0YWJJZCkuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgICAgICQoZXZlbnQudGFyZ2V0KS5jbG9zZXN0KCdsaScpLmFkZENsYXNzKCdhY3RpdmUnKTtcblxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgZ2V0Qm9keTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgaWYodGhpcy5ib2R5ID09PSBudWxsKVxuICAgICAgICAgICAgICAgICAgICB0aGlzLmJvZHkgPSAkKCdib2R5Jyk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuYm9keTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBnZXRPdmVybGF5OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBpZih0aGlzLm92ZXJsYXkgPT09IG51bGwpXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub3ZlcmxheSA9ICQoJy5wb3B1cC1vdmVybGF5Jyk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMub3ZlcmxheTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBnZXRMaW5rOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBpZih0aGlzLmxpbmsgPT09IG51bGwpXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubGluayA9ICQoJy50YWItbGluaycpO1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmxpbms7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgZ2V0Q29udGVudDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgaWYodGhpcy5jb250ZW50ID09PSBudWxsKVxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRlbnQgPSAkKCcudGFiLWNvbnRlbnQgbGknKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5jb250ZW50XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgZ2V0UG9wdXBzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBpZih0aGlzLnBvcHVwcyA9PT0gbnVsbClcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wb3B1cHMgPSAkKCcucG9wdXAtc2ltcGxlJyk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMucG9wdXBzXG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBnZXREb2N1bWVudDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGRvY3VtZW50O1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgU3R5bGVkRHJvcERvd246IGZ1bmN0aW9uKHNlbGVjdG9yLCBtb2RlbCkge1xuICAgICAgICAgICAgICAgIHRoaXMuZGQgPSAkKHNlbGVjdG9yKTtcbiAgICAgICAgICAgICAgICB0aGlzLnBsYWNlaG9sZGVyID0gdGhpcy5kZC5jaGlsZHJlbignc3BhbicpO1xuICAgICAgICAgICAgICAgIHRoaXMub3B0cyA9IHRoaXMuZGQuZmluZCgndWwuZHJvcGRvd24gbGknKTtcbiAgICAgICAgICAgICAgICB0aGlzLnZhbCA9ICcnO1xuICAgICAgICAgICAgICAgIHRoaXMuaW5kZXggPSAtMTtcblxuICAgICAgICAgICAgICAgIHRoaXMuaW5pdEV2ZW50cyA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgb2JqID0gdGhpcztcbiAgICAgICAgICAgICAgICAgICAgb2JqLmRkLm9uKCdjbGljaycsIGZ1bmN0aW9uKGV2ZW50KXtcbiAgICAgICAgICAgICAgICAgICAgICAgICQodGhpcykudG9nZ2xlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgb2JqLm9wdHMub24oJ2NsaWNrJyxmdW5jdGlvbigpe1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG9wdCA9ICQodGhpcyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgY2F0X2lkID0gb3B0LmZpbmQoJ2EnKS5kYXRhKCd2YWx1ZScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgb2JqLnZhbCA9IG9wdC50ZXh0KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBvYmouaW5kZXggPSBvcHQuaW5kZXgoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIG9iai5wbGFjZWhvbGRlci50ZXh0KG9iai52YWwpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICAkKCdib2R5Jykub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgb2JqLmRkLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICB0aGlzLmdldFZhbHVlID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnZhbDtcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIHRoaXMuZ2V0SW5kZXg9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5pbmRleDtcbiAgICAgICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAgICAgdGhpcy5pbml0RXZlbnRzKCk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFNob3cgbG9hZGVyIGFuaW1hdGlvblxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBzaG93TG9hZGVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgJCgnaHRtbCcpLnJlbW92ZUNsYXNzKCdzY3JvbGwnKTtcbiAgICAgICAgICAgICAgICAkKCcjbG9hZGVyLXdyYXBwZXInKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgJCgnLmNhdGFsb2ctd3JhcHBlcicpLmFkZENsYXNzKCdoaWRlJyk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIEhpZGUgbG9hZGVyIGFuaW1hdGlvblxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBoaWRlTG9hZGVyOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgJCgnaHRtbCcpLmFkZENsYXNzKCdzY3JvbGwnKTtcbiAgICAgICAgICAgICAgICAkKCcjbG9hZGVyLXdyYXBwZXInKS5oaWRlKCk7XG4gICAgICAgICAgICAgICAgJCgnLmNhdGFsb2ctd3JhcHBlcicpLnJlbW92ZUNsYXNzKCdoaWRlJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICB9KTtcblxufSkoKTsiLCIoZnVuY3Rpb24oKXtcbiAgICBcInVzZSBzdHJpY3RcIjtcblxuXG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5zZXJ2aWNlcycpLmZhY3RvcnkoJ01hcFNlcnZpY2UnLCBmdW5jdGlvbigkcSwgVXNlclNlcnZpY2UsIFZlbnVlU3RvcmFnZSwgRXZlbnRzU3RvcmFnZSwgVmVuVmFzdFJlcXVlc3QsIEFuY2hvclNtb290aFNjcm9sbCl7XG5cbiAgICAgICAgcmV0dXJuIHtcblxuICAgICAgICAgICAgS0VZX0VWRU5UUzogJ2V2ZW50cycsXG4gICAgICAgICAgICBLRVlfREVBTFM6ICdkZWFscycsXG4gICAgICAgICAgICBLRVlfVkVOVUVTOiAndmVudWVzJyxcblxuICAgICAgICAgICAgWk9PTV9JTklUOiAxNCxcbiAgICAgICAgICAgIFpPT01fREVUQUlMOiAxNyxcblxuICAgICAgICAgICAgcmVxdWVzdDogbnVsbCxcbiAgICAgICAgICAgIG1hcmtlcnM6IHtcbiAgICAgICAgICAgICAgICBldmVudHM6IFtdLFxuICAgICAgICAgICAgICAgIHZlbnVlczogW10sXG4gICAgICAgICAgICAgICAgZGVhbHM6IFtdXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgY3VycmVudE1hcmtlcnNLZXk6ICdldmVudHMnLFxuICAgICAgICAgICAgbWFwOiBudWxsLFxuICAgICAgICAgICAgbWFwT3B0aW9uczogbnVsbCxcbiAgICAgICAgICAgIHVzZXJNYXJrZXI6IG51bGwsXG5cbiAgICAgICAgICAgIHByZXZTdGF0ZTogbnVsbCxcbiAgICAgICAgICAgIHByZXZSZXF1ZXN0OiBudWxsLFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIEV4dGVuZHMgYW4gaW5zdGFuY2Ugd2l0aCBhbiBvYmplY3QgcHJvdmlkZWRcbiAgICAgICAgICAgICAqIEBwYXJhbSBvcHRpb25zXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHNldE9wdGlvbnM6IGZ1bmN0aW9uIChvcHRpb25zKSB7XG4gICAgICAgICAgICAgICAgYW5ndWxhci5leHRlbmQodGhpcywgb3B0aW9ucyk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFNldCBtYXJrZXJzIGZyb20gb2JqZWN0IGdpdmVuXG4gICAgICAgICAgICAgKiBAcGFyYW0gbWFya2Vyc1xuICAgICAgICAgICAgICogQHJldHVybnMge01hcFNlcnZpY2V9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHNldE1hcmtlcnM6IGZ1bmN0aW9uKG1hcmtlcnMpIHtcbiAgICAgICAgICAgICAgICB0aGlzLm1hcmtlcnMgPSBtYXJrZXJzO1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBTZXQgZXZlbnQgbWFya2VycyBmcm9tIGdpdmVuIGFycmF5IG9mIEV2ZW50c1xuICAgICAgICAgICAgICpcbiAgICAgICAgICAgICAqIEBwYXJhbSB7RXZlbnRbXX0gZXZlbnRzXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHNldEV2ZW50TWFya2VyczogZnVuY3Rpb24oZXZlbnRzKSB7XG4gICAgICAgICAgICAgICAgdmFyIG1hcmtlcnMgPSBbXTtcbiAgICAgICAgICAgICAgICB2YXIgc2NvcGUgPSB0aGlzO1xuICAgICAgICAgICAgICAgIGlmKGV2ZW50cykge1xuICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGV2ZW50cy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG1hcmtlciA9IGV2ZW50c1tpXS5nZXRNYXJrZXIoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmtlci5hZGRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgbWEgPSB0aGlzO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKHNjb3BlLm1hcE9wdGlvbnMuem9vbSAhPT0gc2NvcGUuWk9PTV9ERVRBSUwpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5wcmV2U3RhdGUgPSBzY29wZS5tYXBPcHRpb25zO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5tYXBPcHRpb25zID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjZW50ZXI6IG1hLmdldFBvc2l0aW9uKCksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHpvb206IHNjb3BlLlpPT01fREVUQUlMXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5tYXAuc2V0Q2VudGVyKHNjb3BlLm1hcE9wdGlvbnMuY2VudGVyKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5tYXAuc2V0Wm9vbShzY29wZS5tYXBPcHRpb25zLnpvb20pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJy5jYXRhbG9nLXdyYXBwZXInKS5yZW1vdmVDbGFzcygnY2F0YWxvZy13cmFwcGVyLS1mdWxsJywge2R1cmF0aW9uOiA1MDB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBBbmNob3JTbW9vdGhTY3JvbGwuc2Nyb2xsVG8obWEuaWQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyMnICsgbWEuaWQpLmFkZENsYXNzKCdjYXRhbG9nLXdyYXBwZXItLWZ1bGwnLCB7ZHVyYXRpb246IDUwMH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJrZXIuc2V0TWFwKHNjb3BlLm1hcCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJrZXJzLnB1c2gobWFya2VyKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0aGlzLm1hcmtlcnMuZXZlbnRzID0gbWFya2VycztcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIGFkZEV2ZW50TWFya2VyczogZnVuY3Rpb24oZXZlbnRzKSB7XG4gICAgICAgICAgICAgICAgdmFyIG1hcmtlcnMgPSBbXTtcbiAgICAgICAgICAgICAgICB2YXIgc2NvcGUgPSB0aGlzO1xuICAgICAgICAgICAgICAgIGlmKGV2ZW50cykge1xuICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGV2ZW50cy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG1hcmtlciA9IGV2ZW50c1tpXS5nZXRNYXJrZXIoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmtlci5hZGRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgbWEgPSB0aGlzO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKHNjb3BlLm1hcE9wdGlvbnMuem9vbSAhPT0gc2NvcGUuWk9PTV9ERVRBSUwpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5wcmV2U3RhdGUgPSBzY29wZS5tYXBPcHRpb25zO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5tYXBPcHRpb25zID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjZW50ZXI6IG1hLmdldFBvc2l0aW9uKCksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHpvb206IHNjb3BlLlpPT01fREVUQUlMXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5tYXAuc2V0Q2VudGVyKHNjb3BlLm1hcE9wdGlvbnMuY2VudGVyKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5tYXAuc2V0Wm9vbShzY29wZS5tYXBPcHRpb25zLnpvb20pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJy5jYXRhbG9nLXdyYXBwZXInKS5yZW1vdmVDbGFzcygnY2F0YWxvZy13cmFwcGVyLS1mdWxsJywge2R1cmF0aW9uOiA1MDB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBBbmNob3JTbW9vdGhTY3JvbGwuc2Nyb2xsVG8obWEuaWQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyMnICsgbWEuaWQpLmFkZENsYXNzKCdjYXRhbG9nLXdyYXBwZXItLWZ1bGwnLCB7ZHVyYXRpb246IDUwMH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJrZXIuc2V0TWFwKHNjb3BlLm1hcCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJrZXJzLnB1c2gobWFya2VyKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubWFya2Vycy5ldmVudHMucHVzaChtYXJrZXIpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcblxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFNldCB2ZW51ZSBtYXJrZXJzIGZyb20gZ2l2ZW4gYXJyYXkgb2YgVmVudWVzXG4gICAgICAgICAgICAgKiBAcGFyYW0ge1ZlbnVlW119IHZlbnVlc1xuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBzZXRWZW51ZU1hcmtlcnM6IGZ1bmN0aW9uKHZlbnVlcykge1xuICAgICAgICAgICAgICAgIHZhciBtYXJrZXJzID0gW107XG4gICAgICAgICAgICAgICAgdmFyIHNjb3BlID0gdGhpcztcbiAgICAgICAgICAgICAgICBpZih2ZW51ZXMpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2codmVudWVzKTtcbiAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB2ZW51ZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBtYXJrZXIgPSB2ZW51ZXNbaV0uZ2V0TWFya2VyKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJrZXIuYWRkTGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG1hID0gdGhpcztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZihzY29wZS5tYXBPcHRpb25zLnpvb20gIT09IHNjb3BlLlpPT01fREVUQUlMKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUucHJldlN0YXRlID0gc2NvcGUubWFwT3B0aW9ucztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUubWFwT3B0aW9ucyA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2VudGVyOiBtYS5nZXRQb3NpdGlvbigpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB6b29tOiBzY29wZS5aT09NX0RFVEFJTFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUubWFwLnNldENlbnRlcihzY29wZS5tYXBPcHRpb25zLmNlbnRlcik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUubWFwLnNldFpvb20oc2NvcGUubWFwT3B0aW9ucy56b29tKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCcuY2F0YWxvZy13cmFwcGVyJykucmVtb3ZlQ2xhc3MoJ2NhdGFsb2ctd3JhcHBlci0tZnVsbCcsIHtkdXJhdGlvbjogNTAwfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgQW5jaG9yU21vb3RoU2Nyb2xsLnNjcm9sbFRvKG1hLmlkKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjJyArIG1hLmlkKS5hZGRDbGFzcygnY2F0YWxvZy13cmFwcGVyLS1mdWxsJywge2R1cmF0aW9uOiA1MDB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgbWFya2VyLnNldE1hcChzY29wZS5tYXApO1xuICAgICAgICAgICAgICAgICAgICAgICAgbWFya2Vycy5wdXNoKG1hcmtlcik7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdGhpcy5tYXJrZXJzLnZlbnVlcyA9IG1hcmtlcnM7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBhZGRWZW51ZU1hcmtlcnM6IGZ1bmN0aW9uKHZlbnVlcykge1xuICAgICAgICAgICAgICAgIHZhciBtYXJrZXJzID0gW107XG4gICAgICAgICAgICAgICAgdmFyIHNjb3BlID0gdGhpcztcbiAgICAgICAgICAgICAgICBpZih2ZW51ZXMpIHtcbiAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB2ZW51ZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBtYXJrZXIgPSB2ZW51ZXNbaV0uZ2V0TWFya2VyKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBtYXJrZXIuYWRkTGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIG1hID0gdGhpcztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZihzY29wZS5tYXBPcHRpb25zLnpvb20gIT09IHNjb3BlLlpPT01fREVUQUlMKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUucHJldlN0YXRlID0gc2NvcGUubWFwT3B0aW9ucztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUubWFwT3B0aW9ucyA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2VudGVyOiBtYS5nZXRQb3NpdGlvbigpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB6b29tOiBzY29wZS5aT09NX0RFVEFJTFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUubWFwLnNldENlbnRlcihzY29wZS5tYXBPcHRpb25zLmNlbnRlcik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUubWFwLnNldFpvb20oc2NvcGUubWFwT3B0aW9ucy56b29tKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCcuY2F0YWxvZy13cmFwcGVyJykucmVtb3ZlQ2xhc3MoJ2NhdGFsb2ctd3JhcHBlci0tZnVsbCcsIHtkdXJhdGlvbjogNTAwfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgQW5jaG9yU21vb3RoU2Nyb2xsLnNjcm9sbFRvKG1hLmlkKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjJyArIG1hLmlkKS5hZGRDbGFzcygnY2F0YWxvZy13cmFwcGVyLS1mdWxsJywge2R1cmF0aW9uOiA1MDB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgbWFya2VyLnNldE1hcChzY29wZS5tYXApO1xuICAgICAgICAgICAgICAgICAgICAgICAgbWFya2Vycy5wdXNoKG1hcmtlcik7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1hcmtlcnMudmVudWVzLnB1c2gobWFya2VyKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogU2V0IGRlYWxzIG1hcmtlcnMgZnJvbSBnaXZlbiBhcnJheSBvZiBkZWFsc1xuICAgICAgICAgICAgICpcbiAgICAgICAgICAgICAqIFRPRE86IGltcGxlbWVudCBhZnRlciBpbXBsZW1lbnRpbmcgZGVhbHNcbiAgICAgICAgICAgICAqIEBwYXJhbSBtYXJrZXJzXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHNldERlYWxzTWFya2VyczogZnVuY3Rpb24obWFya2Vycykge1xuICAgICAgICAgICAgICAgIHRoaXMubWFya2Vycy5kZWFscyA9IG1hcmtlcnM7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIEhpZGUgYWxsIG1hcmtlcnMgZnJvbSB0aGUgbWFwIGFuZCBkZWxldGUgdGhlbSBmcm9tIHRoaXMgcmVxdWVzdFxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBkZWxldGVBbGxNYXJrZXJzOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICB2YXIgc2NvcGUgPSB0aGlzO1xuICAgICAgICAgICAgICAgIHZhciBldmVudHMgPSB0aGlzLm1hcmtlcnMuZXZlbnRzO1xuICAgICAgICAgICAgICAgIHZhciB2ZW51ZXMgPSB0aGlzLm1hcmtlcnMudmVudWVzO1xuICAgICAgICAgICAgICAgIHZhciBkZWFscyA9IHRoaXMubWFya2Vycy5kZWFscztcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGV0ZU1hcmtlcnMoZXZlbnRzKTtcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGV0ZU1hcmtlcnModmVudWVzKTtcbiAgICAgICAgICAgICAgICB0aGlzLmRlbGV0ZU1hcmtlcnMoZGVhbHMpO1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0QWxsTWFya2Vyc0VtcHR5KCk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFNldCBhbGwgbWFya2VycyBpbiB0aGlzIG9iamVjdCBhcyBhbiBlbXB0eSBhcnJheVxuICAgICAgICAgICAgICogSXQgY2xlYXJpbmcgYWxsIGluZm8gYWJvdXQgbWFya2VycyBpbiB0aGlzIG9iamVjdFxuICAgICAgICAgICAgICogYnV0IG5vdCBhY3R1YWxseSByZW1vdmVzIHRoZW0gZnJvbSB0aGUgbWFwXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHNldEFsbE1hcmtlcnNFbXB0eTogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgIHRoaXMubWFya2VycyA9IHtcbiAgICAgICAgICAgICAgICAgIGV2ZW50czogW10sXG4gICAgICAgICAgICAgICAgICB2ZW51ZXM6IFtdLFxuICAgICAgICAgICAgICAgICAgZGVhbHM6IFtdXG4gICAgICAgICAgICAgIH07XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIERlbGV0ZSBhbGwgbWFya2VycyBmcm9tIHRoZSBtYXAgYnkgc2V0dGluZ1xuICAgICAgICAgICAgICogbWFwIHJlZmVyZW5jZSBpbiBNYXJrZXIgb2JqZWN0cyB0MCBudWxsXG4gICAgICAgICAgICAgKiBAcGFyYW0gbWFya2Vyc1xuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBkZWxldGVNYXJrZXJzOiBmdW5jdGlvbihtYXJrZXJzKSB7XG4gICAgICAgICAgICAgICAgaWYobWFya2VycyAmJiBtYXJrZXJzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICAgICAgZm9yKHZhciBpID0gMDsgaSA8IG1hcmtlcnMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBtYXJrZXIgPSBtYXJrZXJzW2ldO1xuICAgICAgICAgICAgICAgICAgICAgICAgbWFya2VyLnNldE1hcChudWxsKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlbGV0ZSBtYXJrZXJzW2ldO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBDcmVhdGUgdXNlciBtYXJrZXIsIGF0dGFjaCBpdCB0byBtYXBcbiAgICAgICAgICAgICAqIGFuZCBzYXZlIGluIGN1cnJlbnQgTWFwU2VydmljZSBpbnN0YW5jZVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBzaG93VXNlck1hcmtlcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgdmFyIHBvc2l0aW9uID0gdGhpcy5yZXF1ZXN0LmdldFVzZXJQb3NpdGlvbigpO1xuICAgICAgICAgICAgICAgIGlmKHR5cGVvZiBwb3NpdGlvbi5sYXRpdHVkZSAhPT0gXCJ1ZGVmaW5lZFwiICYmIHBvc2l0aW9uLmxhdGl0dWRlKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciB1c2VyTWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7XG4gICAgICAgICAgICAgICAgICAgICAgICBpY29uOiBuZXcgZ29vZ2xlLm1hcHMuTWFya2VySW1hZ2UoJy9pbWcvZ3BzbG9jLnBuZycsIG51bGwsIG51bGwsIG51bGwsIG5ldyBnb29nbGUubWFwcy5TaXplKDE2LCAxNikpLFxuICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IHtsYXQ6IHBvc2l0aW9uLmxhdGl0dWRlLCBsbmc6IHBvc2l0aW9uLmxvbmdpdHVkZX0sXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogXCJ1c2VyUG9zXCJcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIHVzZXJNYXJrZXIuc2V0TWFwKHRoaXMubWFwKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyTWFya2VyICA9IHVzZXJNYXJrZXI7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBIaWRlcyB1c2VyIG1hcmtlciBvbiB0aGUgbWFwIGFuZCByZW1vdmUgaXQgZnJvbSBNYXBTZXJ2aWNlXG4gICAgICAgICAgICAgKiBpbnN0YW5jZVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBoaWRlVXNlck1hcmtlcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgdGhpcy51c2VyTWFya2VyLnNldE1hcChudWxsKTtcbiAgICAgICAgICAgICAgICB0aGlzLnVzZXJNYXJrZXIgPSBudWxsO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKlxuICAgICAgICAgICAgICogSW5pdGlhbGl6ZSBtYXAuIFNldHMgY2VudGVyIGFzIGEgY3VycmVudCB1c2VyIHBvc2l0aW9uLCB3ZSBnb3QgZnJvbSByZXF1ZXN0XG4gICAgICAgICAgICAgKlxuICAgICAgICAgICAgICogQHBhcmFtIHtWZW5WYXN0UmVxdWVzdH0gcmVxdWVzdFxuICAgICAgICAgICAgICogQHBhcmFtIHtzdHJpbmd9IGVsZW1lbnRJZFxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBpbml0aWFsaXplOiBmdW5jdGlvbihyZXF1ZXN0LCBlbGVtZW50SWQpIHtcblxuICAgICAgICAgICAgICAgIHZhciBzY29wZSA9IHRoaXM7XG4gICAgICAgICAgICAgICAgcmV0dXJuICRxLndoZW4ocmVxdWVzdC51cGRhdGVDb29yZGluYXRlcygpKS50aGVuKGZ1bmN0aW9uIChjb29yZGluYXRlcykge1xuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbiA9IHJlcXVlc3QuZ2V0UG9zaXRpb24oKTtcbiAgICAgICAgICAgICAgICAgICAgaWYodHlwZW9mIHBvc2l0aW9uID09PSBcInVuZGVmaW5lZFwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgcG9zaXRpb24gPSByZXF1ZXN0Lm1vY2tQb3NpdGlvbigpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGlmKHR5cGVvZiBjb29yZGluYXRlcyAhPT0gXCJ1bmRlZmluZWRcIikge1xuICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb24gPSBjb29yZGluYXRlcztcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBpZihyZXF1ZXN0LndoZXJlICE9PSBudWxsICYmIHR5cGVvZiByZXF1ZXN0LndoZXJlLmxhdGl0dWRlICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJSZXVlc3Qgd2hlcmUgaXMgbm90IG51bGxcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbiA9IHJlcXVlc3Qud2hlcmU7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgdmFyIGVsZW1lbnQ7XG4gICAgICAgICAgICAgICAgICAgIGlmKGVsZW1lbnRJZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZWxlbWVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGVsZW1lbnRJZCk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBlbGVtZW50ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ21hcC1jYW52YXMnKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB2YXIgbWFwT3B0aW9ucyA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNlbnRlcjogbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhwb3NpdGlvbi5sYXRpdHVkZSwgcG9zaXRpb24ubG9uZ2l0dWRlKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHpvb206IHBvc2l0aW9uLnpvb20gPyBwb3NpdGlvbi56b29tIDogc2NvcGUuWk9PTV9JTklULFxuICAgICAgICAgICAgICAgICAgICAgICAgem9vbUNvbnRyb2w6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICBzY2FsZUNvbnRyb2w6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNhYmxlRGVmYXVsdFVJOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGVzOiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogXCJwb2lcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgeyB2aXNpYmlsaXR5OiBcIm9mZlwiIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgc2NvcGUubWFwID0gbmV3IGdvb2dsZS5tYXBzLk1hcChlbGVtZW50LCBtYXBPcHRpb25zKTtcbiAgICAgICAgICAgICAgICAgICAgc2NvcGUubWFwT3B0aW9ucyA9IG1hcE9wdGlvbnM7XG4gICAgICAgICAgICAgICAgICAgIHNjb3BlLnJlcXVlc3QgPSByZXF1ZXN0O1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gc2NvcGU7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIENoZWNrIGlmIGN1cnJlbnQgcmVxdWVzdCBpcyBub3QgZXF1ZWxzIHByZXZpb3VzIG9uZVxuICAgICAgICAgICAgICogU28gd2UgY2FuIHVwZGF0ZSBtYXJrZXJzXG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgcmVxdWVzdFVwZGF0ZWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIHJldHVybiAhYW5ndWxhci5lcXVhbHModGhpcy5yZXF1ZXN0LCB0aGlzLnByZXZSZXF1ZXN0KTtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogR2V0IG1hcmtlcnMgdGhhdCBzaG91bGQgYmUgZGlzcGxheWVkIGFjY29yZGluZyB0byBjdXJyZW50IHN0YXRlXG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7Kn1cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgZ2V0Q3VycmVudFN0YXRlTWFya2VyczogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMubWFya2Vyc1t0aGlzLmN1cnJlbnRNYXJrZXJzS2V5XTtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogQ2VudGVyIG1hcCBvbiBvYmplY3QgZ2l2ZW5cbiAgICAgICAgICAgICAqXG4gICAgICAgICAgICAgKiBUT0RPOiBleHRlbmQgd2l0aCBkZWFsc1xuICAgICAgICAgICAgICpcbiAgICAgICAgICAgICAqIEBwYXJhbSB7RXZlbnR8VmVudWV9IG9ialxuICAgICAgICAgICAgICogQHJldHVybnMge251bGx9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGNlbnRlck9uT2JqZWN0OiBmdW5jdGlvbihvYmopIHtcbiAgICAgICAgICAgICAgICB2YXIgc2NvcGUgPSB0aGlzO1xuICAgICAgICAgICAgICAgIHZhciBjb29yZGluYXRlcyA9IG9iai5nZXRDb29yZGluYXRlcygpO1xuICAgICAgICAgICAgICAgIHRoaXMucHJldlN0YXRlID0gdGhpcy5tYXBPcHRpb25zO1xuICAgICAgICAgICAgICAgIHRoaXMubWFwT3B0aW9ucyA9IHtcbiAgICAgICAgICAgICAgICAgICAgY2VudGVyOiB7bGF0OiBjb29yZGluYXRlcy5sYXRpdHVkZSwgbG5nOiBjb29yZGluYXRlcy5sb25naXR1ZGV9LFxuICAgICAgICAgICAgICAgICAgICB6b29tOiBzY29wZS5aT09NX0RFVEFJTCxcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIHRoaXMubWFwLnNldENlbnRlcih0aGlzLm1hcE9wdGlvbnMuY2VudGVyKTtcbiAgICAgICAgICAgICAgICB0aGlzLm1hcC5zZXRab29tKHRoaXMubWFwT3B0aW9ucy56b29tKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5tYXA7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFJlc3RvcmUgbWFwIHRvIHByZXZpb3VzIHNhdmVkIHN0YXRlXG4gICAgICAgICAgICAgKlxuICAgICAgICAgICAgICogQHJldHVybnMge251bGx9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHJlc3RvcmVNYXA6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIHZhciB0bXA7XG4gICAgICAgICAgICAgICAgYW5ndWxhci5jb3B5KHRoaXMubWFwT3B0aW9ucywgdG1wKTtcbiAgICAgICAgICAgICAgICB0aGlzLm1hcE9wdGlvbnMgPSB0aGlzLnByZXZTdGF0ZTtcbiAgICAgICAgICAgICAgICB0aGlzLnByZXZTdGF0ZSA9IHRtcDtcbiAgICAgICAgICAgICAgICB0aGlzLm1hcC5zZXRDZW50ZXIodGhpcy5tYXBPcHRpb25zLmNlbnRlcik7XG4gICAgICAgICAgICAgICAgdGhpcy5tYXAuc2V0Wm9vbSh0aGlzLm1hcE9wdGlvbnMuem9vbSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMubWFwO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgIH07XG5cbiAgICB9KTtcblxufSkoKTsiLCIoZnVuY3Rpb24oKSB7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG5cbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLnNlcnZpY2VzJykuZmFjdG9yeSgnQW5jaG9yU21vb3RoU2Nyb2xsJywgZnVuY3Rpb24oKXtcblxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgc2Nyb2xsVG86IGZ1bmN0aW9uIChlSUQpIHtcbiAgICAgICAgICAgICAgICAvLyBUaGlzIHNjcm9sbGluZyBmdW5jdGlvblxuICAgICAgICAgICAgICAgIC8vIGlzIGZyb20gaHR0cDovL3d3dy5pdG5ld2IuY29tL3R1dG9yaWFsL0NyZWF0aW5nLXRoZS1TbW9vdGgtU2Nyb2xsLUVmZmVjdC13aXRoLUphdmFTY3JpcHRcbiAgICAgICAgICAgICAgICB2YXIgc3RhcnRZID0gY3VycmVudFlQb3NpdGlvbigpO1xuICAgICAgICAgICAgICAgIHZhciBzdG9wWSA9IGVsbVlQb3NpdGlvbihlSUQpO1xuICAgICAgICAgICAgICAgIHZhciBkaXN0YW5jZSA9IHN0b3BZID4gc3RhcnRZID8gc3RvcFkgLSBzdGFydFkgOiBzdGFydFkgLSBzdG9wWTtcbiAgICAgICAgICAgICAgICBpZiAoZGlzdGFuY2UgPCAxMDApIHtcbiAgICAgICAgICAgICAgICAgICAgc2Nyb2xsVG8oMCwgc3RvcFkpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHZhciBzcGVlZCA9IE1hdGgucm91bmQoZGlzdGFuY2UgLyAxMDApO1xuICAgICAgICAgICAgICAgIGlmIChzcGVlZCA+PSAyMCkgc3BlZWQgPSAyMDtcbiAgICAgICAgICAgICAgICB2YXIgc3RlcCA9IE1hdGgucm91bmQoZGlzdGFuY2UgLyAyNSk7XG4gICAgICAgICAgICAgICAgdmFyIGxlYXBZID0gc3RvcFkgPiBzdGFydFkgPyBzdGFydFkgKyBzdGVwIDogc3RhcnRZIC0gc3RlcDtcbiAgICAgICAgICAgICAgICB2YXIgdGltZXIgPSAwO1xuICAgICAgICAgICAgICAgIGlmIChzdG9wWSA+IHN0YXJ0WSkge1xuICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBpID0gc3RhcnRZOyBpIDwgc3RvcFk7IGkgKz0gc3RlcCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChcIndpbmRvdy5zY3JvbGxUbygwLCBcIiArIGxlYXBZICsgXCIpXCIsIHRpbWVyICogc3BlZWQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgbGVhcFkgKz0gc3RlcDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChsZWFwWSA+IHN0b3BZKSBsZWFwWSA9IHN0b3BZO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGltZXIrKztcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSBzdGFydFk7IGkgPiBzdG9wWTsgaSAtPSBzdGVwKSB7XG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoXCJ3aW5kb3cuc2Nyb2xsVG8oMCwgXCIgKyBsZWFwWSArIFwiKVwiLCB0aW1lciAqIHNwZWVkKTtcbiAgICAgICAgICAgICAgICAgICAgbGVhcFkgLT0gc3RlcDtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGxlYXBZIDwgc3RvcFkpIGxlYXBZID0gc3RvcFk7XG4gICAgICAgICAgICAgICAgICAgIHRpbWVyKys7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24gY3VycmVudFlQb3NpdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gRmlyZWZveCwgQ2hyb21lLCBPcGVyYSwgU2FmYXJpXG4gICAgICAgICAgICAgICAgICAgIGlmIChzZWxmLnBhZ2VZT2Zmc2V0KSByZXR1cm4gc2VsZi5wYWdlWU9mZnNldDtcbiAgICAgICAgICAgICAgICAgICAgLy8gSW50ZXJuZXQgRXhwbG9yZXIgNiAtIHN0YW5kYXJkcyBtb2RlXG4gICAgICAgICAgICAgICAgICAgIGlmIChkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQgJiYgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcClcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wO1xuICAgICAgICAgICAgICAgICAgICAvLyBJbnRlcm5ldCBFeHBsb3JlciA2LCA3IGFuZCA4XG4gICAgICAgICAgICAgICAgICAgIGlmIChkb2N1bWVudC5ib2R5LnNjcm9sbFRvcCkgcmV0dXJuIGRvY3VtZW50LmJvZHkuc2Nyb2xsVG9wO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gMDtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBmdW5jdGlvbiBlbG1ZUG9zaXRpb24oZUlEKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBlbG0gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChlSUQpID8gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoZUlEKSA6ICQoJ1tuZy1pZD1cIicrIGVJRCArJ1wiXScpO1xuICAgICAgICAgICAgICAgICAgICB2YXIgeSA9IGVsbS5vZmZzZXRUb3A7XG4gICAgICAgICAgICAgICAgICAgIHZhciBub2RlID0gZWxtO1xuICAgICAgICAgICAgICAgICAgICB3aGlsZSAobm9kZS5vZmZzZXRQYXJlbnQgJiYgbm9kZS5vZmZzZXRQYXJlbnQgIT0gZG9jdW1lbnQuYm9keSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgbm9kZSA9IG5vZGUub2Zmc2V0UGFyZW50O1xuICAgICAgICAgICAgICAgICAgICAgICAgeSArPSBub2RlLm9mZnNldFRvcDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4geSAtIDgwO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgIH0pO1xufSkoKTsiLCIoZnVuY3Rpb24oKXtcbiAgICBcInVzZSBzdHJpY3RcIjtcblxuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuc2VydmljZXMnKS5mYWN0b3J5KCdVc2VyTW9kZWwnLCBmdW5jdGlvbigkc3RhdGUsICRodHRwLCBWZW5WYXN0UmVxdWVzdCl7XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqXG4gICAgICAgICAqIEBwYXJhbSB1c2VyRGF0YVxuICAgICAgICAgKiBAY29uc3RydWN0b3JcbiAgICAgICAgICovXG4gICAgICAgIGZ1bmN0aW9uIFVzZXIodXNlckRhdGEpIHtcbiAgICAgICAgICAgIGlmKHVzZXJEYXRhKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkRGF0YSh1c2VyRGF0YSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKlxuICAgICAgICAgKiBAdHlwZSB7e1xuICAgICAgICAgKlxuICAgICAgICAgKiBHRU5ERVJfV09NQU46IG51bWJlcixcbiAgICAgICAgICogR0VOREVSX01BTjogbnVtYmVyLFxuICAgICAgICAgKiBUWVBFX0FUVEVOREVFOiBudW1iZXIsXG4gICAgICAgICAqIFRZUEVfT1JHQU5JWkVSOiBudW1iZXIsXG4gICAgICAgICAqIGxvZ2luOiBzdHJpbmd8bnVsbCxcbiAgICAgICAgICogZW1haWw6IHN0cmluZ3xudWxsLFxuICAgICAgICAgKiBmaXJzdF9uYW1lOiBzdHJpbmd8bnVsbCxcbiAgICAgICAgICogbGFzdF9uYW1lOiBzdHJpbmd8bnVsbCxcbiAgICAgICAgICogZ2VuZGVyOiBpbnR8bnVsbCxcbiAgICAgICAgICogaGlkZV9waG9uZTogbnVtYmVyLFxuICAgICAgICAgKiBwaG9uZTogc3RyaW5nfG51bGwsXG4gICAgICAgICAqIGNyZWF0ZWRfYXQ6IHN0cmluZ3xudWxsLFxuICAgICAgICAgKiB1cGRhdGVkX2F0OiBzdHJpbmd8bnVsbCxcbiAgICAgICAgICogdHlwZTogbnVtYmVyLFxuICAgICAgICAgKiBsYXN0X2xhdDogbnVtYmVyfG51bGwsXG4gICAgICAgICAqIGxhc3RfbG5nOiBudW1iZXJ8bnVsbCxcbiAgICAgICAgICogZmJfaWQ6IHN0cmluZ3xudWxsLFxuICAgICAgICAgKiBnb29nbGVfaWQ6IHN0cmluZ3xudWxsLFxuICAgICAgICAgKiBmYl9waWN0dXJlOiBzdHJpbmd8bnVsbCxcbiAgICAgICAgICogZ29vZ2xlX3BpY3R1cmU6IHN0cmluZ3xudWxsLFxuICAgICAgICAgKiBmYl9hY2Nlc3NfdG9rZW46IHN0cmluZ3xudWxsLFxuICAgICAgICAgKiBvbGRfaWQ6IHN0cmluZ3xudWxsLFxuICAgICAgICAgKiBfaWQ6IHN0cmluZ3xudWxsLFxuICAgICAgICAgKiBsb2FkRGF0YTogVXNlci5sb2FkRGF0YSxcbiAgICAgICAgICogbWFrZUxvZ2luOiBVc2VyLm1ha2VMb2dpbixcbiAgICAgICAgICogaXNPcmdhbml6ZXI6IFVzZXIuaXNPcmdhbml6ZXIsXG4gICAgICAgICAqIHVwZGF0ZUxvZ2luOiBVc2VyLnVwZGF0ZUxvZ2luLFxuICAgICAgICAgKiBzZXRHZW5kZXJGcm9tRGF0YTogVXNlci5zZXRHZW5kZXJGcm9tRGF0YSxcbiAgICAgICAgICogc2V0VHlwZUZyb21EYXRhOiBVc2VyLnNldFR5cGVGcm9tRGF0YSxcbiAgICAgICAgICogcHJlcGFyZUZvclVwZGF0ZVJlcXVlc3Q6IFVzZXIucHJlcGFyZUZvclVwZGF0ZVJlcXVlc3RcbiAgICAgICAgICpcbiAgICAgICAgICogfX1cbiAgICAgICAgICovXG4gICAgICAgIFVzZXIucHJvdG90eXBlID0ge1xuXG4gICAgICAgICAgICBHRU5ERVJfV09NQU46IDAsXG4gICAgICAgICAgICBHRU5ERVJfTUFOOiAxLFxuXG4gICAgICAgICAgICBUWVBFX0FUVEVOREVFOiAwLFxuICAgICAgICAgICAgVFlQRV9PUkdBTklaRVI6IDEsXG5cbiAgICAgICAgICAgIGxvZ2luOiBudWxsLFxuICAgICAgICAgICAgZW1haWw6IG51bGwsXG4gICAgICAgICAgICBmaXJzdF9uYW1lOiBudWxsLFxuICAgICAgICAgICAgbGFzdF9uYW1lOiBudWxsLFxuICAgICAgICAgICAgZ2VuZGVyOiBudWxsLFxuICAgICAgICAgICAgaGlkZV9waG9uZTogMCxcbiAgICAgICAgICAgIHBob25lOiBudWxsLFxuICAgICAgICAgICAgY3JlYXRlZF9hdDogbnVsbCxcbiAgICAgICAgICAgIHVwZGF0ZWRfYXQ6IG51bGwsXG4gICAgICAgICAgICB0eXBlOiB0aGlzLlRZUEVfQVRURU5ERUUsXG4gICAgICAgICAgICBsYXN0X2xhdDogbnVsbCxcbiAgICAgICAgICAgIGxhc3RfbG5nOiBudWxsLFxuICAgICAgICAgICAgZmJfaWQ6IG51bGwsXG4gICAgICAgICAgICBnb29nbGVfaWQ6IG51bGwsXG4gICAgICAgICAgICBmYl9waWN0dXJlOiBudWxsLFxuICAgICAgICAgICAgZ29vZ2xlX3BpY3R1cmU6IG51bGwsXG4gICAgICAgICAgICBmYl9hY2Nlc3NfdG9rZW46IG51bGwsXG4gICAgICAgICAgICBvbGRfaWQ6IG51bGwsXG4gICAgICAgICAgICBfaWQ6IG51bGwsXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGluaXRpYWxpemVkOiBmYWxzZSxcblxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIENyZWF0ZSBvYmplY3QgZnJvbSBnaXZlbiBkYXRhXG4gICAgICAgICAgICAgKiBAcGFyYW0gdXNlckRhdGFcbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgbG9hZERhdGE6IGZ1bmN0aW9uICh1c2VyRGF0YSkge1xuICAgICAgICAgICAgICAgIGFuZ3VsYXIuZXh0ZW5kKHRoaXMsIHVzZXJEYXRhKTtcbiAgICAgICAgICAgICAgICB0aGlzLm1ha2VMb2dpbih1c2VyRGF0YSk7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRHZW5kZXJGcm9tRGF0YSh1c2VyRGF0YSk7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXRUeXBlRnJvbURhdGEodXNlckRhdGEpO1xuICAgICAgICAgICAgICAgIHRoaXMuaW5pdGlhbGl6ZWQgPSB0cnVlO1xuXG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBmaW5kOiBmdW5jdGlvbiAoaWQpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gJGh0dHAuZ2V0KCd1c2VyLycgKyBpZCkudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgICAgICBpZihyZXNwb25zZS5zdWNjZXNzID09PSB0cnVlICYmIHR5cGVvZiByZXNwb25zZS51c2VyICE9PSBcInVuZGVmaW5lZFwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gbmV3IFVzZXIocmVzcG9uc2UudXNlcik7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIE1ha2VzIGxvZ2luIGlmIGl0J3Mgbm90IHNldFxuICAgICAgICAgICAgICpcbiAgICAgICAgICAgICAqIEBwYXJhbSB1c2VyRGF0YVxuICAgICAgICAgICAgICogQHJldHVybnMgeyp9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIG1ha2VMb2dpbjogZnVuY3Rpb24gKHVzZXJEYXRhKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB1c2VyRGF0YS5lbWFpbDtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogQ2FuIHRoaXMgdXNlciBjcmVhdGUgdmVudWVzLCBldmVudHMgb3IgZGVhbHNcbiAgICAgICAgICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBpc09yZ2FuaXplcjogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnR5cGUgPT09IHRoaXMuVFlQRV9PUkdBTklaRVI7XG4gICAgICAgICAgICB9LFxuXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogVXBkYXRlcyBsb2dpbiBvbiBzZXJ2ZXJcbiAgICAgICAgICAgICAqXG4gICAgICAgICAgICAgKiBAcGFyYW0ge3N0cmluZ30gbG9naW5cbiAgICAgICAgICAgICAqIEByZXR1cm5zIHsqfVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICB1cGRhdGVMb2dpbjogZnVuY3Rpb24obG9naW4pIHtcbiAgICAgICAgICAgICAgICB2YXIgcmVxT2JqID0gdGhpcy5wcmVwYXJlRm9yVXBkYXRlUmVxdWVzdCh0aGlzKTtcbiAgICAgICAgICAgICAgICByZXFPYmoubG9naW4gPSBsb2dpbjtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImJlZm9yZSBDYWxsaW5nIHVwZGF0ZTogXCIsIHJlcU9iaik7XG4gICAgICAgICAgICAgICAgcmV0dXJuICRodHRwLnBvc3QoJ3VzZXIvdXBkYXRlJywgcmVxT2JqKS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZS5zdWNjZXNzO1xuICAgICAgICAgICAgICAgIH0sIGZ1bmN0aW9uKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXJyb3Igb2NjdXJlZFwiLCBlcnJvcik7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgXG4gICAgICAgICAgICB1cGRhdGVMYXRMbmc6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIHZhciByZXFPYmogPSB0aGlzLnByZXBhcmVGb3JVcGRhdGVSZXF1ZXN0KHRoaXMpO1xuICAgICAgICAgICAgICAgIHJldHVybiAkcS53aGVuKFZlblZhc3RSZXF1ZXN0LnVwZGF0ZUNvb3JkaW5hdGVzKCkpLnRoZW4oZnVuY3Rpb24gKHBvc2l0aW9uKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlcU9iai5sYXN0X2xhdCA9IHBvc2l0aW9uLmxhdGl0dWRlO1xuICAgICAgICAgICAgICAgICAgICByZXFPYmoubGFzdF9sbmcgPSBwb3NpdGlvbi5sb25naXR1ZGU7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAkaHR0cC5wb3N0KCd1c2VyL3VwZGF0ZScsIHtwYXJhbXM6IHJlcU9ian0pLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZS5zdWNjZXNzO1xuICAgICAgICAgICAgICAgICAgICB9LCBmdW5jdGlvbihlcnJvcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJFcnJvciBvY2N1cmVkXCIsIGVycm9yKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFVwZGF0ZXMgdXNlciBvbiBzZXJ2ZXJcbiAgICAgICAgICAgICAqIFxuICAgICAgICAgICAgICogQHJldHVybnMgeyp9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHVwZGF0ZVVzZXI6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICB2YXIgcmVxT2JqID0gdGhpcy5wcmVwYXJlRm9yVXBkYXRlUmVxdWVzdCh0aGlzKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gJGh0dHAucG9zdCgndXNlci91cGRhdGUnLCB7cGFyYW1zOiByZXFPYmp9KS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZS5zdWNjZXNzO1xuICAgICAgICAgICAgICAgIH0sIGZ1bmN0aW9uKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXJyb3Igb2NjdXJlZFwiLCBlcnJvcik7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9LFxuXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogU2V0IGdlbmRlciBmcm9tIGRhdGEgcHJlc2VudGVkXG4gICAgICAgICAgICAgKlxuICAgICAgICAgICAgICogQHBhcmFtIHVzZXJEYXRhXG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7VXNlcn1cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgc2V0R2VuZGVyRnJvbURhdGE6IGZ1bmN0aW9uICh1c2VyRGF0YSkge1xuICAgICAgICAgICAgICAgIHRoaXMuZ2VuZGVyID0gcGFyc2VJbnQodXNlckRhdGEuZ2VuZGVyKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcztcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogU2V0IHR5cGUgZnJvbSBkYXRhIHByZXNlbnRlZFxuICAgICAgICAgICAgICogQHBhcmFtIHVzZXJEYXRhXG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7VXNlcn1cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgc2V0VHlwZUZyb21EYXRhOiBmdW5jdGlvbiAodXNlckRhdGEpIHtcbiAgICAgICAgICAgICAgICB2YXIgc2NvcGUgPSB0aGlzO1xuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgdXNlckRhdGEudHlwZSA9PT0gXCJ1bmRlZmluZWRcIiB8fCB1c2VyRGF0YS50eXBlID09PSBcIlwiKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMudHlwZSA9IHRoaXMuVFlQRV9BVFRFTkRFRTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB1c2VyRGF0YS50eXBlID0gcGFyc2VJbnQodXNlckRhdGEudHlwZSk7XG4gICAgICAgICAgICAgICAgICAgIHN3aXRjaCAodXNlckRhdGEudHlwZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSBzY29wZS5UWVBFX0FUVEVOREVFOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLnR5cGUgPSBzY29wZS5UWVBFX0FUVEVOREVFO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSBzY29wZS5UWVBFX09SR0FOSVpFUjpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS50eXBlID0gc2NvcGUuVFlQRV9PUkdBTklaRVI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLnR5cGUgPSBzY29wZS5UWVBFX0FUVEVOREVFO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaXNDb25uZWN0ZWRXaXRoRmFjZWJvb2s6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmZiX2lkLmxlbmd0aCA+IDI7IFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgaXNDb25uZWN0ZWRXaXRoR29vZ2xlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZ29vZ2xlX2lkLmxlbmd0aCA+IDI7IFxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBQcmVwYXJlIG9qZWN0IGZvciBwZXJmb3JtaW5nIFBPU1QgdXBkYXRlIHJlcXVlc3RcbiAgICAgICAgICAgICAqXG4gICAgICAgICAgICAgKiBAcGFyYW0gdXNlck9iamVjdFxuICAgICAgICAgICAgICogQHJldHVybnMgeyp9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHByZXBhcmVGb3JVcGRhdGVSZXF1ZXN0OiBmdW5jdGlvbih1c2VyT2JqZWN0KSB7XG4gICAgICAgICAgICAgICAgdmFyIHJlcU9iaiA9IHt9O1xuICAgICAgICAgICAgICAgIGFuZ3VsYXIuY29weShyZXFPYmosIHVzZXJPYmplY3QpO1xuICAgICAgICAgICAgICAgIGRlbGV0ZSByZXFPYmoucGFzc3dvcmQ7XG4gICAgICAgICAgICAgICAgZGVsZXRlIHJlcU9iai5jcmVhdGVkX2F0O1xuICAgICAgICAgICAgICAgIGRlbGV0ZSByZXFPYmouX2lkO1xuICAgICAgICAgICAgICAgIGFuZ3VsYXIuY29weShyZXFPYmouaWQsIHVzZXJPYmplY3QuX2lkKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gcmVxT2JqO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgfTtcblxuICAgICAgICByZXR1cm4gVXNlcjtcbiAgICB9KTtcblxufSkoKTsiLCIoZnVuY3Rpb24oKXtcbiAgICBcInVzZSBzdHJpY3RcIjtcblxuICAgIFxuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuc2VydmljZXMnKS5mYWN0b3J5KCdVc2VyU2VydmljZScsIGZ1bmN0aW9uKCRodHRwLCBVc2VyTW9kZWwsICRsb2NhbFN0b3JhZ2UsICRxLCAkdGltZW91dCl7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICB1c2VyOiBudWxsLFxuICAgICAgICAgICAgZm9ybUZpZWxkczoge30sXG4gICAgICAgICAgICBsb2dpbkVycm9yOiBudWxsLFxuICAgICAgICAgICAgaG9tZVRpbWVvdXQ6IG51bGwsXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogZ2V0IGN1cnJlbnQgdXNlcihubyBtYXR0ZXJzIGhvdylcbiAgICAgICAgICAgICAqIEByZXR1cm5zIHsqfVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBnZXRDdXJyZW50VXNlcjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgaWYodGhpcy51c2VyICE9PSBudWxsICYmIHRoaXMudXNlci5fX2lkKXtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMudXNlcjtcbiAgICAgICAgICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMucmVxdWVzdEN1cnJlbnRVc2VyKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBSZXF1ZXN0IGN1cnJlbnQgdXNlciBmcm9tIHNlcnZlclxuICAgICAgICAgICAgICpcbiAgICAgICAgICAgICAqIEByZXR1cm5zIHsqfVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICByZXF1ZXN0Q3VycmVudFVzZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIHZhciBzY29wZSA9IHRoaXM7XG5cbiAgICAgICAgICAgICAgICBpZih0aGlzLmhvbWVUaW1lb3V0KXsgLy9pZiB0aGVyZSBpcyBhbHJlYWR5IGEgdGltZW91dCBpbiBwcm9jZXNzIGNhbmNlbCBpdFxuICAgICAgICAgICAgICAgICAgICAkdGltZW91dC5jYW5jZWwoc2NvcGUuaG9tZVRpbWVvdXQpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmhvbWVUaW1lb3V0ID0gJHRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICBpZighJGxvY2FsU3RvcmFnZS50b2tlbilcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJGh0dHAuZ2V0KCd1c2VyL2hvbWUnKS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKXtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKHR5cGVvZiByZXNwb25zZSA9PT0gXCJ1bmRlZmluZWRcIiB8fCB0eXBlb2YgcmVzcG9uc2UuZGF0YSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS51c2VyID0gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gc2NvcGUudXNlcjtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKHJlc3BvbnNlID09IG51bGwgfHwgcmVzcG9uc2UubGVuZ3RoID09PSAwIHx8IGFuZ3VsYXIuZXF1YWxzKHt9LCByZXNwb25zZSkpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLnVzZXIgPSBudWxsO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS51c2VyID0gbmV3IFVzZXJNb2RlbChyZXNwb25zZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5ob21lVGltZW91dCA9IG51bGw7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gc2NvcGUudXNlcjtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSwgNTAwMCk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIERvIGxvZ2luIGJ5IHVzZXIgY3JlZGVudGlhbHNcbiAgICAgICAgICAgICAqIEBwYXJhbSB7T2JqZWN0fSBmb3JtRGF0YVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBsb2dpbjogZnVuY3Rpb24oZm9ybURhdGEpIHtcbiAgICAgICAgICAgICAgICB2YXIgc2NvcGUgPSB0aGlzO1xuICAgICAgICAgICAgICAgIHJldHVybiAkaHR0cC5wb3N0KCdhcGkvYXV0aGVudGljYXRlJywgZm9ybURhdGEpLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2Upe1xuICAgICAgICAgICAgICAgICAgICByZXNwb25zZSA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgIGlmKHJlc3BvbnNlLnRva2VuICE9PSBudWxsICYmIHR5cGVvZiByZXNwb25zZS50b2tlbiAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICRsb2NhbFN0b3JhZ2UudG9rZW4gPSByZXNwb25zZS50b2tlbjtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKHJlc3BvbnNlLnRva2VuKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICRodHRwLmdldCgndXNlci9ob21lJykudGhlbihmdW5jdGlvbih1c2VyUmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdXNlclJlc3BvbnNlID0gdXNlclJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKHVzZXJSZXNwb25zZSAhPT0gbnVsbCAmJiB0eXBlb2YgdXNlclJlc3BvbnNlICE9PSBcInVuZGVmaW5lZFwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS51c2VyID0gbmV3IFVzZXJNb2RlbCh1c2VyUmVzcG9uc2UpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBzY29wZS51c2VyO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzY29wZS51c2VyID0gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gc2NvcGUudXNlcjtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0sIGZ1bmN0aW9uKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgIHNjb3BlLmxvZ2luRXJyb3IgPSBlcnJvcjtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogU2V0IGVtYWlsIGFuZCBwYXNzd29yZCB0eXBlZCBpbiBsb2dpbiBmb3JtXG4gICAgICAgICAgICAgKlxuICAgICAgICAgICAgICogQHBhcmFtIGVtYWlsXG4gICAgICAgICAgICAgKiBAcGFyYW0gcGFzc3dvcmRcbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgc2V0TG9naW5Gb3JtRmllbGRzOiBmdW5jdGlvbihlbWFpbCwgcGFzc3dvcmQpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmZvcm1GaWVsZHMuZW1haWwgPSBlbWFpbDtcbiAgICAgICAgICAgICAgICB0aGlzLmZvcm1GaWVsZHMucGFzc3dvcmQgPSBwYXNzd29yZDtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogTG9naW4gdG8gZmFjZWJvb2tcbiAgICAgICAgICAgICAqXG4gICAgICAgICAgICAgKiBAcGFyYW0gRmFjZWJvb2tcbiAgICAgICAgICAgICAqIEByZXR1cm5zIHtVc2VyfVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBsb2dpbkZhY2Vib29rOiBmdW5jdGlvbihGYWNlYm9vaykge1xuICAgICAgICAgICAgICAgIHZhciBzY29wZSA9IHRoaXM7XG4gICAgICAgICAgICAgICAgc2NvcGUudXNlciA9IHt9O1xuICAgICAgICAgICAgICAgIHJldHVybiBGYWNlYm9vay5nZXRMb2dpblN0YXR1cyhmdW5jdGlvbihmYlN0YXR1c1Jlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmKGZiU3RhdHVzUmVzcG9uc2Uuc3RhdHVzID09PSAnY29ubmVjdGVkJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZiU3RhdHVzUmVzcG9uc2UuYXV0aFJlc3BvbnNlLmFjY2Vzc1Rva2VuO1xuICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gRmFjZWJvb2subG9naW4oZnVuY3Rpb24obG9naW5SZXNwb25zZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBsb2dpblJlc3BvbnNlLmF1dGhSZXNwb25zZS5hY2Nlc3NUb2tlbjtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFVwZGF0ZXMgdXNlciBtb2RlbCB3aXRoIGZhY2Vib29rIGNyZWRlbnRpYWxzXG4gICAgICAgICAgICAgKlxuICAgICAgICAgICAgICogQHBhcmFtIHVzZXJcbiAgICAgICAgICAgICAqIEByZXR1cm5zIHsqfVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBiYWNrZW5kRmFjZWJvb2tMb2dpbjogZnVuY3Rpb24odXNlcikge1xuICAgICAgICAgICAgICAgIHZhciBzY29wZSA9IHRoaXM7XG4gICAgICAgICAgICAgICAgcmV0dXJuICRodHRwLnBvc3QoJ3VzZXIvZmFjZWJvb2snLCB1c2VyKS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKXtcbiAgICAgICAgICAgICAgICAgICAgaWYocmVzcG9uc2Uuc3VjY2VzcyA9PT0gdHJ1ZSl7XG4gICAgICAgICAgICAgICAgICAgICAgICBzY29wZS51c2VyID0gcmVzcG9uc2UudXNlcjtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLnVzZXIuaW5pdGlhbGl6ZWQgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHNjb3BlLnVzZXI7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogU2V0IGRhdGEgd2UgZ290IGZyb20gZmFjZWJvb2tcbiAgICAgICAgICAgICAqIHRvIGN1cnJlbnQgdXNlclxuICAgICAgICAgICAgICpcbiAgICAgICAgICAgICAqIEBwYXJhbSBmYlVzZXJEZXRhaWxzXG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7dXBkYXRlV2l0aEZiRGF0YX1cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgdXBkYXRlV2l0aEZiRGF0YTogZnVuY3Rpb24oZmJVc2VyRGV0YWlscykge1xuICAgICAgICAgICAgICAgIHZhciBmb3JtRW1haWwgPSB0eXBlb2YgdGhpcy5mb3JtRmllbGRzLmVtYWlsO1xuICAgICAgICAgICAgICAgIGlmKHR5cGVvZiBmb3JtRW1haWwgIT09IFwidW5kZWZpbmVkXCIgJiYgZm9ybUVtYWlsLmxlbmd0aCA+IDYpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyLmVtYWlsID0gdGhpcy5mb3JtRmllbGRzLmVtYWlsO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMudXNlci5lbWFpbCA9IGZiVXNlckRldGFpbHMuZW1haWw7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHRoaXMudXNlci5mYl9pZCA9IGZiVXNlckRldGFpbHMuaWQ7XG4gICAgICAgICAgICAgICAgdGhpcy51c2VyLmdlbmRlciA9IGZiVXNlckRldGFpbHMuZ2VuZGVyLnRvTG93ZXJDYXNlKCkgPT09IFwibWFsZVwiID8gMSA6IDA7XG4gICAgICAgICAgICAgICAgdGhpcy51c2VyLmZpcnN0X25hbWUgPSBmYlVzZXJEZXRhaWxzLmZpcnN0X25hbWU7XG4gICAgICAgICAgICAgICAgdGhpcy51c2VyLmxhc3RfbmFtZSA9IGZiVXNlckRldGFpbHMubGFzdF9uYW1lO1xuICAgICAgICAgICAgICAgIHRoaXMudXNlci5mYl9waWN0dXJlID0gZmJVc2VyRGV0YWlscy5waWN0dXJlLmRhdGEudXJsO1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBQZXJmb3JtIGxvZ2dpbmcgb3V0XG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7Kn1cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgbG9nb3V0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICB2YXIgc2NvcGUgPSB0aGlzO1xuICAgICAgICAgICAgICAgIHJldHVybiAkaHR0cC5nZXQoJ3VzZXIvbG9nb3V0JykudGhlbihmdW5jdGlvbihyZXNwb25zZSl7XG4gICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlID0gcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgaWYocmVzcG9uc2Uuc3VjY2VzcyA9PT0gdHJ1ZSl7XG4gICAgICAgICAgICAgICAgICAgICAgICBzY29wZS51c2VyID0gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgICAgICRsb2NhbFN0b3JhZ2UudG9rZW4gPSBudWxsO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHNjb3BlLnVzZXI7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogUGVyZm9ybSByZWdpc3RlciBhY3Rpb25cbiAgICAgICAgICAgICAqIEBwYXJhbSB1c2VyRGF0YVxuICAgICAgICAgICAgICogQHJldHVybnMgeyp9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHJlZ2lzdGVyOiBmdW5jdGlvbih1c2VyRGF0YSl7XG4gICAgICAgICAgICAgICAgdmFyIHNjb3BlID0gdGhpcztcbiAgICAgICAgICAgICAgICByZXR1cm4gJGh0dHAucG9zdCgndXNlci9yZWdpc3RlcicsIHVzZXJEYXRhKS50aGVuKGZ1bmN0aW9uKHJlc3BvbnNlKXtcbiAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkdvdCByZXNwb25zZTogXCIsIHJlc3BvbnNlKTtcbiAgICAgICAgICAgICAgICAgICAgaWYocmVzcG9uc2Uuc3VjY2VzcyA9PT0gdHJ1ZSl7XG4gICAgICAgICAgICAgICAgICAgICAgICBzY29wZS51c2VyID0gbmV3IFVzZXJNb2RlbChyZXNwb25zZS51c2VyKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBzY29wZS51c2VyO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgfSk7XG5cbn0pKCk7IiwiKGZ1bmN0aW9uKCl7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG5cblxuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuc2VydmljZXMnKS5mYWN0b3J5KCdWZW51ZScsIGZ1bmN0aW9uKCRodHRwLCBWZW5WYXN0UmVxdWVzdCwgRXZlbnQsIFVwbG9hZCl7XG5cbiAgICAgICAgZnVuY3Rpb24gVmVudWUodmVudWVEYXRhKSB7XG4gICAgICAgICAgICBpZih2ZW51ZURhdGEpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNldERhdGEodmVudWVEYXRhKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIFZlbnVlLnByb3RvdHlwZSA9IHtcblxuICAgICAgICAgICAgbmFtZTogbnVsbCxcbiAgICAgICAgICAgIGJpcnRoZGF5OiBudWxsLFxuICAgICAgICAgICAgY292ZXJfaW1hZ2U6IG51bGwsXG4gICAgICAgICAgICBkZXNjcmlwdGlvbjogbnVsbCxcbiAgICAgICAgICAgIGdlbmVyYWxfaW5mbzogbnVsbCxcbiAgICAgICAgICAgIHN0cmVldDogbnVsbCxcbiAgICAgICAgICAgIHppcDogbnVsbCxcbiAgICAgICAgICAgIHBob25lOiBudWxsLFxuICAgICAgICAgICAgd2Vic2l0ZSA6IG51bGwsXG4gICAgICAgICAgICBwdWJsaWNfdHJhbnNpdCA6IG51bGwsXG4gICAgICAgICAgICBwdWJsaXNoZWQgOiBmYWxzZSxcbiAgICAgICAgICAgIGxvY2F0aW9uIDogbnVsbCxcbiAgICAgICAgICAgIGNhdGVnb3JpZXMgOiBbXSxcbiAgICAgICAgICAgIHVwY29taW5nRXZlbnRzOiBbXSxcbiAgICAgICAgICAgIGN1cnJlbnREZWFsczogW10sXG4gICAgICAgICAgICB1cGNvbWluZ0RlYWxzOiBbXSxcbiAgICAgICAgICAgIF9pZCA6IG51bGwsXG4gICAgICAgICAgICBjbGFzc05hbWU6ICdWZW51ZScsXG5cblxuICAgICAgICAgICAgc2V0RGF0YTogZnVuY3Rpb24gKHZlbnVlRGF0YSkge1xuICAgICAgICAgICAgICAgIGFuZ3VsYXIuZXh0ZW5kKHRoaXMsIHZlbnVlRGF0YSk7XG4gICAgICAgICAgICAgICAgdmFyIHNjb3BlID0gdGhpcztcbiAgICAgICAgICAgICAgICBpZih0eXBlb2Ygc2NvcGUuZXZlbnRzICE9PSAndW5kZWZpbmVkJyAmJiBzY29wZS5ldmVudHMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgICAgICBmb3IodmFyIGkgPSAwOyBpIDwgc2NvcGUuZXZlbnRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5ldmVudHNbaV0gPSBuZXcgRXZlbnQoc2NvcGUuZXZlbnRzW2ldKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0aGlzLmljb24gPSB0aGlzLmdldEljb24oKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcztcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIGZpbmQ6IGZ1bmN0aW9uKGlkKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuICRodHRwLmdldCgndmVudWUvJyArIGlkKS50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgICAgICBpZihyZXNwb25zZSAhPT0gbnVsbClcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBuZXcgVmVudWUocmVzcG9uc2UpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgZ2V0SWNvbjogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGlmKHRoaXMuY2F0ZWdvcmllcy5sZW5ndGggPT09IDAgfHwgIXRoaXMuY2F0ZWdvcmllcyB8fCB0eXBlb2YgdGhpcy5jYXRlZ29yaWVzID09PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJy9pbWcvcXVlc3Rpb24ucG5nJztcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBpZih0eXBlb2YgdGhpcy5jYXRlZ29yaWVzWzBdICE9PSAndW5kZWZpbmVkJyAmJiAgdGhpcy5jYXRlZ29yaWVzWzBdICE9PSBudWxsICYmIHRoaXMuY2F0ZWdvcmllc1swXS5pbWFnZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFwiL2ltZy9jYWNoZS9vcmlnaW5hbC9jYXRlZ29yaWVzL1wiICsgdGhpcy5jYXRlZ29yaWVzWzBdLmltYWdlO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICcvaW1nL3F1ZXN0aW9uLnBuZyc7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgXG4gICAgICAgICAgICBnZXRQaWN0dXJlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBpZih0aGlzLmNvdmVyX2ltYWdlID09PSBudWxsIHx8IHR5cGVvZiB0aGlzLmNvdmVyX2ltYWdlID09PSAndW5kZWZpbmVkJyB8fCB0aGlzLmNvdmVyX2ltYWdlID09PSAnJyl7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAnaHR0cHM6Ly9wbGFjZWhvbGRpdC5pbWdpeC5uZXQvfnRleHQ/dHh0c2l6ZT0zMyZ0eHQ9JyArIHRoaXMubmFtZSArICcmdz0zNTAmaD0xNTAnO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmKHR5cGVvZiB0aGlzLmNvdmVyX2ltYWdlICE9PSAnc3RyaW5nJyl7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmNvdmVyX2ltYWdlXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmKCF0aGlzLmNvdmVyX2ltYWdlLnN0YXJ0c1dpdGgoJ2h0dHAnKSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJy9pbWcvY2FjaGUvb3JpZ2luYWwvdmVudWVzLycgKyB0aGlzLmNvdmVyX2ltYWdlO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5jb3Zlcl9pbWFnZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuXG5cbiAgICAgICAgICAgIHNhdmVPbkJhY2tlbmQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICBpZih0aGlzLmNvdmVyX2ltYWdlICE9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBVcGxvYWQudXBsb2FkKHt1cmw6ICd2ZW51ZS9jcmVhdGUnLCBkYXRhOiB0aGlzfSlcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uIChyZXNwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3A7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmV0dXJuICRodHRwLnBvc3QoJ3ZlbnVlL2NyZWF0ZScsIHRoaXMpLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIFxuICAgICAgICAgICAgZGVsZXRlRnJvbUJhY2tlbmQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gJGh0dHAuZ2V0KCd2ZW51ZS9kZWxldGUvJyt0aGlzLl9pZCkudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBnZXRDb29yZGluYXRlczogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgICAgIGxvbmdpdHVkZTogdGhpcy5sb2NhdGlvbi5jb29yZGluYXRlc1swXSxcbiAgICAgICAgICAgICAgICAgICAgbGF0aXR1ZGU6IHRoaXMubG9jYXRpb24uY29vcmRpbmF0ZXNbMV1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBpc1B1Ymxpc2hlZDogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMucHVibGlzaGVkO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgY3JlYXRlT3BlbkhvdXJzOiBmdW5jdGlvbihvcGVuSG91cnMpIHtcbiAgICAgICAgICAgICAgICBmb3IodmFyIGkgPSAwOyBpIDwgb3BlbkhvdXJzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBkYXlPYmplY3QgPSBvcGVuSG91cnNbaV07XG4gICAgICAgICAgICAgICAgICAgIGRheU9iamVjdC5kYXkgPSBwYXJzZUludChkYXlPYmplY3QuZGF5KTtcbiAgICAgICAgICAgICAgICAgICAgaWYodHlwZW9mIGRheU9iamVjdC5vcGVuID09PSAnb2JqZWN0Jykge1xuICAgICAgICAgICAgICAgICAgICAgICAgZGF5T2JqZWN0Lm9wZW4gPSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF5OiBkYXlPYmplY3QuZGF5LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpbWU6IG1vbWVudChkYXlPYmplY3Qub3Blbi50aW1lKS5mb3JtYXQoJ0hIbW0nKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgZGF5T2JqZWN0Lm9wZW4gPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBpZih0eXBlb2YgZGF5T2JqZWN0LmNsb3NlID09PSAnb2JqZWN0Jykge1xuICAgICAgICAgICAgICAgICAgICAgICAgZGF5T2JqZWN0LmNsb3NlID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRheTogZGF5T2JqZWN0LmRheSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aW1lOiBtb21lbnQoZGF5T2JqZWN0LmNsb3NlLnRpbWUpLmZvcm1hdCgnSEhtbScpXG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkYXlPYmplY3Qub3BlbiA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIG9wZW5Ib3Vyc1tpXSA9IGRheU9iamVjdDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmV0dXJuIG9wZW5Ib3VycztcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIGNyZWF0ZU9wZW5pbmdIb3Vyc1RlbXBsYXRlOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICB2YXIgaG91cnMgPSBbXTtcbiAgICAgICAgICAgICAgICB2YXIgc2NvcGUgPSB0aGlzO1xuICAgICAgICAgICAgICAgIGZvcih2YXIgaSA9IDA7IGkgPCA3OyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgaWYoaSA8IDEgfHwgaSA+IDUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBjbG9zZWQgPSBzY29wZS5nZXRDbG9zZWREYXlPYmplY3QoaSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBob3Vycy5wdXNoKGNsb3NlZCk7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgZGF5ID0gc2NvcGUuZ2V0T3BlbkhvdXJzRGF5T2JqZWN0KGksIFwiMDkwMFwiLCBcIjIwMDBcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICBob3Vycy5wdXNoKGRheSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmV0dXJuIGhvdXJzO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgZ2V0T3BlbkhvdXJzRGF5T2JqZWN0OiBmdW5jdGlvbihkYXksIG9wZW4sIGNsb3NlKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICAgICAgZGF5OiBkYXksXG4gICAgICAgICAgICAgICAgICAgIG9wZW46IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRheTogZGF5LFxuICAgICAgICAgICAgICAgICAgICAgICAgdGltZTogb3BlblxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBjbG9zZToge1xuICAgICAgICAgICAgICAgICAgICAgICAgZGF5OiBkYXksXG4gICAgICAgICAgICAgICAgICAgICAgICB0aW1lOiBjbG9zZVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIGdldENsb3NlZERheU9iamVjdDogZnVuY3Rpb24oZGF5KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICAgICAgZGF5OiBkYXlcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICBnZXRNYXJrZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIHZhciBzY29wZSA9IHRoaXM7XG4gICAgICAgICAgICAgICAgdmFyIGNvb3JkaW5hdGVzID0gc2NvcGUuZ2V0Q29vcmRpbmF0ZXMoKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7XG4gICAgICAgICAgICAgICAgICAgIGljb246IG5ldyBnb29nbGUubWFwcy5NYXJrZXJJbWFnZShzY29wZS5nZXRJY29uKCksIG51bGwsIG51bGwsIG51bGwsIG5ldyBnb29nbGUubWFwcy5TaXplKDMyLCAzMikpLFxuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjoge2xhdDogY29vcmRpbmF0ZXMubGF0aXR1ZGUsIGxuZzogY29vcmRpbmF0ZXMubG9uZ2l0dWRlfSxcbiAgICAgICAgICAgICAgICAgICAgaWQ6ICd2ZW51ZS0nICsgc2NvcGUuX2lkXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIFJldHVybnMgZGlzdGFuY2UgaW4gbWV0ZXIgYmV0d2VlbiBwb2ludCBzcGVjaWZpZWQgYW5kXG4gICAgICAgICAgICAgKiB0aGlzIFZlbnVlXG4gICAgICAgICAgICAgKlxuICAgICAgICAgICAgICogQHBhcmFtIHt7bGF0aXR1ZGU6IGZsb2F0LCBsb25naXR1ZGU6IGZsb2F0fX0gcG9pbnRcbiAgICAgICAgICAgICAqIEByZXR1cm5zIHtudW1iZXJ9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGNvbXB1dGVEaXN0YW5jZVRvOiBmdW5jdGlvbiAocG9pbnQpIHtcbiAgICAgICAgICAgICAgICB2YXIgdmVudWVDb29yZGluYXRlcyA9IHRoaXMuZ2V0Q29vcmRpbmF0ZXMoKTtcbiAgICAgICAgICAgICAgICB2YXIgcG9pbnRMYXRMbmcgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKHtsYXQ6IHBvaW50LmxhdGl0dWRlLCBsbmc6IHBvaW50LmxvbmdpdHVkZX0pO1xuICAgICAgICAgICAgICAgIHZhciBWZW51ZUxhdExuZyA9IG5ldyBnb29nbGUubWFwcy5MYXRMbmcoe2xhdDogdmVudWVDb29yZGluYXRlcy5sYXRpdHVkZSwgbG5nOiB2ZW51ZUNvb3JkaW5hdGVzLmxvbmdpdHVkZX0pO1xuXG4gICAgICAgICAgICAgICAgcmV0dXJuIHBhcnNlRmxvYXQoKGdvb2dsZS5tYXBzLmdlb21ldHJ5LnNwaGVyaWNhbC5jb21wdXRlRGlzdGFuY2VCZXR3ZWVuKHBvaW50TGF0TG5nLCBWZW51ZUxhdExuZykgLyAxMDAwKS50b0ZpeGVkKDIpKTtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogUmV0dXJuIHN0cmluZyBuYW1lIG9mIGRheSBvZiB3ZWVrIGJ5IGl0cyBpbnQgY29kZVxuICAgICAgICAgICAgICogQHBhcmFtIGNvZGVcbiAgICAgICAgICAgICAqIEByZXR1cm5zIHsqfVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBnZXREYXlPZldlZWtCeUNvZGU6IGZ1bmN0aW9uIChjb2RlKSB7XG4gICAgICAgICAgICAgICAgc3dpdGNoIChjb2RlKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMDpcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAnc3VuZGF5JztcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAxOlxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICdtb25kYXknO1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDI6XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gJ3R1ZXNkYXknO1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDM6XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gJ3dlZG5lc2RheSc7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgNDpcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAndGh1cnNkYXknO1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDU6XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gJ2ZyaWRheSc7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgNjpcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAnc2F0dXJkYXknO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICB9O1xuXG4gICAgICAgIHJldHVybiBWZW51ZTtcblxuICAgIH0pO1xuXG59KSgpOyIsIihmdW5jdGlvbigpe1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG5cbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLnNlcnZpY2VzJykuZmFjdG9yeSgnVmVudWVTdG9yYWdlJywgZnVuY3Rpb24oJHEsICRodHRwLCBWZW5WYXN0UmVxdWVzdCwgRXZlbnQsIEV2ZW50c1N0b3JhZ2UsIFZlbnVlKXtcblxuICAgICAgICAvKipcbiAgICAgICAgICogXG4gICAgICAgICAqIEBwYXJhbSB7QXJyYXl9IHZlbnVlc0FycmF5XG4gICAgICAgICAqIEBjb25zdHJ1Y3RvclxuICAgICAgICAgKi9cbiAgICAgICAgZnVuY3Rpb24gVmVudWVTdG9yYWdlKHZlbnVlc0FycmF5KSB7XG4gICAgICAgICAgICBpZih2ZW51ZXNBcnJheSkge1xuICAgICAgICAgICAgICAgIGZvcih2YXIgaSA9IDA7IGkgPCB2ZW51ZXNBcnJheS5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICBpZighKHZlbnVlc0FycmF5W2ldIGluc3RhbmNlb2YgVmVudWUpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2ZW51ZXNBcnJheVtpXSA9IG5ldyBWZW51ZSh2ZW51ZXNBcnJheVtpXSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkQXJyYXkodmVudWVzQXJyYXkpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFxuICAgICAgICAgKiBAdHlwZSB7e1xuICAgICAgICAgKiBcbiAgICAgICAgICogZGF0YTogQXJyYXksIFxuICAgICAgICAgKiBsb2FkaW5nOiBib29sZWFuLCBcbiAgICAgICAgICogbG9hZEFycmF5OiBWZW51ZVN0b3JhZ2UubG9hZEFycmF5LCBcbiAgICAgICAgICogbGlzdDogVmVudWVTdG9yYWdlLmxpc3QsIFxuICAgICAgICAgKiBjcmVhdGU6IFZlbnVlU3RvcmFnZS5jcmVhdGUsIFxuICAgICAgICAgKiBnZXREYXRhOiBWZW51ZVN0b3JhZ2UuZ2V0RGF0YVxuICAgICAgICAgKiBcbiAgICAgICAgICogfX1cbiAgICAgICAgICovXG4gICAgICAgIFZlbnVlU3RvcmFnZS5wcm90b3R5cGUgPSB7XG4gICAgICAgICAgICBkYXRhOiBbXSxcbiAgICAgICAgICAgIGxvYWRpbmc6IGZhbHNlLFxuXG4gICAgICAgICAgICAvKipcbiAgICAgICAgICAgICAqIExvYWQgYXJyYXkgZGF0YSB0byBzdG9yYWdlXG4gICAgICAgICAgICAgKiBcbiAgICAgICAgICAgICAqIEBwYXJhbSB2ZW51ZXNBcnJheVxuICAgICAgICAgICAgICogQHJldHVybnMge1ZlbnVlU3RvcmFnZX1cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgbG9hZEFycmF5OiBmdW5jdGlvbiAodmVudWVzQXJyYXkpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmRhdGEgPSB2ZW51ZXNBcnJheTtcbiAgICAgICAgIFxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKlxuICAgICAgICAgICAgICogQHBhcmFtIHtWZW5WYXN0UmVxdWVzdH0gcmVxdWVzdFxuICAgICAgICAgICAgICogQHJldHVybnMgeyp9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGxvYWQ6IGZ1bmN0aW9uIChyZXF1ZXN0KSB7XG4gICAgICAgICAgICAgICAgdmFyIHNjb3BlID0gdGhpcztcbiAgICAgICAgICAgICAgICBpZih0aGlzLmxvYWRpbmcgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgICAgIHNjb3BlLmxvYWRpbmcgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICBpZihyZXF1ZXN0Lmhhc01vcmVQYWdlcyA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBbXTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJGh0dHAuZ2V0KCd2ZW51ZScsIHtwYXJhbXM6IHJlcXVlc3QuZ2V0UmVxdWVzdE9iamVjdCgpfSkuc3VjY2VzcyhmdW5jdGlvbih2ZW51ZURhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvcih2YXIgaSA9IDA7IGkgPCB2ZW51ZURhdGEubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2ZW51ZURhdGFbaV0gPSBuZXcgVmVudWUodmVudWVEYXRhW2ldKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZighdmVudWVEYXRhW2ldLmQgfHwgdHlwZW9mIHZlbnVlRGF0YVtpXS5kID09PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2ZW51ZURhdGFbaV0uZCA9IHZlbnVlRGF0YVtpXS5jb21wdXRlRGlzdGFuY2VUbyhyZXF1ZXN0LmdldFVzZXJQb3NpdGlvbigpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmVudWVEYXRhW2ldLmQgPSBwYXJzZUZsb2F0KHZlbnVlRGF0YVtpXS5kKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2ZW51ZURhdGFbaV0uZGlzdGFuY2UgPSB2ZW51ZURhdGFbaV0uZCA/IHZlbnVlRGF0YVtpXS5kICsgJyBrbSBhd2F5JyA6ICcnO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWVzdC5yZXF1ZXN0UGVyZm9ybWVkKHZlbnVlRGF0YSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5sb2FkQXJyYXkodmVudWVEYXRhKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBzY29wZTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgZmluZDogZnVuY3Rpb24ocGFyYW1zKSB7XG4gICAgICAgICAgICAgICAgdmFyIHNjb3BlID0gdGhpcztcbiAgICAgICAgICAgICAgICBpZih0aGlzLmxvYWRpbmcgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgICAgIHNjb3BlLmxvYWRpbmcgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJGh0dHAuZ2V0KCd2ZW51ZS9maW5kJywge3BhcmFtczogcGFyYW1zfSkuc3VjY2VzcyhmdW5jdGlvbih2ZW51ZURhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjb3BlLmxvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB2ZW51ZURhdGEuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBDcmVhdGUgbmV3IHZlbnVlXG4gICAgICAgICAgICAgKiBAcGFyYW0gdmVudWVcbiAgICAgICAgICAgICAqIEByZXR1cm5zIHsqfVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBjcmVhdGU6IGZ1bmN0aW9uKHZlbnVlKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuICRodHRwLnBvc3QoJ3ZlbnVlL2NyZWF0ZScsIHtwYXJhbXM6IHZlbnVlfSkuc3VjY2VzcyhmdW5jdGlvbihyZXNwb25zZSl7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogUmV0dXJucyBhbGwgb2JqZWN0cyBpbiBzdG9yYWdlXG4gICAgICAgICAgICAgKiBcbiAgICAgICAgICAgICAqIEByZXR1cm5zIHtBcnJheX1cbiAgICAgICAgICAgICAqL1xuICAgICAgICAgICAgZ2V0RGF0YTogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmRhdGE7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgcmV0dXJuIFZlbnVlU3RvcmFnZTtcbiAgICB9KTtcblxufSkoKTsiLCIoZnVuY3Rpb24oKXtcbiAgICBcInVzZSBzdHJpY3RcIjtcblxuXG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5zZXJ2aWNlcycpLmZhY3RvcnkoJ1ZlblZhc3RSZXF1ZXN0JywgZnVuY3Rpb24oJHEpe1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBAcGFyYW0gcmVxdWVzdERhdGFcbiAgICAgICAgICogQGNvbnN0cnVjdG9yXG4gICAgICAgICAqL1xuICAgICAgICBmdW5jdGlvbiBWZW5WYXN0UmVxdWVzdChyZXF1ZXN0RGF0YSkge1xuICAgICAgICAgICAgaWYocmVxdWVzdERhdGEpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmxvYWREYXRhKHJlcXVlc3REYXRhKTtcbiAgICAgICAgICAgICAgICBpZighdGhpcy53aGVyZSkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZUNvb3JkaW5hdGVzKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gdXBkYXRlQ29vcmRpbmF0ZXMoKSB7XG4gICAgICAgICAgICB2YXIgX19zZWxmID0gdGhpcztcbiAgICAgICAgICAgIGlmKG5hdmlnYXRvci5nZW9sb2NhdGlvbikge1xuICAgICAgICAgICAgICAgIG5hdmlnYXRvci5nZW9sb2NhdGlvbi5nZXRDdXJyZW50UG9zaXRpb24oZnVuY3Rpb24ocG9zaXRpb24pIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF9fc2VsZi5zZXRQb3NpdGlvbihwb3NpdGlvbikuZ2V0UG9zaXRpb24oKTtcbiAgICAgICAgICAgICAgICB9LCBmdW5jdGlvbihlcnJvcikge1xuICAgICAgICAgICAgICAgICAgICBpZihlcnJvcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIF9fc2VsZi5tb2NrUG9zaXRpb24oKS5nZXRQb3NpdGlvbigpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAvKipcbiAgICAgICAgICpcbiAgICAgICAgICogQHR5cGUge3tcbiAgICAgICAgICogd2hlbjogc3RyaW5nfG51bGwsXG4gICAgICAgICAqIHdoZXJlOiBzdHJpbmd8bnVsbCxcbiAgICAgICAgICogY2F0ZWdvcnk6IHN0cmluZ3xudWxsLFxuICAgICAgICAgKiBsYXRpdHVkZTogZmxvYXR8bnVsbCxcbiAgICAgICAgICogbG9uZ2l0dWRlOiBmbG9hdHxudWxsLFxuICAgICAgICAgKiBwYWdlOiBudW1iZXIsXG4gICAgICAgICAqIHdlYjogc3RyaW5nLFxuICAgICAgICAgKiByYWRpdXM6IG51bWJlcixcbiAgICAgICAgICogaGFzTW9yZVBhZ2VzOiBib29sZWFuLFxuICAgICAgICAgKiBsb2FkRGF0YTogVmVuVmFzdFJlcXVlc3QubG9hZERhdGEsXG4gICAgICAgICAqIHVwZGF0ZUNvb3JkaW5hdGVzOiBWZW5WYXN0UmVxdWVzdC51cGRhdGVDb29yZGluYXRlcyxcbiAgICAgICAgICogcmVxdWVzdFBlcmZvcm1lZDogVmVuVmFzdFJlcXVlc3QucmVxdWVzdFBlcmZvcm1lZCxcbiAgICAgICAgICogbW9ja1Bvc2l0aW9uOiBWZW5WYXN0UmVxdWVzdC5tb2NrUG9zaXRpb24sXG4gICAgICAgICAqIHNldFBvc2l0aW9uOiBWZW5WYXN0UmVxdWVzdC5zZXRQb3NpdGlvbixcbiAgICAgICAgICogZ2V0UG9zaXRpb246IFZlblZhc3RSZXF1ZXN0LmdldFBvc2l0aW9uXG4gICAgICAgICAqIH19XG4gICAgICAgICAqL1xuICAgICAgICBWZW5WYXN0UmVxdWVzdC5wcm90b3R5cGUgPSB7XG5cbiAgICAgICAgICAgIFdIRU5fVE9EQVk6ICd0b2RheScsXG4gICAgICAgICAgICBXSEVOX1RPTU9SUk9XOiAndG9tb3Jyb3cnLFxuICAgICAgICAgICAgV0hFTl9XRUVLOiAnd2VlaycsXG4gICAgICAgICAgICBXSEVOX01PTlRIOiAnbW9udGgnLFxuXG4gICAgICAgICAgICB3aGVuOiBudWxsLFxuICAgICAgICAgICAgd2hlcmU6IG51bGwsXG4gICAgICAgICAgICBjYXRlZ29yeTogbnVsbCxcbiAgICAgICAgICAgIGxhdGl0dWRlOiBudWxsLFxuICAgICAgICAgICAgbG9uZ2l0dWRlOiBudWxsLFxuICAgICAgICAgICAgaG9zdHM6IGZhbHNlLFxuICAgICAgICAgICAgcGFnZTogMCxcbiAgICAgICAgICAgIHdlYjogJ3RydWUnLFxuICAgICAgICAgICAgcmFkaXVzOiAyNSxcbiAgICAgICAgICAgIHBlclBhZ2U6IDYwLFxuICAgICAgICAgICAgdGV4dDogbnVsbCxcbiAgICAgICAgICAgIGRhdGU6IG51bGwsXG5cbiAgICAgICAgICAgIGhhc01vcmVQYWdlczogdHJ1ZSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBMb2FkIGRhdGEgdG8gdGhpcyBvYmplY3RcbiAgICAgICAgICAgICAqIEBwYXJhbSBkYXRhXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGxvYWREYXRhOiBmdW5jdGlvbihkYXRhKSB7XG4gICAgICAgICAgICAgICAgYW5ndWxhci5leHRlbmQodGhpcywgZGF0YSk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgXG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHNldFdoZXJlOiBmdW5jdGlvbihsYXRpdHVkZSwgbG9uZ2l0dWRlLCBuYW1lKSB7XG4gICAgICAgICAgICAgICAgdGhpcy53aGVyZSA9IHtcbiAgICAgICAgICAgICAgICAgICAgbGF0aXR1ZGU6IGxhdGl0dWRlLFxuICAgICAgICAgICAgICAgICAgICBsb25naXR1ZGU6IGxvbmdpdHVkZSxcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogbmFtZVxuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgICAgICAgICB9LFxuXG5cblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBVcGRhdGVzIGNvb3JkaW5hdGVzIGFuZCByZXR1cm5zIHVwZGF0ZWQgcG9zaXRpb24gb2JqZWN0XG4gICAgICAgICAgICAgKlxuICAgICAgICAgICAgICogQHJldHVybiBQcm9taXNlXG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHVwZGF0ZUNvb3JkaW5hdGVzOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgdmFyIF9fc2VsZiA9IHRoaXM7XG4gICAgICAgICAgICAgICAgaWYobmF2aWdhdG9yLmdlb2xvY2F0aW9uKSB7XG4gICAgICAgICAgICAgICAgICAgIG5hdmlnYXRvci5nZW9sb2NhdGlvbi5nZXRDdXJyZW50UG9zaXRpb24oZnVuY3Rpb24ocG9zaXRpb24pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBfX3NlbGYuc2V0UG9zaXRpb24ocG9zaXRpb24pLmdldFBvc2l0aW9uKCk7XG4gICAgICAgICAgICAgICAgICAgIH0sIGZ1bmN0aW9uKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZihlcnJvcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBfX3NlbGYubW9ja1Bvc2l0aW9uKCkuZ2V0UG9zaXRpb24oKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgcmVxdWVzdFBlcmZvcm1lZDogZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICBpZihyZXNwb25zZS5sZW5ndGggPT09IDAgfHwgdHlwZW9mIHJlc3BvbnNlID09PSBcInVuZGVmaW5lZFwiIHx8IHJlc3BvbnNlLmxlbmd0aCA8IHRoaXMucGVyUGFnZSkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmhhc01vcmVQYWdlcyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHRoaXMucGFnZSsrO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKlxuICAgICAgICAgICAgICogQHJldHVybnMge1ZlblZhc3RSZXF1ZXN0fVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBtb2NrUG9zaXRpb246IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIHRoaXMubGF0aXR1ZGUgPSAxMy43MjUyNzY0O1xuICAgICAgICAgICAgICAgIHRoaXMubG9uZ2l0dWRlID0gMTAwLjU4NzY0NjtcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcztcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogQHBhcmFtIHBvc2l0aW9uXG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7VmVuVmFzdFJlcXVlc3R9XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHNldFBvc2l0aW9uOiBmdW5jdGlvbihwb3NpdGlvbikge1xuICAgICAgICAgICAgICAgIHRoaXMubGF0aXR1ZGUgPSBwb3NpdGlvbi5jb29yZHMubGF0aXR1ZGU7XG4gICAgICAgICAgICAgICAgdGhpcy5sb25naXR1ZGUgPSBwb3NpdGlvbi5jb29yZHMubG9uZ2l0dWRlO1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBHZXQgcG9zaXRpb24gb2YgcmVxdWVzdFxuICAgICAgICAgICAgICogQHJldHVybnMge3tsYXRpdHVkZTogZmxvYXQsIGxvbmdpdHVkZTogZmxvYXR9fVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBnZXRQb3NpdGlvbjogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgdmFyIHNjb3BlID0gdGhpcztcbiAgICAgICAgICAgICAgICBpZihzY29wZS53aGVyZSAhPT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgbGF0aXR1ZGU6IHNjb3BlLndoZXJlLmxhdGl0dWRlLFxuICAgICAgICAgICAgICAgICAgICAgICAgbG9uZ2l0dWRlOiBzY29wZS53aGVyZS5sb25naXR1ZGUsXG4gICAgICAgICAgICAgICAgICAgICAgICB6b29tOiAxMFxuICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBsYXRpdHVkZTogc2NvcGUubGF0aXR1ZGUsXG4gICAgICAgICAgICAgICAgICAgICAgICBsb25naXR1ZGU6IHNjb3BlLmxvbmdpdHVkZVxuICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIC8qKlxuICAgICAgICAgICAgICogR2V0IHBvc2l0aW9uIG9mIHVzZXJcbiAgICAgICAgICAgICAqXG4gICAgICAgICAgICAgKiBAcmV0dXJucyB7e2xhdGl0dWRlOiAqLCBsb25naXR1ZGU6ICp9fVxuICAgICAgICAgICAgICovXG4gICAgICAgICAgICBnZXRVc2VyUG9zaXRpb246IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIHZhciBzY29wZSA9IHRoaXM7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICAgICAgbGF0aXR1ZGU6IHNjb3BlLmxhdGl0dWRlLFxuICAgICAgICAgICAgICAgICAgICBsb25naXR1ZGU6IHNjb3BlLmxvbmdpdHVkZVxuICAgICAgICAgICAgICAgIH07ICBcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIGlzVmFsaWRXaGVuVmFsdWU6IGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgc3dpdGNoKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgdGhpcy5XSEVOX1dFRUs6XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSB0aGlzLldIRU5fVE9EQVk6XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSB0aGlzLldIRU5fVE9NT1JST1c6XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSB0aGlzLldIRU5fTU9OVEg6XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgZ2V0UmVxdWVzdE9iamVjdDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHZhciBvYmogPSB7fTtcbiAgICAgICAgICAgICAgICB2YXIgc2NvcGUgPSB0aGlzO1xuICAgICAgICAgICAgICAgIGlmKHRoaXMud2hlbiAhPT0gbnVsbCAmJiB0aGlzLmlzVmFsaWRXaGVuVmFsdWUodGhpcy53aGVuKSkge1xuICAgICAgICAgICAgICAgICAgICBvYmoud2hlbiA9IHRoaXMud2hlbjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYodGhpcy5vcmdhbml6ZXIgPT09IG51bGwgfHwgdHlwZW9mIHRoaXMub3JnYW5pemVyID09PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgICAgICB2YXIgcG9zID0gdGhpcy5nZXRQb3NpdGlvbigpO1xuICAgICAgICAgICAgICAgICAgICBpZihwb3MubGF0aXR1ZGUgPT09IG51bGwgJiYgcG9zLmxvbmdpdHVkZSA9PT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2NvcGUubW9ja1Bvc2l0aW9uKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBvYmoubGF0aXR1ZGUgPSB0aGlzLmxhdGl0dWRlO1xuICAgICAgICAgICAgICAgICAgICAgICAgb2JqLmxvbmdpdHVkZSA9IHRoaXMubG9uZ2l0dWRlO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgb2JqLmxhdGl0dWRlID0gcG9zLmxhdGl0dWRlO1xuICAgICAgICAgICAgICAgICAgICAgICAgb2JqLmxvbmdpdHVkZSA9IHBvcy5sb25naXR1ZGU7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICRxLndoZW4odGhpcy51cGRhdGVDb29yZGluYXRlcygpKS50aGVuKGZ1bmN0aW9uKGNvb3JkaW5hdGVzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzY29wZS5tb2NrUG9zaXRpb24oKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmKHRoaXMuaG9zdHMgPT09IHRydWUpIHtcbiAgICAgICAgICAgICAgICAgICAgb2JqLmhvc3RzID0gJ3RydWUnO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZih0aGlzLmNhdGVnb3J5ICE9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgICAgIG9iai5jYXRlZ29yeSA9IHRoaXMuY2F0ZWdvcnk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmKHRoaXMud2hlcmUgJiYgdHlwZW9mIHRoaXMud2hlcmUubGF0aXR1ZGUgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgIG9iai5sYXRpdHVkZSA9IHRoaXMud2hlcmUubGF0aXR1ZGU7XG4gICAgICAgICAgICAgICAgICAgIG9iai5sb25naXR1ZGUgPSB0aGlzLndoZXJlLmxvbmdpdHVkZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYodGhpcy53aGVyZSA9PT0gbnVsbCAmJiB0aGlzLmxhdGl0dWRlICE9PSBudWxsICYmIHRoaXMubG9uZ2l0dWRlICE9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgICAgIG9iai5sYXRpdHVkZSA9IHRoaXMubGF0aXR1ZGU7XG4gICAgICAgICAgICAgICAgICAgIG9iai5sb25naXR1ZGUgPSB0aGlzLmxvbmdpdHVkZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYodGhpcy50eXBlICE9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgICAgIG9iai50eXBlID0gdGhpcy50eXBlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZih0aGlzLnRleHQgIT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICAgICAgb2JqLnRleHQgPSB0aGlzLnRleHQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGlmKHRoaXMuZGF0ZSAhPT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICBvYmouZGF0ZSA9IHRoaXMuZGF0ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYodGhpcy5vcmdhbml6ZXIgIT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICAgICAgb2JqLm9yZ2FuaXplciA9IHRoaXMub3JnYW5pemVyO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBvYmoud2ViID0gdGhpcy53ZWI7XG4gICAgICAgICAgICAgICAgb2JqLnJhZGl1cyA9IHRoaXMucmFkaXVzO1xuICAgICAgICAgICAgICAgIG9iai5wYWdlID0gdGhpcy5wYWdlO1xuICAgICAgICAgICAgICAgIG9iai50aW1lem9uZSA9IGpzdHouZGV0ZXJtaW5lKCkubmFtZSgpO1xuICAgICAgICAgICAgICAgIHJldHVybiBvYmo7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfTtcblxuICAgICAgICByZXR1cm4gVmVuVmFzdFJlcXVlc3Q7XG4gICAgfSk7XG5cbn0pKCk7IiwiLyoqXG4gKiBDcmVhdGVkIGJ5IG9lbSBvbiAzLzE5LzE2LlxuICovXG4oZnVuY3Rpb24oKXtcbiAgICBcInVzZSBzdHJpY3RcIjtcblxuICAgIC8qKlxuICAgICAqIFRoaXMgY29udHJvbGxlciBpcyBkZWFsaW5nIHdpdGggZXZlbnRzIGxpc3Qgdmlld1xuICAgICAqL1xuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuY29udHJvbGxlcnMnKS5jb250cm9sbGVyKCdDYWxlbmRhckN0cmwnLCBmdW5jdGlvbigkcSwgJHNjb3BlLCAkdGltZW91dCwgJHN0YXRlLCBIdG1sSGVscGVyLCBFdmVudHNTdG9yYWdlLCBDaGFuZ2VzTm90aWZpZXIpe1xuXG4gICAgICAgIHZhciBzdGF0ZSA9ICRzdGF0ZS5jdXJyZW50Lm5hbWU7XG4gICAgICAgIHN3aXRjaCAoc3RhdGUpIHtcbiAgICAgICAgICAgIGNhc2UgJ2hvbWUuZXZlbnRzJzpcbiAgICAgICAgICAgICAgICAkKFwiI2NhbGVuZGFyLWZpbHRlclwiKS5pb25DYWxlbmRhcih7XG4gICAgICAgICAgICAgICAgICAgIGxhbmc6IFwiZW5cIixcbiAgICAgICAgICAgICAgICAgICAgc3VuZGF5Rmlyc3Q6IGZhbHNlLFxuICAgICAgICAgICAgICAgICAgICB5ZWFyczogXCIyMDE1LTIwMThcIixcbiAgICAgICAgICAgICAgICAgICAgZm9ybWF0OiBcIllZWVktTU0tRERcIixcbiAgICAgICAgICAgICAgICAgICAgb25DbGljazogZnVuY3Rpb24oZGF0ZSl7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKCcjd2hlbl9zZWFyY2gnKS5jaGlsZHJlbignc3BhbicpLnRleHQoZGF0ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAkdGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuZXZlbnRSZXF1ZXN0LndoZW4gPSBkYXRlO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuZXZlbnRSZXF1ZXN0LmRhdGUgPSBkYXRlO1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnNlYXJjaCgpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlICdob21lLnZlbnVlcyc6XG4gICAgICAgICAgICAgICAgJChcIiNjYWxlbmRhci1maWx0ZXJcIikuYWRkQ2xhc3MoJ2hpZGRlbicpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG5cbiAgICAgICAgJHNjb3BlLnNlYXJjaCA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgSHRtbEhlbHBlci5zaG93TG9hZGVyKCk7XG4gICAgICAgICAgICAkc2NvcGUuZXZlbnRSZXF1ZXN0LnBhZ2UgPSAwO1xuICAgICAgICAgICAgJHNjb3BlLmV2ZW50cyA9IFtdO1xuICAgICAgICAgICAgJHNjb3BlLmV2ZW50UmVxdWVzdC50eXBlID0gJ2V2ZW50cyc7XG4gICAgICAgICAgICAkc2NvcGUuZXZlbnRSZXF1ZXN0Lmhhc01vcmVQYWdlcyA9IHRydWU7XG4gICAgICAgICAgICB2YXIgZXZlbnRzID0gbmV3IEV2ZW50c1N0b3JhZ2UoKTtcbiAgICAgICAgICAgIHJldHVybiAkcS53aGVuKGV2ZW50cy5sb2FkKCRzY29wZS5ldmVudFJlcXVlc3QpKS50aGVuKGZ1bmN0aW9uKGV2ZW50U3RvcmFnZSkge1xuICAgICAgICAgICAgICAgICRzY29wZS5ldmVudHMgPSBldmVudFN0b3JhZ2UuZGF0YTtcbiAgICAgICAgICAgICAgICBDaGFuZ2VzTm90aWZpZXIudmFsdWUgPSAkc2NvcGUuZXZlbnRzO1xuICAgICAgICAgICAgICAgIENoYW5nZXNOb3RpZmllci5ub3RpZnkoQ2hhbmdlc05vdGlmaWVyLlNVQkpfRVZFTlRTX0xPQURFRCwgJHNjb3BlLmV2ZW50cyk7XG4gICAgICAgICAgICAgICAgSHRtbEhlbHBlci5oaWRlTG9hZGVyKCk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIC8vICRzY29wZS5ldmVudFJlcXVlc3QuZ2V0UmVxdWVzdE9iamVjdCgpXG4gICAgICAgIH07XG4gICAgfSk7XG59KSgpOyIsIi8qKlxuICogQ3JlYXRlZCBieSBvZW0gb24gMy8xOS8xNi5cbiAqL1xuKGZ1bmN0aW9uKCl7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG5cblxuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuY29udHJvbGxlcnMnKS5jb250cm9sbGVyKCdEZWFsc0N0cmwnLCBmdW5jdGlvbigkc2NvcGUsICRzdGF0ZSwgVXNlclNlcnZpY2UsIEV2ZW50U2VydmljZSwgTWFwU2VydmljZSl7XG4gICAgICAgIC8vJHNjb3BlLnNob3dpbmdEZXNjcmlwdGlvbiA9IG51bGw7XG4gICAgICAgIC8vJHNjb3BlLnNob3dpbmdFdmVudCA9IG51bGw7XG4gICAgICAgICRzY29wZS5pbml0ID0gZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICRzY29wZS5ldmVudHMgPSB7fTtcbiAgICAgICAgICAgIFVzZXJTZXJ2aWNlLmdldEN1cnJlbnRVc2VyKGZ1bmN0aW9uKHVzZXIpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUudXNlciA9IHVzZXI7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIEV2ZW50U2VydmljZS5saXN0KGZ1bmN0aW9uKGV2ZW50cyl7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmV2ZW50cy5vYmplY3RzID0gZXZlbnRzO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBNYXBTZXJ2aWNlLnJlcXVlc3RNYXAoZnVuY3Rpb24obWFwKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLm1hcCA9IG1hcDtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8vJHNjb3BlLnNob3dJbmZvID0gZnVuY3Rpb24oZWxlbWVudCwgZXZlbnQpe1xuICAgICAgICAvLyAgICBpZigkc2NvcGUuc2hvd2luZ0Rlc2NyaXB0aW9uID09PSBudWxsICYmICRzY29wZS5zaG93aW5nRXZlbnQgPT09IG51bGwpe1xuICAgICAgICAvLyAgICAgICAgJCgnLmV2ZW50LWRlc2NyaXB0aW9uJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAvLyAgICAgICAgJCgnLmV2ZW50JykuaGlkZSgpO1xuICAgICAgICAvLyAgICAgICAgdmFyIGV2ZW50QmxvY2sgPSAkKGVsZW1lbnQudGFyZ2V0KS5jbG9zZXN0KCcuZXZlbnQnKS5lcSgwKTtcbiAgICAgICAgLy8gICAgICAgIGV2ZW50QmxvY2suc2hvdygpO1xuICAgICAgICAvLyAgICAgICAgJCgnI3ZlbnVlLScrZXZlbnQuaWQpLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgLy8gICAgICAgICRzY29wZS5zaG93aW5nRGVzY3JpcHRpb24gPSB0cnVlO1xuICAgICAgICAvLyAgICAgICAgJHNjb3BlLnNob3dpbmdFdmVudCA9IHRydWU7XG4gICAgICAgIC8vICAgIH0gZWxzZSB7XG4gICAgICAgIC8vICAgICAgICAkKCcuZXZlbnQtZGVzY3JpcHRpb24nKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgIC8vICAgICAgICAkKCcuZXZlbnQnKS5zaG93KCdzbG93Jyk7XG4gICAgICAgIC8vICAgICAgICAkc2NvcGUuc2hvd2luZ0Rlc2NyaXB0aW9uID0gbnVsbDtcbiAgICAgICAgLy8gICAgICAgICRzY29wZS5zaG93aW5nRXZlbnQgPSBudWxsO1xuICAgICAgICAvLyAgICB9XG4gICAgICAgIC8vXG4gICAgICAgIC8vfTtcbiAgICAgICAgJHNjb3BlLmluaXQoKTtcbiAgICB9KTtcbn0pKCk7IiwiLyoqXG4gKiBDcmVhdGVkIGJ5IG9lbSBvbiAzLzE5LzE2LlxuICovXG4oZnVuY3Rpb24oKXtcbiAgICBcInVzZSBzdHJpY3RcIjtcblxuXG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5jb250cm9sbGVycycpLmNvbnRyb2xsZXIoJ0V2ZW50Q3JlYXRlQ3RybCcsIGZ1bmN0aW9uKCRzY29wZSwgJHN0YXRlLCBVc2VyU2VydmljZSwgRXZlbnRTZXJ2aWNlLCBWZW51ZVNlcnZpY2UsIE1hcFNlcnZpY2UsIENhdGVnb3J5U2VydmljZSkge1xuICAgICAgICAkc2NvcGUubmV3RXZlbnQgPSB7fTtcbiAgICAgICAgJHNjb3BlLmRhdGVQaWNrZXIgPSB7XG4gICAgICAgICAgICBvcGVuZWQ6IGZhbHNlXG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLmluaXQgPSBmdW5jdGlvbigpe1xuXG4gICAgICAgICAgICBEYXRlLnByb3RvdHlwZS5hZGRIb3VycyA9IGZ1bmN0aW9uKGgpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNldFRpbWUodGhpcy5nZXRUaW1lKCkgKyAoaCo2MCo2MCoxMDAwKSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgdmFyIHN0YXJ0RGF0ZUluaXQgPSBuZXcgRGF0ZSgpO1xuICAgICAgICAgICAgJHNjb3BlLm5ld0V2ZW50LnN0YXJ0X2RhdGUgPVxuICAgICAgICAgICAgICAgIHN0YXJ0RGF0ZUluaXQuZ2V0RnVsbFllYXIoKSArXCIuXCIrXG4gICAgICAgICAgICAgICAgKFwiMFwiICsgKHN0YXJ0RGF0ZUluaXQuZ2V0TW9udGgoKSsxKSkuc2xpY2UoLTIpICtcIi5cIitcbiAgICAgICAgICAgICAgICAoXCIwXCIgKyBzdGFydERhdGVJbml0LmdldERhdGUoKSkuc2xpY2UoLTIpOy8qICsgXCIgXCIgKyovXG4gICAgICAgICAgICAgICAgLy9tLmdldEhvdXJzKCkgKyAnOicgKyBtLmdldE1pbnV0ZXMoKTtcbiAgICAgICAgICAgIHZhciBlbmRzSW5Jbml0ID0gbmV3IERhdGUoKS5hZGRIb3VycygyKTtcbiAgICAgICAgICAgICRzY29wZS5uZXdFdmVudC5lbmRzX2luID0gZW5kc0luSW5pdC5nZXRGdWxsWWVhcigpICtcIi5cIitcbiAgICAgICAgICAgICAgICAoXCIwXCIgKyAoZW5kc0luSW5pdC5nZXRNb250aCgpKzEpKS5zbGljZSgtMikgK1wiLlwiK1xuICAgICAgICAgICAgICAgIChcIjBcIiArIGVuZHNJbkluaXQuZ2V0RGF0ZSgpKS5zbGljZSgtMik7XG4gICAgICAgICAgICAvLyRzY29wZS5uZXdFdmVudC5kdXJhdGlvbiA9IDMwO1xuICAgICAgICAgICAgJHNjb3BlLm5ld0V2ZW50LnNvdXJjZV9pZCA9IDM7XG4gICAgICAgICAgICAkc2NvcGUubmV3RXZlbnQuY2F0ZWdvcnkgPSBcIlwiO1xuXG4gICAgICAgICAgICBWZW51ZVNlcnZpY2UucmVxdWVzdExpc3RWZW51ZXMoZnVuY3Rpb24odmVudWVzKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLnZlbnVlcyA9IHZlbnVlcztcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgQ2F0ZWdvcnlTZXJ2aWNlLmxpc3RDYXRlZ29yaWVzKGZ1bmN0aW9uKGNhdGVnb3JpZXMpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUuY2F0ZWdvcmllcyA9IGNhdGVnb3JpZXM7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgJCgnI2NhdGVnb3J5Jykuc2VsZWN0MigpO1xuXG4gICAgICAgICAgICAkKCcjZXZlbnREYXRlJykuZGF0ZXRpbWVwaWNrZXIoe1xuICAgICAgICAgICAgICAgIHRpbWVwaWNrZXI6dHJ1ZSxcbiAgICAgICAgICAgICAgICBmb3JtYXQ6IFwiWS5kLm0gSDppXCIsXG4gICAgICAgICAgICAgICAgc3RhcnREYXRlOiBuZXcgRGF0ZSgpLFxuICAgICAgICAgICAgICAgIG1pbkRhdGU6IG5ldyBEYXRlKClcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAkKCcjZXZlbnREYXRlJykuY2hhbmdlKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICRzY29wZS5uZXdFdmVudC5kdXJhdGlvbkhpbnQgPSAkc2NvcGUuZ2V0RXZlbnREdXJhdGlvbkhpbnQoKTtcbiAgICAgICAgICAgICAgICAkc2NvcGUubmV3RXZlbnQuc3RhcnRfZGF0ZSA9ICQoJyNldmVudERhdGUnKS52YWwoKTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAkKCcjZW5kc19pbicpLmRhdGV0aW1lcGlja2VyKHtcbiAgICAgICAgICAgICAgICB0aW1lcGlja2VyOnRydWUsXG4gICAgICAgICAgICAgICAgZm9ybWF0OiBcIlkuZC5tIEg6aVwiLFxuICAgICAgICAgICAgICAgIHN0YXJ0RGF0ZTogbmV3IERhdGUoKS5hZGRIb3VycygyKSxcbiAgICAgICAgICAgICAgICBtaW5EYXRlOiBuZXcgRGF0ZSgpLmFkZEhvdXJzKDIpXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgJCgnI2VuZHNfaW4nKS5jaGFuZ2UoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLm5ld0V2ZW50LmR1cmF0aW9uSGludCA9ICRzY29wZS5nZXRFdmVudER1cmF0aW9uSGludCgpO1xuICAgICAgICAgICAgICAgICRzY29wZS5uZXdFdmVudC5lbmRzX2luID0gJCgnI2VuZHNfaW4nKS52YWwoKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgLy9Vc2VyU2VydmljZS5nZXRDdXJyZW50VXNlcigkc2NvcGUsICRzdGF0ZSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLnRvZ2dsZUFkZHJlc3NTZWFyY2ggPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmKCRzY29wZS5uZXdFdmVudC52ZW51ZSA9PT0gXCJcIiB8fCB0eXBlb2YgJHNjb3BlLm5ld0V2ZW50LnZlbnVlID09PSBcInVuZGVmaW5lZFwiKXtcbiAgICAgICAgICAgICAgICAkKCcjYWRkcmVzcy1zZWFyY2gnKS5zaG93KCdzbG93Jyk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICQoJyNhZGRyZXNzLXNlYXJjaCcpLmhpZGUoJ3Nsb3cnKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5jcmVhdGVFdmVudCA9IGZ1bmN0aW9uKCRldmVudCl7XG4gICAgICAgICAgICAkZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICRzY29wZS5uZXdFdmVudC5jYXRlZ29yeSA9ICQoJyNjYXRlZ29yeScpLnZhbCgpO1xuICAgICAgICAgICAgaWYodHlwZW9mICRzY29wZS5uZXdFdmVudC5hZGRyZXNzID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiAkc2NvcGUubmV3RXZlbnQudmVudWUgPT09IFwidW5kZWZpbmVkXCIpIHtcbiAgICAgICAgICAgICAgICB2YXIgYWRkcmVzc09iaiA9ICRzY29wZS5uZXdFdmVudC5hZGRyZXNzO1xuICAgICAgICAgICAgICAgIHZhciBsYXQgPSBhZGRyZXNzT2JqLmdlb21ldHJ5LmxvY2F0aW9uLmxhdCgpO1xuICAgICAgICAgICAgICAgIHZhciBsbmcgPSBhZGRyZXNzT2JqLmdlb21ldHJ5LmxvY2F0aW9uLmxuZygpO1xuICAgICAgICAgICAgICAgICRzY29wZS5uZXdFdmVudC5hZGRyZXNzID0gYWRkcmVzc09iai5mb3JtYXR0ZWRfYWRkcmVzcztcbiAgICAgICAgICAgICAgICAkc2NvcGUubmV3RXZlbnQubGF0aXR1ZGUgPSBsYXQ7XG4gICAgICAgICAgICAgICAgJHNjb3BlLm5ld0V2ZW50LmxvbmdpdHVkZSA9IGxuZztcblxuICAgICAgICAgICAgICAgIFZlbnVlU2VydmljZS5maW5kKHtcbiAgICAgICAgICAgICAgICAgICAgc291cmNlOiAyLFxuICAgICAgICAgICAgICAgICAgICBsYXRpdHVkZTogbGF0LFxuICAgICAgICAgICAgICAgICAgICBsb25naXR1ZGU6IGxuZyxcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogYWRkcmVzc09iai5uYW1lXG4gICAgICAgICAgICAgICAgfSwgZnVuY3Rpb24odmVudWUpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYodmVudWUubGVuZ3RoID09PSAwKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5jcmVhdGVWZW51ZUZyb21BZGRyZXNzKGFkZHJlc3NPYmosIGZ1bmN0aW9uKHZlbnVlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5ld0V2ZW50LnZlbnVlX2lkID0gdmVudWUuaWQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5ld0V2ZW50LmxhdGl0dWRlID0gdmVudWUubGF0aXR1ZGU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5ld0V2ZW50LmxvbmdpdHVkZSA9IHZlbnVlLmxvbmdpdHVkZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUubmV3RXZlbnQuYWRkcmVzcyA9IHZlbnVlLnN0cmVldDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUubmV3RXZlbnQudmVudWVfaWQgPSB2ZW51ZS5pZDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBFdmVudFNlcnZpY2UuY3JlYXRlKCRzY29wZS5uZXdFdmVudCwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzdGF0ZS5nbygnb3JnYW5pemVyX2hvbWUnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmVudWUgPSB2ZW51ZVswXTtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5uZXdFdmVudC5sYXRpdHVkZSA9IHZlbnVlLmxhdGl0dWRlO1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5ld0V2ZW50LmxvbmdpdHVkZSA9IHZlbnVlLmxvbmdpdHVkZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5uZXdFdmVudC5hZGRyZXNzID0gdmVudWUuc3RyZWV0O1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5ld0V2ZW50LnZlbnVlX2lkID0gdmVudWUuaWQ7XG4gICAgICAgICAgICAgICAgICAgICAgICBFdmVudFNlcnZpY2UuY3JlYXRlKCRzY29wZS5uZXdFdmVudCwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHN0YXRlLmdvKCdvcmdhbml6ZXJfaG9tZScpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgVmVudWVTZXJ2aWNlLmZpbmQoe2lkOiAkc2NvcGUubmV3RXZlbnQudmVudWV9LCBmdW5jdGlvbih2ZW51ZSl7XG4gICAgICAgICAgICAgICAgICAgIGlmKHZlbnVlLmxlbmd0aCA9PT0gMClcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgdmVudWUgPSB2ZW51ZVswXTtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5ld0V2ZW50LmxhdGl0dWRlID0gdmVudWUubGF0aXR1ZGU7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5uZXdFdmVudC5sb25naXR1ZGUgPSB2ZW51ZS5sb25naXR1ZGU7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5uZXdFdmVudC5hZGRyZXNzID0gdmVudWUuc3RyZWV0O1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUubmV3RXZlbnQudmVudWVfaWQgPSB2ZW51ZS5pZDtcbiAgICAgICAgICAgICAgICAgICAgRXZlbnRTZXJ2aWNlLmNyZWF0ZSgkc2NvcGUubmV3RXZlbnQsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHN0YXRlLmdvKCdvcmdhbml6ZXJfaG9tZScpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuY3JlYXRlVmVudWVGcm9tQWRkcmVzcyA9IGZ1bmN0aW9uKGFkZHJlc3NPYmosIGNhbGxiYWNrKSB7XG4gICAgICAgICAgICB2YXIgdmVudWUgPSB7fTtcbiAgICAgICAgICAgIHZlbnVlLm5hbWUgPSBhZGRyZXNzT2JqLm5hbWU7XG4gICAgICAgICAgICB2ZW51ZS5zb3VyY2VfaWQgPSAyO1xuICAgICAgICAgICAgdmVudWUuZGVzY3JpcHRpb24gPSBcIlwiO1xuICAgICAgICAgICAgdmVudWUuZ2VuZXJhbF9pbmZvID0gXCJcIjtcbiAgICAgICAgICAgIHZlbnVlLnBob25lID0gYWRkcmVzc09iai5mb3JtYXR0ZWRfcGhvbmVfbnVtYmVyO1xuICAgICAgICAgICAgdmVudWUucGhvbmUgPSBhZGRyZXNzT2JqLndlYnNpdGU7XG4gICAgICAgICAgICB2ZW51ZS5zdHJlZXQgPSBhZGRyZXNzT2JqLnZpY2luaXR5O1xuICAgICAgICAgICAgdmVudWUubGF0aXR1ZGUgPSBhZGRyZXNzT2JqLmdlb21ldHJ5LmxvY2F0aW9uLmxhdCgpO1xuICAgICAgICAgICAgdmVudWUubG9uZ2l0dWRlID0gYWRkcmVzc09iai5nZW9tZXRyeS5sb2NhdGlvbi5sbmcoKTtcbiAgICAgICAgICAgIFZlbnVlU2VydmljZS5jcmVhdGUodmVudWUsIGNhbGxiYWNrKTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuYWRkcmVzc1NlbGVjdGVkID0gZnVuY3Rpb24oKXtcbiAgICAgICAgICAgIGlmKHR5cGVvZiAkc2NvcGUubmV3RXZlbnQuYWRkcmVzcyA9PT0gXCJvYmplY3RcIikge1xuICAgICAgICAgICAgICAgICRzY29wZS5nZXRWZW51ZUJ5TG9jYXRpb24oJHNjb3BlLm5ld0V2ZW50LmFkZHJlc3MpO1xuICAgICAgICAgICAgICAgICRzY29wZS5tYXAgPSBNYXBTZXJ2aWNlLmNlbnRlck9uRXZlbnQoe1xuICAgICAgICAgICAgICAgICAgICBsYXRpdHVkZTogJHNjb3BlLm5ld0V2ZW50LmFkZHJlc3MuZ2VvbWV0cnkubG9jYXRpb24ubGF0KCksXG4gICAgICAgICAgICAgICAgICAgIGxvbmdpdHVkZTogJHNjb3BlLm5ld0V2ZW50LmFkZHJlc3MuZ2VvbWV0cnkubG9jYXRpb24ubG5nKClcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAkc2NvcGUubWFwLmxhdGl0dWRlID0gJHNjb3BlLm5ld0V2ZW50LmFkZHJlc3MuZ2VvbWV0cnkubG9jYXRpb24ubGF0KCk7XG4gICAgICAgICAgICAgICAgJHNjb3BlLm1hcC5sb25naXR1ZGUgPSAkc2NvcGUubmV3RXZlbnQuYWRkcmVzcy5nZW9tZXRyeS5sb2NhdGlvbi5sbmcoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuZ2V0VmVudWVCeUxvY2F0aW9uID0gZnVuY3Rpb24oZ29vZ2xlUmVzcG9uc2UpIHtcbiAgICAgICAgICAgIHZhciBsb2NhdGlvbiA9IHtcbiAgICAgICAgICAgICAgICBsYXRpdHVkZTogZ29vZ2xlUmVzcG9uc2UuZ2VvbWV0cnkubG9jYXRpb24ubGF0KCksXG4gICAgICAgICAgICAgICAgbG9uZ2l0dWRlOiBnb29nbGVSZXNwb25zZS5nZW9tZXRyeS5sb2NhdGlvbi5sbmcoKVxuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIFZlbnVlU2VydmljZS5nZXRCeUxvY2F0aW9uKGxvY2F0aW9uLCBmdW5jdGlvbih2ZW51ZSkge1xuICAgICAgICAgICAgICAgIGlmKHZlbnVlICE9PSBudWxsKXtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5ld0V2ZW50LnZlbnVlID0gdmVudWUuaWQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLmdldEV2ZW50RHVyYXRpb25IaW50ID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXIgZHVyYXRpb24gPSAkc2NvcGUuZ2V0RXZlbnREdXJhdGlvbigpO1xuICAgICAgICAgICAgaWYoZHVyYXRpb24uZGF5cyA+IDApIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJEdXJhdGlvbjogXCIgKyBkdXJhdGlvbi5kYXlzICsgXCIgZGF5c1wiO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIFwiRHVyYXRpb246IFwiICsgZHVyYXRpb24ubWludXRlcyArIFwiIG1pbnV0ZXNcIjtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuZ2V0RXZlbnREdXJhdGlvbiA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyIHN0YXJ0aW5nQXQgPSAkc2NvcGUucGFyc2VEYXRlKCRzY29wZS5uZXdFdmVudC5zdGFydF9kYXRlKTtcbiAgICAgICAgICAgIHZhciBmaW5pc2hBdCA9ICRzY29wZS5wYXJzZURhdGUoJHNjb3BlLm5ld0V2ZW50LmVuZHNfaW4pO1xuICAgICAgICAgICAgdmFyIGRpZmZNcyA9IChzdGFydGluZ0F0IC0gZmluaXNoQXQpOyAvLyBtaWxsaXNlY29uZHMgYmV0d2VlbiBub3cgJiBDaHJpc3RtYXNcbiAgICAgICAgICAgIHZhciBkaWZmRGF5cyA9IE1hdGgucm91bmQoZGlmZk1zIC8gODY0MDAwMDApOyAvLyBkYXlzXG4gICAgICAgICAgICB2YXIgZGlmZkhycyA9IE1hdGgucm91bmQoKGRpZmZNcyAlIDg2NDAwMDAwKSAvIDM2MDAwMDApOyAvLyBob3Vyc1xuICAgICAgICAgICAgdmFyIGRpZmZNaW5zID0gTWF0aC5yb3VuZCgoKGRpZmZNcyAlIDg2NDAwMDAwKSAlIDM2MDAwMDApIC8gNjAwMDApOyAvLyBtaW51dGVzXG5cbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgZGF5czogZGlmZkRheXMsXG4gICAgICAgICAgICAgICAgaG91cnM6IGRpZmZIcnMsXG4gICAgICAgICAgICAgICAgbWludXRlczogZGlmZk1pbnNcbiAgICAgICAgICAgIH07XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLnBhcnNlRGF0ZSA9IGZ1bmN0aW9uKGRhdGVTdHJpbmcpIHtcbiAgICAgICAgICAgIHZhciBkYXRlUGFydHMgPSBkYXRlU3RyaW5nLnNwbGl0KCcgJyk7XG4gICAgICAgICAgICB2YXIgZGF0ZSA9IGRhdGVQYXJ0c1swXS5zcGxpdCgnLicpO1xuICAgICAgICAgICAgaWYodHlwZW9mIGRhdGVQYXJ0c1sxXSAhPT0gXCJ1bmRlZmluZWRcIil7XG4gICAgICAgICAgICAgICAgdmFyIHRpbWUgPSBkYXRlUGFydHNbMV0uc3BsaXQoJzonKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IERhdGUoZGF0ZVswXSwgZGF0ZVsxXSwgZGF0ZVsyXSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gbmV3IERhdGUoZGF0ZVswXSwgZGF0ZVsxXSwgZGF0ZVsyXSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLmR1cmF0aW9uRGVmaW5lZCA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgcmV0dXJuIHR5cGVvZiAkc2NvcGUubmV3RXZlbnQuZHVyYXRpb24gIT09IFwidW5kZWZpbmVkXCI7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLmluaXQoKTtcbiAgICB9KTtcbn0pKCk7IiwiLyoqXG4gKiBDcmVhdGVkIGJ5IG9lbSBvbiAzLzE5LzE2LlxuICovXG4oZnVuY3Rpb24oKXtcbiAgICBcInVzZSBzdHJpY3RcIjtcblxuICAgIC8qKlxuICAgICAqIFRoaXMgY29udHJvbGxlciBpcyBkZWFsaW5nIHdpdGggZXZlbnRzIGxpc3Qgdmlld1xuICAgICAqL1xuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuY29udHJvbGxlcnMnKS5jb250cm9sbGVyKCdFdmVudEZpbHRlckN0cmwnLCBmdW5jdGlvbigkcSwgJHRpbWVvdXQsICRzY29wZSwgJHJvb3RTY29wZSwgJHN0YXRlLCBIdG1sSGVscGVyLCBDaGFuZ2VzTm90aWZpZXIsIEV2ZW50c1N0b3JhZ2Upe1xuXG4gICAgICAgIHZhciBfdGltZW91dDtcblxuXG4gICAgICAgICRzY29wZS5nZXRFdmVudFJlcXVlc3QgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhciBzdGF0ZSA9ICRzdGF0ZS5jdXJyZW50Lm5hbWU7XG4gICAgICAgICAgICBzd2l0Y2ggKHN0YXRlKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAnaG9tZS5ldmVudHMnOlxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJHNjb3BlLmV2ZW50UmVxdWVzdDtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSAnb3JnYW5pemVyLmV2ZW50cyc6XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAkc2NvcGUub3JnRXZlbnRSZXF1ZXN0O1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuXG4gICAgICAgIGFuZ3VsYXIuZWxlbWVudChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgSHRtbEhlbHBlci5pbml0KCk7XG4gICAgICAgICAgICB2YXIgY2F0ZWdvcnkgPSAkc2NvcGUuZ2V0RXZlbnRSZXF1ZXN0KCkuY2F0ZWdvcnk7XG4gICAgICAgICAgICB2YXIgY2F0ZWdvcmllcyA9ICRzY29wZS5ldmVudENhdGVnb3JpZXM7XG4gICAgICAgICAgICB2YXIgY2F0ZWdvcnlOYW1lID0gbnVsbDtcbiAgICAgICAgICAgIGlmKGNhdGVnb3J5ICE9PSBudWxsICYmIHR5cGVvZiBjYXRlZ29yaWVzICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgIGZvcih2YXIgaSA9IDA7IGkgPCBjYXRlZ29yaWVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmKGNhdGVnb3JpZXNbaV0uX2lkID09IGNhdGVnb3J5KXtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhdGVnb3J5TmFtZSA9IGNhdGVnb3JpZXNbaV0ubmFtZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKGNhdGVnb3J5TmFtZSAhPT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICR0aW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAkKCcjY2F0ZWdvcnlfc2VhcmNoJykuY2hpbGRyZW4oJ3NwYW4nKS50ZXh0KGNhdGVnb3J5TmFtZSk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBuZXcgSHRtbEhlbHBlci5TdHlsZWREcm9wRG93bignI2NhdGVnb3J5X3NlYXJjaCcsIGNhdGVnb3J5KTtcbiAgICAgICAgICAgIG5ldyBIdG1sSGVscGVyLlN0eWxlZERyb3BEb3duKCcjd2hlbl9zZWFyY2gnLCAkc2NvcGUuZ2V0RXZlbnRSZXF1ZXN0KCkud2hlbik7XG4gICAgICAgICAgICB2YXIgaW5wdXQgPSAkKCcubG9jYXRpb24taW5wdXQnKS5maXJzdCgpO1xuICAgICAgICAgICAgJHNjb3BlLmF1dG9jb21wbGV0ZSA9IG5ldyBnb29nbGUubWFwcy5wbGFjZXMuQXV0b2NvbXBsZXRlKGlucHV0WzBdLCB7dHlwZXM6IFsnKGNpdGllcyknXX0pO1xuICAgICAgICAgICAgJHNjb3BlLmF1dG9jb21wbGV0ZS5hZGRMaXN0ZW5lcigncGxhY2VfY2hhbmdlZCcsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICB2YXIgcGxhY2UgPSAkc2NvcGUuYXV0b2NvbXBsZXRlLmdldFBsYWNlKCk7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmdldEV2ZW50UmVxdWVzdCgpLnNldFdoZXJlKHBsYWNlLmdlb21ldHJ5LmxvY2F0aW9uLmxhdCgpLCBwbGFjZS5nZW9tZXRyeS5sb2NhdGlvbi5sbmcoKSwgcGxhY2UubmFtZSk7XG4gICAgICAgICAgICAgICAgJHNjb3BlLnNlYXJjaCgpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgICRzY29wZS5zZWFyY2hCeVRleHQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZihfdGltZW91dCl7IC8vaWYgdGhlcmUgaXMgYWxyZWFkeSBhIHRpbWVvdXQgaW4gcHJvY2VzcyBjYW5jZWwgaXRcbiAgICAgICAgICAgICAgICAkdGltZW91dC5jYW5jZWwoX3RpbWVvdXQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgX3RpbWVvdXQgPSAkdGltZW91dChmdW5jdGlvbigpe1xuICAgICAgICAgICAgICAgICRzY29wZS5zZWFyY2goKTtcbiAgICAgICAgICAgICAgICBfdGltZW91dCA9IG51bGw7XG4gICAgICAgICAgICB9LCA1MDApO1xuICAgICAgICB9O1xuXG5cbiAgICAgICAgJHNjb3BlLnNlbGVjdENhdGVnb3J5ID0gZnVuY3Rpb24oY2F0ZWdvcnkpIHtcbiAgICAgICAgICAgIGlmKGNhdGVnb3J5ID09PSBudWxsKXtcbiAgICAgICAgICAgICAgICAkc2NvcGUuZ2V0RXZlbnRSZXF1ZXN0KCkuY2F0ZWdvcnkgPSBudWxsO1xuICAgICAgICAgICAgICAgICQoJyNjYXRlZ29yeV9zZWFyY2gnKS5jaGlsZHJlbignc3BhbicpLnRleHQoJy0tIEFsbCAtLScpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUuZ2V0RXZlbnRSZXF1ZXN0KCkuY2F0ZWdvcnkgPSBjYXRlZ29yeS5faWQ7XG4gICAgICAgICAgICAgICAgJCgnI2NhdGVnb3J5X3NlYXJjaCcpLmNoaWxkcmVuKCdzcGFuJykudGV4dChjYXRlZ29yeS5uYW1lKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgICRzY29wZS5zZWFyY2goKTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuc2VsZWN0V2hlbiA9IGZ1bmN0aW9uKHZhbHVlKSB7XG4gICAgICAgICAgICAkc2NvcGUuZ2V0RXZlbnRSZXF1ZXN0KCkuZGF0ZSA9IG51bGw7XG4gICAgICAgICAgICAkc2NvcGUuZ2V0RXZlbnRSZXF1ZXN0KCkud2hlbiA9IHZhbHVlO1xuICAgICAgICAgICAgaWYodmFsdWUgPT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICAkKCcjd2hlbl9zZWFyY2gnKS5jaGlsZHJlbignc3BhbicpLnRleHQoJy0tIEFsbCAtLScpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgJCgnI3doZW5fc2VhcmNoJykuY2hpbGRyZW4oJ3NwYW4nKS50ZXh0KHZhbHVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgICRzY29wZS5zZWFyY2goKTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUub3JnYW5pemVyU2VhcmNoID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBIdG1sSGVscGVyLnNob3dMb2FkZXIoKTtcbiAgICAgICAgICAgICRzY29wZS5vcmdFdmVudFJlcXVlc3QucGFnZSA9IDA7XG4gICAgICAgICAgICAkc2NvcGUuZXZlbnRzID0gW107XG4gICAgICAgICAgICAkc2NvcGUub3JnRXZlbnRSZXF1ZXN0LnR5cGUgPSAnZXZlbnRzJztcbiAgICAgICAgICAgICRzY29wZS5vcmdFdmVudFJlcXVlc3QuaGFzTW9yZVBhZ2VzID0gdHJ1ZTtcbiAgICAgICAgICAgIHZhciBldmVudHMgPSBuZXcgRXZlbnRzU3RvcmFnZSgpO1xuICAgICAgICAgICAgcmV0dXJuICRxLndoZW4oZXZlbnRzLmxvYWQoJHNjb3BlLm9yZ0V2ZW50UmVxdWVzdCkpLnRoZW4oZnVuY3Rpb24oZXZlbnRTdG9yYWdlKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLm9yZ0V2ZW50cyA9IGV2ZW50U3RvcmFnZS5kYXRhO1xuICAgICAgICAgICAgICAgIENoYW5nZXNOb3RpZmllci5vcmdfdmFsdWUgPSAkc2NvcGUub3JnRXZlbnRzO1xuICAgICAgICAgICAgICAgIENoYW5nZXNOb3RpZmllci5ub3RpZnkoQ2hhbmdlc05vdGlmaWVyLlNVQkpfT1JHX0VWRU5UU19MT0FERUQpO1xuICAgICAgICAgICAgICAgIEh0bWxIZWxwZXIuaGlkZUxvYWRlcigpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLnVzZXJTZWFyY2ggPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIEh0bWxIZWxwZXIuc2hvd0xvYWRlcigpO1xuICAgICAgICAgICAgJHNjb3BlLmV2ZW50UmVxdWVzdC5wYWdlID0gMDtcbiAgICAgICAgICAgICRzY29wZS5ldmVudHMgPSBbXTtcbiAgICAgICAgICAgICRzY29wZS5ldmVudFJlcXVlc3QudHlwZSA9ICdldmVudHMnO1xuICAgICAgICAgICAgJHNjb3BlLmV2ZW50UmVxdWVzdC5oYXNNb3JlUGFnZXMgPSB0cnVlO1xuICAgICAgICAgICAgdmFyIGV2ZW50cyA9IG5ldyBFdmVudHNTdG9yYWdlKCk7XG4gICAgICAgICAgICByZXR1cm4gJHEud2hlbihldmVudHMubG9hZCgkc2NvcGUuZXZlbnRSZXF1ZXN0KSkudGhlbihmdW5jdGlvbihldmVudFN0b3JhZ2UpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUuZXZlbnRzID0gZXZlbnRTdG9yYWdlLmRhdGE7XG4gICAgICAgICAgICAgICAgQ2hhbmdlc05vdGlmaWVyLnZhbHVlID0gJHNjb3BlLmV2ZW50cztcbiAgICAgICAgICAgICAgICBDaGFuZ2VzTm90aWZpZXIubm90aWZ5KENoYW5nZXNOb3RpZmllci5TVUJKX0VWRU5UU19MT0FERUQsICRzY29wZS5ldmVudHMpO1xuICAgICAgICAgICAgICAgIEh0bWxIZWxwZXIuaGlkZUxvYWRlcigpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLnNlYXJjaCA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyIHN0YXRlID0gJHN0YXRlLmN1cnJlbnQubmFtZTtcbiAgICAgICAgICAgIHN3aXRjaCAoc3RhdGUpIHtcbiAgICAgICAgICAgICAgICBjYXNlICdob21lLmV2ZW50cyc6XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS51c2VyU2VhcmNoKCk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgJ29yZ2FuaXplci5ldmVudHMnOlxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUub3JnYW5pemVyU2VhcmNoKCk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgIH0pO1xufSkoKTsiLCIvKipcbiAqIENyZWF0ZWQgYnkgb2VtIG9uIDMvMTkvMTYuXG4gKi9cbihmdW5jdGlvbigpe1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG4gICAgLyoqXG4gICAgICogVGhpcyBjb250cm9sbGVyIGlzIGRlYWxpbmcgd2l0aCBldmVudHMgbGlzdCB2aWV3XG4gICAgICovXG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5jb250cm9sbGVycycpLmNvbnRyb2xsZXIoJ0V2ZW50c0N0cmwnLCBmdW5jdGlvbigkcSwgJHNjb3BlLCAkc3RhdGUsIEh0bWxIZWxwZXIsIENoYW5nZXNOb3RpZmllcil7XG5cblxuICAgICAgICAkc2NvcGUuJHdhdGNoKCdldmVudHMnLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmKCRzY29wZS5ldmVudHMpIHtcbiAgICAgICAgICAgICAgICBmb3IodmFyIGkgPSAwOyBpIDwgJHNjb3BlLmV2ZW50cy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICB2YXIgZGlzdGFuY2UgPSAkc2NvcGUuZXZlbnRzW2ldLmNvbXB1dGVEaXN0YW5jZVRvKCRzY29wZS5ldmVudFJlcXVlc3QuZ2V0VXNlclBvc2l0aW9uKCkpO1xuICAgICAgICAgICAgICAgICAgICBpZihkaXN0YW5jZSA+IDMwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkaXN0YW5jZSA9ICc+IDMwJztcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuZXZlbnRzW2ldLmRpc3RhbmNlID0gZGlzdGFuY2UgPyBkaXN0YW5jZSArICcga20gYXdheScgOiAnJztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgSHRtbEhlbHBlci5oaWRlTG9hZGVyKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH0pO1xufSkoKTsiLCIvKipcbiAqIENyZWF0ZWQgYnkgb2VtIG9uIDMvMTkvMTYuXG4gKi9cbihmdW5jdGlvbigpe1xuICAgIFwidXNlIHN0cmljdFwiO1xuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuY29udHJvbGxlcnMnKS5jb250cm9sbGVyKCdIZWFkZXJDdHJsJywgZnVuY3Rpb24oJHNjb3BlLCAkcm9vdFNjb3BlLCAkbG9jYWxTdG9yYWdlLCAkdGltZW91dCwgJHEsICRodHRwLCAkc3RhdGUsICRsb2NhdGlvbiwgJGF1dGgsIEh0bWxIZWxwZXIsIFVzZXJTZXJ2aWNlLCBGYWNlYm9vayl7XG5cbiAgICAgICAgJHNjb3BlLnJlbmRlcl90YWIgPSBIdG1sSGVscGVyLnJlbmRlcl90YWI7XG4gICAgICAgICRzY29wZS5jbG9zZVBvcHVwcyA9IEh0bWxIZWxwZXIuY2xvc2VQb3B1cHM7XG4gICAgICAgICRzY29wZS5zaG93UG9wVXAgPSBIdG1sSGVscGVyLnNob3dQb3BVcDtcblxuICAgICAgICAkc2NvcGUubG9naW5Gb3JtID0ge1xuICAgICAgICAgICAgZW1haWw6IFwiXCIsXG4gICAgICAgICAgICBwYXNzd29yZDogXCJcIlxuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5yZWdpc3RlckZvcm0gPSB7XG4gICAgICAgICAgICB0eXBlOiAwXG4gICAgICAgIH07XG5cbiAgICAgICAgJCgnYm9keScpLmNsaWNrKGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICAgICAgICBpZihcbiAgICAgICAgICAgICAgICAhJChldmVudC50YXJnZXQpLmlzKCdhJylcbiAgICAgICAgICAgICAgICAmJlxuICAgICAgICAgICAgICAgICEkKGV2ZW50LnRhcmdldCkuaXMoJ2lucHV0JylcbiAgICAgICAgICAgICAgICAmJlxuICAgICAgICAgICAgICAgICEkKGV2ZW50LnRhcmdldCkuaXMoJ2xhYmVsJylcbiAgICAgICAgICAgICl7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmNsb3NlUG9wdXBzKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG5cbiAgICAgICAgJHJvb3RTY29wZS4kb24oJyRzdGF0ZUNoYW5nZVN1Y2Nlc3MnLCBmdW5jdGlvbigpe1xuICAgICAgICAgICAgdmFyIGV2ZW50TGkgPSAkKCcjbWVudS1ldmVudHMnKS5jbG9zZXN0KCdsaScpO1xuICAgICAgICAgICAgdmFyIHZlbnVlTGkgPSAkKCcjbWVudS12ZW51ZXMnKS5jbG9zZXN0KCdsaScpO1xuICAgICAgICAgICAgdmFyIGRlYWxMaSA9ICQoJyNtZW51LWRlYWxzJykuY2xvc2VzdCgnbGknKTtcbiAgICAgICAgICAgIHN3aXRjaCgkbG9jYXRpb24ucGF0aCgpLnN1YnN0cmluZygzKSl7XG4gICAgICAgICAgICAgICAgY2FzZSAnZXZlbnRzJzpcbiAgICAgICAgICAgICAgICAgICAgdmVudWVMaS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAgICAgICAgIGRlYWxMaS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAgICAgICAgIGV2ZW50TGkuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlICd2ZW51ZXMnOlxuICAgICAgICAgICAgICAgICAgICBkZWFsTGkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgICAgICAgICBldmVudExpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICAgICAgICAgdmVudWVMaS5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgICdkZWFscyc6XG4gICAgICAgICAgICAgICAgICAgIGV2ZW50TGkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgICAgICAgICB2ZW51ZUxpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICAgICAgICAgZGVhbExpLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBMb2cgY3VycmVudCB1c2VyIG91dFxuICAgICAgICAgKi9cbiAgICAgICAgJHNjb3BlLmxvZ291dCA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJGxvY2FsU3RvcmFnZS50b2tlbiA9IG51bGw7XG4gICAgICAgICAgICAkcS53aGVuKFVzZXJTZXJ2aWNlLmxvZ291dCgpKS50aGVuKGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICR0aW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUudXNlciA9IG51bGw7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogTG9nIHVzZXIgaW5cbiAgICAgICAgICovXG4gICAgICAgICRzY29wZS5zdWJtaXRMb2dpbiA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJHNjb3BlLnRvZ2dsZVBvcFVwTG9hZGluZygpO1xuICAgICAgICAgICAgVXNlclNlcnZpY2UubG9naW4oJHNjb3BlLmxvZ2luRm9ybSkudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgIGlmKHR5cGVvZiByZXNwb25zZS5faWQgIT09IFwidW5kZWZpbmVkXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnVzZXIgPSByZXNwb25zZTtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnRvZ2dsZVBvcFVwTG9hZGluZygpO1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuY2xvc2VQb3B1cHMoKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmxvZ2luRXJyb3IgPSAnRXJyb3Igb2NjdXJlZCwgcGxlYXNlIHRyeSByZWxvYWQgdGhlIHBhZ2UgYW5kIHJlcGVhdCB5b3VyIGFjdGlvbic7XG4gICAgICAgICAgICAgICAgICAgICAgICAkKCcjbG9naW4tZXJyb3InKS5yZW1vdmVDbGFzcygnaGlkZGVuJyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIExvZ2luIHdpdGggZmFjZWJvb2tcbiAgICAgICAgICovXG4gICAgICAgICRzY29wZS5sb2dpbkZhY2Vib29rID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgJHEud2hlbihVc2VyU2VydmljZS5sb2dpbkZhY2Vib29rKEZhY2Vib29rKSkudGhlbihmdW5jdGlvbih1cykge1xuICAgICAgICAgICAgICAgIGlmKHVzLmF1dGhSZXNwb25zZSAmJiB1cy5hdXRoUmVzcG9uc2UuYWNjZXNzVG9rZW4pIHtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmxvZ2luRm9ybSA9IHtmYWNlYm9va19hY2Nlc3NfdG9rZW46IHVzLmF1dGhSZXNwb25zZS5hY2Nlc3NUb2tlbn07XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5zdWJtaXRMb2dpbigpO1xuICAgICAgICAgICAgICAgIH0gIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUubG9naW5FcnJvciA9ICdFcnJvciBvY2N1cmVkLCBwbGVhc2UgdHJ5IHJlbG9hZCB0aGUgcGFnZSBhbmQgcmVwZWF0IHlvdXIgYWN0aW9uJztcbiAgICAgICAgICAgICAgICAgICAgJCgnI2xvZ2luLWVycm9yJykucmVtb3ZlQ2xhc3MoJ2hpZGRlbicpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBMb2dpbiB3aXRoIGZhY2Vib29rXG4gICAgICAgICAqL1xuICAgICAgICAkc2NvcGUubG9naW5Hb29nbGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAkaHR0cC5wb3N0KCdodHRwczovL3ZlbnZhc3QuY29tL2F1dGgvZ29vZ2xlJykuc3VjY2VzcyhmdW5jdGlvbiAocmVzcG9uc2UsIHN0YXR1cywgaGVhZGVycykge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiU29tZSByZXNwOiBcIiwgcmVzcG9uc2UpO1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiU3RhdHVzOiBcIiwgc3RhdHVzKTtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkhlYWRlcnM6IFwiLCBoZWFkZXJzKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS50b2dnbGVQb3BVcExvYWRpbmcgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICQoJyNsb2dpbl9mb3JtJykudG9nZ2xlQ2xhc3MoJ2hpZGRlbicpO1xuICAgICAgICAgICAgJCgnI3JlZ2lzdGVyX2Zvcm0nKS50b2dnbGVDbGFzcygnaGlkZGVuJyk7XG4gICAgICAgICAgICAkKCcubG9hZGVyLXdyYXBwZXInKS50b2dnbGUoKTtcbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogUmVnaXN0ZXIgbmV3IHVzZXJcbiAgICAgICAgICovXG4gICAgICAgICRzY29wZS5yZWdpc3RlciA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJHNjb3BlLnN1Ym1pdExvZ2luKCk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIENvbm5lY3QgZmFjZWJvb2sgYWNjb3VudCBvbiByZWdpc3RyYXRpb25cbiAgICAgICAgICovXG4gICAgICAgICRzY29wZS5yZWdpc3RlckZhY2Vib29rID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkc2NvcGUubG9naW5GYWNlYm9vaygpO1xuICAgICAgICB9O1xuICAgICAgICBcblxuXG5cblxuICAgICAgICBzd2l0Y2goJGxvY2F0aW9uLnBhdGgoKS5zdWJzdHJpbmcoMykpe1xuICAgICAgICAgICAgY2FzZSAnJzpcbiAgICAgICAgICAgICAgICAkKCcjbWVudS1ldmVudHMnKS5jbG9zZXN0KCdsaScpLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgJ2V2ZW50cyc6XG4gICAgICAgICAgICAgICAgJCgnI21lbnUtZXZlbnRzJykuY2xvc2VzdCgnbGknKS5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlICd2ZW51ZXMnOlxuICAgICAgICAgICAgICAgICQoJyNtZW51LXZlbnVlcycpLmNsb3Nlc3QoJ2xpJykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAgJ2RlYWxzJzpcbiAgICAgICAgICAgICAgICAkKCcjbWVudS1kZWFscycpLmNsb3Nlc3QoJ2xpJykuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgfSk7XG59KSgpOyIsIi8qKlxuICogQ3JlYXRlZCBieSBvZW0gb24gMy8xOS8xNi5cbiAqL1xuKGZ1bmN0aW9uKCl7XG4gICAgLy8gXCJ1c2Ugc3RyaWN0XCI7XG5cblxuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuY29udHJvbGxlcnMnKS5jb250cm9sbGVyKCdIb21lQ3RybCcsIGZ1bmN0aW9uKCRxLCAkc2NvcGUsICR0aW1lb3V0LCAkd2luZG93LCAkYW5jaG9yU2Nyb2xsLCAkc3RhdGUsIEFuY2hvclNtb290aFNjcm9sbCwgSHRtbEhlbHBlciwgRXZlbnRzU3RvcmFnZSwgVmVudWVTdG9yYWdlLCBFdmVudCwgVXNlclNlcnZpY2UsIFZlblZhc3RSZXF1ZXN0LCBNYXBTZXJ2aWNlLCBDaGFuZ2VzTm90aWZpZXIsIENhdGVnb3JpZXNTdG9yYWdlKXtcblxuXG4gICAgICAgICRzY29wZS5pbml0ID0gZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICRzY29wZS53aW5faGVpZ2h0X3BhZGRlZCA9ICQod2luZG93KS5oZWlnaHQoKSAqIDEuMzQ7XG4gICAgICAgICAgICAkd2luZG93Lm9uc2Nyb2xsID0gJHNjb3BlLnJldmVhbE9uU2Nyb2xsO1xuXG4gICAgICAgICAgICAkc2NvcGUuZXZlbnRDYXRlZ29yaWVzUmVxdWVzdCA9IG5ldyBWZW5WYXN0UmVxdWVzdCh7dHlwZTogJ2V2ZW50cyd9KTtcbiAgICAgICAgICAgIGlmKHR5cGVvZiAkc2NvcGUuZXZlbnRSZXF1ZXN0ID09PSAndW5kZWZpbmVkJyB8fCAkc2NvcGUuZXZlbnRSZXF1ZXN0ID09PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmV2ZW50UmVxdWVzdCA9IG5ldyBWZW5WYXN0UmVxdWVzdCh7d2hlbjogXCJ3ZWVrXCJ9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgICRzY29wZS52ZW51ZUNhdGVnb3JpZXNSZXF1ZXN0ID0gbmV3IFZlblZhc3RSZXF1ZXN0KHt0eXBlOiAndmVudWVzJ30pO1xuICAgICAgICAgICAgaWYodHlwZW9mICRzY29wZS52ZW51ZXNSZXF1ZXN0ID09PSAndW5kZWZpbmVkJyB8fCAkc2NvcGUudmVudWVzUmVxdWVzdCA9PT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICRzY29wZS52ZW51ZXNSZXF1ZXN0ID0gbmV3IFZlblZhc3RSZXF1ZXN0KHt0eXBlOiAndmVudWVzJ30pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgSHRtbEhlbHBlci5zaG93TG9hZGVyKCk7XG4gICAgICAgICAgICAkc2NvcGUuaW5pdFVzZXIoKTtcbiAgICAgICAgICAgICRzY29wZS5pbml0RXZlbnRzKCk7XG4gICAgICAgICAgICAkc2NvcGUuaW5pdFZlbnVlcygpO1xuICAgICAgICAgICAgQ2hhbmdlc05vdGlmaWVyLnN1YnNjcmliZShDaGFuZ2VzTm90aWZpZXIuU1VCSl9WRU5VRVNfTE9BREVELCAkc2NvcGUsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICR0aW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICBpZih0eXBlb2YgIENoYW5nZXNOb3RpZmllci52ZW51ZXNfdmFsdWUgIT09IFwidW5kZWZpbmVkXCIgJiYgQ2hhbmdlc05vdGlmaWVyLnZlbnVlc192YWx1ZSAhPT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnZlbnVlcyA9IENoYW5nZXNOb3RpZmllci52ZW51ZXNfdmFsdWU7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgQ2hhbmdlc05vdGlmaWVyLnN1YnNjcmliZShDaGFuZ2VzTm90aWZpZXIuU1VCSl9FVkVOVFNfTE9BREVELCAkc2NvcGUsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAkdGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmKHR5cGVvZiAgQ2hhbmdlc05vdGlmaWVyLnZhbHVlICE9PSBcInVuZGVmaW5lZFwiICYmIENoYW5nZXNOb3RpZmllci52YWx1ZSAhPT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmV2ZW50cyA9IENoYW5nZXNOb3RpZmllci52YWx1ZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIENoYW5nZXNOb3RpZmllci5zdWJzY3JpYmUoQ2hhbmdlc05vdGlmaWVyLlNVQkpfRVZFTlRTX0FEREVELCAkc2NvcGUsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIHZhciBlcyA9IG5ldyBFdmVudHNTdG9yYWdlKCk7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmFkZFNjcm9sbEhhbmRsZXIoZXMsICRzY29wZS5ldmVudFJlcXVlc3QsICdldmVudHMnKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgQ2hhbmdlc05vdGlmaWVyLnN1YnNjcmliZShDaGFuZ2VzTm90aWZpZXIuU1VCSl9WRU5VRVNfQURERUQsICRzY29wZSwgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgdmFyIHZzID0gbmV3IFZlbnVlU3RvcmFnZSgpO1xuICAgICAgICAgICAgICAgICRzY29wZS5hZGRTY3JvbGxIYW5kbGVyKHZzLCAkc2NvcGUudmVudWVzUmVxdWVzdCwgJ3ZlbnVlcycpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLmluaXRFdmVudHMgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhciBldmVudHMgPSBuZXcgRXZlbnRzU3RvcmFnZSgpO1xuICAgICAgICAgICAgdmFyIGNhdGVnb3JpZXMgPSBuZXcgQ2F0ZWdvcmllc1N0b3JhZ2UoKTtcbiAgICAgICAgICAgIHJldHVybiAkcS53aGVuKGV2ZW50cy5sb2FkKCRzY29wZS5ldmVudFJlcXVlc3QpKS50aGVuKGZ1bmN0aW9uKGV2ZW50U3RvcmFnZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiAkcS53aGVuKGNhdGVnb3JpZXMubG9hZCgkc2NvcGUuZXZlbnRDYXRlZ29yaWVzUmVxdWVzdCkpLnRoZW4oZnVuY3Rpb24oY2F0ZWdvcnlTdG9yYWdlKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmKHR5cGVvZiBjYXRlZ29yeVN0b3JhZ2UgPT09ICd1bmRlZmluZWQnIHx8IGNhdGVnb3J5U3RvcmFnZSA9PT0gbnVsbCl7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmV2ZW50cyA9IGV2ZW50U3RvcmFnZS5kYXRhO1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuZXZlbnRDYXRlZ29yaWVzID0gY2F0ZWdvcnlTdG9yYWdlLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgIENoYW5nZXNOb3RpZmllci5ub3RpZnkoQ2hhbmdlc05vdGlmaWVyLlNVQkpfRVZFTlRTX0xPQURFRCk7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5hZGRTY3JvbGxIYW5kbGVyKGV2ZW50cywgJHNjb3BlLmV2ZW50UmVxdWVzdCwgJ2V2ZW50cycpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5pbml0VmVudWVzID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXIgdmVudWVzID0gbmV3IFZlbnVlU3RvcmFnZSgpO1xuICAgICAgICAgICAgdmFyIGNhdGVnb3JpZXMgPSBuZXcgQ2F0ZWdvcmllc1N0b3JhZ2UoKTtcbiAgICAgICAgICAgIHJldHVybiAkcS53aGVuKHZlbnVlcy5sb2FkKCRzY29wZS52ZW51ZXNSZXF1ZXN0KSkudGhlbihmdW5jdGlvbih2ZW51ZVN0b3JhZ2UpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gJHEud2hlbihjYXRlZ29yaWVzLmxvYWQoJHNjb3BlLnZlbnVlQ2F0ZWdvcmllc1JlcXVlc3QpKS50aGVuKGZ1bmN0aW9uKGNhdGVnb3J5U3RvcmFnZSkge1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUudmVudWVzID0gdmVudWVTdG9yYWdlLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS52ZW51ZUNhdGVnb3JpZXMgPSBjYXRlZ29yeVN0b3JhZ2UuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgQ2hhbmdlc05vdGlmaWVyLm5vdGlmeShDaGFuZ2VzTm90aWZpZXIuU1VCSl9WRU5VRVNfTE9BREVEKTtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmFkZFNjcm9sbEhhbmRsZXIodmVudWVzLCAkc2NvcGUudmVudWVzUmVxdWVzdCwgJ3ZlbnVlcycpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5pbml0VXNlciA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJHEud2hlbihVc2VyU2VydmljZS5nZXRDdXJyZW50VXNlcigpKS50aGVuKGZ1bmN0aW9uKHVzZXIpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUudXNlciA9IHVzZXI7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuYWRkU2Nyb2xsSGFuZGxlciA9IGZ1bmN0aW9uKHN0b3JhZ2VJbnN0YW5jZSwgcmVxdWVzdEluc3RhbmNlLCBzY29wZVN0b3JhZ2UpIHtcbiAgICAgICAgICAgICQod2luZG93KS5zY3JvbGwoZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICB2YXIgc3RhdGUgPSAkc3RhdGUuY3VycmVudC5uYW1lO1xuICAgICAgICAgICAgICAgIHZhciBsYXN0RWxlbWVudCA9IEh0bWxIZWxwZXIuZ2V0Q29vcmRzKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jYXRhbG9nLXdyYXBwZXI6bGFzdC1jaGlsZCcpKTtcbiAgICAgICAgICAgICAgICB2YXIgbGFzdEVsZW1lbnRIZWlnaHQgPSAkKCcuY2F0YWxvZy13cmFwcGVyOmxhc3QtY2hpbGQnKS5lcSgwKS5vdXRlckhlaWdodCgpO1xuICAgICAgICAgICAgICAgIGlmKHdpbmRvdy5wYWdlWU9mZnNldCArIHdpbmRvdy5zY3JlZW4uaGVpZ2h0ID49IGxhc3RFbGVtZW50IC0gKGxhc3RFbGVtZW50SGVpZ2h0ICsgNTApKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUmVhY2ggYm90dG9tXCIpO1xuICAgICAgICAgICAgICAgICAgICBpZihzdGF0ZSA9PT0gJ2hvbWUuJytzY29wZVN0b3JhZ2Upe1xuICAgICAgICAgICAgICAgICAgICAgICAgJHEud2hlbihzdG9yYWdlSW5zdGFuY2UubG9hZChyZXF1ZXN0SW5zdGFuY2UpKS50aGVuKGZ1bmN0aW9uKHN0b3JhZ2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZihzdG9yYWdlICYmIHN0b3JhZ2UuZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IodmFyIGk9IDA7IGkgPCBzdG9yYWdlLmRhdGEubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZVtzY29wZVN0b3JhZ2VdLnB1c2goc3RvcmFnZS5kYXRhW2ldKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUubm90aWZ5TW9yZUl0ZW1zTG9hZGVkKHNjb3BlU3RvcmFnZSwgc3RvcmFnZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcbiAgICAgICAgXG4gICAgICAgICRzY29wZS5ub3RpZnlNb3JlSXRlbXNMb2FkZWQgPSBmdW5jdGlvbihzY29wZVN0b3JhZ2UsIHN0b3JhZ2UpIHtcbiAgICAgICAgICAgIHN3aXRjaChzY29wZVN0b3JhZ2UpIHtcbiAgICAgICAgICAgICAgICBjYXNlICdldmVudHMnOlxuICAgICAgICAgICAgICAgICAgICBDaGFuZ2VzTm90aWZpZXIudmFsdWUgPSBzdG9yYWdlLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgIENoYW5nZXNOb3RpZmllci5ub3RpZnkoQ2hhbmdlc05vdGlmaWVyLlNVQkpfRVZFTlRTX0FEREVEKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgICAgICBjYXNlICd2ZW51ZXMnOlxuICAgICAgICAgICAgICAgICAgICBDaGFuZ2VzTm90aWZpZXIudmVudWVzX3ZhbHVlID0gc3RvcmFnZS5kYXRhO1xuICAgICAgICAgICAgICAgICAgICBDaGFuZ2VzTm90aWZpZXIubm90aWZ5KENoYW5nZXNOb3RpZmllci5TVUJKX1ZFTlVFU19BRERFRCk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5yZXZlYWxPblNjcm9sbCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBzY3JvbGxlZCA9ICQod2luZG93KS5zY3JvbGxUb3AoKTtcblxuICAgICAgICAgICAkKCcucmV2ZWFsT25TY3JvbGwuYW5pbWF0ZWQnKS5lYWNoKGZ1bmN0aW9uKGksIGVsZW1lbnQpIHtcbiAgICAgICAgICAgICAgIHZhciBfX3NlbGYgPSAkKHRoaXMpO1xuICAgICAgICAgICAgICAgdmFyIG9mZnNldFRvcCA9IF9fc2VsZi5vZmZzZXQoKS50b3A7XG4gICAgICAgICAgICAgICBpZiAoc2Nyb2xsZWQgKyAkc2NvcGUud2luX2hlaWdodF9wYWRkZWQgPCBvZmZzZXRUb3ApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgX19zZWxmLnJlbW92ZUNsYXNzKCdhbmltYXRlZCcpO1xuICAgICAgICAgICAgICAgICAgICAgICBfX3NlbGYucmVtb3ZlQ2xhc3MoX19zZWxmLmRhdGEoJ2FuaW1hdGlvbicpKTtcbiAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICQoXCIucmV2ZWFsT25TY3JvbGw6bm90KC5hbmltYXRlZClcIikuZWFjaChmdW5jdGlvbiAoaSwgZWxlbWVudCkge1xuICAgICAgICAgICAgICAgIHZhciBfX3NlbGYgPSAkKHRoaXMpO1xuICAgICAgICAgICAgICAgIHZhciBvZmZzZXRUb3AgPSBfX3NlbGYub2Zmc2V0KCkudG9wO1xuICAgICAgICAgICAgICAgIGlmIChzY3JvbGxlZCArICRzY29wZS53aW5faGVpZ2h0X3BhZGRlZCA+IG9mZnNldFRvcCkge1xuICAgICAgICAgICAgICAgICAgICBpZihfX3NlbGYuZGF0YSgndGltZW91dCcpICYmIGkgPiA1KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvL3dpbmRvdy5zZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9fc2VsZi5hZGRDbGFzcygnYW5pbWF0ZWQnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfX3NlbGYuYWRkQ2xhc3MoX19zZWxmLmRhdGEoJ2FuaW1hdGlvbicpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vfSwgMCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcbiAgICAgICAgXG4gICAgICAgICRzY29wZS50b2dnbGVJbmZvID0gZnVuY3Rpb24ob2JqZWN0KSB7XG4gICAgICAgICAgICB2YXIgZWxlbWVudCA9ICQoJyMnKyBvYmplY3QuY2xhc3NOYW1lLnRvTG93ZXJDYXNlKCkgKyAnLScgKyBvYmplY3QuX2lkKTtcbiAgICAgICAgICAgIGlmKGVsZW1lbnQuaGFzQ2xhc3MoJ2NhdGFsb2ctd3JhcHBlci0tZnVsbCcpKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmNsb3NlSW5mbygpO1xuICAgICAgICAgICAgICAgIC8vICRzY29wZS5jbG9zZUNsaWNrKGV2ZW50KTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLnNob3dJbmZvKG9iamVjdCk7XG4gICAgICAgICAgICAgICAgLy8gJHNjb3BlLm9uQ2xpY2soZXZlbnQpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5zaG93SW5mbyA9IGZ1bmN0aW9uKG9iamVjdCkge1xuICAgICAgICAgICAgdmFyIHRpdGxlID0gXCJWZW5WYXN0IFwiICsgb2JqZWN0LmNsYXNzTmFtZSArIFwiIC0gXCIgKyBvYmplY3QudGl0bGU7XG4gICAgICAgICAgICB2YXIgZG9tSWRlbnRmaWVyID0gb2JqZWN0LmNsYXNzTmFtZS50b0xvd2VyQ2FzZSgpICsnLScrIG9iamVjdC5faWQ7XG4gICAgICAgICAgICAkKCcjbWV0YS10aXRsZScpLmF0dHIoJ2NvbnRlbnQnLCB0aXRsZSk7XG4gICAgICAgICAgICAkKCd0aXRsZScpLnRleHQodGl0bGUpO1xuICAgICAgICAgICAgJCgnI21ldGEtdHlwZScpLmF0dHIoJ2NvbnRlbnQnLCBcIndlYnNpdGVcIik7XG4gICAgICAgICAgICAkKCcjbWV0YS1pbWFnZScpLmF0dHIoJ2NvbnRlbnQnLCBvYmplY3QucGljdHVyZSk7XG4gICAgICAgICAgICAkKCcjbWV0YS1kZXNjcmlwdGlvbicpLmF0dHIoJ2NvbnRlbnQnLCBvYmplY3QuZGVzY3JpcHRpb24pO1xuICAgICAgICAgICAgJCgnI21ldGEtdXJsJykuYXR0cignY29udGVudCcsICdodHRwOi8vdmVudmFzdC5jb20vIS8nKyBkb21JZGVudGZpZXIpO1xuICAgICAgICAgICAgJCgnLmNhdGFsb2ctd3JhcHBlcicpLnJlbW92ZUNsYXNzKCdjYXRhbG9nLXdyYXBwZXItLWZ1bGwnLCB7ZHVyYXRpb246IDUwMH0pO1xuICAgICAgICAgICAgTWFwU2VydmljZS5jZW50ZXJPbk9iamVjdChvYmplY3QpO1xuICAgICAgICAgICAgQW5jaG9yU21vb3RoU2Nyb2xsLnNjcm9sbFRvKGRvbUlkZW50Zmllcik7XG4gICAgICAgICAgICAkKCcjJyArIGRvbUlkZW50ZmllcikuYWRkQ2xhc3MoJ2NhdGFsb2ctd3JhcHBlci0tZnVsbCcsIHtkdXJhdGlvbjogNTAwfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLmNsb3NlSW5mbyA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJCgnLmNhdGFsb2ctd3JhcHBlcicpLnJlbW92ZUNsYXNzKCdjYXRhbG9nLXdyYXBwZXItLWZ1bGwnLCB7ZHVyYXRpb246IDUwMH0pO1xuICAgICAgICAgICAgJHNjb3BlLm1hcCA9IE1hcFNlcnZpY2UucmVzdG9yZU1hcCgpO1xuICAgICAgICB9O1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgICRzY29wZS5pbml0KCk7XG4gICAgfSk7XG59KSgpOyIsIi8qKlxuICogQ3JlYXRlZCBieSBvZW0gb24gMy8xOS8xNi5cbiAqL1xuKGZ1bmN0aW9uKCl7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG5cbiAgICAvKipcbiAgICAgKiBUaGlzIGNvbnRyb2xsZXIgaXMgZGVhbGluZyB3aXRoIGV2ZW50cyBsaXN0IHZpZXdcbiAgICAgKi9cbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLmNvbnRyb2xsZXJzJykuY29udHJvbGxlcignTWFwQ3RybCcsIGZ1bmN0aW9uKCRxLCAkc2NvcGUsICRzdGF0ZSwgVXNlclNlcnZpY2UsIFZlblZhc3RSZXF1ZXN0LCBNYXBTZXJ2aWNlLCBDaGFuZ2VzTm90aWZpZXIsIHVpR21hcEdvb2dsZU1hcEFwaSwgSHRtbEhlbHBlcil7XG5cbiAgICAgICAgLy8gYW5ndWxhci5lbGVtZW50KGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgY2F0ZWdvcnkgPSAkKCcuY2F0ZWdvcnknKTtcbiAgICAgICAgICAgIHZhciBzdGlja3lTZWxlY3RvciA9ICcuc3RpY2t5JztcbiAgICAgICAgICAgIHZhciBlbCA9ICQoc3RpY2t5U2VsZWN0b3IpO1xuICAgICAgICAgICAgdmFyIHN0aWNreVRvcCA9ICQoc3RpY2t5U2VsZWN0b3IpLm9mZnNldCgpLnRvcDsgLy8gcmV0dXJucyBudW1iZXJcbiAgICAgICAgICAgIHZhciBzdGlja3lIZWlnaHQgPSAkKHN0aWNreVNlbGVjdG9yKS5oZWlnaHQoKTtcblxuICAgICAgICAgICAgJCh3aW5kb3cpLnNjcm9sbChmdW5jdGlvbigpe1xuICAgICAgICAgICAgICAgIGlmKCQoJ2Zvb3RlcicpLm9mZnNldCgpKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBsaW1pdCA9ICQoJ2Zvb3RlcicpLm9mZnNldCgpLnRvcCAtIHN0aWNreUhlaWdodCAtIDIwIC0gMjAwO1xuICAgICAgICAgICAgICAgICAgICB2YXIgd2luZG93VG9wID0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpOyAvLyByZXR1cm5zIG51bWJlclxuICAgICAgICAgICAgICAgICAgICBpZiAod2luZG93VG9wID4gMjAwICYmIHN0aWNreVRvcCA8IHdpbmRvd1RvcCl7XG4gICAgICAgICAgICAgICAgICAgICAgICBlbC5jc3MoeyBwb3NpdGlvbjogJ2ZpeGVkJywgdG9wOiAwLCBcIm1hcmdpbi1sZWZ0XCI6IFwiNzIlXCJ9KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsLmNzcyh7IHBvc2l0aW9uOiAnJywgdG9wOiBcIlwiLCBcIm1hcmdpbi1sZWZ0XCI6IFwiXCJ9KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIGlmIChsaW1pdCA8IHdpbmRvd1RvcCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGRpZmYgPSBsaW1pdCAtIHdpbmRvd1RvcDtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsLmNzcyh7dG9wOiBkaWZmfSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgLy8gfSk7XG5cbiAgICAgICAgJHNjb3BlLmluaXRpYWxpemVNYXAgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRxLndoZW4oTWFwU2VydmljZS5pbml0aWFsaXplKCRzY29wZS5nZXRTdGF0ZVJlcXVlc3QoKSkpLnRoZW4oZnVuY3Rpb24obWFwU2VydmljZSkge1xuICAgICAgICAgICAgICAgICRzY29wZS5tYXBTZXJ2aWNlID0gbWFwU2VydmljZTtcbiAgICAgICAgICAgICAgICB1aUdtYXBHb29nbGVNYXBBcGkudGhlbihmdW5jdGlvbihtYXBzKSB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5sb2FkTWFya2VycygpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLmxvYWRNYXJrZXJzID0gZnVuY3Rpb24ob2JqZWN0cykge1xuICAgICAgICAgICAgdmFyIHN0YXRlID0gJHN0YXRlLmN1cnJlbnQubmFtZTtcbiAgICAgICAgICAgIHN3aXRjaCAoc3RhdGUpIHtcbiAgICAgICAgICAgICAgICBjYXNlICdob21lLmV2ZW50cyc6XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5sb2FkRXZlbnRNYXJrZXJzKG9iamVjdHMpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlICdob21lLnZlbnVlcyc6XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5sb2FkVmVudWVNYXJrZXJzKG9iamVjdHMpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICB9O1xuICAgICAgICBcbiAgICAgICAgJHNjb3BlLmxvYWRFdmVudE1hcmtlcnMgPSBmdW5jdGlvbihvYmplY3RzKSB7XG4gICAgICAgICAgICBpZihvYmplY3RzKVxuICAgICAgICAgICAgICAgICRzY29wZS5tYXBTZXJ2aWNlLnNldEV2ZW50TWFya2VycyhvYmplY3RzKTtcbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICAkc2NvcGUubWFwU2VydmljZS5zZXRFdmVudE1hcmtlcnMoJHNjb3BlLmV2ZW50cyk7XG5cbiAgICAgICAgICAgICRzY29wZS5tYXBTZXJ2aWNlLnNob3dVc2VyTWFya2VyKCk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLmxvYWRWZW51ZU1hcmtlcnMgPSBmdW5jdGlvbihvYmplY3RzKSB7XG4gICAgICAgICAgICBpZihvYmplY3RzKVxuICAgICAgICAgICAgICAgICRzY29wZS5tYXBTZXJ2aWNlLnNldFZlbnVlTWFya2VycyhvYmplY3RzKTtcbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICAkc2NvcGUubWFwU2VydmljZS5zZXRWZW51ZU1hcmtlcnMoJHNjb3BlLnZlbnVlcyk7XG5cbiAgICAgICAgICAgICRzY29wZS5tYXBTZXJ2aWNlLnNob3dVc2VyTWFya2VyKCk7XG4gICAgICAgIH07XG4gICAgICAgIFxuXG4gICAgICAgICRzY29wZS5yZWxvYWRNYXJrZXJzID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXIgc3RhdGUgPSAkc3RhdGUuY3VycmVudC5uYW1lO1xuICAgICAgICAgICAgc3dpdGNoIChzdGF0ZSkge1xuICAgICAgICAgICAgICAgIGNhc2UgJ2hvbWUuZXZlbnRzJzpcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnJlbG9hZEV2ZW50TWFya2VycygpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlICdob21lLnZlbnVlcyc6XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5yZWxvYWRWZW51ZU1hcmtlcnMoKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIFxuICAgICAgICAkc2NvcGUucmVsb2FkRXZlbnRNYXJrZXJzID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkcS53aGVuKE1hcFNlcnZpY2UuaW5pdGlhbGl6ZSgkc2NvcGUuZXZlbnRSZXF1ZXN0KSkudGhlbihmdW5jdGlvbihtYXBTZXJ2aWNlKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLm1hcFNlcnZpY2UgPSBtYXBTZXJ2aWNlO1xuICAgICAgICAgICAgICAgICRzY29wZS5tYXBTZXJ2aWNlLmRlbGV0ZUFsbE1hcmtlcnMoKTtcbiAgICAgICAgICAgICAgICBpZih0eXBlb2YgQ2hhbmdlc05vdGlmaWVyLnZhbHVlICE9PSBcInVuZGVmaW5lZFwiICYmIENoYW5nZXNOb3RpZmllci52YWx1ZSAhPT0gbnVsbCl7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5sb2FkTWFya2VycyhDaGFuZ2VzTm90aWZpZXIudmFsdWUpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5sb2FkTWFya2VycygpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuICAgICAgICBcbiAgICAgICAgJHNjb3BlLnJlbG9hZFZlbnVlTWFya2VycyA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJHEud2hlbihNYXBTZXJ2aWNlLmluaXRpYWxpemUoJHNjb3BlLnZlbnVlc1JlcXVlc3QpKS50aGVuKGZ1bmN0aW9uKG1hcFNlcnZpY2UpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUubWFwU2VydmljZSA9IG1hcFNlcnZpY2U7XG4gICAgICAgICAgICAgICAgJHNjb3BlLm1hcFNlcnZpY2UuZGVsZXRlQWxsTWFya2VycygpO1xuICAgICAgICAgICAgICAgIGlmKHR5cGVvZiBDaGFuZ2VzTm90aWZpZXIudmVudWVzX3ZhbHVlICE9PSBcInVuZGVmaW5lZFwiICYmIENoYW5nZXNOb3RpZmllci52ZW51ZXNfdmFsdWUgIT09IG51bGwpe1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUubG9hZE1hcmtlcnMoQ2hhbmdlc05vdGlmaWVyLnZlbnVlc192YWx1ZSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmxvYWRNYXJrZXJzKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG4gICAgICAgIFxuICAgICAgICAkc2NvcGUuYWRkTWFya2VycyA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyIHN0YXRlID0gJHN0YXRlLmN1cnJlbnQubmFtZTtcbiAgICAgICAgICAgIHN3aXRjaCAoc3RhdGUpIHtcbiAgICAgICAgICAgICAgICBjYXNlICdob21lLmV2ZW50cyc6XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5hZGRFdmVudE1hcmtlcnMoKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSAnaG9tZS52ZW51ZXMnOlxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYWRkVmVudWVNYXJrZXJzKCk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5nZXRTdGF0ZVJlcXVlc3QgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhciBzdGF0ZSA9ICRzdGF0ZS5jdXJyZW50Lm5hbWU7XG4gICAgICAgICAgICBzd2l0Y2ggKHN0YXRlKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAnaG9tZS5ldmVudHMnOlxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJHNjb3BlLmV2ZW50UmVxdWVzdDtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSAnaG9tZS52ZW51ZXMnOlxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJHNjb3BlLnZlbnVlc1JlcXVlc3Q7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgICRzY29wZS5hZGRWZW51ZU1hcmtlcnMgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmKENoYW5nZXNOb3RpZmllci52ZW51ZXNfdmFsdWUpXG4gICAgICAgICAgICAgICAgJHNjb3BlLm1hcFNlcnZpY2UuYWRkVmVudWVNYXJrZXJzKENoYW5nZXNOb3RpZmllci52ZW51ZXNfdmFsdWUpO1xuICAgICAgICB9O1xuICAgICAgICBcbiAgICAgICAgJHNjb3BlLmFkZEV2ZW50TWFya2VycyA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYoQ2hhbmdlc05vdGlmaWVyLnZhbHVlKVxuICAgICAgICAgICAgICAgICRzY29wZS5tYXBTZXJ2aWNlLmFkZEV2ZW50TWFya2VycyhDaGFuZ2VzTm90aWZpZXIudmFsdWUpO1xuICAgICAgICB9O1xuXG4gICAgICAgIGFuZ3VsYXIuZWxlbWVudChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgJHNjb3BlLmluaXRpYWxpemVNYXAoKTtcbiAgICAgICAgfSk7XG5cblxuICAgICAgICBDaGFuZ2VzTm90aWZpZXIuc3Vic2NyaWJlKENoYW5nZXNOb3RpZmllci5TVUJKX0VWRU5UU19MT0FERUQsICRzY29wZSwgJHNjb3BlLnJlbG9hZE1hcmtlcnMpO1xuICAgICAgICBDaGFuZ2VzTm90aWZpZXIuc3Vic2NyaWJlKENoYW5nZXNOb3RpZmllci5TVUJKX1ZFTlVFU19MT0FERUQsICRzY29wZSwgJHNjb3BlLnJlbG9hZE1hcmtlcnMpO1xuXG4gICAgICAgIENoYW5nZXNOb3RpZmllci5zdWJzY3JpYmUoQ2hhbmdlc05vdGlmaWVyLlNVQkpfRVZFTlRTX0FEREVELCAkc2NvcGUsICRzY29wZS5hZGRNYXJrZXJzKTtcbiAgICAgICAgQ2hhbmdlc05vdGlmaWVyLnN1YnNjcmliZShDaGFuZ2VzTm90aWZpZXIuU1VCSl9WRU5VRVNfQURERUQsICRzY29wZSwgJHNjb3BlLmFkZE1hcmtlcnMpO1xuICAgIH0pO1xufSkoKTsiLCIvKipcbiAqIENyZWF0ZWQgYnkgb2VtIG9uIDMvMTkvMTYuXG4gKi9cbihmdW5jdGlvbigpe1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG4gICAgLyoqXG4gICAgICogVGhpcyBjb250cm9sbGVyIGlzIGRlYWxpbmcgd2l0aCB2ZW51ZXMgbGlzdCB2aWV3XG4gICAgICovXG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5jb250cm9sbGVycycpLmNvbnRyb2xsZXIoJ1ZlbnVlRmlsdGVyQ3RybCcsIGZ1bmN0aW9uKCRxLCAkdGltZW91dCwgJHNjb3BlLCAkcm9vdFNjb3BlLCAkc3RhdGUsIEh0bWxIZWxwZXIsIENoYW5nZXNOb3RpZmllciwgVmVudWVTdG9yYWdlKXtcblxuICAgICAgICB2YXIgX3RpbWVvdXQ7XG4gICAgICAgICRzY29wZS50ZXh0U2VhcmNoSW5BY3Rpb24gPSBmYWxzZTtcbiAgICAgICAgJHNjb3BlLnRleHRTZWFyY2hmaXJzdENhbGwgPSB0cnVlO1xuXG4gICAgICAgICRzY29wZS5nZXRWZW51ZVJlcXVlc3QgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhciBzdGF0ZSA9ICRzdGF0ZS5jdXJyZW50Lm5hbWU7XG4gICAgICAgICAgICBzd2l0Y2ggKHN0YXRlKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAnaG9tZS52ZW51ZXMnOlxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJHNjb3BlLnZlbnVlc1JlcXVlc3Q7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgJ29yZ2FuaXplci52ZW51ZXMnOlxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJHNjb3BlLm9yZ1ZlbnVlc1JlcXVlc3Q7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuXG4gICAgICAgIGFuZ3VsYXIuZWxlbWVudChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgSHRtbEhlbHBlci5pbml0KCk7XG5cbiAgICAgICAgICAgIHZhciBjYXRlZ29yeSA9ICRzY29wZS5nZXRWZW51ZVJlcXVlc3QoKS5jYXRlZ29yeTtcbiAgICAgICAgICAgIHZhciBjYXRlZ29yaWVzID0gJHNjb3BlLnZlbnVlQ2F0ZWdvcmllcztcbiAgICAgICAgICAgIHZhciBjYXRlZ29yeU5hbWUgPSBudWxsO1xuICAgICAgICAgICAgaWYoY2F0ZWdvcnkgIT09IG51bGwgJiYgdHlwZW9mIGNhdGVnb3JpZXMgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgZm9yKHZhciBpID0gMDsgaSA8IGNhdGVnb3JpZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgaWYoY2F0ZWdvcmllc1tpXS5faWQgPT0gY2F0ZWdvcnkpe1xuICAgICAgICAgICAgICAgICAgICAgICAgY2F0ZWdvcnlOYW1lID0gY2F0ZWdvcmllc1tpXS5uYW1lO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoY2F0ZWdvcnlOYW1lICE9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgJHRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICQoJyNjYXRlZ29yeV9zZWFyY2gnKS5jaGlsZHJlbignc3BhbicpLnRleHQoY2F0ZWdvcnlOYW1lKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgbmV3IEh0bWxIZWxwZXIuU3R5bGVkRHJvcERvd24oJyNjYXRlZ29yeV9zZWFyY2gnLCAkc2NvcGUudmVudWVzUmVxdWVzdC5jYXRlZ29yeSk7XG4gICAgICAgICAgICBuZXcgSHRtbEhlbHBlci5TdHlsZWREcm9wRG93bignI3doZW5fc2VhcmNoJywgJHNjb3BlLnZlbnVlc1JlcXVlc3QuY2F0ZWdvcnkpO1xuICAgICAgICAgICAgdmFyIGlucHV0ID0gJCgnLmxvY2F0aW9uLWlucHV0JykuZmlyc3QoKTtcbiAgICAgICAgICAgICRzY29wZS5hdXRvY29tcGxldGUgPSBuZXcgZ29vZ2xlLm1hcHMucGxhY2VzLkF1dG9jb21wbGV0ZShpbnB1dFswXSwge3R5cGVzOiBbJyhjaXRpZXMpJ119KTtcbiAgICAgICAgICAgICRzY29wZS5hdXRvY29tcGxldGUuYWRkTGlzdGVuZXIoJ3BsYWNlX2NoYW5nZWQnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgdmFyIHBsYWNlID0gJHNjb3BlLmF1dG9jb21wbGV0ZS5nZXRQbGFjZSgpO1xuICAgICAgICAgICAgICAgICRzY29wZS5nZXRWZW51ZVJlcXVlc3QoKS5zZXRXaGVyZShwbGFjZS5nZW9tZXRyeS5sb2NhdGlvbi5sYXQoKSwgcGxhY2UuZ2VvbWV0cnkubG9jYXRpb24ubG5nKCkpO1xuICAgICAgICAgICAgICAgICRzY29wZS5zZWFyY2goKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcblxuICAgICAgICAkc2NvcGUuc2VhcmNoQnlUZXh0ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgaWYoX3RpbWVvdXQpeyAvL2lmIHRoZXJlIGlzIGFscmVhZHkgYSB0aW1lb3V0IGluIHByb2Nlc3MgY2FuY2VsIGl0XG4gICAgICAgICAgICAgICAgJHRpbWVvdXQuY2FuY2VsKF90aW1lb3V0KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF90aW1lb3V0ID0gJHRpbWVvdXQoZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICAkc2NvcGUuc2VhcmNoKCk7XG4gICAgICAgICAgICAgICAgX3RpbWVvdXQgPSBudWxsO1xuICAgICAgICAgICAgfSwgNTAwKTtcbiAgICAgICAgfTtcblxuXG4gICAgICAgICRzY29wZS5zZWxlY3RDYXRlZ29yeSA9IGZ1bmN0aW9uKGNhdGVnb3J5KSB7XG4gICAgICAgICAgICBpZihjYXRlZ29yeSA9PT0gbnVsbCl7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmdldFZlbnVlUmVxdWVzdCgpLmNhdGVnb3J5ID0gbnVsbDtcbiAgICAgICAgICAgICAgICAkKCcjY2F0ZWdvcnlfc2VhcmNoJykuY2hpbGRyZW4oJ3NwYW4nKS50ZXh0KCctLSBBbGwgLS0nKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLmdldFZlbnVlUmVxdWVzdCgpLmNhdGVnb3J5ID0gY2F0ZWdvcnkuX2lkO1xuICAgICAgICAgICAgICAgICQoJyNjYXRlZ29yeV9zZWFyY2gnKS5jaGlsZHJlbignc3BhbicpLnRleHQoY2F0ZWdvcnkubmFtZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAkc2NvcGUuc2VhcmNoKCk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLm9yZ2FuaXplclNlYXJjaCA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgSHRtbEhlbHBlci5zaG93TG9hZGVyKCk7XG4gICAgICAgICAgICAkc2NvcGUub3JnVmVudWVzUmVxdWVzdC5wYWdlID0gMDtcbiAgICAgICAgICAgICRzY29wZS5vcmdWZW51ZXMgPSBbXTtcbiAgICAgICAgICAgICRzY29wZS5vcmdWZW51ZXNSZXF1ZXN0LnR5cGUgPSAndmVudWVzJztcbiAgICAgICAgICAgICRzY29wZS5vcmdWZW51ZXNSZXF1ZXN0Lmhhc01vcmVQYWdlcyA9IHRydWU7XG4gICAgICAgICAgICAkc2NvcGUub3JnVmVudWVzUmVxdWVzdC5vcmdhbml6ZXIgPSB0cnVlO1xuICAgICAgICAgICAgdmFyIHZlbnVlcyA9IG5ldyBWZW51ZVN0b3JhZ2UoKTtcbiAgICAgICAgICAgIHJldHVybiAkcS53aGVuKHZlbnVlcy5sb2FkKCRzY29wZS5vcmdWZW51ZXNSZXF1ZXN0KSkudGhlbihmdW5jdGlvbih2ZW51ZVN0b3JhZ2UpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUub3JnVmVudWVzID0gdmVudWVTdG9yYWdlLmRhdGE7XG4gICAgICAgICAgICAgICAgQ2hhbmdlc05vdGlmaWVyLm9yZ192ZW51ZXMgPSAkc2NvcGUub3JnVmVudWVzO1xuICAgICAgICAgICAgICAgIENoYW5nZXNOb3RpZmllci5ub3RpZnkoQ2hhbmdlc05vdGlmaWVyLlNVQkpfT1JHX1ZFTlVFU19MT0FERUQpO1xuICAgICAgICAgICAgICAgIEh0bWxIZWxwZXIuaGlkZUxvYWRlcigpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLnVzZXJTZWFyY2ggPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIEh0bWxIZWxwZXIuc2hvd0xvYWRlcigpO1xuICAgICAgICAgICAgJHNjb3BlLnZlbnVlc1JlcXVlc3QucGFnZSA9IDA7XG4gICAgICAgICAgICAkc2NvcGUudmVudWVzID0gW107XG4gICAgICAgICAgICAkc2NvcGUudmVudWVzUmVxdWVzdC50eXBlID0gJ3ZlbnVlcyc7XG4gICAgICAgICAgICAkc2NvcGUudmVudWVzUmVxdWVzdC5oYXNNb3JlUGFnZXMgPSB0cnVlO1xuICAgICAgICAgICAgdmFyIHZlbnVlcyA9IG5ldyBWZW51ZVN0b3JhZ2UoKTtcbiAgICAgICAgICAgIHJldHVybiAkcS53aGVuKHZlbnVlcy5sb2FkKCRzY29wZS52ZW51ZXNSZXF1ZXN0KSkudGhlbihmdW5jdGlvbih2ZW51ZVN0b3JhZ2UpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUudmVudWVzID0gdmVudWVTdG9yYWdlLmRhdGE7XG4gICAgICAgICAgICAgICAgQ2hhbmdlc05vdGlmaWVyLnZlbnVlc192YWx1ZSA9ICRzY29wZS52ZW51ZXM7XG4gICAgICAgICAgICAgICAgQ2hhbmdlc05vdGlmaWVyLm5vdGlmeShDaGFuZ2VzTm90aWZpZXIuU1VCSl9WRU5VRVNfTE9BREVEKTtcbiAgICAgICAgICAgICAgICBIdG1sSGVscGVyLmhpZGVMb2FkZXIoKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG5cbiAgICAgICAgJHNjb3BlLnNlYXJjaCA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyIHN0YXRlID0gJHN0YXRlLmN1cnJlbnQubmFtZTtcbiAgICAgICAgICAgIHN3aXRjaCAoc3RhdGUpIHtcbiAgICAgICAgICAgICAgICBjYXNlICdob21lLnZlbnVlcyc6XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS51c2VyU2VhcmNoKCk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgJ29yZ2FuaXplci52ZW51ZXMnOlxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUub3JnYW5pemVyU2VhcmNoKCk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgIH0pO1xufSkoKTsiLCIvKipcbiAqIENyZWF0ZWQgYnkgb2VtIG9uIDMvMTkvMTYuXG4gKi9cbihmdW5jdGlvbigpe1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG5cbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLmNvbnRyb2xsZXJzJykuY29udHJvbGxlcignVmVudWVzQ3RybCcsIGZ1bmN0aW9uKCRzY29wZSwgSHRtbEhlbHBlcil7XG4gICAgICAgICRzY29wZS4kd2F0Y2goJ3ZlbnVlcycsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYoJHNjb3BlLnZlbnVlcykge1xuICAgICAgICAgICAgICAgIEh0bWxIZWxwZXIuaGlkZUxvYWRlcigpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9KTtcbn0pKCk7IiwiLyoqXG4gKiBDcmVhdGVkIGJ5IG9lbSBvbiAzLzE5LzE2LlxuICovXG4oZnVuY3Rpb24oKXtcbiAgICBcInVzZSBzdHJpY3RcIjtcblxuICAgIC8qKlxuICAgICAqIFRoaXMgY29udHJvbGxlciBpcyBkZWFsaW5nIHdpdGggZXZlbnRzIGxpc3Qgdmlld1xuICAgICAqL1xuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuY29udHJvbGxlcnMnKS5jb250cm9sbGVyKCdPcmdDYWxlbmRhckN0cmwnLCBmdW5jdGlvbigkcSwgJHNjb3BlLCAkdGltZW91dCwgJHN0YXRlLCBIdG1sSGVscGVyLCBFdmVudHNTdG9yYWdlLCBDaGFuZ2VzTm90aWZpZXIpe1xuXG4gICAgICAgIHZhciBzdGF0ZSA9ICRzdGF0ZS5jdXJyZW50Lm5hbWU7XG4gICAgICAgIHN3aXRjaCAoc3RhdGUpIHtcbiAgICAgICAgICAgIGNhc2UgJ29yZ2FuaXplci5ldmVudHMnOlxuICAgICAgICAgICAgICAgICQoXCIjY2FsZW5kYXItZmlsdGVyXCIpLmlvbkNhbGVuZGFyKHtcbiAgICAgICAgICAgICAgICAgICAgbGFuZzogXCJlblwiLFxuICAgICAgICAgICAgICAgICAgICBzdW5kYXlGaXJzdDogZmFsc2UsXG4gICAgICAgICAgICAgICAgICAgIHllYXJzOiBcIjIwMTUtMjAxOFwiLFxuICAgICAgICAgICAgICAgICAgICBmb3JtYXQ6IFwiWVlZWS1NTS1ERFwiLFxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrOiBmdW5jdGlvbihkYXRlKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICQoJyN3aGVuX3NlYXJjaCcpLmNoaWxkcmVuKCdzcGFuJykudGV4dChkYXRlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICR0aW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5vcmdFdmVudFJlcXVlc3Qud2hlbiA9IGRhdGU7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5vcmdFdmVudFJlcXVlc3QuZGF0ZSA9IGRhdGU7XG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuc2VhcmNoKCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgJ29yZ2FuaXplci52ZW51ZXMnOlxuICAgICAgICAgICAgICAgICQoXCIjY2FsZW5kYXItZmlsdGVyXCIpLmFkZENsYXNzKCdoaWRkZW4nKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuXG4gICAgICAgICRzY29wZS5zZWFyY2ggPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIEh0bWxIZWxwZXIuc2hvd0xvYWRlcigpO1xuICAgICAgICAgICAgJHNjb3BlLm9yZ0V2ZW50UmVxdWVzdC5wYWdlID0gMDtcbiAgICAgICAgICAgICRzY29wZS5vcmdFdmVudHMgPSBbXTtcbiAgICAgICAgICAgICRzY29wZS5vcmdFdmVudFJlcXVlc3QudHlwZSA9ICdldmVudHMnO1xuICAgICAgICAgICAgJHNjb3BlLm9yZ0V2ZW50UmVxdWVzdC5oYXNNb3JlUGFnZXMgPSB0cnVlO1xuICAgICAgICAgICAgdmFyIGV2ZW50cyA9IG5ldyBFdmVudHNTdG9yYWdlKCk7XG4gICAgICAgICAgICByZXR1cm4gJHEud2hlbihldmVudHMubG9hZCgkc2NvcGUub3JnRXZlbnRSZXF1ZXN0KSkudGhlbihmdW5jdGlvbihldmVudFN0b3JhZ2UpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIlN0b3JhZ2U6IFwiLCBldmVudFN0b3JhZ2UpO1xuICAgICAgICAgICAgICAgICRzY29wZS5vcmdFdmVudHMgPSBldmVudFN0b3JhZ2UuZGF0YTtcbiAgICAgICAgICAgICAgICBDaGFuZ2VzTm90aWZpZXIub3JnX3ZhbHVlID0gJHNjb3BlLm9yZ0V2ZW50cztcbiAgICAgICAgICAgICAgICBDaGFuZ2VzTm90aWZpZXIubm90aWZ5KENoYW5nZXNOb3RpZmllci5TVUJKX09SR19FVkVOVFNfTE9BREVELCAkc2NvcGUub3JnRXZlbnRzKTtcbiAgICAgICAgICAgICAgICBIdG1sSGVscGVyLmhpZGVMb2FkZXIoKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgLy8gJHNjb3BlLm9yZ0V2ZW50UmVxdWVzdC5nZXRSZXF1ZXN0T2JqZWN0KClcbiAgICAgICAgfTtcbiAgICB9KTtcbn0pKCk7IiwiKGZ1bmN0aW9uKCl7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG5cblxuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuY29udHJvbGxlcnMnKS5jb250cm9sbGVyKCdFdmVudENyZWF0ZUN0cmwnLCBmdW5jdGlvbigkc2NvcGUsICRyb290U2NvcGUsICR0aW1lb3V0LCAkcSwgJHN0YXRlLCBIdG1sSGVscGVyLCBVcGxvYWQsIFZlblZhc3RSZXF1ZXN0LCBFdmVudHNTdG9yYWdlLCBWZW51ZVN0b3JhZ2UsIEV2ZW50LCBWZW51ZSwgTWFwU2VydmljZSkge1xuXG4gICAgICAgIHZhciBfdGltZW91dDtcblxuICAgICAgICAkc2NvcGUuaW5pdCA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYoISRyb290U2NvcGUubmV3RXZlbnQpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUubmV3RXZlbnQgPSB7fTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLm5ld0V2ZW50ID0gJHJvb3RTY29wZS5uZXdFdmVudDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgICRzY29wZS5lcnJvcnMgPSBbXTtcblxuICAgICAgICAgICAgJHNjb3BlLnZlbnVlQ3JlYXRlZCA9IGZhbHNlO1xuXG4gICAgICAgICAgICBpZigkc2NvcGUub3JnVmVudWVzICYmICRzY29wZS5vcmdWZW51ZXMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgICRzY29wZS5uZXdFdmVudC5jcmVhdGVWZW51ZSA9IGZhbHNlO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUubmV3RXZlbnQuY3JlYXRlVmVudWUgPSB0cnVlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAkKCcjY2F0ZWdvcnknKS5zZWxlY3QyKCk7XG4gICAgICAgICAgICAkKCcjdmVudWUnKS5zZWxlY3QyKCk7XG5cbiAgICAgICAgICAgICRzY29wZS5ldmVudE1hcCA9IE1hcFNlcnZpY2UuaW5pdGlhbGl6ZShuZXcgVmVuVmFzdFJlcXVlc3Qoe3doZXJlOiBudWxsfSksICdldmVudC1tYXAnKTtcblxuICAgICAgICAgICAgJCgnI3N0YXJ0QXQnKS5kYXRldGltZXBpY2tlcih7XG4gICAgICAgICAgICAgICAgdGltZXBpY2tlcjp0cnVlLFxuICAgICAgICAgICAgICAgIGZvcm1hdDogXCJZLmQubSBIOmlcIixcbiAgICAgICAgICAgICAgICBzdGFydERhdGU6IG1vbWVudCgpLFxuICAgICAgICAgICAgICAgIG1pbkRhdGU6IG1vbWVudCgpLFxuICAgICAgICAgICAgICAgIG9uQ2hhbmdlRGF0ZVRpbWU6IGZ1bmN0aW9uKGN1cnJlbnQsICRpbnB1dCkge1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUubmV3RXZlbnQuc3RhcnRfYXQgPSAkaW5wdXQudmFsKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG5cblxuXG4gICAgICAgICAgICAkKCcjZW5kQXQnKS5kYXRldGltZXBpY2tlcih7XG4gICAgICAgICAgICAgICAgdGltZXBpY2tlcjp0cnVlLFxuICAgICAgICAgICAgICAgIGZvcm1hdDogXCJZLmQubSBIOmlcIixcbiAgICAgICAgICAgICAgICBzdGFydERhdGU6IG1vbWVudCgpLmFkZCgzMCwgJ20nKSxcbiAgICAgICAgICAgICAgICBtaW5EYXRlOiBtb21lbnQoKS5hZGQoMzAsICdtJyksXG4gICAgICAgICAgICAgICAgb25DaGFuZ2VEYXRlVGltZTogZnVuY3Rpb24oY3VycmVudCwgJGlucHV0KSB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5uZXdFdmVudC5lbmRfYXQgPSAkaW5wdXQudmFsKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIEh0bWxIZWxwZXIuaGlkZUxvYWRlcigpO1xuICAgICAgICAgICAgJCgnLmwtY29udGVudCcpLndpZHRoKCcxMDAlJyk7XG5cbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUucmVtb3ZlUGljdHVyZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJHNjb3BlLm5ld0V2ZW50LnBpY3R1cmUgPSBudWxsO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5pbml0T3JnVmVudWVzU2VsZWN0ID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZih0eXBlb2YgJHNjb3BlLm5ld0V2ZW50LnZlbnVlID09PSAnb2JqZWN0JyAmJiAkc2NvcGUubmV3RXZlbnQudmVudWUgIT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICAkKCcjYWRkcmVzcy1zZWFyY2gnKS5oaWRlKCk7XG4gICAgICAgICAgICAgICAgJHNjb3BlLiR3YXRjaCgnb3JnVmVudWVzJywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICQoJyN2ZW51ZScpLnZhbCgkc2NvcGUubmV3RXZlbnQudmVudWUuX2lkKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuXG4gICAgICAgICRzY29wZS50b2dnbGVBZGRyZXNzU2VhcmNoID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZigkc2NvcGUubmV3RXZlbnQudmVudWUgPT09IFwiXCIgfHwgdHlwZW9mICRzY29wZS5uZXdFdmVudC52ZW51ZSA9PT0gXCJ1bmRlZmluZWRcIil7XG4gICAgICAgICAgICAgICAgJCgnI2FkZHJlc3Mtc2VhcmNoJykuc2hvdygnc2xvdycpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAkKCcjYWRkcmVzcy1zZWFyY2gnKS5oaWRlKCdzbG93Jyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLmNyZWF0ZUxvY2F0aW9uID0gZnVuY3Rpb24obGF0LCBsbmcpIHtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgdHlwZTogXCJQb2ludFwiLFxuICAgICAgICAgICAgICAgIGNvb3JkaW5hdGVzOiBbbG5nLCBsYXRdXG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLmNyZWF0ZUV2ZW50ID0gZnVuY3Rpb24oJGV2ZW50KXtcbiAgICAgICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgJHNjb3BlLm5ld0V2ZW50LmNhdGVnb3JpZXMgPSBbJCgnI2NhdGVnb3J5JykudmFsKCldO1xuICAgICAgICAgICAgLy9NZWFucyB0aGF0IGV2ZW50IHdlcmUgcGlja2VkIGFmdGVyIGFkZHJlc3Mgc2VhcmNoXG4gICAgICAgICAgICAkcS53aGVuKCRzY29wZS5zZXRFdmVudEF0dHJpYnV0ZXMoKSkudGhlbihmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICB2YXIgZXZlbnQgPSBuZXcgRXZlbnQoJHNjb3BlLm5ld0V2ZW50KTtcbiAgICAgICAgICAgICAgICAkcS53aGVuKGV2ZW50LnNhdmVPbkJhY2tlbmQoKSkudGhlbihmdW5jdGlvbihyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgICAgICAkc3RhdGUuZ28oJ29yZ2FuaXplci5ldmVudHMnLCB7fSwge3JlbG9hZDogdHJ1ZX0pO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cblxuXG4gICAgICAgICRzY29wZS5zZXRFdmVudEF0dHJpYnV0ZXMgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmKHR5cGVvZiAkc2NvcGUubmV3RXZlbnQuYWRkcmVzcyA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgJHNjb3BlLm5ld0V2ZW50LnZlbnVlID09PSBcInVuZGVmaW5lZFwiKSB7XG4gICAgICAgICAgICAgICAgdmFyIGFkZHJlc3NPYmogPSAkc2NvcGUubmV3RXZlbnQuYWRkcmVzcztcbiAgICAgICAgICAgICAgICB2YXIgbGF0ID0gYWRkcmVzc09iai5nZW9tZXRyeS5sb2NhdGlvbi5sYXQoKTtcbiAgICAgICAgICAgICAgICB2YXIgbG5nID0gYWRkcmVzc09iai5nZW9tZXRyeS5sb2NhdGlvbi5sbmcoKTtcbiAgICAgICAgICAgICAgICAkc2NvcGUubmV3RXZlbnQuYWRkcmVzcyA9IGFkZHJlc3NPYmouZm9ybWF0dGVkX2FkZHJlc3M7XG4gICAgICAgICAgICAgICAgJHNjb3BlLm5ld0V2ZW50LmxvY2F0aW9uID0gJHNjb3BlLmNyZWF0ZUxvY2F0aW9uKGxhdCwgbG5nKTtcbiAgICAgICAgICAgICAgICBpZigkc2NvcGUubmV3RXZlbnQudmVudWVfaWQgPT09IG51bGwgfHwgdHlwZW9mICRzY29wZS5uZXdFdmVudC52ZW51ZV9pZCA9PT0gXCJ1bmRlZmluZWRcIikge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJHEud2hlbigkc2NvcGUuY3JlYXRlVmVudWVGcm9tQWRkcmVzcyhhZGRyZXNzT2JqKSkudGhlbihmdW5jdGlvbih2ZW51ZUNyZWF0ZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKHZlbnVlQ3JlYXRlZCA9PT0gZmFsc2UgfHwgdmVudWVDcmVhdGVkID09PSAtMSB8fCB0eXBlb2YgdmVudWVDcmVhdGVkID09PSBcInVuZGVmaW5lZFwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGVycm9yID0gXCJWZW51ZSBjYW4ndCBiZSBjcmVhdGVkLiBQbGVhc2UsIHRyeSBhZ2FpbiBsYXRlciBvciBjaGVjayBhZ2FpbiB0aGUgdmVudWUgZGF0YSB5b3UgZW50ZXJlZCBpbiB0aGUgZm9ybSBiZWxsb3chXCI7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmVycm9ycy5wdXNoKGVycm9yKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5uZXdFdmVudC52ZW51ZV9pZCA9IHZlbnVlQ3JlYXRlZC5faWQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuICRzY29wZS5uZXdFdmVudDtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2UgaWYodHlwZW9mICRzY29wZS5uZXdFdmVudC52ZW51ZSAhPT0gXCJ1bmRlZmluZWRcIikgeyAvL2V2ZW50IG93bmVkIGJ5IHVzZXIgd2VyZSBwaWNrZWRcbiAgICAgICAgICAgICAgICAkc2NvcGUubmV3RXZlbnQudmVudWVfaWQgPSAkc2NvcGUubmV3RXZlbnQudmVudWU7XG4gICAgICAgICAgICAgICAgdmFyIHZlbnVlU3RvcmFnZSA9IG5ldyBWZW51ZVN0b3JhZ2UoKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gJHEud2hlbih2ZW51ZVN0b3JhZ2UuZmluZCh7XG4gICAgICAgICAgICAgICAgICAgIF9pZDogJHNjb3BlLm5ld0V2ZW50LnZlbnVlX2lkXG4gICAgICAgICAgICAgICAgfSkpLnRoZW4oZnVuY3Rpb24oZXhpc3RpbmdWZW51ZXMpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJHb3QgZXhpc3RpbmcgdmVudWVzOiBcIiwgZXhpc3RpbmdWZW51ZXMpO1xuICAgICAgICAgICAgICAgICAgICB2YXIgdmVudWUgPSBleGlzdGluZ1ZlbnVlcy5kYXRhWzBdO1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUubmV3RXZlbnQuYWRkcmVzcyA9IHZlbnVlLnN0cmVldDtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm5ld0V2ZW50LmxvY2F0aW9uID0gdmVudWUubG9jYXRpb247XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAkc2NvcGUubmV3RXZlbnQ7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgLy9cbiAgICAgICAgJHNjb3BlLmNyZWF0ZVZlbnVlRnJvbUFkZHJlc3MgPSBmdW5jdGlvbihhZGRyZXNzT2JqKSB7XG4gICAgICAgICAgICB2YXIgdmVudWUgPSBuZXcgVmVudWUoe1xuICAgICAgICAgICAgICAgIG5hbWU6IGFkZHJlc3NPYmoubmFtZSA/IGFkZHJlc3NPYmoubmFtZSA6IG51bGwsXG4gICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IFwiXCIsXG4gICAgICAgICAgICAgICAgZ2VuZXJhbF9pbmZvOiBcIlwiLFxuICAgICAgICAgICAgICAgIHBob25lOiBhZGRyZXNzT2JqLmZvcm1hdHRlZF9waG9uZV9udW1iZXIgPyBhZGRyZXNzT2JqLmZvcm1hdHRlZF9waG9uZV9udW1iZXIgOiBudWxsLFxuICAgICAgICAgICAgICAgIHdlYnNpdGU6IGFkZHJlc3NPYmoud2Vic2l0ZSA/IGFkZHJlc3NPYmoud2Vic2l0ZSA6IG51bGwsXG4gICAgICAgICAgICAgICAgc3RyZWV0OiBhZGRyZXNzT2JqLnZpY2luaXR5LFxuICAgICAgICAgICAgICAgIGxvY2F0aW9uOiAkc2NvcGUuY3JlYXRlTG9jYXRpb24oYWRkcmVzc09iai5nZW9tZXRyeS5sb2NhdGlvbi5sYXQoKSwgYWRkcmVzc09iai5nZW9tZXRyeS5sb2NhdGlvbi5sbmcoKSlcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgcmV0dXJuICRxLndoZW4odmVudWUuc2F2ZU9uQmFja2VuZCgpKS50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgIGlmKHJlc3BvbnNlLnN0YXR1cyA9PT0gMjAwICYmIHJlc3BvbnNlLmRhdGEudmVudWUgIT09IFwidW5kZWZpbmVkXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnZlbnVlQ3JlYXRlZCA9IHJlc3BvbnNlLmRhdGEudmVudWU7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnZlbnVlQ3JlYXRlZCA9IC0xO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gJHNjb3BlLnZlbnVlQ3JlYXRlZDtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG5cblxuICAgICAgICAkc2NvcGUuYWRkcmVzc1NlbGVjdGVkID0gZnVuY3Rpb24oKXtcbiAgICAgICAgICAgIGlmKHR5cGVvZiAkc2NvcGUubmV3RXZlbnQuYWRkcmVzcyA9PT0gXCJvYmplY3RcIikge1xuICAgICAgICAgICAgICAgIE1hcFNlcnZpY2UubWFwLnNldENlbnRlcigkc2NvcGUubmV3RXZlbnQuYWRkcmVzcy5nZW9tZXRyeS5sb2NhdGlvbik7XG4gICAgICAgICAgICAgICAgTWFwU2VydmljZS5tYXAuc2V0Wm9vbSgxNik7XG4gICAgICAgICAgICAgICAgdmFyIHZlbnVlU3RvcmFnZSA9IG5ldyBWZW51ZVN0b3JhZ2UoKTtcbiAgICAgICAgICAgICAgICB2YXIgYWRkcmVzc09iaiA9ICRzY29wZS5uZXdFdmVudC5hZGRyZXNzO1xuICAgICAgICAgICAgICAgIHZhciBsYXQgPSBhZGRyZXNzT2JqLmdlb21ldHJ5LmxvY2F0aW9uLmxhdCgpO1xuICAgICAgICAgICAgICAgIHZhciBsbmcgPSBhZGRyZXNzT2JqLmdlb21ldHJ5LmxvY2F0aW9uLmxuZygpO1xuICAgICAgICAgICAgICAgICRxLndoZW4odmVudWVTdG9yYWdlLmZpbmQoe1xuICAgICAgICAgICAgICAgICAgICBsYXRpdHVkZTogbGF0LFxuICAgICAgICAgICAgICAgICAgICBsb25naXR1ZGU6IGxuZyxcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogYWRkcmVzc09iai5uYW1lXG4gICAgICAgICAgICAgICAgfSkpLnRoZW4oZnVuY3Rpb24oZXhpc3RpbmdWZW51ZXMpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYoZXhpc3RpbmdWZW51ZXMuZGF0YS5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmV4aXN0aW5nVmVudWVzID0gZXhpc3RpbmdWZW51ZXMuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLm5hbWVDaGFuZ2VkID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgaWYoX3RpbWVvdXQpeyAvL2lmIHRoZXJlIGlzIGFscmVhZHkgYSB0aW1lb3V0IGluIHByb2Nlc3MgY2FuY2VsIGl0XG4gICAgICAgICAgICAgICAgJHRpbWVvdXQuY2FuY2VsKF90aW1lb3V0KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIF90aW1lb3V0ID0gJHRpbWVvdXQoZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICAkc2NvcGUuYWRkcmVzc1NlbGVjdGVkKCk7XG4gICAgICAgICAgICAgICAgX3RpbWVvdXQgPSBudWxsO1xuICAgICAgICAgICAgfSwgMTAwMCk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLmNyZWF0ZU5ld1ZlbnVlQ2hhbmdlZCA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJCgnLmNhdGFsb2ctaXRlbScpLmNzcygnYm9yZGVyJywgJzBweCcpO1xuICAgICAgICAgICAgaWYoJHNjb3BlLm5ld0V2ZW50LmNyZWF0ZVZlbnVlID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgICRzY29wZS5uZXdFdmVudC52ZW51ZV9pZCA9IG51bGw7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLnBpY2tWZW51ZSA9IGZ1bmN0aW9uKGV2ZW50LCB2ZW51ZSkge1xuICAgICAgICAgICAgJCgnLmNhdGFsb2ctaXRlbScpLmNzcygnYm9yZGVyJywgJzBweCcpO1xuICAgICAgICAgICAgJHNjb3BlLm5ld0V2ZW50LnZlbnVlX2lkID0gdmVudWUuX2lkO1xuICAgICAgICAgICAgJHNjb3BlLm5ld0V2ZW50LmNyZWF0ZVZlbnVlID0gZmFsc2U7XG4gICAgICAgICAgICAkKGV2ZW50LnRhcmdldCkuY2xvc2VzdCgnLmNhdGFsb2ctaXRlbScpLmNzcygnYm9yZGVyJywgJzFweCBzb2xpZCBibHVlJyk7XG4gICAgICAgIH07XG5cblxuICAgICAgICAkc2NvcGUuaW5pdCgpO1xuICAgIH0pO1xufSkoKTtcbiIsIihmdW5jdGlvbigpe1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG5cbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLmNvbnRyb2xsZXJzJykuY29udHJvbGxlcignVmVudWVDcmVhdGVDdHJsJywgZnVuY3Rpb24oJHNjb3BlLCAkcm9vdFNjb3BlLCAkdGltZW91dCwgJHEsICRzdGF0ZSwgSHRtbEhlbHBlciwgVXBsb2FkLCBWZW5WYXN0UmVxdWVzdCwgRXZlbnRzU3RvcmFnZSwgVmVudWVTdG9yYWdlLCBFdmVudCwgVmVudWUsIE1hcFNlcnZpY2UpIHtcblxuICAgICAgICB2YXIgX3RpbWVvdXQ7XG5cbiAgICAgICAgJHNjb3BlLmluaXQgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGlmKCEkcm9vdFNjb3BlLm5ld1ZlbnVlKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLm5ld1ZlbnVlID0gbmV3IFZlbnVlKHt9KTtcbiAgICAgICAgICAgICAgICAkc2NvcGUub3BlbmluZ0hvdXJzID0gJHNjb3BlLm5ld1ZlbnVlLmNyZWF0ZU9wZW5pbmdIb3Vyc1RlbXBsYXRlKCk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICRzY29wZS5uZXdWZW51ZSA9ICRyb290U2NvcGUubmV3VmVudWU7XG4gICAgICAgICAgICAgICAgaWYoJHNjb3BlLm5ld1ZlbnVlLmJ1c2luZXNzX2hvdXJzKXtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm9wZW5pbmdIb3VycyA9ICRzY29wZS5uZXdWZW51ZS5jcmVhdGVPcGVuSG91cnMoJHNjb3BlLm5ld1ZlbnVlLmJ1c2luZXNzX2hvdXJzKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZXtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm9wZW5pbmdIb3VycyA9ICRzY29wZS5uZXdWZW51ZS5jcmVhdGVPcGVuaW5nSG91cnNUZW1wbGF0ZSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgICRzY29wZS5lcnJvcnMgPSBbXTtcbiAgICAgICAgICAgICQoJyNjYXRlZ29yeScpLnNlbGVjdDIoKTtcbiAgICAgICAgICAgIGlmKCRzY29wZS5uZXdWZW51ZS5jYXRlZ29yaWVzLmxlbmd0aCA+IDApe1xuICAgICAgICAgICAgICAgICRzY29wZS5uZXdWZW51ZS5jYXRlZ29yeSA9ICRzY29wZS5uZXdWZW51ZS5jYXRlZ29yaWVzWzBdO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAkc2NvcGUuZXZlbnRNYXAgPSBNYXBTZXJ2aWNlLmluaXRpYWxpemUobmV3IFZlblZhc3RSZXF1ZXN0KHt3aGVyZTogbnVsbH0pLCAndmVudWUtbWFwJyk7XG5cbiAgICAgICAgICAgIEh0bWxIZWxwZXIuaGlkZUxvYWRlcigpO1xuICAgICAgICAgICAgJCgnLmwtY29udGVudCcpLndpZHRoKCcxMDAlJyk7XG4gICAgICAgICAgICBcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuYnVzaW5lc3NIb3Vyc0NoZWNrZWQgPSBmdW5jdGlvbihkYXlPYmplY3QpIHtcbiAgICAgICAgICAgIHJldHVybiB0eXBlb2YgZGF5T2JqZWN0Lm9wZW4gIT09IFwidW5kZWZpbmVkXCI7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLmJ1c2luZXNzSG91cnNUb2dnbGUgPSBmdW5jdGlvbihldmVudCwgZGF5T2JqKSB7XG4gICAgICAgICAgICB2YXIgY2hlY2tlZCA9ICQoZXZlbnQudGFyZ2V0KVswXS5jaGVja2VkO1xuICAgICAgICAgICAgaWYoY2hlY2tlZCkge1xuICAgICAgICAgICAgICAgIGRheU9iaiA9ICRzY29wZS5uZXdWZW51ZS5nZXRPcGVuSG91cnNEYXlPYmplY3QoZGF5T2JqLmRheSwgJzA5MDAnLCAnMjAwMCcpO1xuICAgICAgICAgICAgICAgICR0aW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUub3BlbmluZ0hvdXJzW2RheU9iai5kYXldID0gZGF5T2JqO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRheU9iaik7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGRheU9iai5vcGVuID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgZGF5T2JqLmNsb3NlID0gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cblxuICAgICAgICAkc2NvcGUucmVtb3ZlUGljdHVyZSA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJHNjb3BlLm5ld1ZlbnVlLmNvdmVyX2ltYWdlID0gbnVsbDtcbiAgICAgICAgfTtcbiAgICAgICAgXG5cbiAgICAgICAgJHNjb3BlLmNyZWF0ZUxvY2F0aW9uID0gZnVuY3Rpb24obGF0LCBsbmcpIHtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgdHlwZTogXCJQb2ludFwiLFxuICAgICAgICAgICAgICAgIGNvb3JkaW5hdGVzOiBbbG5nLCBsYXRdXG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLmNyZWF0ZVZlbnVlID0gZnVuY3Rpb24oJGV2ZW50KXtcbiAgICAgICAgICAgICRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgJHNjb3BlLm5ld1ZlbnVlLmNhdGVnb3JpZXMgPSBbJCgnI2NhdGVnb3J5JykudmFsKCldO1xuICAgICAgICAgICAgJHNjb3BlLm5ld1ZlbnVlLmJ1c2luZXNzX2hvdXJzID0gJHNjb3BlLm9wZW5pbmdIb3VycztcbiAgICAgICAgICAgICRzY29wZS5uZXdWZW51ZS5hZGRyZXNzID0gJHNjb3BlLm5ld1ZlbnVlLmFkZHJlc3MuZm9ybWF0dGVkX2FkZHJlc3M7XG4gICAgICAgICAgICAkc2NvcGUubmV3VmVudWUuc3RyZWV0ID0gJHNjb3BlLm5ld1ZlbnVlLmFkZHJlc3M7XG4gICAgICAgICAgICAkcS53aGVuKCRzY29wZS5uZXdWZW51ZS5zYXZlT25CYWNrZW5kKCkpLnRoZW4oZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAkc3RhdGUuZ28oJ29yZ2FuaXplci52ZW51ZXMnLCB7fSwge3JlbG9hZDogdHJ1ZX0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cblxuICAgICAgICAkc2NvcGUuYWRkcmVzc1NlbGVjdGVkID0gZnVuY3Rpb24oKXtcbiAgICAgICAgICAgIGlmKHR5cGVvZiAkc2NvcGUubmV3VmVudWUuYWRkcmVzcyA9PT0gXCJvYmplY3RcIikge1xuICAgICAgICAgICAgICAgIE1hcFNlcnZpY2UubWFwLnNldENlbnRlcigkc2NvcGUubmV3VmVudWUuYWRkcmVzcy5nZW9tZXRyeS5sb2NhdGlvbik7XG4gICAgICAgICAgICAgICAgTWFwU2VydmljZS5tYXAuc2V0Wm9vbSgxNik7XG4gICAgICAgICAgICAgICAgdmFyIHZlbnVlU3RvcmFnZSA9IG5ldyBWZW51ZVN0b3JhZ2UoKTtcbiAgICAgICAgICAgICAgICB2YXIgYWRkcmVzc09iaiA9ICRzY29wZS5uZXdWZW51ZS5hZGRyZXNzO1xuICAgICAgICAgICAgICAgIHZhciBsYXQgPSBhZGRyZXNzT2JqLmdlb21ldHJ5LmxvY2F0aW9uLmxhdCgpO1xuICAgICAgICAgICAgICAgIHZhciBsbmcgPSBhZGRyZXNzT2JqLmdlb21ldHJ5LmxvY2F0aW9uLmxuZygpO1xuICAgICAgICAgICAgICAgICRzY29wZS5uZXdWZW51ZS5sb2NhdGlvbiA9ICRzY29wZS5jcmVhdGVMb2NhdGlvbihsYXQsIGxuZyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLnRvZ2dsZVNlbGVjdGlvbiA9IGZ1bmN0aW9uKGV2ZW50LCBjYXRlZ29yeSkge1xuICAgICAgICAgICAgdmFyIGNoZWNrZWQgPSAkKGV2ZW50LnRhcmdldClbMF0uY2hlY2tlZDtcbiAgICAgICAgICAgIHZhciBjYXRlZ29yaWVzID0gJHNjb3BlLm5ld1ZlbnVlLmV2ZW50Q2F0ZWdvcmllcztcbiAgICAgICAgICAgIGlmKGNoZWNrZWQpIHtcbiAgICAgICAgICAgICAgICBpZih0eXBlb2YgY2F0ZWdvcmllcyA9PT0gXCJ1bmRlZmluZWRcIilcbiAgICAgICAgICAgICAgICAgICAgY2F0ZWdvcmllcyA9IFtdO1xuICAgICAgICAgICAgICAgIGNhdGVnb3JpZXMucHVzaChjYXRlZ29yeS5faWQpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBpZih0eXBlb2YgY2F0ZWdvcmllcyAhPT0gXCJ1bmRlZmluZWRcIiAmJiBjYXRlZ29yaWVzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGluZGV4ID0gY2F0ZWdvcmllcy5pbmRleE9mKGNhdGVnb3J5Ll9pZCk7XG4gICAgICAgICAgICAgICAgICAgIGNhdGVnb3JpZXMuc3BsaWNlKGluZGV4LCAxKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAkc2NvcGUubmV3VmVudWUuZXZlbnRDYXRlZ29yaWVzID0gY2F0ZWdvcmllcztcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuaW5pdCgpO1xuICAgIH0pO1xufSkoKTtcbiIsIi8qKlxuICogQ3JlYXRlZCBieSBvZW0gb24gMy8xOS8xNi5cbiAqL1xuKGZ1bmN0aW9uKCl7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5jb250cm9sbGVycycpLmNvbnRyb2xsZXIoJ09yZ0hlYWRlckN0cmwnLCBmdW5jdGlvbigkc2NvcGUsICRyb290U2NvcGUsICR0aW1lb3V0LCAkcSwgJHN0YXRlLCAkbG9jYXRpb24sIEh0bWxIZWxwZXIsIFVzZXJTZXJ2aWNlLCBGYWNlYm9vayl7XG5cblxuICAgICAgICAkc2NvcGUuaGlnaGxpZ2h0TWVudUl0ZW0gPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgc3RhdGUgPSAkc3RhdGUuY3VycmVudC5uYW1lO1xuICAgICAgICAgICAgdmFyIGV2ZW50TGkgPSAkKCcjbWVudS1ldmVudHMnKS5jbG9zZXN0KCdsaScpO1xuICAgICAgICAgICAgdmFyIHZlbnVlTGkgPSAkKCcjbWVudS12ZW51ZXMnKS5jbG9zZXN0KCdsaScpO1xuICAgICAgICAgICAgdmFyIGRlYWxMaSA9ICQoJyNtZW51LWRlYWxzJykuY2xvc2VzdCgnbGknKTtcbiAgICAgICAgICAgIHZhciBob3N0c0xpID0gJCgnI21lbnUtaG9zdHMnKS5jbG9zZXN0KCdsaScpO1xuICAgICAgICAgICAgc3dpdGNoKHN0YXRlKXtcbiAgICAgICAgICAgICAgICBjYXNlICdvcmdhbml6ZXIuZXZlbnRzJzpcbiAgICAgICAgICAgICAgICAgICAgdmVudWVMaS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAgICAgICAgIGRlYWxMaS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAgICAgICAgIGhvc3RzTGkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgICAgICAgICBldmVudExpLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSAnb3JnYW5pemVyLnZlbnVlcyc6XG4gICAgICAgICAgICAgICAgICAgIGRlYWxMaS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAgICAgICAgIGV2ZW50TGkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgICAgICAgICBob3N0c0xpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICAgICAgICAgdmVudWVMaS5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgICdvcmdhbml6ZXIuZGVhbHMnOlxuICAgICAgICAgICAgICAgICAgICBldmVudExpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICAgICAgICAgdmVudWVMaS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAgICAgICAgIGhvc3RzTGkucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgICAgICAgICBkZWFsTGkuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlICdvcmdhbml6ZXIuaG9zdHMnOlxuICAgICAgICAgICAgICAgICAgICBldmVudExpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgICAgICAgICAgICAgdmVudWVMaS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAgICAgICAgIGRlYWxMaS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgICAgICAgICAgICAgIGhvc3RzTGkuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICAvKipcbiAgICAgICAgICogTG9nIGN1cnJlbnQgdXNlciBvdXRcbiAgICAgICAgICovXG4gICAgICAgICRzY29wZS5sb2dvdXQgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRxLndoZW4oVXNlclNlcnZpY2UubG9nb3V0KCkpLnRoZW4oZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgJHRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS51c2VyID0gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgJHN0YXRlLmdvKCdob21lLmV2ZW50cycpO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJChkb2N1bWVudCkucmVhZHkoJHNjb3BlLmhpZ2hsaWdodE1lbnVJdGVtKTtcbiAgICAgICAgJHJvb3RTY29wZS4kb24oJyRzdGF0ZUNoYW5nZVN1Y2Nlc3MnLCAkc2NvcGUuaGlnaGxpZ2h0TWVudUl0ZW0pO1xuICAgIH0pO1xufSkoKTsiLCIvKipcbiAqIENyZWF0ZWQgYnkgb2VtIG9uIDMvMTkvMTYuXG4gKi9cbihmdW5jdGlvbigpe1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG5cbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLmNvbnRyb2xsZXJzJykuY29udHJvbGxlcignT3JnSG9tZUN0cmwnLCBmdW5jdGlvbigkcSwgJHNjb3BlLCAkdGltZW91dCwgJHdpbmRvdywgJGFuY2hvclNjcm9sbCwgJHN0YXRlLCBBbmNob3JTbW9vdGhTY3JvbGwsIEh0bWxIZWxwZXIsIEV2ZW50c1N0b3JhZ2UsIFZlbnVlU3RvcmFnZSwgRXZlbnQsIFVzZXJTZXJ2aWNlLCBWZW5WYXN0UmVxdWVzdCwgTWFwU2VydmljZSwgQ2hhbmdlc05vdGlmaWVyLCBDYXRlZ29yaWVzU3RvcmFnZSl7XG5cbiAgICAgICAgJHNjb3BlLmluaXQgPSBmdW5jdGlvbigpe1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJJbml0IGNhbGxlZFwiKTtcblxuICAgICAgICAgICAgSHRtbEhlbHBlci5zaG93TG9hZGVyKCk7XG4gICAgICAgICAgICAkc2NvcGUud2luX2hlaWdodF9wYWRkZWQgPSAkKHdpbmRvdykuaGVpZ2h0KCkgKiAxLjM0O1xuICAgICAgICAgICAgJHdpbmRvdy5vbnNjcm9sbCA9ICRzY29wZS5yZXZlYWxPblNjcm9sbDtcbiAgICAgICAgICAgICRzY29wZS5vcmdFdmVudFJlcXVlc3QgPSBuZXcgVmVuVmFzdFJlcXVlc3Qoe3doZW46IFwid2Vla1wiLCBvcmdhbml6ZXI6IHRydWV9KTtcbiAgICAgICAgICAgICRzY29wZS5vcmdWZW51ZXNSZXF1ZXN0ID0gbmV3IFZlblZhc3RSZXF1ZXN0KHtvcmdhbml6ZXI6IHRydWV9KTtcbiAgICAgICAgICAgICRzY29wZS5ob3N0c1JlcXVlc3QgPSBuZXcgVmVuVmFzdFJlcXVlc3Qoe2hvc3RzOiB0cnVlfSk7XG5cbiAgICAgICAgICAgICRxLndoZW4oJHNjb3BlLmluaXRVc2VyKCkpLnRoZW4oZnVuY3Rpb24odXNlcikge1xuXG4gICAgICAgICAgICAgICAgJHNjb3BlLmluaXRFdmVudHMoKTtcbiAgICAgICAgICAgICAgICAkc2NvcGUuaW5pdFZlbnVlcygpO1xuICAgICAgICAgICAgICAgICRzY29wZS5pbml0SG9zdHMoKTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBDaGFuZ2VzTm90aWZpZXIuc3Vic2NyaWJlKENoYW5nZXNOb3RpZmllci5TVUJKX09SR19WRU5VRVNfTE9BREVELCAkc2NvcGUsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICR0aW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICBpZih0eXBlb2YgIENoYW5nZXNOb3RpZmllci5vcmdfdmVudWVzICE9PSBcInVuZGVmaW5lZFwiICYmIENoYW5nZXNOb3RpZmllci5vcmdfdmVudWVzICE9PSBudWxsKVxuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm9yZ1ZlbnVlcyA9IENoYW5nZXNOb3RpZmllci5vcmdfdmVudWVzO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBDaGFuZ2VzTm90aWZpZXIuc3Vic2NyaWJlKENoYW5nZXNOb3RpZmllci5TVUJKX09SR19FVkVOVFNfTE9BREVELCAkc2NvcGUsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAkdGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmKHR5cGVvZiAgQ2hhbmdlc05vdGlmaWVyLm9yZ192YWx1ZSAhPT0gXCJ1bmRlZmluZWRcIiAmJiBDaGFuZ2VzTm90aWZpZXIub3JnX3ZhbHVlICE9PSBudWxsKVxuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm9yZ0V2ZW50cyA9IENoYW5nZXNOb3RpZmllci5vcmdfdmFsdWU7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuaW5pdEV2ZW50cyA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyIGV2ZW50cyA9IG5ldyBFdmVudHNTdG9yYWdlKCk7XG4gICAgICAgICAgICB2YXIgY2F0ZWdvcmllcyA9IG5ldyBDYXRlZ29yaWVzU3RvcmFnZSgpO1xuICAgICAgICAgICAgJHNjb3BlLmV2ZW50Q2F0ZWdvcmllc1JlcXVlc3QgPSBuZXcgVmVuVmFzdFJlcXVlc3Qoe3R5cGU6ICdldmVudHMnfSk7XG4gICAgICAgICAgICByZXR1cm4gJHEud2hlbihldmVudHMubG9hZCgkc2NvcGUub3JnRXZlbnRSZXF1ZXN0KSkudGhlbihmdW5jdGlvbihldmVudFN0b3JhZ2UpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gJHEud2hlbihjYXRlZ29yaWVzLmxvYWQoJHNjb3BlLmV2ZW50Q2F0ZWdvcmllc1JlcXVlc3QpKS50aGVuKGZ1bmN0aW9uKGNhdGVnb3J5U3RvcmFnZSkge1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUub3JnRXZlbnRzID0gZXZlbnRTdG9yYWdlLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5ldmVudENhdGVnb3JpZXMgPSBjYXRlZ29yeVN0b3JhZ2UuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgQ2hhbmdlc05vdGlmaWVyLm9yZ192YWx1ZSA9ICRzY29wZS5vcmdFdmVudHM7XG4gICAgICAgICAgICAgICAgICAgIENoYW5nZXNOb3RpZmllci5ub3RpZnkoQ2hhbmdlc05vdGlmaWVyLlNVQkpfT1JHX0VWRU5UU19MT0FERUQpO1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUuYWRkU2Nyb2xsSGFuZGxlcihldmVudHMsICRzY29wZS5vcmdFdmVudFJlcXVlc3QsICdvcmdFdmVudHMnKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuaW5pdFZlbnVlcyA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyIHZlbnVlcyA9IG5ldyBWZW51ZVN0b3JhZ2UoKTtcbiAgICAgICAgICAgIHZhciBjYXRlZ29yaWVzID0gbmV3IENhdGVnb3JpZXNTdG9yYWdlKCk7XG4gICAgICAgICAgICAkc2NvcGUudmVudWVDYXRlZ29yaWVzUmVxdWVzdCA9IG5ldyBWZW5WYXN0UmVxdWVzdCh7dHlwZTogJ3ZlbnVlcyd9KTtcbiAgICAgICAgICAgIHJldHVybiAkcS53aGVuKHZlbnVlcy5sb2FkKCRzY29wZS5vcmdWZW51ZXNSZXF1ZXN0KSkudGhlbihmdW5jdGlvbih2ZW51ZVN0b3JhZ2UpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gJHEud2hlbihjYXRlZ29yaWVzLmxvYWQoJHNjb3BlLnZlbnVlQ2F0ZWdvcmllc1JlcXVlc3QpKS50aGVuKGZ1bmN0aW9uKGNhdGVnb3J5U3RvcmFnZSkge1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUub3JnVmVudWVzID0gdmVudWVTdG9yYWdlLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS52ZW51ZUNhdGVnb3JpZXMgPSBjYXRlZ29yeVN0b3JhZ2UuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgQ2hhbmdlc05vdGlmaWVyLm9yZ192ZW51ZXMgPSAkc2NvcGUub3JnVmVudWVzO1xuICAgICAgICAgICAgICAgICAgICBDaGFuZ2VzTm90aWZpZXIubm90aWZ5KENoYW5nZXNOb3RpZmllci5TVUJKX09SR19WRU5VRVNfTE9BREVEKTtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmFkZFNjcm9sbEhhbmRsZXIodmVudWVzLCAkc2NvcGUub3JnVmVudWVzUmVxdWVzdCwgJ29yZ1ZlbnVlcycpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5pbml0SG9zdHMgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhciBob3N0cyA9IG5ldyBWZW51ZVN0b3JhZ2UoKTtcbiAgICAgICAgICAgIHZhciBjYXRlZ29yaWVzID0gbmV3IENhdGVnb3JpZXNTdG9yYWdlKCk7XG4gICAgICAgICAgICByZXR1cm4gJHEud2hlbihob3N0cy5sb2FkKCRzY29wZS5ob3N0c1JlcXVlc3QpKS50aGVuKGZ1bmN0aW9uKHZlbnVlU3RvcmFnZSkge1xuICAgICAgICAgICAgICAgICRzY29wZS5ob3N0cyA9IHZlbnVlU3RvcmFnZS5kYXRhO1xuICAgICAgICAgICAgICAgIENoYW5nZXNOb3RpZmllci5ob3N0cyA9ICRzY29wZS5ob3N0cztcbiAgICAgICAgICAgICAgICAvLyBDaGFuZ2VzTm90aWZpZXIubm90aWZ5KENoYW5nZXNOb3RpZmllci5TVUJKX09SR19WRU5VRVNfTE9BREVEKTtcbiAgICAgICAgICAgICAgICAkc2NvcGUuYWRkU2Nyb2xsSGFuZGxlcihob3N0cywgJHNjb3BlLmhvc3RzUmVxdWVzdCwgJ2hvc3RzJyk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuaW5pdFVzZXIgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRxLndoZW4oVXNlclNlcnZpY2UuZ2V0Q3VycmVudFVzZXIoKSkudGhlbihmdW5jdGlvbih1c2VyKSB7XG4gICAgICAgICAgICAgICAgaWYodXNlciA9PT0gbnVsbCB8fCB0eXBlb2YgdXNlciA9PT0gXCJ1bmRlZmluZWRcIilcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICRzdGF0ZS5nbygnaG9tZS5ldmVudHMnLCB7fSwge3JlbG9hZDogdHJ1ZX0pO1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICRzY29wZS51c2VyID0gdXNlcjtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5hZGRTY3JvbGxIYW5kbGVyID0gZnVuY3Rpb24oc3RvcmFnZUluc3RhbmNlLCByZXF1ZXN0SW5zdGFuY2UsIHNjb3BlU3RvcmFnZSkge1xuICAgICAgICAgICAgJCh3aW5kb3cpLnNjcm9sbChmdW5jdGlvbigpe1xuICAgICAgICAgICAgICAgIHZhciBsYXN0RWxlbWVudCA9IEh0bWxIZWxwZXIuZ2V0Q29vcmRzKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5jYXRhbG9nLXdyYXBwZXI6bGFzdC1jaGlsZCcpKTtcbiAgICAgICAgICAgICAgICBpZih3aW5kb3cucGFnZVlPZmZzZXQgKyB3aW5kb3cuc2NyZWVuLmhlaWdodCA+PSBsYXN0RWxlbWVudCAtIDgwMCkge1xuICAgICAgICAgICAgICAgICAgICAkcS53aGVuKHN0b3JhZ2VJbnN0YW5jZS5sb2FkKHJlcXVlc3RJbnN0YW5jZSkpLnRoZW4oZnVuY3Rpb24oc3RvcmFnZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYoc3RvcmFnZSAmJiBzdG9yYWdlLmRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IodmFyIGk9IDA7IGkgPCBzdG9yYWdlLmRhdGEubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlW3Njb3BlU3RvcmFnZV0ucHVzaChzdG9yYWdlLmRhdGFbaV0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUubm90aWZ5TW9yZUl0ZW1zTG9hZGVkKHNjb3BlU3RvcmFnZSwgc3RvcmFnZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuICAgICAgICBcbiAgICAgICAgJHNjb3BlLm5vdGlmeU1vcmVJdGVtc0xvYWRlZCA9IGZ1bmN0aW9uKHNjb3BlU3RvcmFnZSwgc3RvcmFnZSkge1xuICAgICAgICAgICAgc3dpdGNoKHNjb3BlU3RvcmFnZSkge1xuICAgICAgICAgICAgICAgIGNhc2UgJ29yZ0V2ZW50cyc6XG4gICAgICAgICAgICAgICAgICAgIENoYW5nZXNOb3RpZmllci5vcmdfdmFsdWUgPSBzdG9yYWdlLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgIENoYW5nZXNOb3RpZmllci5ub3RpZnkoQ2hhbmdlc05vdGlmaWVyLlNVQkpfRVZFTlRTX0FEREVEKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgICAgICBjYXNlICdvcmdWZW51ZXMnOlxuICAgICAgICAgICAgICAgICAgICBDaGFuZ2VzTm90aWZpZXIub3JnX3ZlbnVlcyA9IHN0b3JhZ2UuZGF0YTtcbiAgICAgICAgICAgICAgICAgICAgQ2hhbmdlc05vdGlmaWVyLm5vdGlmeShDaGFuZ2VzTm90aWZpZXIuU1VCSl9WRU5VRVNfQURERUQpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUucmV2ZWFsT25TY3JvbGwgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgc2Nyb2xsZWQgPSAkKHdpbmRvdykuc2Nyb2xsVG9wKCk7XG5cbiAgICAgICAgICAgJCgnLnJldmVhbE9uU2Nyb2xsLmFuaW1hdGVkJykuZWFjaChmdW5jdGlvbihpLCBlbGVtZW50KSB7XG4gICAgICAgICAgICAgICB2YXIgX19zZWxmID0gJCh0aGlzKTtcbiAgICAgICAgICAgICAgIHZhciBvZmZzZXRUb3AgPSBfX3NlbGYub2Zmc2V0KCkudG9wO1xuICAgICAgICAgICAgICAgaWYgKHNjcm9sbGVkICsgJHNjb3BlLndpbl9oZWlnaHRfcGFkZGVkIDwgb2Zmc2V0VG9wKSB7XG4gICAgICAgICAgICAgICAgICAgICAgIF9fc2VsZi5yZW1vdmVDbGFzcygnYW5pbWF0ZWQnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgX19zZWxmLnJlbW92ZUNsYXNzKF9fc2VsZi5kYXRhKCdhbmltYXRpb24nKSk7XG4gICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICQoXCIucmV2ZWFsT25TY3JvbGw6bm90KC5hbmltYXRlZClcIikuZWFjaChmdW5jdGlvbiAoaSwgZWxlbWVudCkge1xuICAgICAgICAgICAgICAgIHZhciBfX3NlbGYgPSAkKHRoaXMpO1xuICAgICAgICAgICAgICAgIHZhciBvZmZzZXRUb3AgPSBfX3NlbGYub2Zmc2V0KCkudG9wO1xuICAgICAgICAgICAgICAgIGlmIChzY3JvbGxlZCArICRzY29wZS53aW5faGVpZ2h0X3BhZGRlZCA+IG9mZnNldFRvcCkge1xuICAgICAgICAgICAgICAgICAgICBpZihfX3NlbGYuZGF0YSgndGltZW91dCcpICYmIGkgPiA1KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBfX3NlbGYuYWRkQ2xhc3MoJ2FuaW1hdGVkJyk7XG4gICAgICAgICAgICAgICAgICAgICAgICBfX3NlbGYuYWRkQ2xhc3MoX19zZWxmLmRhdGEoJ2FuaW1hdGlvbicpKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuICAgICAgICBcbiAgICAgICAgJHNjb3BlLnRvZ2dsZUluZm8gPSBmdW5jdGlvbihvYmplY3QpIHtcbiAgICAgICAgICAgIHZhciBlbGVtZW50ID0gJCgnIycrIG9iamVjdC5jbGFzc05hbWUudG9Mb3dlckNhc2UoKSArICctJyArIG9iamVjdC5faWQpO1xuICAgICAgICAgICAgaWYoZWxlbWVudC5oYXNDbGFzcygnY2F0YWxvZy13cmFwcGVyLS1mdWxsJykpIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUuY2xvc2VJbmZvKCk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICRzY29wZS5zaG93SW5mbyhvYmplY3QpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5zaG93SW5mbyA9IGZ1bmN0aW9uKG9iamVjdCkge1xuICAgICAgICAgICAgdmFyIHRpdGxlID0gXCJWZW5WYXN0IFwiICsgb2JqZWN0LmNsYXNzTmFtZSArIFwiIC0gXCIgKyBvYmplY3QudGl0bGU7XG4gICAgICAgICAgICB2YXIgZG9tSWRlbnRmaWVyID0gb2JqZWN0LmNsYXNzTmFtZS50b0xvd2VyQ2FzZSgpICsnLScrIG9iamVjdC5faWQ7XG4gICAgICAgICAgICAkKCcjbWV0YS10aXRsZScpLmF0dHIoJ2NvbnRlbnQnLCB0aXRsZSk7XG4gICAgICAgICAgICAkKCd0aXRsZScpLnRleHQodGl0bGUpO1xuICAgICAgICAgICAgJCgnI21ldGEtdHlwZScpLmF0dHIoJ2NvbnRlbnQnLCBcIndlYnNpdGVcIik7XG4gICAgICAgICAgICAkKCcjbWV0YS1pbWFnZScpLmF0dHIoJ2NvbnRlbnQnLCBvYmplY3QucGljdHVyZSk7XG4gICAgICAgICAgICAkKCcjbWV0YS1kZXNjcmlwdGlvbicpLmF0dHIoJ2NvbnRlbnQnLCBvYmplY3QuZGVzY3JpcHRpb24pO1xuICAgICAgICAgICAgJCgnI21ldGEtdXJsJykuYXR0cignY29udGVudCcsICdodHRwOi8vdmVudmFzdC5jb20vIS8nKyBkb21JZGVudGZpZXIpO1xuICAgICAgICAgICAgJCgnLmNhdGFsb2ctd3JhcHBlcicpLnJlbW92ZUNsYXNzKCdjYXRhbG9nLXdyYXBwZXItLWZ1bGwnLCB7ZHVyYXRpb246IDUwMH0pO1xuICAgICAgICAgICAgTWFwU2VydmljZS5jZW50ZXJPbk9iamVjdChvYmplY3QpO1xuICAgICAgICAgICAgQW5jaG9yU21vb3RoU2Nyb2xsLnNjcm9sbFRvKGRvbUlkZW50Zmllcik7XG4gICAgICAgICAgICAkKCcjJyArIGRvbUlkZW50ZmllcikuYWRkQ2xhc3MoJ2NhdGFsb2ctd3JhcHBlci0tZnVsbCcsIHtkdXJhdGlvbjogNTAwfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLmNsb3NlSW5mbyA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJCgnLmNhdGFsb2ctd3JhcHBlcicpLnJlbW92ZUNsYXNzKCdjYXRhbG9nLXdyYXBwZXItLWZ1bGwnLCB7ZHVyYXRpb246IDUwMH0pO1xuICAgICAgICAgICAgJHNjb3BlLm1hcCA9IE1hcFNlcnZpY2UucmVzdG9yZU1hcCgpO1xuICAgICAgICB9O1xuICAgICAgICBcbiAgICAgICAgXG4gICAgICAgICRzY29wZS5pbml0KCk7XG4gICAgfSk7XG59KSgpOyIsIi8qKlxuICogQ3JlYXRlZCBieSBvZW0gb24gMy8xOS8xNi5cbiAqL1xuKGZ1bmN0aW9uKCl7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG5cblxuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuY29udHJvbGxlcnMnKS5jb250cm9sbGVyKCdIb3N0c0N0cmwnLCBmdW5jdGlvbigkc2NvcGUsICRzdGF0ZSwgSHRtbEhlbHBlcil7XG5cbiAgICAgICAgJHNjb3BlLm9wZW5WZW51ZUNyZWF0aW9uID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAkc3RhdGUuZ28oJ29yZ2FuaXplci5jcmVhdGVfdmVudWUnKTtcbiAgICAgICAgfTtcbiAgICAgICAgXG4gICAgICAgICRzY29wZS4kd2F0Y2goJ2hvc3RzJywgZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZigkc2NvcGUuaG9zdHMpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkhvc3RzOiBcIiwgJHNjb3BlLmhvc3RzKTtcbiAgICAgICAgICAgICAgICBmb3IodmFyIGkgPSAwOyBpIDwgJHNjb3BlLmhvc3RzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBkaXN0YW5jZSA9ICRzY29wZS5ob3N0c1tpXS5jb21wdXRlRGlzdGFuY2VUbygkc2NvcGUuaG9zdHNSZXF1ZXN0LmdldFVzZXJQb3NpdGlvbigpKTtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGQgPSAkc2NvcGUuaG9zdHNbaV0uY29tcHV0ZURpc3RhbmNlVG8oJHNjb3BlLmhvc3RzUmVxdWVzdC5nZXRVc2VyUG9zaXRpb24oKSk7XG4gICAgICAgICAgICAgICAgICAgIGlmKGRpc3RhbmNlID4gMzApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3RhbmNlID0gJz4gMzAnO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5ob3N0c1tpXS5kaXN0YW5jZSA9IGRpc3RhbmNlID8gZGlzdGFuY2UgKyAnIGttIGF3YXknIDogJyc7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5ob3N0c1tpXS5kID0gZDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgJHNjb3BlLmhvc3RzLnNvcnQoZnVuY3Rpb24oYSxiKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBhLmQgLSBiLmQ7XG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICBIdG1sSGVscGVyLmhpZGVMb2FkZXIoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfSk7XG59KSgpOyIsIi8qKlxuICogQ3JlYXRlZCBieSBvZW0gb24gMy8xOS8xNi5cbiAqL1xuKGZ1bmN0aW9uKCl7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG5cbiAgICAvKipcbiAgICAgKiBUaGlzIGNvbnRyb2xsZXIgaXMgZGVhbGluZyB3aXRoIGV2ZW50cyBsaXN0IHZpZXdcbiAgICAgKi9cbiAgICBhbmd1bGFyLm1vZHVsZSgnYXBwLmNvbnRyb2xsZXJzJykuY29udHJvbGxlcignT3JnTWFwQ3RybCcsIGZ1bmN0aW9uKCRxLCAkc2NvcGUsICRzdGF0ZSwgVXNlclNlcnZpY2UsIFZlblZhc3RSZXF1ZXN0LCBNYXBTZXJ2aWNlLCBDaGFuZ2VzTm90aWZpZXIsIHVpR21hcEdvb2dsZU1hcEFwaSl7XG5cblxuICAgICAgICB2YXIgY2F0ZWdvcnkgPSAkKCcuY2F0ZWdvcnknKTtcbiAgICAgICAgdmFyIHN0aWNreVNlbGVjdG9yID0gJy5zdGlja3knO1xuICAgICAgICB2YXIgZWwgPSAkKHN0aWNreVNlbGVjdG9yKTtcbiAgICAgICAgdmFyIHN0aWNreVRvcCA9ICQoc3RpY2t5U2VsZWN0b3IpLm9mZnNldCgpLnRvcDsgLy8gcmV0dXJucyBudW1iZXJcbiAgICAgICAgdmFyIHN0aWNreUhlaWdodCA9ICQoc3RpY2t5U2VsZWN0b3IpLmhlaWdodCgpO1xuXG4gICAgICAgICQod2luZG93KS5zY3JvbGwoZnVuY3Rpb24oKXtcbiAgICAgICAgICAgIHZhciBsaW1pdCA9ICQoJ2Zvb3RlcicpLm9mZnNldCgpLnRvcCAtIHN0aWNreUhlaWdodCAtIDIwIC0gMjAwO1xuICAgICAgICAgICAgdmFyIHdpbmRvd1RvcCA9ICQod2luZG93KS5zY3JvbGxUb3AoKTsgLy8gcmV0dXJucyBudW1iZXJcbiAgICAgICAgICAgIGlmICh3aW5kb3dUb3AgPiAyMDAgJiYgc3RpY2t5VG9wIDwgd2luZG93VG9wKXtcbiAgICAgICAgICAgICAgICBlbC5jc3MoeyBwb3NpdGlvbjogJ2ZpeGVkJywgdG9wOiAwLCBcIm1hcmdpbi1sZWZ0XCI6IFwiNzIlXCJ9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIGVsLmNzcyh7IHBvc2l0aW9uOiAnJywgdG9wOiBcIlwiLCBcIm1hcmdpbi1sZWZ0XCI6IFwiXCJ9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKGxpbWl0IDwgd2luZG93VG9wKSB7XG4gICAgICAgICAgICAgICAgdmFyIGRpZmYgPSBsaW1pdCAtIHdpbmRvd1RvcDtcbiAgICAgICAgICAgICAgICBlbC5jc3Moe3RvcDogZGlmZn0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICAkc2NvcGUuaW5pdGlhbGl6ZU1hcCA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJHEud2hlbihNYXBTZXJ2aWNlLmluaXRpYWxpemUoJHNjb3BlLmdldFN0YXRlUmVxdWVzdCgpLCAnb3JnLW1hcC1jYW52YXMnKSkudGhlbihmdW5jdGlvbihtYXBTZXJ2aWNlKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLm9yZ01hcFNlcnZpY2UgPSBtYXBTZXJ2aWNlO1xuICAgICAgICAgICAgICAgIHVpR21hcEdvb2dsZU1hcEFwaS50aGVuKGZ1bmN0aW9uKG1hcHMpIHtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmxvYWRNYXJrZXJzKCk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUubG9hZE1hcmtlcnMgPSBmdW5jdGlvbihvYmplY3RzKSB7XG4gICAgICAgICAgICB2YXIgc3RhdGUgPSAkc3RhdGUuY3VycmVudC5uYW1lO1xuICAgICAgICAgICAgc3dpdGNoIChzdGF0ZSkge1xuICAgICAgICAgICAgICAgIGNhc2UgJ29yZ2FuaXplci5ldmVudHMnOlxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUubG9hZEV2ZW50TWFya2VycyhvYmplY3RzKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSAnb3JnYW5pemVyLnZlbnVlcyc6XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5sb2FkVmVudWVNYXJrZXJzKG9iamVjdHMpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlICdvcmdhbml6ZXIuaG9zdHMnOlxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUubG9hZEhvc3RzTWFya2VycyhvYmplY3RzKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgfTtcblxuICAgICAgICAkc2NvcGUubG9hZEV2ZW50TWFya2VycyA9IGZ1bmN0aW9uKG9iamVjdHMpIHtcbiAgICAgICAgICAgIGlmKG9iamVjdHMgJiYgdHlwZW9mIG9iamVjdHMgIT09IFwidW5kZWZpbmVkXCIpe1xuICAgICAgICAgICAgICAgICRzY29wZS5vcmdNYXBTZXJ2aWNlLnNldEV2ZW50TWFya2VycyhvYmplY3RzKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLm9yZ01hcFNlcnZpY2Uuc2V0RXZlbnRNYXJrZXJzKCRzY29wZS5vcmdFdmVudHMpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAkc2NvcGUub3JnTWFwU2VydmljZS5zaG93VXNlck1hcmtlcigpO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5sb2FkVmVudWVNYXJrZXJzID0gZnVuY3Rpb24ob2JqZWN0cykge1xuICAgICAgICAgICAgaWYob2JqZWN0cylcbiAgICAgICAgICAgICAgICAkc2NvcGUub3JnTWFwU2VydmljZS5zZXRWZW51ZU1hcmtlcnMob2JqZWN0cyk7XG4gICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAgJHNjb3BlLm9yZ01hcFNlcnZpY2Uuc2V0VmVudWVNYXJrZXJzKCRzY29wZS5vcmdWZW51ZXMpO1xuXG4gICAgICAgICAgICAkc2NvcGUub3JnTWFwU2VydmljZS5zaG93VXNlck1hcmtlcigpO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5sb2FkSG9zdHNNYXJrZXJzID0gZnVuY3Rpb24ob2JqZWN0cykge1xuICAgICAgICAgICAgaWYob2JqZWN0cylcbiAgICAgICAgICAgICAgICAkc2NvcGUub3JnTWFwU2VydmljZS5zZXRWZW51ZU1hcmtlcnMob2JqZWN0cyk7XG4gICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAgJHNjb3BlLm9yZ01hcFNlcnZpY2Uuc2V0VmVudWVNYXJrZXJzKCRzY29wZS5ob3N0cyk7XG5cbiAgICAgICAgICAgICRzY29wZS5vcmdNYXBTZXJ2aWNlLnNob3dVc2VyTWFya2VyKCk7XG4gICAgICAgIH07XG4gICAgICAgIFxuXG4gICAgICAgICRzY29wZS5yZWxvYWRNYXJrZXJzID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICB2YXIgc3RhdGUgPSAkc3RhdGUuY3VycmVudC5uYW1lO1xuICAgICAgICAgICAgc3dpdGNoIChzdGF0ZSkge1xuICAgICAgICAgICAgICAgIGNhc2UgJ29yZ2FuaXplci5ldmVudHMnOlxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUucmVsb2FkRXZlbnRNYXJrZXJzKCk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgJ29yZ2FuaXplci52ZW51ZXMnOlxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUucmVsb2FkVmVudWVNYXJrZXJzKCk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgICBcbiAgICAgICAgJHNjb3BlLnJlbG9hZEV2ZW50TWFya2VycyA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICRxLndoZW4oTWFwU2VydmljZS5pbml0aWFsaXplKCRzY29wZS5nZXRTdGF0ZVJlcXVlc3QoKSwgJ29yZy1tYXAtY2FudmFzJykpLnRoZW4oZnVuY3Rpb24obWFwU2VydmljZSkge1xuICAgICAgICAgICAgICAgICAkc2NvcGUub3JnTWFwU2VydmljZSA9IG1hcFNlcnZpY2U7XG4gICAgICAgICAgICAgICAgJHNjb3BlLm9yZ01hcFNlcnZpY2UuZGVsZXRlQWxsTWFya2VycygpO1xuICAgICAgICAgICAgICAgIGlmKHR5cGVvZiBDaGFuZ2VzTm90aWZpZXIub3JnX3ZhbHVlICE9PSBcInVuZGVmaW5lZFwiICYmIENoYW5nZXNOb3RpZmllci5vcmdfdmFsdWUgIT09IG51bGwpe1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUubG9hZE1hcmtlcnMoQ2hhbmdlc05vdGlmaWVyLm9yZ192YWx1ZSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmxvYWRNYXJrZXJzKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuICAgICAgICBcbiAgICAgICAgJHNjb3BlLnJlbG9hZFZlbnVlTWFya2VycyA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJHEud2hlbihNYXBTZXJ2aWNlLmluaXRpYWxpemUoJHNjb3BlLmdldFN0YXRlUmVxdWVzdCgpLCAnb3JnLW1hcC1jYW52YXMnKSkudGhlbihmdW5jdGlvbihtYXBTZXJ2aWNlKSB7XG4gICAgICAgICAgICAgICAgJHNjb3BlLm9yZ01hcFNlcnZpY2UgPSBtYXBTZXJ2aWNlO1xuICAgICAgICAgICAgICAgICRzY29wZS5vcmdNYXBTZXJ2aWNlLmRlbGV0ZUFsbE1hcmtlcnMoKTtcbiAgICAgICAgICAgICAgICBpZih0eXBlb2YgQ2hhbmdlc05vdGlmaWVyLm9yZ192ZW51ZXMgIT09IFwidW5kZWZpbmVkXCIgJiYgQ2hhbmdlc05vdGlmaWVyLm9yZ192ZW51ZXMgIT09IG51bGwpe1xuICAgICAgICAgICAgICAgICAgICAkc2NvcGUubG9hZE1hcmtlcnMoQ2hhbmdlc05vdGlmaWVyLm9yZ192ZW51ZXMpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5sb2FkTWFya2VycygpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9O1xuICAgICAgICBcbiAgICAgICAgJHNjb3BlLmFkZE1hcmtlcnMgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIHZhciBzdGF0ZSA9ICRzdGF0ZS5jdXJyZW50Lm5hbWU7XG4gICAgICAgICAgICBzd2l0Y2ggKHN0YXRlKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAnb3JnYW5pemVyLmV2ZW50cyc6XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5hZGRFdmVudE1hcmtlcnMoKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSAnb3JnYW5pemVyLnZlbnVlcyc6XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5hZGRWZW51ZU1hcmtlcnMoKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLmdldFN0YXRlUmVxdWVzdCA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdmFyIHN0YXRlID0gJHN0YXRlLmN1cnJlbnQubmFtZTtcbiAgICAgICAgICAgIHN3aXRjaCAoc3RhdGUpIHtcbiAgICAgICAgICAgICAgICBjYXNlICdvcmdhbml6ZXIuZXZlbnRzJzpcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuICRzY29wZS5vcmdFdmVudFJlcXVlc3Q7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgJ29yZ2FuaXplci52ZW51ZXMnOlxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gJHNjb3BlLm9yZ1ZlbnVlc1JlcXVlc3Q7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgJ29yZ2FuaXplci5ob3N0cyc6XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAkc2NvcGUuaG9zdHNSZXF1ZXN0O1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgICAgXG4gICAgICAgIFxuICAgICAgICAkc2NvcGUuYWRkVmVudWVNYXJrZXJzID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICBpZihDaGFuZ2VzTm90aWZpZXIub3JnX3ZlbnVlcylcbiAgICAgICAgICAgICAgICAkc2NvcGUub3JnTWFwU2VydmljZS5hZGRWZW51ZU1hcmtlcnMoQ2hhbmdlc05vdGlmaWVyLm9yZ192ZW51ZXMpO1xuICAgICAgICB9O1xuICAgICAgICBcbiAgICAgICAgJHNjb3BlLmFkZEV2ZW50TWFya2VycyA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYoQ2hhbmdlc05vdGlmaWVyLm9yZ192YWx1ZSlcbiAgICAgICAgICAgICAgICAkc2NvcGUub3JnTWFwU2VydmljZS5hZGRFdmVudE1hcmtlcnMoQ2hhbmdlc05vdGlmaWVyLm9yZ192ZW51ZXMpO1xuICAgICAgICB9O1xuXG4gICAgICAgIGFuZ3VsYXIuZWxlbWVudChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgJHNjb3BlLmluaXRpYWxpemVNYXAoKTtcbiAgICAgICAgfSk7XG5cblxuICAgICAgICBDaGFuZ2VzTm90aWZpZXIuc3Vic2NyaWJlKENoYW5nZXNOb3RpZmllci5TVUJKX09SR19FVkVOVFNfTE9BREVELCAkc2NvcGUsICRzY29wZS5yZWxvYWRNYXJrZXJzKTtcbiAgICAgICAgQ2hhbmdlc05vdGlmaWVyLnN1YnNjcmliZShDaGFuZ2VzTm90aWZpZXIuU1VCSl9PUkdfVkVOVUVTX0xPQURFRCwgJHNjb3BlLCAkc2NvcGUucmVsb2FkTWFya2Vycyk7XG5cbiAgICAgICAgQ2hhbmdlc05vdGlmaWVyLnN1YnNjcmliZShDaGFuZ2VzTm90aWZpZXIuU1VCSl9PUkdfRVZFTlRTX0FEREVELCAkc2NvcGUsICRzY29wZS5hZGRNYXJrZXJzKTtcbiAgICAgICAgQ2hhbmdlc05vdGlmaWVyLnN1YnNjcmliZShDaGFuZ2VzTm90aWZpZXIuU1VCSl9PUkdfVkVOVUVTX0FEREVELCAkc2NvcGUsICRzY29wZS5hZGRNYXJrZXJzKTtcbiAgICB9KTtcbn0pKCk7IiwiLyoqXG4gKiBDcmVhdGVkIGJ5IG9lbSBvbiAzLzE5LzE2LlxuICovXG4oZnVuY3Rpb24oKXtcbiAgICBcInVzZSBzdHJpY3RcIjtcblxuXG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5jb250cm9sbGVycycpLmNvbnRyb2xsZXIoJ0RlYWxzQ3RybCcsIGZ1bmN0aW9uKCRzY29wZSwgJHN0YXRlLCBVc2VyU2VydmljZSwgRXZlbnRTZXJ2aWNlLCBNYXBTZXJ2aWNlKXtcbiAgICAgICAgLy8kc2NvcGUuc2hvd2luZ0Rlc2NyaXB0aW9uID0gbnVsbDtcbiAgICAgICAgLy8kc2NvcGUuc2hvd2luZ0V2ZW50ID0gbnVsbDtcbiAgICAgICAgJHNjb3BlLmluaXQgPSBmdW5jdGlvbigpe1xuICAgICAgICAgICAgJHNjb3BlLmV2ZW50cyA9IHt9O1xuICAgICAgICAgICAgVXNlclNlcnZpY2UuZ2V0Q3VycmVudFVzZXIoZnVuY3Rpb24odXNlcikge1xuICAgICAgICAgICAgICAgICRzY29wZS51c2VyID0gdXNlcjtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgRXZlbnRTZXJ2aWNlLmxpc3QoZnVuY3Rpb24oZXZlbnRzKXtcbiAgICAgICAgICAgICAgICAkc2NvcGUuZXZlbnRzLm9iamVjdHMgPSBldmVudHM7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIE1hcFNlcnZpY2UucmVxdWVzdE1hcChmdW5jdGlvbihtYXApIHtcbiAgICAgICAgICAgICAgICAkc2NvcGUubWFwID0gbWFwO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH07XG5cbiAgICAgICAgLy8kc2NvcGUuc2hvd0luZm8gPSBmdW5jdGlvbihlbGVtZW50LCBldmVudCl7XG4gICAgICAgIC8vICAgIGlmKCRzY29wZS5zaG93aW5nRGVzY3JpcHRpb24gPT09IG51bGwgJiYgJHNjb3BlLnNob3dpbmdFdmVudCA9PT0gbnVsbCl7XG4gICAgICAgIC8vICAgICAgICAkKCcuZXZlbnQtZGVzY3JpcHRpb24nKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICAgIC8vICAgICAgICAkKCcuZXZlbnQnKS5oaWRlKCk7XG4gICAgICAgIC8vICAgICAgICB2YXIgZXZlbnRCbG9jayA9ICQoZWxlbWVudC50YXJnZXQpLmNsb3Nlc3QoJy5ldmVudCcpLmVxKDApO1xuICAgICAgICAvLyAgICAgICAgZXZlbnRCbG9jay5zaG93KCk7XG4gICAgICAgIC8vICAgICAgICAkKCcjdmVudWUtJytldmVudC5pZCkuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAvLyAgICAgICAgJHNjb3BlLnNob3dpbmdEZXNjcmlwdGlvbiA9IHRydWU7XG4gICAgICAgIC8vICAgICAgICAkc2NvcGUuc2hvd2luZ0V2ZW50ID0gdHJ1ZTtcbiAgICAgICAgLy8gICAgfSBlbHNlIHtcbiAgICAgICAgLy8gICAgICAgICQoJy5ldmVudC1kZXNjcmlwdGlvbicpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAgICAgLy8gICAgICAgICQoJy5ldmVudCcpLnNob3coJ3Nsb3cnKTtcbiAgICAgICAgLy8gICAgICAgICRzY29wZS5zaG93aW5nRGVzY3JpcHRpb24gPSBudWxsO1xuICAgICAgICAvLyAgICAgICAgJHNjb3BlLnNob3dpbmdFdmVudCA9IG51bGw7XG4gICAgICAgIC8vICAgIH1cbiAgICAgICAgLy9cbiAgICAgICAgLy99O1xuICAgICAgICAkc2NvcGUuaW5pdCgpO1xuICAgIH0pO1xufSkoKTsiLCIvKipcbiAqIENyZWF0ZWQgYnkgb2VtIG9uIDMvMTkvMTYuXG4gKi9cbihmdW5jdGlvbigpe1xuICAgIFwidXNlIHN0cmljdFwiO1xuXG4gICAgLyoqXG4gICAgICogVGhpcyBjb250cm9sbGVyIGlzIGRlYWxpbmcgd2l0aCBldmVudHMgbGlzdCB2aWV3XG4gICAgICovXG4gICAgYW5ndWxhci5tb2R1bGUoJ2FwcC5jb250cm9sbGVycycpLmNvbnRyb2xsZXIoJ09yZ0V2ZW50c0N0cmwnLCBmdW5jdGlvbigkcSwgJHJvb3RTY29wZSwgJHNjb3BlLCAkc3RhdGUsIEh0bWxIZWxwZXIsIENoYW5nZXNOb3RpZmllcil7XG5cbiAgICAgICAgLy8gJCgnLmwtY29udGVudCcpLndpZHRoKCc3MiUnKTtcbiAgICAgICAgXG4gICAgICAgICRzY29wZS5lZGl0RXZlbnQgPSBmdW5jdGlvbiAoJGV2ZW50LCBldmVudCkge1xuICAgICAgICAgICAgJGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgJHJvb3RTY29wZS5uZXdFdmVudCA9IGV2ZW50O1xuICAgICAgICAgICAgJHN0YXRlLmdvKCdvcmdhbml6ZXIuY3JlYXRlX2V2ZW50Jywge30pO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5kZWxldGVFdmVudCA9IGZ1bmN0aW9uICgkZXZlbnQsIGV2ZW50KSB7XG4gICAgICAgICAgICAkZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgICBmb3IodmFyIGkgPSAwOyBpIDwgJHNjb3BlLm9yZ0V2ZW50cy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgIGlmKCRzY29wZS5vcmdFdmVudHNbaV0uX2lkID09PSBldmVudC5faWQpIHtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm9yZ0V2ZW50cy5zcGxpY2UoaSwgMSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZXZlbnQuZGVsZXRlRnJvbUJhY2tlbmQoKTtcbiAgICAgICAgfTtcbiAgICAgICAgXG4gICAgICAgICRzY29wZS5vcGVuRXZlbnRDcmVhdGlvbiA9IGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgJHN0YXRlLmdvKCdvcmdhbml6ZXIuY3JlYXRlX2V2ZW50Jyk7XG4gICAgICAgIH07XG4gICAgICAgIFxuXG4gICAgICAgICRzY29wZS4kd2F0Y2goJ2V2ZW50cycsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYoJHNjb3BlLmV2ZW50cykge1xuICAgICAgICAgICAgICAgIGZvcih2YXIgaSA9IDA7IGkgPCAkc2NvcGUuZXZlbnRzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBkaXN0YW5jZSA9ICRzY29wZS5ldmVudHNbaV0uY29tcHV0ZURpc3RhbmNlVG8oJHNjb3BlLmV2ZW50UmVxdWVzdC5nZXRVc2VyUG9zaXRpb24oKSk7XG4gICAgICAgICAgICAgICAgICAgIGlmKGRpc3RhbmNlID4gMzApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3RhbmNlID0gJz4gMzAnO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5ldmVudHNbaV0uZGlzdGFuY2UgPSBkaXN0YW5jZSA/IGRpc3RhbmNlICsgJyBrbSBhd2F5JyA6ICcnO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBIdG1sSGVscGVyLmhpZGVMb2FkZXIoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfSk7XG59KSgpOyIsIi8qKlxuICogQ3JlYXRlZCBieSBvZW0gb24gMy8xOS8xNi5cbiAqL1xuKGZ1bmN0aW9uKCl7XG4gICAgXCJ1c2Ugc3RyaWN0XCI7XG5cblxuICAgIGFuZ3VsYXIubW9kdWxlKCdhcHAuY29udHJvbGxlcnMnKS5jb250cm9sbGVyKCdPcmdWZW51ZXNDdHJsJywgZnVuY3Rpb24oJHNjb3BlLCAkcm9vdFNjb3BlLCAkc3RhdGUsIEh0bWxIZWxwZXIpe1xuXG4gICAgICAgIC8vICQoJy5sLWNvbnRlbnQnKS53aWR0aCgnNzIlJyk7XG5cbiAgICAgICAgJHJvb3RTY29wZS5uZXdWZW51ZSA9IG51bGw7XG4gICAgICAgIFxuICAgICAgICAkc2NvcGUub3BlblZlbnVlQ3JlYXRpb24gPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICRzdGF0ZS5nbygnb3JnYW5pemVyLmNyZWF0ZV92ZW51ZScpO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5lZGl0VmVudWUgPSBmdW5jdGlvbihldmVudCwgdmVudWUpIHtcbiAgICAgICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICAgICAgJHJvb3RTY29wZS5uZXdWZW51ZSA9IHZlbnVlO1xuICAgICAgICAgICAgJHN0YXRlLmdvKCdvcmdhbml6ZXIuY3JlYXRlX3ZlbnVlJywge30pO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5kZWxldGVWZW51ZSA9IGZ1bmN0aW9uKGV2ZW50LCB2ZW51ZSkge1xuICAgICAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgICBmb3IodmFyIGkgPSAwOyBpIDwgJHNjb3BlLm9yZ1ZlbnVlcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgIGlmKCRzY29wZS5vcmdWZW51ZXNbaV0uX2lkID09PSB2ZW51ZS5faWQpIHtcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm9yZ1ZlbnVlcy5zcGxpY2UoaSwgMSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdmVudWUuZGVsZXRlRnJvbUJhY2tlbmQoKTtcbiAgICAgICAgfTtcbiAgICAgICAgXG4gICAgICAgICRzY29wZS4kd2F0Y2goJ29yZ1ZlbnVlcycsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgaWYoJHNjb3BlLm9yZ1ZlbnVlcykge1xuICAgICAgICAgICAgICAgIGZvcih2YXIgaSA9IDA7IGkgPCAkc2NvcGUub3JnVmVudWVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBkaXN0YW5jZSA9ICRzY29wZS5vcmdWZW51ZXNbaV0uY29tcHV0ZURpc3RhbmNlVG8oJHNjb3BlLm9yZ1ZlbnVlc1JlcXVlc3QuZ2V0VXNlclBvc2l0aW9uKCkpO1xuICAgICAgICAgICAgICAgICAgICB2YXIgZCA9ICRzY29wZS5vcmdWZW51ZXNbaV0uY29tcHV0ZURpc3RhbmNlVG8oJHNjb3BlLm9yZ1ZlbnVlc1JlcXVlc3QuZ2V0VXNlclBvc2l0aW9uKCkpO1xuICAgICAgICAgICAgICAgICAgICBpZihkaXN0YW5jZSA+IDMwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkaXN0YW5jZSA9ICc+IDMwJztcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUub3JnVmVudWVzW2ldLmRpc3RhbmNlID0gZGlzdGFuY2UgPyBkaXN0YW5jZSArICcga20gYXdheScgOiAnJztcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLm9yZ1ZlbnVlc1tpXS5kID0gZDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgJHNjb3BlLm9yZ1ZlbnVlcy5zb3J0KGZ1bmN0aW9uKGEsYikge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gYS5kIC0gYi5kO1xuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgSHRtbEhlbHBlci5oaWRlTG9hZGVyKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH0pO1xufSkoKTsiXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
