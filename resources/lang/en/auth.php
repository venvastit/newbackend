<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials does not match any of our records.',
    'password' => "Password doesn't match specified account",
    'inactive' => 'Your account is not activated yet, please activate it before login',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'server' => 'Internal server error. Please, try again later',

];
