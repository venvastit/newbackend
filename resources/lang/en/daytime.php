<?php
/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/11/16
 * Time: 1:05 PM.
 */

return [
    'sun' => 'Sunday',
    'mon' => 'Monday',
    'tue' => 'Tuesday',
    'wed' => 'Wednesday',
    'thu' => 'Thursday',
    'fri' => 'Friday',
    'sat' => 'Saturday',
];
