@extends('layout.main_static')

@section('content')
    <h1 style="color: green;">Your account was activated!</h1>
    <p style="color: white;">You can now log in to your account using your email and password</p>
@endsection