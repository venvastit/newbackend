@extends('layout.main_static')

@section('content')

<form method="post" class="login-form" action="{{ route('user.create_password.do') }}">
    
    <h1>Create password for <small>{{ $user->getEmail() }}</small></h1>
    
    {{ csrf_field() }}

    <input type="hidden" name="code" value="{{ $user->getActivationCode() }}">
    <input type="hidden" name="email" value="{{ $user->getEmail() }}">

    <div class="form-group">
        <label for="new-password" class="label">New password</label>
        <input id="new-password" type="password" name="password" class="form-control" required="required">
    </div>

    <div class="form-group">
        <label for="login-form-repeat-password" class="label">Repeat password</label>
        <input id="login-form-repeat-password"type="password" name="repeat-password" class="form-control" required="required">
    </div>
    
    <center>
        <button type="submit" name="login_form" class="btn btn-secondary">Create password</button>
    </center>
</form>
@endsection