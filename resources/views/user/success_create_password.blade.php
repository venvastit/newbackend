@extends('layout.main_static')

@section('content')
    <h1 style="color: green;">Your password were created successfuly!</h1>
    <p style="color: white;">You can now log in to your account using your email and new password</p>
@endsection