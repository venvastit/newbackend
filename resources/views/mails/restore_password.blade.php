@extends('mails._base')
@section('title')
    Restore password at VenVast
@endsection
@section('content_title')
    Restore your password
@endsection
@section('content')
    <p class="text">Hello! We received a request to change your password on VenVast. Click the link
    bellow to set your new password: <br/>
    <a href="{{'https://api.venvast.com/user/create_password' . '?code=' . $user->getActivationCode() . '&email=' . $user->getEmail() }}">Restore password</a>
    <br/>
    <small>If you don't want to change your password you can ingore this email</small>
    </p>
@endsection