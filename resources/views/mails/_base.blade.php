<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraph.org/schema/">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width">
    <title> @yield('title') </title>
    <style type="text/css">
        @font-face {
            font-family: SourceSans;
            src: url("font.otf") format("opentype");
        }

        @font-face {
            font-family: SourceSans;
            font-weight: normal;
            src: url("fontNotBold.otf") format("opentype");
        }

        @font-face {
            font-family: 'SourceSansPro-Regular';
            src: url('SourceSansPro-Regular.eot?#iefix') format('embedded-opentype'), url('SourceSansPro-Regular.otf') format('opentype'),
            url('SourceSansPro-Regular.woff') format('woff'), url('SourceSansPro-Regular.ttf') format('truetype'), url('SourceSansPro-Regular.svg#SourceSansPro-Regular') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        body {
            background-color: rgb(238, 238, 238);
            font-family: 'SourceSansPro-Regular', 'SourceSans', sans-serif !important;
        }

        .container {
            margin: 0 auto;
            width: 600px;
        }

        .text {
            font-family: 'SourceSansPro-Regular', 'Open Sans', sans-serif !important;
            text-align: justify;
            color: #7f8c8d;
        }

        .logo {
            margin-top: 30px;
            color: rgb(0, 194, 224);
            text-align: center;
            text-transform: uppercase;
            font-size: 36px;
            font-family: Sourcesans;
        }

        .upper {
            background-color: rgb(0, 194, 224);
            color: white;
            font-size: 30px;
            text-align: center;
            height: 60px;
            margin: 0 auto;
            padding: 60px;
            border-top-left-radius: 7px;
            border-top-right-radius: 7px;
            font-family: Arial;
            font-weight: bold;
        }

        .lower {
            background: #FFFFFF;
            overflow: hidden;
            border-radius: 4px;
            box-shadow: 0px 3px 0px #DDDDDD;
            height: 185px !important;
            padding: 20px;
            max-width: 100% !important;
        }

        .third {
            margin-top: 10px;
        }

        .bar {
            margin-top: 10px;
        }

        .bar-left {
            background-color: white;
            box-shadow: 0px 2px 0px #DDDDDD;
            border-top-left-radius: 4px;
            border-bottom-left-radius: 4px;
            width: 83%;
            float: left;
            vertical-align: middle;
            color: #7f8c8d;
            font-weight: bold;
            min-height: 50px;
        }

        .bar-middle {
            background-color: white;
            box-shadow: 0px 2px 0px #DDDDDD;
            vertical-align: text-bottom;
            width: 15%;
            float: left;

            color: rgb(0, 194, 224);
            font-size: 10px;
            min-height: 50px;
        }

        .bar-right {
            background-color: rgb(0, 194, 224);
            box-shadow: 0px 2px 0px #DDDDDD;
            border-top-right-radius: 5px;
            border-bottom-right-radius: 5px;
            width: 17%;
            float: right;
            text-align: center;
            color: white;
            font-size: 10px;
            height: 50px;
        }

        .bar-right-white {
            background-color: white;
            box-shadow: 0px 2px 0px #DDDDDD;
            border-top-right-radius: 5px;
            border-bottom-right-radius: 5px;
            width: 17%;
            float: right;
            text-align: center;
            color: white;
            font-size: 10px;
            height: 50px;
        }

        .inner-right {
            padding: 10px;
            font-size: 13px;
        }

        .inner-left {
            padding: 14px 11px;
            font-size: 17px;
            font-family: SourceSansPro-Regular;
        }

        .bottom {
            margin-top: 30px;
        }

        .bar-left:hover {
            background-color: rgb(0, 194, 224);
        }

        .bar-middle:hover {
            background-color: rgb(0, 194, 224);
            color: rgb(0, 194, 224);
        }

        .inner-left:hover {
            color: #FFFFFF;
        }

        .bar-right:hover {
            background-color: #FFFFFF;
        }

        .inner-right:hover {
            color: rgb(0, 194, 244);
        }
    </style>
</head>
<body>
<div class="container">
    <div class="logo">
        VENVAST
    </div>
    <div class="upper">
        @yield('content_title')
    </div>
    <div class="lower">
        @yield('content')
    </div>
</div>
<div style="color: #707070;font-family: Arial;font-size: 12px;line-height: 125%;text-align: center;max-width: 100%; margin: 20px 0px!important;">
    <em>Copyright &copy; VENVAST</em>
</div>
</body>
</html>
