@extends('mails._base')

@section('title')
    VenVast Registration
@endsection
@section('content_title')
    Create your VenVast password
@endsection
@section('content')
    <p class="text">Hello, {{ $user->getEmail() }}</p>
    <p class="text">Thanks for joining VenVast Beta! Updates and new features are released weekly!
    In order to finish registration process follow the link and create your password:
        <a href="{{'https://api.venvast.com/user/create_password' . '?code=' . $user->getActivationCode() . '&email=' . $user->getEmail() }}">Create password & activate account</a>
    </p>
@endsection