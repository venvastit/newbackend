@extends('mails._base')
@section('title')
    Successful registration
@endsection
@section('content_title')
    Your new VenVast account is ready
@endsection
@section('content')
    <p class="text">Congratulations! You have finished registration process at VenVast.com. Thanks for your interest in our Startup!</p>
    <p class="text">
        We will send you link to download our app and more details about web application and mobile applications as soon as we make public release!
    </p>
@endsection