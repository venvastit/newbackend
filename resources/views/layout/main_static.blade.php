<style type="text/css">
    
    body {
        color: #232323;
        font-family: 'Open Sans',Arial,sans-serif;
        font-size: 14px;
        overflow-x: hidden;
        width: 100%;
    }

    .content-center {
        left: 50%;
        position: absolute;
        top: 50%;
        transform: translateX(-50%) translateY(-50%);
        width: 33.33333%;
        float: left;
    }

    form {
        display: block;
        margin-top: 0em;
    }

    .content-center h1 {
        border-bottom: 1px solid rgba(255,255,255,.06);
        color: #fff;
        font-size: 30px;
        font-weight: 400;
        margin: 0 0 30px;
        padding: 0 0 30px;
        text-align: center;
        font-family: Montserrat,Arial,sans-serif;
        line-height: 1.5;
    }

    .form-group {
        margin-bottom: 1rem;
    }

    .form-control {
        border: 0;
        border-radius: 19px;
        font-size: 14px;
        height: 42px;
        padding: 0 22px;
        width: 100%;
        margin: 0;
        line-height: 1.5;
    }


    .label {
        display: inline-block;
        padding: .25em .4em;
        font-size: 75%;
        line-height: 1;
        color: #fff;
        white-space: nowrap;
        vertical-align: baseline;
        border-radius: .25rem;
        font-weight: 700;
        text-align: center;
        cursor: pointer;
        margin: 0 0 15px;
    }

    label {
        text-transform: uppercase;
    }


    .btn-secondary {
        background-color: #33BFCB;
        background-image: linear-gradient(to bottom,#33BFCB,#30b3bf);
    }

    .btn {
        border: 0;
        border-radius: 19px;
        color: #fff;
        font-size: 13px;
        font-weight: 700;
        height: 38px;
        line-height: 38px;
        padding: 0 30px;
        transition: background-color .4s ease;
        text-shadow: 0 1px 1px rgba(35,35,35,.2);
        display: inline-block;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        cursor: pointer;
    }

    button {
        -webkit-appearance: button;
    }

</style>
<body style="background-color: rgb(50, 50, 50);">
<div>
    <div class="content-center">
        @yield('content')
    </div>
</div>
</body>