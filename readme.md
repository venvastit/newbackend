# [VenVast](https://venvast.com) Backend Application

## Powered by [Laravel PHP Framework](https://github.com/laravel/laravel)


## Dependencies
* PHP >= 7.0
* Laravel Framework >= 5.2
* Doctrine ODM
* intervention/image
* Redis
* predis/predis
* jeroennoten/laravel-adminlte
* tymon/jwt-auth
* yajra/laravel-datatables-oracle
* spatie/laravel-newsletter
* skagarwal/google-places-api
* laravel/socialite


##License
Limited Site License.

