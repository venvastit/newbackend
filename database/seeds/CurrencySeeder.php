<?php

use Illuminate\Database\Seeder;
use App\Core\Repositories\CurrencyRepository;
use App\Core\Entities\Money\Currency;

/**
 * Created by PhpStorm.
 *
 * User: hlogeon
 * Date: 9/12/16
 * Time: 6:18 AM
 */
class CurrencySeeder extends Seeder
{
    private $repo;

    public function __construct(CurrencyRepository $repository)
    {
        $this->repo = $repository;
    }

    public function run()
    {
        /** @var Currency $usd */
        $usd = new Currency(Currency::USD);
        /** @var Currency $thb */
        $thb = new Currency(Currency::THB);
        $this->repo->persist($thb);
        $this->repo->persist($usd);
        $this->repo->flush();
    }
}
