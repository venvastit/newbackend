<?php

use Illuminate\Database\Seeder;
use App\Domains\Category\Repositories\CategoryRepository;
use App\Domains\Category\Entities\Category;

/**
 * Created by PhpStorm.
 * User: hlogeon
 * Date: 9/5/16
 * Time: 4:30 AM.
 */
class CategorySeeder extends Seeder
{
    /**
     * @var CategoryRepository
     */
    protected $repo;

    public function __construct(CategoryRepository $repository)
    {
        $this->repo = $repository;
    }

    public function run()
    {
        $this->venues();
        $this->events();
    }

    public function venues()
    {
        $type = Category::TYPE_VENUES;
        $categories = [
            [
                'name' => 'Nightclubs',
                'priority' => 0,
                'type' => $type,
            ],
            [
                'name' => 'Bars & Pubs',
                'priority' => 1,
                'type' => $type,
            ],
            [
                'name' => 'Shopping Malls',
                'priority' => 2,
                'type' => $type,
            ],
            [
                'name' => "Cafe's & Bakeries",
                'priority' => 3,
                'type' => $type,
            ],
            [
                'name' => 'Restaurants',
                'priority' => 4,
                'type' => $type,
            ],
            [
                'name' => 'Cinemas',
                'priority' => 5,
                'type' => $type,
            ],
            [
                'name' => 'Accomodation',
                'priority' => 6,
                'type' => $type,
            ],
            [
                'name' => 'Fitness & Gym Clubs',
                'priority' => 7,
                'type' => $type,
            ],
            [
                'name' => 'Art Galleries & Museums',
                'priority' => 8,
                'type' => $type,
            ],
            [
                'name' => 'Spa & Wellness',
                'priority' => 9,
                'type' => $type,
            ],
            [
                'name' => 'Tours & Sightseeing',
                'priority' => 10,
                'type' => $type,
            ],
            [
                'name' => 'Bookshops & Libraries',
                'priority' => 11,
                'type' => $type,
            ],
            [
                'name' => 'Groceries',
                'priority' => 12,
                'type' => $type,
            ],
            [
                'name' => 'Others',
                'priority' => 13,
                'type' => $type,
            ],
        ];

        foreach ($categories as $categoryParams) {
            $category = new Category($categoryParams['name'], $categoryParams['type'], $categoryParams['priority']);
            $this->repo->persist($category);
        }
        $this->repo->flush();
    }

    public function events()
    {
        $type = Category::TYPE_EVENTS;
        $categories = [
            [
                'name' => 'Party',
                'priority' => 1,
                'type' => $type,
            ],
            [
                'name' => 'Festival',
                'priority' => 2,
                'type' => $type,
            ],
            [
                'name' => 'Concert',
                'priority' => 3,
                'type' => $type,
            ],
            [
                'name' => 'Exhibition',
                'priority' => 4,
                'type' => $type,
            ],
            [
                'name' => 'Conference',
                'priority' => 5,
                'type' => $type,
            ],
            [
                'name' => 'Activity',
                'priority' => 6,
                'type' => $type,
            ],
            [
                'name' => 'Market or Fair',
                'priority' => 7,
                'type' => $type,
            ],
            [
                'name' => 'Meetup',
                'priority' => 8,
                'type' => $type,
            ],
            [
                'name' => 'Dining & Tasting',
                'priority' => 9,
                'type' => $type,
            ],
            [
                'name' => 'Open Mic',
                'priority' => 10,
                'type' => $type,
            ],
            [
                'name' => 'Lesson or lecture',
                'priority' => 11,
                'type' => $type,
            ],
            [
                'name' => 'Other',
                'priority' => 12,
                'type' => $type,
            ],
        ];

        foreach ($categories as $categoryParams) {
            $category = new Category($categoryParams['name'], $categoryParams['type'], $categoryParams['priority']);
            $this->repo->persist($category);
        }

        $this->repo->flush();
    }
}
